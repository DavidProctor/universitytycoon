﻿using UniversityDLL.Tiles;

namespace UniversityDLL.ActivityPoints
{
    public class StudyBoothActivityPoint : ActivityPoint
    {
        public StudyBoothActivityPoint(ITileContent tileContent)
        {
            TileContent = tileContent;
        }

        public override void ExecuteActivity()
        {
            
        }

        public override ActivityPoint GetNewInstance()
        {
            return new StudyBoothActivityPoint(TileContent.GetClone());
        }
    }
}
