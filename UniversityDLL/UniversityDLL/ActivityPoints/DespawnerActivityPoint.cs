﻿using System;

namespace UniversityDLL.ActivityPoints
{
    [Serializable]
    public class DespawnerActivityPoint : ActivityPoint
    {
        public override void ExecuteActivity()
        {
            if (IsOccupied())
            {
                Occupant.Despawn();
                Occupant = null;
            }
        }

        public override ActivityPoint GetNewInstance()
        {
            return new DespawnerActivityPoint();
        }
    }
}