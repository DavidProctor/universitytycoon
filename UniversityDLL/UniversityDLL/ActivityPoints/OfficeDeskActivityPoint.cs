﻿using System;
using UniversityDLL.Tiles;

namespace UniversityDLL.ActivityPoints
{
    public class OfficeDeskActivityPoint : ActivityPoint
    {
        public OfficeDeskActivityPoint(ITileContent tileContent)
        {
            TileContent = tileContent;
        }

        public override void ExecuteActivity()
        {
            throw new NotImplementedException();
        }

        public override ActivityPoint GetNewInstance()
        {
            return new OfficeDeskActivityPoint(TileContent.GetClone());
        }
    }
}
