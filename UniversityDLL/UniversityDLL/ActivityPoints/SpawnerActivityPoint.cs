﻿using System;
using UniversityDLL.People;

namespace UniversityDLL.ActivityPoints
{
    [Serializable]
    public class SpawnerActivityPoint : ActivityPoint
    {
        public override void ExecuteActivity()
        {
            if( IsOccupied() )
            {
                Occupant.Spawn(Tile);
                Occupant = null;
            }
        }

        public override ActivityPoint GetNewInstance()
        {
            return new SpawnerActivityPoint();
        }
    }
}