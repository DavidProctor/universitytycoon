﻿using System;
using UniversityDLL.Tiles;

namespace UniversityDLL.ActivityPoints
{
    [Serializable]
    public class ClassSeatActivityPoint : ActivityPoint
    {
        public ClassSeatActivityPoint(ITileContent tileContent)
        {
            TileContent = tileContent;
        }

        public override void ExecuteActivity()
        {
            throw new NotImplementedException();
        }

        public override ActivityPoint GetNewInstance()
        {
            return new ClassSeatActivityPoint(TileContent.GetClone());
        }
    }
}