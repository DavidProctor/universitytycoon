﻿using System;
using UniversityDLL.Tiles;

namespace UniversityDLL.ActivityPoints
{
    [Serializable]
    public class PubSeatActivityPoint : ActivityPoint
    {
        public PubSeatActivityPoint(ITileContent tileContent)
        {
            TileContent = tileContent;
        }

        public override void ExecuteActivity()
        {
            throw new NotImplementedException();
        }

        public override ActivityPoint GetNewInstance()
        {
            return new PubSeatActivityPoint(TileContent.GetClone());
        }
    }
}