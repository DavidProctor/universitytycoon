﻿using System.Linq;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace UniversityDLL.ActivityPoints
{
    public class LecternActivityPoint : ActivityPoint
    {
        private readonly ICalendar _calendar;

        public LecternActivityPoint(ICalendar calendar, ITileContent tileContent)
        {
            _calendar = calendar;
            TileContent = tileContent;
        }

        public override void ExecuteActivity()
        {
            var classroom = Tile.Room as BookableRoom;
            var prof = Occupant as IProfessor;
            if (!IsOccupied() || prof == null || classroom == null)
            {
                return;
            }
            var course = classroom.GetSchedule(_calendar.CurrentTime.Semester)
                                  .GetBooking(_calendar.CurrentTime.DayAndTime)
                                  ?.Course;
            if( course == null ) return;

            var students = classroom.GetActivityPoints()
                                    .Where(p => p is ClassSeatActivityPoint && p.IsOccupied() && p.Occupant is IStudent)
                                    .Select(p => p.Occupant as IStudent);

            var effectiveness = prof.TeachingSkill - (students.Count()/25);
            effectiveness = effectiveness > 0 ? effectiveness : 1;

            foreach (var student in students)
            {
                student.Study(course, effectiveness);
            }
        }

        public override ActivityPoint GetNewInstance()
        {
            return new LecternActivityPoint(_calendar, TileContent.GetClone());
        }
    }
}
