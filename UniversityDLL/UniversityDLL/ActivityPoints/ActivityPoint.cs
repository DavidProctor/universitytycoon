﻿using System;
using UnityEngine;
using UniversityDLL.People;
using UniversityDLL.Tiles;

namespace UniversityDLL.ActivityPoints
{
    [Serializable]
    public abstract class ActivityPoint
    {
        public virtual ITile Tile { get; set; }
        public virtual ICharacter Occupant { get; protected set; }
        public ITileContent TileContent { get; protected set; }

        public virtual bool IsOccupied()
        {
            return Occupant != null;
        }

        public virtual bool TrySetOccupant(ICharacter character)
        {
            if( Tile != null && !IsOccupied() )
            {
                Occupant = character;
                Tile.Room?.OccupyActivityPoint(this);
                return true;
            }
            return false;
        }

        public virtual void Vacate()
        {
            Occupant = null;
            Tile.Room.VacateActivityPoint(this);
        }

        public abstract void ExecuteActivity();

        public abstract ActivityPoint GetNewInstance();
    }
}