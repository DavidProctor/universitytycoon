﻿namespace UniversityDLL.NameGenerators
{
    public static class PubNameGenerator
    {
        public static string GenerateName()
        {
            if (Utils.IsTrueWithChance(65))
            {
                return GeneratePersonName();
            }
            return GenerateComboName();
        }

        private static string GeneratePersonName()
        {
            string prefix = Utils.IsTrueWithChance(30) ? $"{Prefixes.GetRandom()} " : "";

            if (Utils.IsTrueWithChance(40))
            {
                return $"The {prefix}{FirstNames.GetRandom()} { LastNames.GetRandom()} {TypesOfPub.GetRandom()}";
            }

            return $"{prefix}{FirstNames.GetRandom()} {LastNames.GetRandom()}'s {TypesOfPub.GetRandom()}";
        }

        private static string GenerateComboName()
        {
            return $"The {BarnAnimals.GetRandom()} and {PubbySoundingNouns.GetRandom()} {TypesOfPub.GetRandom()}";
        }

        private static readonly string[] Prefixes = 
        {
            "Big",
            "Dirty",
            "Left Foot",
            "Smilin'",
            "Crabby"
        };

        private static readonly string[] FirstNames = 
        {
            "Filthy",
            "Buckeye",
            "Harry",
            "Professor",
            "Admiral",
            "John",
            "Pete",
            "Ceili",
            "Tom",
            "Wilfred"
        };

        private static readonly string[] LastNames = 
        {
            "O'Flanagan",
            "McNasty",
            "Liverpool",
            "Hallahan",
            "Montcalm",
            "Crabwalk",
            "Goldstein",
            "Africa",
            "Washington",
            "Corleone"
        };

        private static readonly string[] BarnAnimals = 
        {
            "Swine",
            "Horse",
            "Hound",
            "Fox",
            "Cock",
            "Bull",
            "Mouse",
            "Sheep",
            "Crow",
            "Snake"
        };

        private static readonly string[] PubbySoundingNouns = 
        {
            "Gate",
            "Nail",
            "Hoe",
            "Rake",
            "Lighthouse",
            "Range",
            "Boot",
            "Mug",
            "Scroll",
            "Ward"
        };

        private static readonly string[] TypesOfPub = 
        {
            "Pub",
            "Public House",
            "Tavern",
            "Bar and Grill",
            "Gastropub",
            "Inn",
            "Irish Pub",
            "Bar"
        };
    }
}