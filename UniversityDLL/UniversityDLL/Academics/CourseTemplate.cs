﻿using System;

namespace UniversityDLL.Academics
{
    public interface ICourseTemplate
    {
        string Name { get; }
        int RequiredHours { get; }
        Year Year { get; }
        IDepartment Department { get; }
    }

    [Serializable]
    public enum Year
    {
        First,
        Second,
        Third,
        Fourth
    }

    [Serializable]
    public class CourseTemplate : ICourseTemplate
    {
        public string Name { get; }
        public int RequiredHours { get; }
        public Year Year { get; }
        public IDepartment Department { get; }
        
        public CourseTemplate(string name, int requiredHours, Year year, IDepartment department)
        {
            Name = name;
            RequiredHours = requiredHours;
            Year = year;
            Department = department;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
