﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UniversityDLL.Academics
{
    public interface IDepartment
    {
        string Name { get; }
        int AdmissionsCapacity { get; set; }
        int RequiredCreditsPerYear { get; }
        int PrestigeCost { get; }

        List<ICourseTemplate> GetCourses();
        List<ICourseInstance> GetScheduledCourses(Semester semester);

        void AddCourse(ICourseTemplate course);
        void AddScheduledCourse(ICourseInstance course);
    }

    [Serializable]
    public class Department : IDepartment
    {
        public List<ICourseInstance> CourseInstances;
        public List<ICourseTemplate> CourseTemplates;
        public string Name { get; }
        public int RequiredCreditsPerYear { get; }
        public int PrestigeCost { get; }
        public int AdmissionsCapacity
        {
            get { return _admissionsCapacity; }
            set
            {
                _admissionsCapacity = value >= 0 && value <= 100 ? value : _admissionsCapacity;
            }
        }

        private int _admissionsCapacity;

        public Department(string name, int prestigeCost = 75)
        {
            Name = name;
            AdmissionsCapacity = 10;
            RequiredCreditsPerYear = 12;
            PrestigeCost = prestigeCost;
            CourseInstances = new List<ICourseInstance>();
            CourseTemplates = new List<ICourseTemplate>();
        }

        public void AddCourse(ICourseTemplate course)
        {
            CourseTemplates.Add(course);
        }

        public void AddScheduledCourse(ICourseInstance course)
        {
            if( !CourseInstances.Contains(course) )
            {
                CourseInstances.Add(course);
            }
        }

        public List<ICourseTemplate> GetCourses()
        {
            return CourseTemplates;
        }

        public List<ICourseInstance> GetScheduledCourses(Semester semester)
        {
            return CourseInstances.Where(c => c.Semester.Equals(semester)).ToList();
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if( obj == this ) return true;
            if( obj.GetType() != typeof(Department) ) return false;
            var comparer = obj as Department;
            // ReSharper disable once PossibleNullReferenceException
            return comparer.Name == Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
