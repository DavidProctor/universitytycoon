﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.People;

namespace UniversityDLL.Academics
{
    public interface ICourseInstance
    {
        ICourseTemplate Template { get; }
        Semester Semester { get; }
        IProfessor Professor { get; set; }
        List<IRoomBooking> ScheduledSessions { get; }
        List<ICourseEnrolment> Enrolments { get; }
        bool IsReady();
        void AddEnrolment(ICourseEnrolment enrolment);
        void AddRoomBooking(IRoomBooking booking);
        void RemoveRoomBooking(IRoomBooking booking);
        int GetCapacity();
    }

    [Serializable]
    public class CourseInstance : ICourseInstance
    {
        public ICourseTemplate Template { get; set; }
        public Semester Semester { get; set; }
        public IProfessor Professor { get; set; }
        public List<ICourseEnrolment> Enrolments { get; set; }
        public List<IRoomBooking> ScheduledSessions { get; set; }

        public CourseInstance(Semester semester, ICourseTemplate course)
        {
            Semester = semester;
            Enrolments = new List<ICourseEnrolment>();
            Template = course;
            ScheduledSessions = new List<IRoomBooking>(Template.RequiredHours);
        }

        public void AddEnrolment(ICourseEnrolment enrolment)
        {
            if( Enrolments.Count >= GetCapacity() ) throw new ArgumentOutOfRangeException();

            Enrolments.Add(enrolment);
        }

        public void AddRoomBooking(IRoomBooking booking)
        {
            ScheduledSessions.Add(booking);
        }

        public void RemoveRoomBooking(IRoomBooking booking)
        {
            ScheduledSessions.Remove(booking);
        }

        public int GetCapacity()
        {
            return ScheduledSessions.Min(s => s.Room.Capacity);
        }

        public bool IsReady()
        {
            return ScheduledSessions.Count == Template.RequiredHours && Professor != null;
        }
    }
}
