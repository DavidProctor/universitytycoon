﻿using System;
using UniversityDLL.Rooms;

namespace UniversityDLL.Academics
{
    public interface IRoomBooking
    {
        Room Room { get; }
        ICourseInstance Course { get; }
        DayAndTime DayAndTime { get; }

        void Destroy();
    }

    [Serializable]
    public class RoomBooking : IRoomBooking
    {
        public Room Room { get; }
        public ICourseInstance Course { get; }
        public DayAndTime DayAndTime { get; }

        public RoomBooking(Room room, ICourseInstance course, DayAndTime time)
        {
            Room = room;
            Course = course;
            DayAndTime = time;

            Course.AddRoomBooking(this);
        }

        public void Destroy()
        {
            Course.RemoveRoomBooking(this);
        }
    }
}