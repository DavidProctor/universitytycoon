﻿using System;
using UniversityDLL.GameMangement;
using UniversityDLL.People;

namespace UniversityDLL.Academics
{
    public interface ICourseEnrolment
    {
        IStudent Student { get; }
        ICourseInstance Course { get; }
        int StudyValue { get; }

        void AddStudyValue(int value);
    }

    [Serializable]
    public class CourseEnrolment : ICourseEnrolment
    {
        public IStudent Student { get; }
        public ICourseInstance Course { get; }
        public int StudyValue { get { return _studyValue; } }

        private int _studyValue = 0;

        public CourseEnrolment(IStudent student, ICourseInstance course)
        {
            Student = student;
            Course = course;

            Student.AddEnrolment(this);
            Course.AddEnrolment(this);
            GameState.GetInstance().AddEnrolment(this);
        }

        public void AddStudyValue(int value)
        {
            _studyValue += value;
        }
    }
}
