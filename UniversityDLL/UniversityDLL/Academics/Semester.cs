﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UniversityDLL.Academics
{
    [Serializable]
    public enum Season
    {
        Winter,
        Summer,
        Fall
    }

    [Serializable]
    public class Semester
    {
        public int Year { get; }
        public Season Season { get; }

        private Semester() { }

        private Semester(int year, Season season)
        {
            Year = year;
            Season = season;
        }

        private static readonly List<Semester> Instances = new List<Semester>();
        
        public static Semester GetSemester(int year, Season season)
        {
            var instance = Instances.FirstOrDefault(s => s.Year == year && s.Season == season);
            if(instance == null)
            {
                instance = new Semester(year, season);
                Instances.Add(instance);
            }

            return instance;
        }

        public static IEnumerable<Semester> GetAllSemesters()
        {
            return Instances;
        }

        public Semester GetNext()
        {
            if(Season == Season.Fall )
            {
                return GetSemester(Year + 1, Season.Winter);
            }
            return GetSemester(Year, Season + 1);
        }

        public override bool Equals(object obj)
        {
            if( obj == null || obj.GetType() != typeof(Semester) ) return false;
            var other = obj as Semester;
            // ReSharper disable once PossibleNullReferenceException
            return other.Season == Season && other.Year == Year;
        }

        public override int GetHashCode()
        {
            var hashcode = 31;
            hashcode += Season.GetHashCode() * 31;
            hashcode += Year.GetHashCode() * 31;
            return hashcode;
        }

        public override string ToString()
        {
            return $"{Season} {Year}";
        }
    }
}
