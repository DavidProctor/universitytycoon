﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.Rooms;

namespace UniversityDLL.Academics
{
    public interface IRoomSchedule
    {
        List<IRoomBooking> Bookings { get; }

        IRoomBooking GetBooking(DayAndTime time);
        void Schedule(DayAndTime time, ICourseInstance course);
        void RemoveBooking(DayAndTime time);
    }

    [Serializable]
    public class RoomSchedule : IRoomSchedule
    {
        public Room Room { get; set; }
        public List<IRoomBooking> Bookings { get { return _schedule; } }
        public Semester Semester { get; set; }

        private readonly List<IRoomBooking> _schedule;

        public RoomSchedule(Room room, Semester semester)
        {
            Room = room;
            Semester = semester;
            _schedule = new List<IRoomBooking>();
        }

        public void Schedule(DayAndTime time, ICourseInstance course)
        {
            _schedule.Add(new RoomBooking(Room, course, time));
        }

        public IRoomBooking GetBooking(DayAndTime time)
        {
            return _schedule.FirstOrDefault(b => b.DayAndTime.Equals(time));
        }

        public void RemoveBooking(DayAndTime time)
        {
            var booking = _schedule.FirstOrDefault(b => b.DayAndTime.Equals(time));
            if( booking == null ) return;
            booking.Destroy();
            _schedule.Remove(booking);
        }
    }
}
