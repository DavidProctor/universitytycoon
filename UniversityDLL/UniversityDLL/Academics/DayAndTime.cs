﻿using System;

namespace UniversityDLL.Academics
{
    [Serializable]
    public class DayAndTime
    {
        public readonly DayOfWeek Day;
        public readonly int Hour;

        private DayAndTime(DayOfWeek day, int hour)
        {
            Day = day;
            Hour = hour;
        }

        public override bool Equals(object obj)
        {
            if( obj == this ) return true;
            if( obj.GetType() != typeof(DayAndTime) ) return false;
            var comparer = obj as DayAndTime;
            // ReSharper disable once PossibleNullReferenceException
            return comparer.Day == Day && comparer.Hour == Hour;
        }

        public override int GetHashCode()
        {
            var hashcode = 17;
            hashcode = hashcode + (31 * Day.GetHashCode());
            hashcode = hashcode + (31 * Hour);
            return hashcode;
        }

        public bool IsWeekday()
        {
            return Day != DayOfWeek.Saturday && Day != DayOfWeek.Sunday;
        }

        public bool IsWorkingHours()
        {
            return Hour >= 9 && Hour <= 5;
        }

        public static DayAndTime FromDetails(DayOfWeek day, int hour)
        {
            return new DayAndTime(day, hour);
        }

        public static DayAndTime FromDateTime(DateTime dateTime)
        {
            return new DayAndTime(dateTime.DayOfWeek, dateTime.Hour);
        }

        public override string ToString()
        {
            return $"{Day} at {Hour}:00";
        }
    }
}