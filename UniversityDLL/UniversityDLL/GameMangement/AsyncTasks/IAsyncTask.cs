﻿namespace UniversityDLL.GameMangement.AsyncTasks
{
    public interface IAsyncTask
    {
        void Execute();
    }
}