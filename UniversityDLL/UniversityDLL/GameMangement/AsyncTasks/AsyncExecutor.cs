﻿using System.Collections.Generic;
using System.Linq;

namespace UniversityDLL.GameMangement.AsyncTasks
{
    public interface IAsyncExecutor
    {
        int QueuedTasks { get; }

        void AddTask(IAsyncTask task);
        void ExecuteNextTask();
    }

    public class AsyncExecutor : IAsyncExecutor
    {
        public int QueuedTasks { get { return _queue.Count; } }

        private readonly Queue<IAsyncTask> _queue = new Queue<IAsyncTask>();

        public void AddTask(IAsyncTask task)
        {
            _queue.Enqueue(task);
        }

        public void ExecuteNextTask()
        {
            if (!_queue.Any())
            {
                return;
            }
            var task = _queue.Dequeue();
            task.Execute();
        }
    }
}