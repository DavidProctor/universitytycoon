﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.GameMangement
{
    class SaveGridGenerator : IGridGenerator
    {
        public int Width { get { return _grid.Width; } }
        public int Height { get { return _grid.Height; } }

        private readonly IGrid _grid;

        public SaveGridGenerator(IGrid grid)
        {
            _grid = grid;
        }

        public IGrid GenerateGrid()
        {
            for(int x = 0; x < _grid.Width; ++x )
            {
                for(int z = 0; z < _grid.Height; ++z )
                {
                    var tile = _grid.GetTile(x, z);
                    tile.TileContentCollection.Render();
                    if (!(tile.Room is ROutdoors))
                    {
                        Debug.Log($"{tile}: {tile.Room}");
                    }
                    //_grid.GetTile(x, z).TileContentCollection.Render();
                }
            }

            return _grid;
        }
    }

    public class GameSaver
    {
        public void Save(IGameState gameState, string fileDestination)
        {
            Stream fileStream = File.Create(fileDestination);

            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(fileStream, gameState);

            fileStream.Close();
        }

        public GameState Load(string fileSource)
        {
            if(!File.Exists(fileSource))
            {
                throw new FileNotFoundException();
            }

            Stream fileStream = File.OpenRead(fileSource);
            BinaryFormatter deserializer = new BinaryFormatter();
            GameState gameState = (GameState) deserializer.Deserialize(fileStream);
            fileStream.Close();

            return gameState;
        }
    }
}
