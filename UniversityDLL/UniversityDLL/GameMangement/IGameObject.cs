﻿using UnityEngine;

namespace UniversityDLL.GameMangement
{
    public interface IGameObject
    {
        string Name { get; set; }
        GameObject GameObject { get; }
        Orientation Orientation { get; }

        IGameObject GetClone();
        void SetClickableObject(IGameEntity entity);

        IGameObject Render(float x, float y, float z);
        IGameObject Render(float x, float y, float z, Color color);

        void SetOrientation(Orientation orientation);

        void Destroy();
        void Hide();
        void Show();
    }
}
