﻿using System;

namespace UniversityDLL.GameMangement
{
    [Serializable]
    public class BankruptcyException: Exception
    {
        
    }

    public interface IScore
    {
        long Money { get; }
        int Prestige { get; }

        void AddMoney(long amount);
        void SubtractMoney(long amount);
        void AddPrestige(int amount);
        void SubtractPrestige(int amount);
    }

    [Serializable]
    public class Score : IScore
    {
        public long Money { get; private set; }
        public int Prestige { get; private set; }

        public void AddMoney(long amount)
        {
            if (amount < 0) throw new ArgumentOutOfRangeException();

            Money += amount;
        }

        public void SubtractMoney(long amount)
        {
            if (amount < 0) throw new ArgumentOutOfRangeException();
            if (Money - amount < 0) throw new BankruptcyException();

            Money -= amount;
        }

        public void AddPrestige(int amount)
        {
            if (amount < 0) throw new ArgumentOutOfRangeException();

            Prestige += amount;
        }

        public void SubtractPrestige(int amount)
        {
            if(amount < 0) throw new ArgumentOutOfRangeException();
            if(Prestige - amount < 0) throw new BankruptcyException();

            Prestige -= amount;
        }
    }
}
