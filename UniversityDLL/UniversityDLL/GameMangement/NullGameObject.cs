﻿using UnityEngine;
using UniversityDLL.Tiles;

namespace UniversityDLL.GameMangement
{
    public class NullGameObject : IGameObject
    {
        public string Name { get; set; }
        public GameObject GameObject { get { return null; } }
        public Orientation Orientation { get { return default(Orientation); } }

        public NullGameObject()
        {
            Name = "NullLayer GameObject";
        }

        public IGameObject GetClone()
        {
            return new NullGameObject
            {
                Name = Name
            };
        }

        public void SetClickableObject(IGameEntity entity)
        {
        }

        public IGameObject Render(float x, float y, float z)
        {
            return this;
        }

        public IGameObject Render(float x, float y, float z, Color color)
        {
            return this;
        }

        public void SetOrientation(Orientation orientation)
        {
        }
        
        public void Destroy()
        {
        }

        public void Hide()
        {
        }

        public void Show()
        {
        }
    }
}