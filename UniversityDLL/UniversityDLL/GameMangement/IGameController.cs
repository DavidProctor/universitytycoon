﻿using UnityEngine;
using UniversityDLL.ControlManagement;
using UniversityDLL.GameMangement.AsyncTasks;

namespace UniversityDLL.GameMangement
{
    public enum ControlMode
    {
        Standard,
        Wait
    }

    public interface IGameController
    {
        Controls Controls { get; }
        IAsyncExecutor AsyncExecutor { get; }

        void SetControlMode(ControlMode mode);
    }
}