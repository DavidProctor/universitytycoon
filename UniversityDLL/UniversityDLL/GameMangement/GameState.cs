﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniversityDLL.Academics;
using UniversityDLL.People;
using UniversityDLL.People.Planners;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace UniversityDLL.GameMangement
{
    public interface IGameState
    {
        IGrid Grid { get; set; }
        ICalendar Calendar { get; set; }
        IScore Score { get; set; }
        IEnumerable<IDepartment> Departments { get; }
        IEnumerable<IStudent> Students { get; }
        IEnumerable<IProfessor> Professors { get; }
        IEnumerable<IAdministrator> Administrators { get; }
        IEnumerable<ICourseEnrolment> Enrolments { get; }
        IEnumerable<Room> Rooms { get; }
        IEnumerable<IObjective> Objectives { get; }

        void AddDepartment(IDepartment department);
        void EnrolStudent(IStudent student);
        void HireProfessor(IProfessor professor);
        void HireAdministrator(IAdministrator administrator);
        void AddEnrolment(ICourseEnrolment enrolment);
        void RegisterRoom(Room room);
        void AddObjective(IObjective objective);
        void ClearCompletedObjectives();
        IEnumerable<BookableRoom> GetLectureHalls();
        void SetMode(GameState.GameStarterMode mode);
        IEnumerator ProcessTurn();
        void HourlyUpdate();
    }

    [Serializable]
    public class GameState : IGameState
    {
        public enum GameStarterMode
        {
            Regular,
            Demo,
            Test
        }

        public static readonly int CreditsPerYear = 18;

        private static GameState _instance = new GameState();

        public static IGameState GetInstance()
        {
            return _instance;
        }

        public GameStarterMode Mode { get { return _mode ?? GameStarterMode.Regular; } }
        public IGrid Grid { get; set; }
        public ICalendar Calendar { get; set; }
        public IScore Score { get; set; }
        public IEnumerable<IDepartment> Departments { get { return _departments; } }
        public IEnumerable<IStudent> Students { get { return _students; } }
        public IEnumerable<IProfessor> Professors { get { return _professors; } }
        public IEnumerable<IAdministrator> Administrators { get { return _administrators; } }
        public IEnumerable<ICourseEnrolment> Enrolments { get { return _enrolments; } }
        public IEnumerable<Room> Rooms { get { return _rooms; } }
        public IEnumerable<IObjective> Objectives { get { return _objectives; } }

        private GameStarterMode? _mode;
        private readonly List<IDepartment> _departments = new List<IDepartment>();
        private readonly List<IStudent> _students = new List<IStudent>();
        private readonly List<IProfessor> _professors = new List<IProfessor>();
        private readonly List<IAdministrator> _administrators = new List<IAdministrator>();
        private readonly List<ICourseEnrolment> _enrolments = new List<ICourseEnrolment>();
        private readonly List<Room> _rooms = new List<Room>(); 
        private readonly List<IObjective> _objectives = new List<IObjective>();

        protected GameState()
        {
        }

        public void EnrolStudent(IStudent student)
        {
            _students.Add(student);
        }

        public void HireProfessor(IProfessor professor)
        {
            _professors.Add(professor);
        }

        public void HireAdministrator(IAdministrator administrator)
        {
            _administrators.Add(administrator);
        }

        public void AddEnrolment(ICourseEnrolment enrolment)
        {
            _enrolments.Add(enrolment);
        }

        public void RegisterRoom(Room room)
        {
            _rooms.Add(room);
        }

        public void AddObjective(IObjective objective)
        {
            _objectives.Add(objective);
        }

        public void ClearCompletedObjectives()
        {
            _objectives.ForEach(objective => objective.ExecuteCompletionAction());
            _objectives.RemoveAll(objective => objective.IsComplete());
        }

        public static GameState LoadSave(string path)
        {
            var saver = new GameSaver();
            _instance = saver.Load(path);
            
            var gridGenerator = new SaveGridGenerator(_instance.Grid);
            _instance.Grid = gridGenerator.GenerateGrid();
            _instance.Grid.ConnectNeighbors();
            return _instance;
        }

        public IEnumerable<BookableRoom> GetLectureHalls()
        {
            var lectureHalls = new List<BookableRoom>();
            foreach (var room in Rooms)
            {
                if (room is RLectureHall)
                {
                    lectureHalls.Add(room as RLectureHall);
                }
            }
            return lectureHalls;
        }

        public void SetMode(GameStarterMode mode)
        {
            if (_mode.HasValue)
            {
                throw new InvalidOperationException($"Cannot set game mode to {mode} - it is already {_mode.Value}");
            }
            _mode = mode;
        }

        public void AddDepartment(IDepartment department)
        {
            Score.SubtractPrestige(department.PrestigeCost);
            _departments.Add(department);
        }

        public IEnumerator ProcessTurn()
        {
            var endTime = GetEndTime();

            while (Calendar.CurrentTime < endTime)
            {
                Calendar.Tick();
                if (Calendar.CurrentTime.QuarterHour == QuarterHour.Zero)
                {
                    HourlyUpdate();
                }
                yield return 0;
            }

            var studentScheduler = new StudentDayPlanner(this);
            foreach (var student in Students)
            {
                studentScheduler.PlanDay(student, Calendar.CurrentTime);
            }

            var profScheduler = new ProfessorDayPlanner(this);
            foreach (var professor in Professors)
            {
                profScheduler.PlanDay(professor, Calendar.CurrentTime);
            }
        }

        public void HourlyUpdate()
        {
            foreach (var room in Rooms)
            {
                room.HourlyUpdate();
            }
        }

        private TimeUnit GetEndTime()
        {
            while (true)
            {
                var endTime = Calendar.CurrentTime.AddTicks(GetRandomTickCount());
                if (endTime.Hour < 17 && endTime.Hour > 9 && endTime.Day != DayOfWeek.Saturday && endTime.Day != DayOfWeek.Sunday)
                {
                    return endTime;
                }
            }
        }

        private int GetRandomTickCount()
        {
            var random = new Random();
            var ticks = random.Next(3, 8) * TimeUnit.TicksPerDay;
            ticks += random.Next(0, TimeUnit.TicksPerDay);
            return ticks;
        }
    }
}
