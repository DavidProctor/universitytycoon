﻿namespace UniversityDLL.GameMangement.Factories
{
    public interface IGameObjectFactory
    {
        IGameObject MakeModelWrapper(string modelPath);
    }
}