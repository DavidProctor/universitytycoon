﻿using System;
using UniversityDLL.People;
using UniversityDLL.People.Behaviors;

namespace UniversityDLL.GameMangement.Factories
{
    public interface ICharacterFactory
    {
        IStudent MakeStudent();
        IProfessor MakeProfessor();
        IAdministrator MakeAdministrator();
    }

    [Serializable]
    public class CharacterFactory : ICharacterFactory
    {
        public enum CharacterType
        {
            Student,
            Professor,
            Administrator
        }

        private readonly IGameState _gameState;
        private readonly IGameObjectFactory _gameObjectFactory;
        private readonly INameGenerator _nameGenerator;

        public CharacterFactory(IGameState gameState, IGameObjectFactory gameObjectFactory, INameGenerator nameGenerator)
        {
            _gameState = gameState;
            _gameObjectFactory = gameObjectFactory;
            _nameGenerator = nameGenerator;
        }

        public IStudent MakeStudent()
        {
            var sex = Utils.IsTrueWithChance(50) ? Sex.Male : Sex.Female;
            var name = _nameGenerator.GetName(sex);
            var major = _gameState.Departments.GetRandom();
            var modelPath = sex == Sex.Female ? "Characters/Lady" : "Characters/Guy";
            var model = _gameObjectFactory.MakeModelWrapper(modelPath);
            var renderer = new CharacterRenderer(model);
            var behavior = MakeCharacterBehavior(model, CharacterType.Student);
            var schedule = new PersonalSchedule();

            var student = new Student(name, major, renderer, behavior, schedule);
            behavior.Character = student;
            model.SetClickableObject(student);
            return student;
        }

        public IProfessor MakeProfessor()
        {
            var sex = Utils.IsTrueWithChance(50) ? Sex.Male : Sex.Female;
            var name = _nameGenerator.GetName(sex);
            var modelPath = "Characters/ProfPrefab";
            var model = _gameObjectFactory.MakeModelWrapper(modelPath);
            var renderer = new CharacterRenderer(model);
            var behavior = MakeCharacterBehavior(model, CharacterType.Professor);
            var schedule = new PersonalSchedule();

            var professor = new Professor(name, behavior, schedule, renderer, 5);
            behavior.Character = professor;
            model.SetClickableObject(professor);
            return professor;
        }

        public IAdministrator MakeAdministrator()
        {
            var sex = Utils.IsTrueWithChance(50) ? Sex.Male : Sex.Female;
            var name = _nameGenerator.GetName(sex);
            var modelPath = "Characters/ProfPrefab";
            var model = _gameObjectFactory.MakeModelWrapper(modelPath);
            var renderer = new CharacterRenderer(model);
            var behavior = MakeCharacterBehavior(model, CharacterType.Administrator);
            var schedule = new PersonalSchedule();

            var administrator = new Administrator(name, behavior, schedule, renderer);
            behavior.Character = administrator;
            model.SetClickableObject(administrator);
            return administrator;
        }

        protected virtual ICharacterBehavior MakeCharacterBehavior(IGameObject gameObject, CharacterType characterType)
        {
            switch (characterType)
            {
                case CharacterType.Professor:
                    return gameObject.GameObject.AddComponent<ProfessorCharacterBehavior>();

                default:
                   return gameObject.GameObject.AddComponent<BaseCharacterBehavior>();
                    
            }
        }
    }
}
