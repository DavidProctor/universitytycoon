﻿using System;
using UniversityDLL.People;

namespace UniversityDLL.GameMangement.Factories
{
    public interface IObjectFactoryFacade
    {
        ICharacterFactory CharacterFactory { get; }

        IStudent MakeStudent();
    }

    [Serializable]
    public class ObjectFactoryFacade : IObjectFactoryFacade
    {
        public ICharacterFactory CharacterFactory { get; }

        public ObjectFactoryFacade(ICharacterFactory characterFactory)
        {
            CharacterFactory = characterFactory;
        }

        public IStudent MakeStudent()
        {
            return CharacterFactory.MakeStudent();
        }
    }
}