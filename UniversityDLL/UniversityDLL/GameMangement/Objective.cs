﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UniversityDLL.GameMangement
{
    public interface IObjective
    {
        IEnumerable<IObjective> SubObjectives { get; }
        bool IsComplete();
        void AddSubObjective(IObjective subObjective);
        void ExecuteCompletionAction();
    }

    public class Objective : IObjective
    {
        public IEnumerable<IObjective> SubObjectives { get { return _subObjectives; } }

        private readonly string _text;
        private readonly Func<bool> _completionPredicate;
        private readonly Action _completionAction;
        private readonly IList<IObjective> _subObjectives = new List<IObjective>();

        public Objective(string text)
            : this(text, () => true, () => { })
        { }

        public Objective(string text, Func<bool> completionPredicate, Action completionAction)
        {
            _text = text;
            _completionPredicate = completionPredicate;
            _completionAction = completionAction;
        }

        public bool IsComplete()
        {
            return _completionPredicate() && _subObjectives.All(o => o.IsComplete());
        }

        public void ExecuteCompletionAction()
        {
            if( IsComplete() )
            {
                _completionAction();
            }
        }

        public void AddSubObjective(IObjective subObjective)
        {
            _subObjectives.Add(subObjective);
        }

        public override string ToString()
        {
            return _text;
        }
    }
}