﻿using System;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.People;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace UniversityDLL.GameMangement
{
    public class GameLoader
    {
        public IGameState LoadGame(IGridGenerator gridGenerator, IObjectFactoryFacade factoryFacade)
        {
            var gameState = GameState.GetInstance();

            gameState.Grid = gridGenerator.GenerateGrid();
            gameState.Grid.ConnectNeighbors();
            gameState.Calendar = new Calendar();
            gameState.Score = new Score();

            ScheduleStandardEvents(gameState, factoryFacade);

            var president = factoryFacade.CharacterFactory.MakeAdministrator();
            
            return gameState;
        }

        private void ScheduleStandardEvents(IGameState gameState, IObjectFactoryFacade objectFactory)
        {
            var startSemester = Semester.GetSemester(2006, Season.Fall);

            var lectureScheduleDate = new TimeUnit(startSemester, 1, DayOfWeek.Sunday, 1, QuarterHour.Fifteen);
            var lectureScheduleEvent = new LectureScheduleCalendarEvent(lectureScheduleDate, gameState, objectFactory.CharacterFactory);
            gameState.Calendar.AddEvent(lectureScheduleEvent);

            // Real admission date: var admissionDate = new DateTime(2006, 10, 09);
            var admissionDate = new TimeUnit(startSemester, 1, DayOfWeek.Sunday, 12, QuarterHour.Thirty);
            var admissionsEvent = new AdmissionsCalendarEvent(admissionDate, gameState, objectFactory);
            gameState.Calendar.AddEvent(admissionsEvent);
            
            // Real course selection date: var courseSelectionDate = new DateTime(2006, 11, 06);
            var courseSelectionDate = new TimeUnit(startSemester, 1, DayOfWeek.Monday, 3, QuarterHour.Fortyfive);
            var courseSelectionEvent = new CourseSelectionCalendarEvent(courseSelectionDate, gameState);
            gameState.Calendar.AddEvent(courseSelectionEvent);
            
            var tuitionDate = new TimeUnit(2006, Season.Fall, 4, DayOfWeek.Wednesday);
            var tuitionEvent = new TuitionCalendarEvent(tuitionDate, gameState);
            gameState.Calendar.AddEvent(tuitionEvent);
        }
    }
}
