﻿using System;
using System.Linq;
using UniversityDLL.ActivityPoints;
using UniversityDLL.NameGenerators;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public class RPub : Room
    {
        public RPub()
        {
            Name = PubNameGenerator.GenerateName();
        }

        public override string ToString()
        {
            return Name;
        }

        public override bool IsReady()
        {
            return ActivityPoints.Any(p => p is PubSeatActivityPoint);
        }
    }
}