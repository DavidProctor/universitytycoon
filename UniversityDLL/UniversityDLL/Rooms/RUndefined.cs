﻿using System;
using UniversityDLL.GameMangement;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public class RUndefined : Room
    {
        private static readonly RUndefined Instance = new RUndefined();

        public static RUndefined GetInstance()
        {
            return Instance;
        }

        private RUndefined()
        {
            Name = "Undefined Room";
            GameState.GetInstance().RegisterRoom(this);
        }
    }
}