﻿namespace UniversityDLL.Rooms
{
    public interface IRoomFactory
    {
        Room MakeLectureHall();
    }

    public class RoomFactory : IRoomFactory
    {
        public Room MakeLectureHall()
        {
            return new RLectureHall();
        }
    }
}
