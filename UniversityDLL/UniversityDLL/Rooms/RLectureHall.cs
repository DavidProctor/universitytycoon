﻿using System;
using System.Linq;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.ActivityPoints;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public class RLectureHall : BookableRoom
    {
        public override int Capacity { get { return ActivityPoints.Count(p => p is ClassSeatActivityPoint); } }
        
        private static int _instanceCount;

        public RLectureHall()
        {
            Name = $"Lecture Hall {_instanceCount++}";
        }

        public override bool IsReady()
        {
            var hasSeat = false;
            var hasLectern = false;

            foreach (var activityPoint in ActivityPoints)
            {
                if (!hasSeat && activityPoint is ClassSeatActivityPoint)
                {
                    hasSeat = true;
                }
                if (!hasLectern && activityPoint is LecternActivityPoint)
                {
                    hasLectern = true;
                }
                if (hasSeat && hasLectern)
                {
                    return true;
                }
            }
            return false;
        }

        public override void HourlyUpdate()
        {
            if(!IsReady()) return;

            var lectern = ActivityPoints.First(p => p is LecternActivityPoint);
            lectern.ExecuteActivity();
        }
    }
}
