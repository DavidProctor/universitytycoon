﻿using System;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public class ROutdoors : Room
    {
        private static readonly ROutdoors Instance = new ROutdoors();

        public static ROutdoors GetInstance()
        {
            return Instance;
        }

        public override int Capacity { get { return int.MaxValue; } }

        private ROutdoors()
        {
            Name = "Outdoors";
        }
    }
}