﻿using System;
using System.Linq;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public class RAdminOffice : Room
    {
        public IAdministrator Owner { get; private set; }

        public RAdminOffice()
        {
            Name = "Empty administrator's office";
        }

        public override bool IsReady()
        {
            return ActivityPoints.Any(p => p is OfficeDeskActivityPoint);
        }

        public void SetOwner(IAdministrator owner)
        {
            Owner = owner;
            Owner.SetOffice(this);
            Name = $"{Owner.Name}'s office";
        }
    }
}
