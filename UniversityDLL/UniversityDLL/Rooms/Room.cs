﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.Academics;
using UniversityDLL.ActivityPoints;
using UniversityDLL.Tiles;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public abstract class Room : IWaypoint, IGameEntity
    {
        public virtual IEnumerable<IPathConnection> Connections { get { return PathConnections; } }

        public virtual IList<IWaypoint> Neighbors
        {
            get
            {
                return PathConnections.SelectMany(c => c.Nodes)
                                       .Where(t => !t.Equals(this))
                                       .ToList();
            }
        }

        public string Name { get; protected set; }
        public readonly List<ITile> Tiles;

        public virtual int Capacity { get { return Tiles.Count; } }

        protected readonly List<Exit> Exits = new List<Exit>();
        protected readonly List<ActivityPoint> ActivityPoints = new List<ActivityPoint>();
        protected readonly List<ActivityPoint> AvailableActivityPoints = new List<ActivityPoint>();
        protected readonly List<IPathConnection> PathConnections = new List<IPathConnection>();

        protected Room()
        {
            Tiles = new List<ITile>();
        }

        public virtual void SetTiles(IEnumerable<ITile> tiles)
        {
            Tiles.AddRange(tiles);
        }

        public virtual void AddExit(ITile tile, Room room)
        {
            var exit = new Exit { Room = room, Tile = tile };
            Exits.Add(exit);
        }

        public virtual List<ITile> GetExitsToRoom(Room room)
        {
            return (from exit 
                    in Exits
                    where exit.Room == room
                    select exit.Tile).ToList();
        }

        public virtual List<ITile> GetAllExits()
        {
            return Exits.Select(e => e.Tile).ToList();
        }

        public void RemoveExit(ITile tile)
        {
            for( int i = 0; i < Exits.Count; ++i )
            {
                if( Exits[i].Tile == tile )
                {
                    Exits.RemoveAt(i);
                    return;
                }
            }
        }

        public virtual bool IsWalkable()
        {
            return true;
        }

        public virtual void AddActivityPoint(ActivityPoint activityPoint)
        {
            ActivityPoints.Add(activityPoint);
            AvailableActivityPoints.Add(activityPoint);
        }

        public virtual List<ActivityPoint> GetActivityPoints()
        {
            return ActivityPoints;
        }

        public virtual ActivityPoint GetAvailableActivityPoint<T>() where T : ActivityPoint
        {
            return AvailableActivityPoints.Where(p => p is T).GetRandom();
        }

        public virtual void VacateActivityPoint(ActivityPoint activityPoint)
        {
            AvailableActivityPoints.Add(activityPoint);
        }

        public virtual void OccupyActivityPoint(ActivityPoint activityPoint)
        {
            if (ActivityPoints.Contains(activityPoint))
            {
                AvailableActivityPoints.Remove(activityPoint);
            }
        }

        public virtual void RemoveActivityPoint(ActivityPoint activityPoint)
        {
            ActivityPoints.Remove(activityPoint);
        }

        public virtual void AddConnection(IWaypoint neighbor)
        {
            var connection = PathConnection.GetConnection(this, neighbor);
            PathConnections.Add(connection);
        }

        public virtual void RemoveConnection(IWaypoint neighbor)
        {
            var connection = PathConnection.GetConnection(this, neighbor);
            PathConnections.Remove(connection);
        }

        public virtual void HourlyUpdate()
        {
            
        }

        public virtual bool IsReady()
        {
            return true;
        }
    }

    [Serializable]
    public abstract class BookableRoom : Room
    {
        protected readonly List<RoomSchedule> RoomSchedule;

        protected BookableRoom()
        {
            RoomSchedule = new List<RoomSchedule>();
        }

        public virtual IRoomSchedule GetSchedule(Semester semester)
        {
            var schedule = RoomSchedule.FirstOrDefault(s => s.Semester.Equals(semester));
            if( schedule == null )
            {
                schedule = new RoomSchedule(this, semester);
                RoomSchedule.Add(schedule);
            }
            return schedule;
        }

        public virtual void AddBooking(DayAndTime dayAndTime, ICourseInstance course, Semester semester)
        {
            var schedule = GetSchedule(semester);
            schedule.Schedule(dayAndTime, course);
        }
        
        public void RemoveBooking(DayAndTime dayAndTime, Semester semester)
        {
            var schedule = GetSchedule(semester);
            schedule.RemoveBooking(dayAndTime);
        }
    }

    [Serializable]
    public struct Exit
    {
        public ITile Tile;
        public Room Room;
    }
}
