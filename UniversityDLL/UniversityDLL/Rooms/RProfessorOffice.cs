﻿using System;
using System.Linq;
using UniversityDLL.ActivityPoints;

namespace UniversityDLL.Rooms
{
    [Serializable]
    public class RProfessorOffice : Room
    {
        public override int Capacity { get { return ActivityPoints.Count(p => p is OfficeDeskActivityPoint); } }

        public override bool IsReady()
        {
            return ActivityPoints.Any(p => p is OfficeDeskActivityPoint);
        }
    }
}
