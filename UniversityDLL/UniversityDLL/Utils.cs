﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniversityDLL.Rooms;
using Random = System.Random;

namespace UniversityDLL
{
    public static class Utils
    {
        private const float FloatTolerance = 0.1f;
        private static readonly Random Randy = new Random();

        public static string DebugText = string.Empty;

        public static bool FloatEquals(float a, float b)
        {
            return a >= b - FloatTolerance && a <= b + FloatTolerance;
        }

        public static bool IsTrueWithChance(int chance)
        {
            return Randy.Next(100) <= chance;
        }

        public static void ActOnGameObjectAndChildren(this GameObject target, Action<GameObject> action)
        {
            action(target);
            foreach (Transform child in target.transform)
            {
                child.gameObject.ActOnGameObjectAndChildren(action);
            }
        }

        public static Room GetRandomRoomOfType<TRoom>(this IEnumerable<Room> rooms) where TRoom : Room
        {
            return rooms.Where(r => r is TRoom).GetRandom();
        }
    }
}
