﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UniversityDLL.People.Factions
{
    class Faction
    {
        static int factionCount = 0;

        private int id;
        private List<FactionUnit> _units;

        public Faction()
        {
            id = ++factionCount;
        }

        public override string ToString()
        {
            return $"Faction {id}";
        }
    }

    class FactionUnit
    {
        
    }

    class ControlPoint
    {
        
    }

    class FactionGame
    {
        public ControlPoint[] ControlPoints { get; }
        public Faction[] Factions { get; }

        public FactionGame(int points, int factions)
        {
            ControlPoints = new ControlPoint[points];
            Factions = new Faction[factions];
            for (int i = 0; i < factions; ++i)
            {
                Factions[i] = new Faction();
            }
            foreach (var controlPoint in ControlPoints)
            {
                
            }
        }
    }
}
