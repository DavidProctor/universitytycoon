﻿using UniversityDLL.Scheduling;

namespace UniversityDLL.People.Planners
{
    public interface IDayPlanner
    {
        void PlanDay(ICharacter character, TimeUnit dayTimeUnit);
    }
}