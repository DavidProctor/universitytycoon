﻿using System;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;

namespace UniversityDLL.People.Planners
{
    public class ProfessorDayPlanner : IDayPlanner
    {
        private readonly IGameState _gameState;

        public ProfessorDayPlanner(IGameState gameState)
        {
            _gameState = gameState;
        }

        public void PlanDay(ICharacter character, TimeUnit dayTimeUnit)
        {
            var hasArrived = false;
            var day = dayTimeUnit.Day;
            if( day == DayOfWeek.Saturday || day == DayOfWeek.Sunday )
            {
                return;
            }

            for( int hour = 8; hour < 25; ++hour )
            {
                var dayAndTime = DayAndTime.FromDetails(day, hour);

                var booking = character.PersonalSchedule.GetLocationAtTime(dayAndTime);
                if( booking != null )
                {
                    hasArrived = true;
                    continue;
                }

                if( !hasArrived )
                {
                    continue;
                }

                if( Utils.IsTrueWithChance(70) )
                {
                    var room = _gameState.Rooms.GetRandomRoomOfType<RProfessorOffice>();
                    if (room != null)
                    {
                        character.PersonalSchedule.AddOneTimeBooking(dayAndTime, room);
                    }
                    continue;
                }

                if( Utils.IsTrueWithChance(10) )
                {
                    var room = _gameState.Rooms.GetRandomRoomOfType<RPub>();
                    if (room != null)
                    {
                        character.PersonalSchedule.AddOneTimeBooking(dayAndTime, room);
                    }
                    continue;
                }

                if( !character.PersonalSchedule.HasLaterBooking(dayAndTime) )
                {
                    break;
                }
            }
        }
    }
}