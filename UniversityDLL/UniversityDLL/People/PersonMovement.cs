﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniversityDLL.ActivityPoints;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLL.People
{
    public class PersonMovement : MonoBehaviour
    {
        public bool IsMoving { get; private set; }
        public bool IsSpawned { get { return transform.position.y < 9000; } }
        public ICharacterBehavior CharacterBehavior { get; set; }

        private PathProvider _pathProvider;
        private readonly float tilesWalkedPerHour = 100f;

        public void Start()
        {
            _pathProvider = PathProvider.GetInstance();
            IsMoving = false;
        }

        public void SetDestination(ActivityPoint activityPoint)
        {
            var currentTile = GetCurrentTile();
            if (activityPoint.Tile == currentTile)
            {
                return;
            }
            WalkBetweenTiles(currentTile, activityPoint.Tile);
        }

        private void WalkBetweenTiles(ITile start, ITile end)
        {
            List<ITile> path;
            try
            {
                path = _pathProvider.GetPath(start, end);
            }
            catch( NoPathExistsException ex )
            {
                Debug.LogError(ex.Message);
                return;
            }
            StartCoroutine(WalkPath(path));
        }

        private IEnumerator WalkPath(List<ITile> path)
        {
            IsMoving = true;
            foreach( var tile in path )
            {
                yield return StartCoroutine(WalkTo(tile));
            }
            IsMoving = false;
        }

        private IEnumerator WalkTo(ITile tile)
        {
            var position = new Vector3(tile.X, 0, tile.Z);
            while( Vector3.Distance(transform.position, position) > 0.01f )
            {
                var distance = tilesWalkedPerHour * (1 / GameState.GetInstance().Calendar.SecondsPerTick) * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, position, distance);
                yield return 0;
            }
            transform.position = position;
        }

        public ITile GetCurrentTile()
        {
            if( transform.position.y > 9000f )
            {
                var tile = GameState.GetInstance().Grid.GetTile(0, 0);
                CharacterBehavior.Character.Spawn(tile);
                return tile;
            }

            var x = Mathf.RoundToInt(transform.position.x);
            var z = Mathf.RoundToInt(transform.position.z);
            return GameState.GetInstance().Grid.GetTile(x, z);
        }
    }
}