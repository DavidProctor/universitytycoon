﻿using System;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.People
{
    public interface IAdministrator : ICharacter
    {
        RAdminOffice Office { get; }

        void SetOffice(RAdminOffice office);
    }

    [Serializable]
    class Administrator : IAdministrator
    {
        public string Name { get; }
        public ICharacterBehavior Behavior { get; }
        public IPersonalSchedule PersonalSchedule { get; }
        public ICharacterRenderer Renderer { get; }
        public RAdminOffice Office { get; private set; }

        public Administrator(string name,
            ICharacterBehavior behavior,
            IPersonalSchedule schedule,
            ICharacterRenderer renderer)
        {
            Name = name;
            Behavior = behavior;
            PersonalSchedule = schedule;
            Renderer = renderer;

            GameState.GetInstance().HireAdministrator(this);
        }

        public void SetOffice(RAdminOffice office)
        {
            Office = office;
        }

        public void Spawn(ITile tile)
        {
            Renderer.Spawn(tile);
        }

        public void Despawn()
        {
            Renderer.Despawn();
        }
    }
}
