﻿using System;
using System.Collections.Generic;

namespace UniversityDLL.People
{
    [Serializable]
    public enum Sex
    {
        Male,
        Female
    }

    public interface INameGenerator
    {
        string GetName(Sex sex);
    }

    [Serializable]
    public class NameGenerator : INameGenerator
    {
        public string GetName(Sex sex)
        {
            var firstName = sex == Sex.Female ? FemaleFirstNames.GetRandom() : MaleFirstNames.GetRandom();
            return $"{firstName} {LastNames.GetRandom()}";
        }

        private static readonly List<string> FemaleFirstNames = new List<string>
        {
            "Jessica",
            "Ashley",
            "Brittany",
            "Amanda",
            "Sarah",
            "Jennifer",
            "Melissa",
            "Amy",
            "Heather",
            "Michelle",
            "Angela",
            "Kimberly",
            "Lisa",
            "Brittany",
            "Samantha",
            "Ashley",
            "Emily",
        };

        private static readonly List<string> MaleFirstNames = new List<string>
        {
            "Michael",
            "Christopher",
            "Matthew",
            "Joshua",
            "David",
            "Andrew",
            "Jason",
            "James",
            "John",
            "Robert",
            "Tyler",
            "Daniel",
            "Christopher"
        };

        private static readonly List<string> LastNames = new List<string>
        {
            "Smith",
            "Johnson",
            "Williams",
            "Jones",
            "Brown",
            "Davis",
            "Miller",
            "Wilson",
            "Moore",
            "Taylor",
            "Anderson",
            "Thomas",
            "Jackson",
            "White",
            "Harris",
            "Martin",
            "Thompson",
            "Garcia",
            "Martinez",
            "Robinson",
            "Clark",
            "Rodriguez",
            "Lewis",
            "Lee",
            "Walker",
            "Hall",
            "Allen",
            "Young",
            "Hernandez",    
            "King"     
        };
    }
}