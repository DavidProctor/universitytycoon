﻿using UnityEngine;
using UniversityDLL.ActivityPoints;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;

namespace UniversityDLL.People.Behaviors
{
    internal class ProfessorCharacterBehavior : MonoBehaviour, ICharacterBehavior
    {
        public ICharacter Character { get; set; }
        private ICalendar _calendar;
        private PersonMovement _personMovement;

        public void Start()
        {
            _personMovement = gameObject.AddComponent<PersonMovement>();
            _personMovement.CharacterBehavior = this;
            _calendar = GameState.GetInstance().Calendar;
        }

        private ActivityPoint _point;

        public void Update()
        {
            if (Character == null)
            {
                return;
            }

            var dayAndTime = _calendar.CurrentTime.DayAndTime;
            var location = Character.PersonalSchedule.GetLocationAtTime(dayAndTime);
            if( location != null )
            {
                var hasMoved = _point != null && location != _point.Tile.Room;
                if( hasMoved )
                {
                    _point.Vacate();
                }

                if( !_personMovement.IsSpawned || hasMoved )
                {
                    var activityPoint = ChooseActivityPoint(location);
                    if( activityPoint != null && activityPoint.TrySetOccupant(Character) )
                    {
                        _point = activityPoint;
                        Character.Spawn(activityPoint.Tile);
                        Character.Renderer.GameObject.SetOrientation(_point.TileContent.Orientation);
                    }
                }
            }
            else if( _personMovement.IsSpawned )
            {
                _point?.Vacate();
                _point = null;
                Character.Despawn();
            }
        }

        private ActivityPoint ChooseActivityPoint(Room room)
        {
            if (room is RLectureHall)
            {
                return room.GetAvailableActivityPoint<LecternActivityPoint>();
            }
            if (room is RProfessorOffice)
            {
                return room.GetAvailableActivityPoint<OfficeDeskActivityPoint>();
            }
            if (room is RPub)
            {
                return room.GetAvailableActivityPoint<PubSeatActivityPoint>();
            }

            return null;
        }
    }
}