﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.Rooms;

namespace UniversityDLL.People
{
    public interface IPersonalSchedule
    {
        void AddBooking(DayAndTime dayAndTime, Room room);
        void AddOneTimeBooking(DayAndTime dayAndTime, Room room);
        void ClearOneTimeActivities();
        Room GetLocationAtTime(DayAndTime dayAndTime);
        List<Booking> GetBookingsByDay(DayOfWeek day);
        bool HasLaterBooking(DayAndTime dayAndTime);
    }

    [Serializable]
    public class Booking
    {
        public bool IsOneTime { get; }
        public DayAndTime DayAndTime { get; }
        public Room Room { get; }

        private Booking(DayAndTime dayAndTime, Room room, bool isOneTime)
        {
            IsOneTime = isOneTime;
            DayAndTime = dayAndTime;
            Room = room;
        }

        public static Booking OneTime(DayAndTime dayAndTime, Room room)
        {
            return new Booking(dayAndTime, room, true);
        }

        public static Booking Repeating(DayAndTime dayAndTime, Room room)
        {
            return new Booking(dayAndTime, room, false);
        }
    }

    [Serializable]
    public class PersonalSchedule : IPersonalSchedule
    {
        private readonly List<Booking> _sundayBookings;
        private readonly List<Booking> _mondayBookings;
        private readonly List<Booking> _tuesdayBookings;
        private readonly List<Booking> _wednesdayBookings;
        private readonly List<Booking> _thursdayBookings;
        private readonly List<Booking> _fridayBookings;
        private readonly List<Booking> _saturdayBookings;

        public PersonalSchedule()
        {
            _sundayBookings = new List<Booking>();
            _mondayBookings = new List<Booking>();
            _tuesdayBookings = new List<Booking>();
            _wednesdayBookings = new List<Booking>();
            _thursdayBookings = new List<Booking>();
            _fridayBookings = new List<Booking>();
            _saturdayBookings = new List<Booking>();
        }

        public void AddBooking(DayAndTime dayAndTime, Room room)
        {
            var booking = Booking.Repeating(dayAndTime, room);
            GetBookingsByDay(dayAndTime.Day).Add(booking);
        }

        public void AddOneTimeBooking(DayAndTime dayAndTime, Room room)
        {
            var booking = Booking.OneTime(dayAndTime, room);
            GetBookingsByDay(dayAndTime.Day).Add(booking);
        }

        public void ClearOneTimeActivities()
        {
            foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
            {
                GetBookingsByDay(day).RemoveAll(booking => booking.IsOneTime);
            }
        }

        public Room GetLocationAtTime(DayAndTime dayAndTime)
        {
            var bookings = GetBookingsByDay(dayAndTime.Day);
            return (from booking 
                    in bookings
                    where booking.DayAndTime.Hour == dayAndTime.Hour
                    select booking.Room).FirstOrDefault();
        }

        public List<Booking> GetBookingsByDay(DayOfWeek day)
        {
            switch(day)
            {
                case DayOfWeek.Sunday:
                    return _sundayBookings;
                case DayOfWeek.Monday:
                    return _mondayBookings;
                case DayOfWeek.Tuesday:
                    return _tuesdayBookings;
                case DayOfWeek.Wednesday:
                    return _wednesdayBookings;
                case DayOfWeek.Thursday:
                    return _thursdayBookings;
                case DayOfWeek.Friday:
                    return _fridayBookings;
                case DayOfWeek.Saturday:
                    return _saturdayBookings;
                default:
                    return null;
            }
        }

        public bool HasLaterBooking(DayAndTime dayAndTime)
        {
            var bookings = GetBookingsByDay(dayAndTime.Day);
            return bookings.Any(booking => booking.DayAndTime.Hour >= dayAndTime.Hour);
        }
    }
}
