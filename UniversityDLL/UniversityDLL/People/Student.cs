﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLL.People
{
    public interface IStudent : ICharacter
    {
        IDepartment Major { get; }
        IList<ICourseEnrolment> Enrolments { get; }
        // ReSharper disable once InconsistentNaming
        float GPA { get; }
        Year Year { get; }

        void AddEnrolment(ICourseEnrolment enrolment);
        void Study(ICourseInstance course, int points);
    }

    [Serializable]
    public class Student : IStudent
    {
        public string Name { get; }
        public IDepartment Major { get; }
        public ICharacterRenderer Renderer { get; }
        public IList<ICourseEnrolment> Enrolments { get; }
        public ICharacterBehavior Behavior { get; }
        public IPersonalSchedule PersonalSchedule { get; }
        // ReSharper disable once InconsistentNaming
        public float GPA { get; }
        public Year Year { get { return GetYear(); } }

        public Student(string name,
            IDepartment major, 
            ICharacterRenderer renderer, 
            ICharacterBehavior behavior,
            IPersonalSchedule schedule)
        {
            Name = name;
            Major = major;
            Renderer = renderer;
            Behavior = behavior;
            PersonalSchedule = schedule;
            Enrolments = new List<ICourseEnrolment>();
        }

        public void AddEnrolment(ICourseEnrolment enrolment)
        {
            foreach(var session in enrolment.Course.ScheduledSessions)
            {
                PersonalSchedule.AddBooking(session.DayAndTime, session.Room);
            }
            Enrolments.Add(enrolment);
        }

        public void Study(ICourseInstance course, int points)
        {
            var enrolment = Enrolments.FirstOrDefault(e => e.Course == course);

            enrolment?.AddStudyValue(points);
        }

        private Year GetYear()
        {
            var creditsCompleted = Enrolments.Aggregate(0, (total, next) => total += next.Course.Template.RequiredHours);
            if(creditsCompleted >= GameState.CreditsPerYear * 3)
            {
                return Year.Fourth;
            }
            if(creditsCompleted >= GameState.CreditsPerYear * 2)
            {
                return Year.Third;
            }
            if(creditsCompleted >= GameState.CreditsPerYear)
            {
                return Year.Second;
            }
            return Year.First;
        }

        public void Spawn(ITile tile)
        {
            Renderer.Spawn(tile);
        }

        public void Despawn()
        {
            Renderer.Despawn();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
