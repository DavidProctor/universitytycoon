﻿using UniversityDLL.Tiles;

namespace UniversityDLL.People
{
    public interface ICharacter : IGameEntity
    {
        ICharacterBehavior Behavior { get; }
        IPersonalSchedule PersonalSchedule { get; }
        ICharacterRenderer Renderer { get; }
        void Spawn(ITile tile);
        void Despawn();
    }

    public interface ICharacterBehavior
    {
        ICharacter Character { get; set; }
    }

    public interface IStudentFactory
    {
        IStudent MakeStudent(INameGenerator nameGenerator);
    }
}