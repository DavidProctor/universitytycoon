﻿using System;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLL.People
{
    public interface ICharacterRenderer
    {
        IGameObject GameObject { get; }
        bool IsSpawned();
        void Spawn(ITile tile);
        void Despawn();
    }

    [Serializable]
    public class CharacterRenderer : ICharacterRenderer
    {
        public IGameObject GameObject { get; }

        public CharacterRenderer(IGameObject gameObject)
        {
            GameObject = gameObject;
        }

        public bool IsSpawned()
        {
            return GameObject.GameObject.transform.position.y < 9000f;
        }

        public void Spawn(ITile tile)
        {
            GameObject.Render(tile.X, 0f, tile.Z);
        }

        public void Despawn()
        {
            GameObject.Render(0, 10000f, 0);
        }
    }
}
