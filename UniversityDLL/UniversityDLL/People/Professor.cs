﻿using System;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLL.People
{
    public interface IProfessor : ICharacter
    {
        void SetCourse(ICourseInstance course);
        int TeachingSkill { get; }
    }

    [Serializable]
    public class Professor : IProfessor
    {
        public string Name { get; }
        public ICharacterBehavior Behavior { get; }
        public IPersonalSchedule PersonalSchedule { get; }
        public ICharacterRenderer Renderer { get; }
        public int TeachingSkill { get; }

        public Professor(string name,
            ICharacterBehavior behavior,
            IPersonalSchedule schedule,
            ICharacterRenderer renderer,
            int teachingSkill)
        {
            Name = name;
            Behavior = behavior;
            PersonalSchedule = schedule;
            Renderer = renderer;
            TeachingSkill = teachingSkill;
            GameState.GetInstance().HireProfessor(this);
        }

        public void SetCourse(ICourseInstance course)
        {
            foreach (var booking in course.ScheduledSessions)
            {
                PersonalSchedule.AddBooking(booking.DayAndTime, booking.Room);
            }
        }

        public void Spawn(ITile tile)
        {
            Renderer.Spawn(tile);
        }

        public void Despawn()
        {
            Renderer.Despawn();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
