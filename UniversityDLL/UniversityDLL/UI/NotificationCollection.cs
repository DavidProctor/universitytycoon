﻿using System.Collections.Generic;
using UniversityDLL.GameMangement;

namespace UniversityDLL.UI
{
    public class NotificationCollection : List<Notification>
    {
        public static NotificationCollection Instance { get; } = new NotificationCollection();

        private NotificationCollection()
        {}

        public void AddNotification(Notification notification)
        {
            Add(notification);
        }

        public void ClearElapsedNotifications()
        {
            var currentTime = GameState.GetInstance().Calendar.CurrentTime;
            RemoveAll(n => n.Expiry <= currentTime);
        }
    }
}