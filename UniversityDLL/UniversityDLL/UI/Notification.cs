﻿using System;
using UnityEngine;
using UniversityDLL.GameMangement;
using UniversityDLL.Scheduling;

namespace UniversityDLL.UI
{
    public class Notification
    {
        private readonly string _text;

        public string Text => _text;
        public TimeUnit Expiry { get; }
        public float SpawnTime { get; }
        public Action OnClick { get; }

        public static Notification WithExpiry(string text, TimeUnit expiry)
        {
            return new Notification(text, expiry);
        }

        public static Notification WithAction(string text, Action onClick)
        {
            return new Notification(text, null, onClick);
        }

        private Notification(string text, TimeUnit expiry = null, Action onClick = null)
        {
            _text = text;
            Expiry = expiry ?? new TimeUnit(long.MaxValue);
            OnClick = onClick;
            SpawnTime = Time.time;
        }

        public override string ToString()
        {
            return _text;
        }
    }
}