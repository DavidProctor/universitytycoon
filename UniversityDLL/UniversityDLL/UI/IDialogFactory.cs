﻿namespace UniversityDLL.UI
{
    public interface IDialogFactory
    {
        IDialog GetInfoDialog(IGameEntity entity);
    }
}