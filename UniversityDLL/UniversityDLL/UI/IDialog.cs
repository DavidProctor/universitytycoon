﻿using UnityEngine;

namespace UniversityDLL.UI
{
    public interface IDialog
    {
        GameObject GameObject { get; } 
    }
}