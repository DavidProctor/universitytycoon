﻿using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.UI
{
    public class TooltipContent
    {
        public string Title { get; }
        public string Content { get; }

        public TooltipContent(string title, string content)
        {
            Title = title;
            Content = content;
        }
    }

    public static class TooltipContentFactory
    {
        private static readonly IGameState GameState = GameMangement.GameState.GetInstance();

        public static TooltipContent MakeContent(IGameEntity entity)
        {
            if (entity is ITile)
            {
                var tile = entity as ITile;
                if (tile.Room is ROutdoors || tile.Room is RUndefined)
                {
                    return null;
                }
                return MakeRoomContent(tile.Room);
            }
            if (entity is IStudent)
            {
                return MakeStudentContent(entity as IStudent);
            }
            if (entity is IProfessor)
            {
                return MakeProfContent(entity as IProfessor);
            }

            return null;
        }

        private static TooltipContent MakeStudentContent(IStudent student)
        {
            return new TooltipContent(student.Name, $"{student.Year} year {student.Major}");
        }

        private static TooltipContent MakeProfContent(IProfessor prof)
        {
            return new TooltipContent(prof.Name, $"Professor");
        }

        private static TooltipContent MakeRoomContent(Room room)
        {
            if (room is ROutdoors || room is RUndefined)
            {
                return null;
            }
            if (room is RLectureHall)
            {
                return MakeLectureHallContent(room as RLectureHall);
            }
            return new TooltipContent(room.Name, null);
        }

        private static TooltipContent MakeLectureHallContent(RLectureHall lectureHall)
        {
            var time = GameState.Calendar.CurrentTime;
            var schedule = lectureHall.GetSchedule(time.Semester);
            var booking = schedule.GetBooking(time.DayAndTime);
            var content = booking == null
                ? $"Capacity {lectureHall.Capacity}"
                : $"{booking.Course.Template.Name}\nCapacity {lectureHall.Capacity}";


            return new TooltipContent(lectureHall.Name, content);
        }
    }
}
