﻿using System.Collections.Generic;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Selections
{
    public interface ISelection
    {
        void StartSelection(ITile startTile);
        void ChangeSelection(ITile currentTile);

        IList<ITile> GetSelectedTiles();
        IList<ITile> GetEdgeTiles();
    }
}
