﻿using System.Collections.Generic;
using System.Linq;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Selections
{
    public class LineSelection : ISelection
    {
        private enum Direction
        {
            X,
            Z
        }

        private readonly IGrid _grid;
        private readonly HashSet<ITile> _selectedTiles;
        private int _startX;
        private int _startZ;
        private Direction? _direction;

        public LineSelection(IGrid grid)
        {
            _grid = grid;
            _selectedTiles = new HashSet<ITile>();
        }

        public void StartSelection(ITile startTile)
        {
            _direction = null;
            _selectedTiles.Clear();
            _selectedTiles.Add(startTile);
            _startX = startTile.X;
            _startZ = startTile.Z;
        }

        public void ChangeSelection(ITile currentTile)
        {
            _selectedTiles.Clear();

            if (!_direction.HasValue)
            {
                _direction = currentTile.X != _startX ? Direction.Z : Direction.X;
            }

            var lowX = _startX < currentTile.X ? _startX : currentTile.X;
            var highX = _startX > currentTile.X ? _startX : currentTile.X;
            var lowZ = _startZ < currentTile.Z ? _startZ : currentTile.Z;
            var highZ = _startZ > currentTile.Z ? _startZ : currentTile.Z;
            var xLine = _direction == Direction.X ? _startX : currentTile.X;
            var zLine = _direction == Direction.Z ? _startZ : currentTile.Z;

            for (int x = lowX; x <= highX; ++x)
            {
                _selectedTiles.Add(_grid.GetTile(x, zLine));
            }

            for (int z = lowZ; z <= highZ; ++z)
            {
                _selectedTiles.Add(_grid.GetTile(xLine, z));
            }
        }

        public IList<ITile> EndSelection(ITile endTile)
        {
            return _selectedTiles.ToList();
        }

        public IList<ITile> GetSelectedTiles()
        {
            return _selectedTiles.ToList();
        }

        public IList<ITile> GetEdgeTiles()
        {
            return _selectedTiles.ToList();
        }
    }
}
