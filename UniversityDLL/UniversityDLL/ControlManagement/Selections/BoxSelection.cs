﻿using System.Collections.Generic;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Selections
{
    public class BoxSelection : ISelection
    {
        private struct SelectionEdges
        {
            public int LowX;
            public int HighX;
            public int LowZ;
            public int HighZ;
        }

        private int _startX;
        private int _startZ;
        private int _currentX;
        private int _currentZ;
        private bool _selectionStarted;

        private readonly IGrid _grid;

        public BoxSelection(IGrid grid)
        {
            _grid = grid;
            _selectionStarted = false;
        }

        public void StartSelection(ITile tile)
        {
            _startX = tile.X;
            _startZ = tile.Z;
            _currentX = tile.X;
            _currentZ = tile.Z;
            _selectionStarted = true;
        }

        public void ChangeSelection(ITile tile)
        {
            _currentX = tile.X;
            _currentZ = tile.Z;
        }

        public IList<ITile> GetSelectedTiles()
        {
            var selectedTiles = new List<ITile>();
            if(!_selectionStarted)
            {
                return selectedTiles;
            }
            var edges = GetSelectionEdges();

            for(var x = edges.LowX; x <= edges.HighX; ++x)
            {
                for(var z = edges.LowZ; z <= edges.HighZ; ++z)
                {
                    selectedTiles.Add(_grid.GetTile(x,z));
                }
            }

            return selectedTiles;
        }

        public IList<ITile> GetEdgeTiles()
        {
            var selectedTiles = new List<ITile>();
            var edges = GetSelectionEdges();

            for (var x = edges.LowX; x <= edges.HighX; ++x)
            {
                for (var z = edges.LowZ; z <= edges.HighZ; ++z)
                {
                    if(x == edges.LowX || x == edges.HighX
                        || z == edges.LowZ || z == edges.HighZ)
                    {
                        selectedTiles.Add(_grid.GetTile(x, z));
                    }
                }
            }

            return selectedTiles;
        }

        private SelectionEdges GetSelectionEdges()
        {
            return new SelectionEdges
            {
                LowX = _startX < _currentX ? _startX : _currentX,
                HighX = _startX > _currentX ? _startX : _currentX,
                LowZ = _startZ < _currentZ ? _startZ : _currentZ,
                HighZ = _startZ > _currentZ ? _startZ : _currentZ,
            };
        }
    }
}
