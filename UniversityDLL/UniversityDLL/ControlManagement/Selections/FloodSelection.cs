﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Selections
{
    public class FloodSelection : ISelection
    {
        private readonly HashSet<ITile> _selectedTiles;
        private readonly HashSet<ITile> _edgeTiles;
        private readonly Func<ITile, ITile, bool> _shouldFill;

        public FloodSelection()
            : this((s, t) => !t.TileContentCollection.HasLayer(TileLayer.Wall))
        { }

        public FloodSelection(Func<ITile, ITile, bool> shouldFill)
        {
            _selectedTiles = new HashSet<ITile>();
            _edgeTiles = new HashSet<ITile>();
            _shouldFill = shouldFill;
        }

        public void StartSelection(ITile startTile)
        {
            _selectedTiles.Clear();
            _edgeTiles.Clear();

            if (startTile.Room == ROutdoors.GetInstance())
            {
                return;
            }

            _selectedTiles.Add(startTile);

            if(!_shouldFill(startTile, startTile))
            {
                return;
            }

            SelectValidNeighbors(startTile);
        }

        public void ChangeSelection(ITile currentTile)
        {
            if (!_selectedTiles.Contains(currentTile))
            {
                StartSelection(currentTile);
            }
        }

        public IList<ITile> GetSelectedTiles()
        {
            return _selectedTiles.ToList();
        }

        public IList<ITile> GetEdgeTiles()
        {
            return _edgeTiles.ToList();
        }

        private void SelectValidNeighbors(ITile tile)
        {
            foreach(var neighbor in tile.Neighbors)
            {
                var neighboringTile = neighbor as ITile;
                if (neighboringTile == null)
                {
                    continue;
                }

                if(_shouldFill(tile, neighboringTile)) 
                {
                    if (_selectedTiles.Contains(neighboringTile))
                    {
                        continue;
                    }
                    _selectedTiles.Add(neighboringTile);
                    SelectValidNeighbors(neighboringTile);
                }
                else
                {
                    _edgeTiles.Add(tile);
                }
            }
        }
    }
}
