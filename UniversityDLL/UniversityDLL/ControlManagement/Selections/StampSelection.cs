﻿using System.Collections.Generic;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Selections
{
    public class StampSelection : ISelection
    {
        private struct MinAndMax
        {
            public int Min { get; }
            public int Max { get; }

            public MinAndMax(int min, int max)
            {
                Min = min;
                Max = max;
            }
        }

        private ITile _pivot;
        private readonly IStamp _stamp;
        private readonly IGrid _grid;

        public StampSelection(IStamp stamp, IGrid grid)
        {
            _stamp = stamp;
            _grid = grid;
        }

        public void StartSelection(ITile startTile)
        {
            _pivot = startTile;
        }

        public void ChangeSelection(ITile currentTile)
        {
            _pivot = currentTile;
        }

        public IList<ITile> GetSelectedTiles()
        {
            if (_pivot == null)
            {
                return new List<ITile>();
            }

            var xRange = GetXRange();
            var zRange = GetZRange();

            var selected = new List<ITile>();
            for (int x = xRange.Min; x < xRange.Max; ++x)
            {
                for (int z = zRange.Min; z < zRange.Max; ++z)
                {
                    selected.Add(_grid.GetTile(x,z));
                }
            }

            return selected;
        }

        private MinAndMax GetXRange()
        {
            var min = _pivot.X - (_stamp.Width / 2);
            min = min < 0 ? 0 : min;
            var max = min + _stamp.Width;
            max = max >= _grid.Width ? _grid.Width - 1 : max;
            min = max == _grid.Width - 1 ? max - _stamp.Width : min;
            return new MinAndMax(min, max);
        }

        private MinAndMax GetZRange()
        {
            var min = _pivot.Z - (_stamp.Height / 2);
            min = min < 0 ? 0 : min;
            var max = min + _stamp.Height;
            max = max >= _grid.Height ? _grid.Height - 1 : max;
            min = max == _grid.Height - 1 ? max - _stamp.Height : min;
            return new MinAndMax(min, max);
        }

        public IList<ITile> GetEdgeTiles()
        {
            return new List<ITile>();
        }
    }
}