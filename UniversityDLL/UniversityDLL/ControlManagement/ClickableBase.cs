﻿using UnityEngine;

namespace UniversityDLL.ControlManagement
{
    public abstract class ClickableBase : MonoBehaviour
    {
         public abstract IGameEntity Entity { get; protected set; }
    }
}