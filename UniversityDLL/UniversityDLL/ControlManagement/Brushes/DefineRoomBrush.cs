﻿using System;
using UnityEngine;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Brushes
{
    public class DefineRoomBrush : IBrush
    {
        public ISelection Selection { get; }
        public IGameState GameState { get; }

        private readonly Func<Room> _createRoomFunc; 

        public DefineRoomBrush(ISelection selection, Func<Room> createRoomFunc, IGameState gameState)
        {
            Selection = selection;
            _createRoomFunc = createRoomFunc;
            GameState = gameState;
        }

        public void Execute()
        {
            var room = _createRoomFunc();
            var tiles = Selection.GetSelectedTiles();
            room.SetTiles(tiles);

            foreach (var tile in tiles)
            {
                if (tile.HasLayer(TileLayer.ActivityPoint))
                {
                    var oldRoom = tile.Room;
                    var activityPoint = oldRoom.GetActivityPoints().Find(p => p.Tile == tile);
                    oldRoom.RemoveActivityPoint(activityPoint);
                    room.AddActivityPoint(activityPoint);
                }
                tile.Room = room;
            }

            GameState.RegisterRoom(room);
        }

        public bool CanExecute()
        {
            return true;
        }

        public void Rotate()
        {
            throw new NotImplementedException();
        }
    }
}
