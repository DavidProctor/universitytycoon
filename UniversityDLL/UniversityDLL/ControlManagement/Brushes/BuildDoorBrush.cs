﻿using System.Collections.Generic;
using System.Linq;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Brushes
{
    public class BuildDoorBrush : IBrush
    {
        private readonly ISelection _selection;
        private readonly ITileContent _tileContent;
        private readonly IPathProvider _pathProvider;

        public BuildDoorBrush(ISelection selection, ITileContent tileContent, IPathProvider pathProvider)
        {
            _selection = selection;
            _tileContent = tileContent;
            _pathProvider = pathProvider;
        }

        public void Execute()
        {
            var selectedTiles = _selection.GetSelectedTiles();
            if (!CanExecute(selectedTiles))
            {
                return;
            }

            foreach (var selectedTile in selectedTiles)
            {
                selectedTile.AddTileContent(_tileContent.GetClone());
                UpdateNavigationWithDoor(selectedTile);
            }
        }

        public bool CanExecute()
        {
            return CanExecute(_selection.GetSelectedTiles());
        }

        public void Rotate()
        {
            _tileContent.Rotate();
        }

        private bool CanExecute(IList<ITile> tiles)
        {
            return tiles.All(tile => tile.BuildIsValid(_tileContent));
        }

        private void UpdateNavigationWithDoor(ITile selectedTile)
        {
            var rooms = new HashSet<Room>();
            foreach (var tile in selectedTile.Neighbors.Select(t => t as ITile))
            {
                rooms.Add(tile.Room);
            }
            var room1 = rooms.First();
            var room2 = rooms.Last();
            room1.AddExit(selectedTile, room2);
            room2.AddExit(selectedTile, room1);
            room1.AddConnection(room2);
            room2.AddConnection(room1);

            PlotRoutesInConnectedRooms(selectedTile, rooms);
        }

        private void PlotRoutesInConnectedRooms(ITile selectedTile, HashSet<Room> rooms)
        {
            foreach (var room in rooms)
            {
                PlotRoutesInSingleRoom(selectedTile, room);
            }
        }

        private void PlotRoutesInSingleRoom(ITile selectedTile, Room room)
        {
            foreach (var neighbor in room.Neighbors.Select(r => r as Room))
            {
                var exits = room.GetExitsToRoom(neighbor);
                PlotRouteToExits(selectedTile, exits);
            }
        }

        private void PlotRouteToExits(ITile selectedTile, List<ITile> exits)
        {
            foreach (var exit in exits)
            {
                if (exit != selectedTile)
                {
                    _pathProvider.AddPathAsync(selectedTile, exit);
                }
            }
        }
    }
}