﻿namespace UniversityDLL.ControlManagement.Brushes
{
    public interface IBrush
    {
        void Execute();
        bool CanExecute();
        void Rotate();
    }
}
