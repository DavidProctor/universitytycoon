﻿using System.Collections.Generic;
using System.Linq;
using UniversityDLL.ActivityPoints;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Brushes
{
    public class AddActivityPointBrush : IBrush
    {
        private readonly ISelection _selection;
        private readonly ActivityPoint[] _activityPoints;
        private readonly ITileContent _tileContent;
        private readonly IPathProvider _pathProvider;

        public AddActivityPointBrush(ISelection selection, ITileContent tileContent, IPathProvider pathProvider, params ActivityPoint[] activityPoints)
        {
            _selection = selection;
            _activityPoints = activityPoints;
            _tileContent = tileContent;
            _pathProvider = pathProvider;
        }

        public void Execute()
        {
            var selectedTiles = _selection.GetSelectedTiles();
            if (!CanExecute(selectedTiles))
            {
                return;
            }

            foreach (var selectedTile in selectedTiles)
            {
                selectedTile.AddTileContent(_tileContent.GetClone());
                var room = selectedTile.Room;
                foreach (var activityPoint in _activityPoints)
                {
                    var newPoint = activityPoint.GetNewInstance();
                    newPoint.Tile = selectedTile;
                    room.AddActivityPoint(newPoint);
                    selectedTile.SetActivityPoint(newPoint);
                }
                _pathProvider.AddDestinationTile(selectedTile);
            }
        }

        public bool CanExecute()
        {
            var tiles = _selection.GetSelectedTiles();
            return CanExecute(tiles);
        }

        public void Rotate()
        {
            _tileContent.Rotate();
        }

        private bool CanExecute(IList<ITile> tiles)
        {
            return tiles.All(tile => tile.BuildIsValid(_tileContent));
        }
    }
}