﻿using System.Collections.Generic;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Brushes
{
    public class BuildBrush : IBrush
    {
        public ISelection Selection { get; }
        public ITileContent EdgeContent { get; }
        public ITileContent MiddleContent { get; }

        private readonly Room _undefinedRoom = RUndefined.GetInstance();
        private readonly IScore _score;

        public BuildBrush(ISelection selection, ITileContent edge, ITileContent middle, IScore score)
        {
            Selection = selection;
            EdgeContent = edge;
            MiddleContent = middle;

            _score = score;
        }

        public void Execute()
        {
            var total = 0L;
            var tiles = Selection.GetSelectedTiles();
            var edgeTiles = Selection.GetEdgeTiles();

            if (!CanExecute(tiles, edgeTiles))
            {
                return;
            }

            foreach(var tile in tiles)
            {
                if(edgeTiles.Contains(tile))
                {
                    if (EdgeContent.Layer == TileLayer.NullLayer)
                    {
                        continue;
                    }

                    SetUndefinedRoomIfEligible(tile, EdgeContent.Layer);
                    tile.AddTileContent(EdgeContent.GetClone());
                    total += EdgeContent.PurchaseCost;
                    continue;
                }

                if (MiddleContent.Layer == TileLayer.NullLayer)
                {
                    continue;
                }
                SetUndefinedRoomIfEligible(tile, MiddleContent.Layer);
                tile.AddTileContent(MiddleContent.GetClone());
                total += MiddleContent.PurchaseCost;
            }

            _score.SubtractMoney(total);
        }

        public bool CanExecute()
        {
            var tiles = Selection.GetSelectedTiles();
            var edgeTiles = Selection.GetEdgeTiles();

            return CanExecute(tiles, edgeTiles);
        }

        public void Rotate()
        {
            if (EdgeContent != MiddleContent)
            {
                EdgeContent.Rotate();
            }
            MiddleContent.Rotate();
        }

        private bool CanExecute(IList<ITile> tiles, IList<ITile> edgeTiles)
        {
            foreach( var tile in tiles )
            {
                if( edgeTiles.Contains(tile) && !tile.BuildIsValid(EdgeContent) )
                {
                    return false;
                }
                if( !tile.BuildIsValid(MiddleContent) )
                {
                    return false;
                }
            }

            return true;
        }

        private void SetUndefinedRoomIfEligible(ITile tile, TileLayer buildLayer)
        {
            if( tile.Room is ROutdoors &&
                (buildLayer == TileLayer.Basement || buildLayer == TileLayer.Floor) )
            {
                tile.Room = _undefinedRoom;
            }
        }
    }
}
