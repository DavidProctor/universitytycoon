﻿using System.Collections.Generic;
using System.Linq;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.Brushes
{
    public class StampBrush : IBrush
    {
        private struct BuildOperation
        {
            public ITile Tile { get; }
            public ITileContent TileContent { get; }

            public BuildOperation(ITile tile, ITileContent tileContent)
            {
                Tile = tile;
                TileContent = tileContent;
            }
        }

        private readonly IStamp _stamp;
        private readonly ISelection _selection;

        public StampBrush(IStamp stamp, ISelection selection)
        {
            _stamp = stamp;
            _selection = selection;
        }


        public void Execute()
        {
            var tiles = _selection.GetSelectedTiles();
            var contents = _stamp.GetContents();

            var xOffset = tiles.Min(t => t.X);
            var zOffxet = tiles.Min(t => t.Z);
            
            var buildOperations = new List<BuildOperation>();
            foreach (var tile in tiles)
            {
                var content = contents[tile.X - xOffset, tile.Z - zOffxet];
                if (content == null)
                {
                    continue;
                }

                if (!tile.BuildIsValid(content))
                {
                    return;
                }

                buildOperations.Add(new BuildOperation(tile, content));
            }

            foreach (var buildOperation in buildOperations)
            {
                buildOperation.Tile.AddTileContent(buildOperation.TileContent);
            }
        }

        public bool CanExecute()
        {
            return false;
        }

        public void Rotate()
        {
            throw new System.NotImplementedException();
        }
    }
}