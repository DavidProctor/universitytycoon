﻿using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.Previews;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.MouseModes
{
    public class BrushMouseMode : MouseMode
    {
        public IBrush Brush { get; }
        public IPreview Preview { get; }

        public BrushMouseMode(IBrush brush, IPreview preview)
        {
            Brush = brush;
            Preview = preview;
        }
        
        public override void OnLeftMouseDown(IGameEntity target)
        {
            if (target is ITile)
            {
                Preview.StartPreview(target as ITile);
            }
        }

        public override void OnMoveWhileLeftMouseDown(IGameEntity target)
        {
            if( target is ITile )
            {
                Preview.ChangePreview(target as ITile);
            }
        }

        public override void OnLeftMouseUp(IGameEntity target)
        {
            Preview.Destroy();
            Brush.Execute();
        }

        public override void OnMoveWhileLeftMouseUp(IGameEntity target)
        {
            if( target is ITile )
            {
                Preview.StartPreview(target as ITile);
            }
        }

        public override void Destroy()
        {
            Preview.Destroy();
        }

        public override bool CanExecute()
        {
            return Brush.CanExecute();
        }

        public override void Rotate()
        {
            Brush.Rotate();
            Preview.RenderPreview();
        }
    }
}
