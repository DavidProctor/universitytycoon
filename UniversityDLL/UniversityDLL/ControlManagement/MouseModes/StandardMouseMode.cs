﻿using UniversityDLL.UI;

namespace UniversityDLL.ControlManagement.MouseModes
{
    public class StandardMouseMode : MouseMode
    {
        private readonly IDialogFactory _dialogFactory;

        public StandardMouseMode(IDialogFactory dialogFactory)
        {
            _dialogFactory = dialogFactory;
        }

        public override void OnLeftMouseUp(IGameEntity entity)
        {
            _dialogFactory.GetInfoDialog(entity);
        }
    }
}
