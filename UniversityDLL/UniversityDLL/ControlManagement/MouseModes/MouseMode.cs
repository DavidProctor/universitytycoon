﻿namespace UniversityDLL.ControlManagement.MouseModes
{
    public abstract class MouseMode
    {
        public virtual bool CanExecute()
        {
            return true;
        }

        public virtual void OnLeftMouseDown(IGameEntity target) { }

        public virtual void OnMoveWhileLeftMouseDown(IGameEntity target) { }

        public virtual void OnLeftMouseUp(IGameEntity target) { }

        public virtual void OnMoveWhileLeftMouseUp(IGameEntity target) { }

        public virtual void Destroy() { }

        public virtual void Rotate() { }
    }
}
