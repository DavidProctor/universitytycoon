﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement.MouseModes
{
    public class StampMouseMode : MouseMode
    {
        public struct MouseModePlacement
        {
            public int X { get; }
            public int Z { get; }
            public MouseMode MouseMode { get; }

            public MouseModePlacement(int x, int z, MouseMode mouseMode)
            {
                X = x;
                Z = z;
                MouseMode = mouseMode;
            }
        }

        public Orientation Orientation { get { return _orientationTracker.Orientation; } }

        private readonly OrientationTracker _orientationTracker = new OrientationTracker();
        private readonly List<MouseModePlacement> _stampContents = new List<MouseModePlacement>();
        private readonly IGrid _grid;
        private int _width;
        private int _height;

        public StampMouseMode(IGrid grid)
        {
            _grid = grid;
        }

        public void SetMouseModeAtPosition(int x, int z, MouseMode mouseMode)
        {
            if (mouseMode == null)
            {
                return;
            }

            _stampContents.Add(new MouseModePlacement(x, z, mouseMode));
            _width = x + 1 > _width ? x + 1 : _width;
            _height = z + 2 > _height ? z + 1 : _height;
        }

        public MouseMode[,] GetStampContents()
        {
            var contents = new MouseMode[_width, _height];
            foreach (var mouseModePlacement in _stampContents)
            {
                contents[mouseModePlacement.X, mouseModePlacement.Z] = mouseModePlacement.MouseMode;
            }
            return contents;
        }

        public override void OnLeftMouseDown(IGameEntity target)
        {
            ExecuteActionOnOffsetEntities(target, (m, t) => m.OnLeftMouseDown(t));
        }

        public override void OnMoveWhileLeftMouseDown(IGameEntity target)
        {
            ExecuteActionOnOffsetEntities(target, (m,t) => m.OnMoveWhileLeftMouseDown(t));
        }

        public override void OnLeftMouseUp(IGameEntity target)
        {
            var abortSignals = new List<bool>();
            ExecuteActionOnOffsetEntities(target, (m, t) => { if (!m.CanExecute()) abortSignals.Add(true); });
            if (abortSignals.Any())
            {
                return;
            }

            ExecuteActionOnOffsetEntities(target, (m,t) => m.OnLeftMouseUp(t));
        }

        public override void OnMoveWhileLeftMouseUp(IGameEntity target)
        {
            ExecuteActionOnOffsetEntities(target, (m,t) => m.OnMoveWhileLeftMouseUp(t));
        }

        public override void Destroy()
        {
            foreach (var mouseModePlacement in _stampContents)
            {
                mouseModePlacement.MouseMode.Destroy();
            }
        }

        private void ExecuteActionOnOffsetEntities(IGameEntity target, Action<MouseMode, ITile> action)
        {
            if (!(target is ITile))
            {
                return;
            }
            var targetTile = target as ITile;

            var xPivot = _width/2;
            var zPivot = _height/2;

            foreach (var mouseModePlacement in _stampContents)
            {
                if (mouseModePlacement.MouseMode == null)
                {
                    return;
                }

                var xPos = targetTile.X + (mouseModePlacement.X - xPivot);
                var zPos = targetTile.Z + (mouseModePlacement.Z - zPivot);
                var tile = _grid.GetTile(xPos, zPos);
                action(mouseModePlacement.MouseMode, tile);
            }
        }

        public override void Rotate()
        {
            var unrotated = GetStampContents();
            var n = unrotated.GetLength(0);
            var m = unrotated.GetLength(1);
            ResetContents();

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    SetMouseModeAtPosition(j, n - 1 - i, unrotated[i, j]);
                }
            }

            _orientationTracker.NextRotation();
            foreach (var mouseModePlacement in _stampContents)
            {
                mouseModePlacement.MouseMode.Rotate();
            }
        }

        private void ResetContents()
        {
            _stampContents.Clear();
            _width = 0;
            _height = 0;
        }
    }
}