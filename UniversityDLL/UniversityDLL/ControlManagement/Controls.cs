﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.GameMangement;
using UniversityDLL.UI;

namespace UniversityDLL.ControlManagement
{
    public class Controls
    {
        public MouseMode MouseMode { get; private set; }
        public MouseMode AltMouseMode { get; private set; }

        private readonly IGameState _gameState = GameState.GetInstance();
        private readonly float _raycastLength = 1000f;
        private int _clickTargetLayer = -1;

        public Controls(IDialogFactory dialogFactory)
        {
            MouseMode = new StandardMouseMode(dialogFactory);
            AltMouseMode = MouseMode;
        }

        public void SetMouseMode(MouseMode mouseMode)
        {
            MouseMode.Destroy();
            AltMouseMode.Destroy();
            MouseMode = mouseMode;
        }

        public void SetAltMouseMode(MouseMode mouseMode)
        {
            MouseMode.Destroy();
            AltMouseMode.Destroy();
            AltMouseMode = mouseMode;
        }

        public Vector3 GetMousePosition()
        {
            return Input.mousePosition;
        }

        public IGameEntity GetGameEntityAtMouse()
        {
            if( EventSystem.current.IsPointerOverGameObject() )
            {
                return null;
            }

            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if( Physics.Raycast(ray, out hit, _raycastLength, GetClickTargetLayer()) )
            {
                var clickedTransform = hit.transform.parent ?? hit.transform;

                var entity = clickedTransform.GetComponent<IClickable>();
                if( entity?.Entity != null )
                {
                    return entity.Entity;
                }

                return GetTileAtHit(hit);
            }
            return null;
        }

        private IGameEntity GetTileAtHit(RaycastHit hit)
        {
            var x = (int)Math.Round(hit.point.x);
            var z = (int)Math.Round(hit.point.z);

            return _gameState.Grid.GetTile(x, z);
        }

        private int GetClickTargetLayer()
        {
            return _clickTargetLayer == -1 ? 1 << LayerMask.NameToLayer("ClickTarget") : _clickTargetLayer;
        }
    }

    public interface IClickable
    {
        IGameEntity Entity { get; }
    }
}
