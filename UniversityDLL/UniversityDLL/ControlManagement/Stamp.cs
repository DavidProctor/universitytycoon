﻿using UniversityDLL.Tiles;

namespace UniversityDLL.ControlManagement
{
    public interface IStamp
    {
        int Width { get; }
        int Height { get; }

        void SetTileContentAtPoint(int x, int z, ITileContent tileContent);
        ITileContent[,] GetContents();
    }
    
    public class Stamp : IStamp
    {
        public int Width { get; }
        public int Height { get; }

        private readonly ITileContent[,] _contents;

        public Stamp(int width, int height)
        {
            Width = width;
            Height = height;

            _contents = new ITileContent[Width, Height];
        }

        public void SetTileContentAtPoint(int x, int z, ITileContent tileContent)
        {
            _contents[x, z] = tileContent;
        }

        public ITileContent[,] GetContents()
        {
            return _contents;
        }
    }
}