﻿namespace UniversityDLL
{
    public interface IGameEntity
    {
         string Name { get; }
    }
}