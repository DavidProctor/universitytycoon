﻿using System;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;

namespace UniversityDLL.Scheduling
{
    [Serializable]
    public class AdmissionsCalendarEvent : ICalendarEvent
    {
        public TimeUnit Date { get; set; }

        private IGameState _gameState;
        private IObjectFactoryFacade _objectFactory;

        public AdmissionsCalendarEvent(TimeUnit date, IGameState gameState, IObjectFactoryFacade objectFactory)
        {
            _gameState = gameState;
            _objectFactory = objectFactory;
            Date = date;
        }

        public void Execute()
        {
            var semester = Date.Semester;
            var quota = 0;
            foreach (var department in _gameState.Departments)
            {
                foreach (var course in department.GetScheduledCourses(semester))
                {
                    quota += course.GetCapacity();
                }
            }

            for (int s = 0; s < quota; ++s)
            {
                _gameState.EnrolStudent(_objectFactory.MakeStudent());
            }
        }
            

        public void Execute1()
        {
            var semester = Date.Semester;
            var quota = 0;
            foreach(var department in _gameState.Departments)
            {
                var departmentQuota = 0;
                var firstYearCourses = department.GetScheduledCourses(semester).Where(c => c.Template.Year == Year.First && c.IsReady());
                foreach(var course in firstYearCourses)
                {
                    departmentQuota += course.GetCapacity();
                }
                var modifiedQuota = Convert.ToInt32(departmentQuota * (department.AdmissionsCapacity / 100f));
                quota += modifiedQuota;
            }

            for(int s = 0; s < quota; ++s )
            {
                _gameState.EnrolStudent(_objectFactory.MakeStudent());
            }
        }

        public void Destroy()
        {
            _gameState = null;
            _objectFactory = null;
        }

        public TimeUnit GetRescheduleDate()
        {
            return Date.AddSemesters(1);
        }
    }
}
