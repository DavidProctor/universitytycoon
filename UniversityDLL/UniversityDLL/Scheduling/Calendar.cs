﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.Academics;

namespace UniversityDLL.Scheduling
{
    public interface ICalendar
    {
        TimeUnit CurrentTime { get; set; }
        IList<ICalendarEvent> FutureEvents { get; }
        float SecondsPerTick { get; set; }

        void Tick();
        void AddEvent(ICalendarEvent newEvent);
    }

    [Serializable]
    public class Calendar : ICalendar
    {
        public TimeUnit CurrentTime { get; set; }
        public IList<ICalendarEvent> FutureEvents { get; }
        public float SecondsPerTick { get; set; }

        public static readonly DayOfWeek[] Weekdays = 
        {
            DayOfWeek.Monday,
            DayOfWeek.Tuesday,
            DayOfWeek.Wednesday,
            DayOfWeek.Thursday,
            DayOfWeek.Friday
        };

        public Calendar()
        {
            CurrentTime = new TimeUnit(Semester.GetSemester(2006, Season.Fall), 1, DayOfWeek.Monday, 12, QuarterHour.Zero);
            FutureEvents = new List<ICalendarEvent>();
            SecondsPerTick = -1f;
        }

        public void Tick()
        {
            CurrentTime = CurrentTime.Tick();

            var eventsToday = FutureEvents.Where(e => e.Date.CompareTo(CurrentTime) <= 0);
            ExecuteEvents(eventsToday);
        }

        public void AddEvent(ICalendarEvent newEvent)
        {
            FutureEvents.Add(newEvent);
        }

        private void ExecuteEvents(IEnumerable<ICalendarEvent> events)
        {
            foreach( var e in events )
            {
                e.Execute();
                e.Date = e.GetRescheduleDate();
            }
        }
    }
}
