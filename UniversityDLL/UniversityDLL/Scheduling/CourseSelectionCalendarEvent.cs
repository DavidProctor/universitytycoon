﻿using System;
using System.Linq;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.People;

namespace UniversityDLL.Scheduling
{
    [Serializable]
    public class CourseSelectionCalendarEvent : ICalendarEvent
    {
        public TimeUnit Date { get; set; }

        private readonly IGameState _gameState;

        public CourseSelectionCalendarEvent(TimeUnit date, IGameState gameState)
        {
            _gameState = gameState;
            Date = date;
        }

        public void Execute()
        {
            var semester = _gameState.Calendar.CurrentTime.Semester;
            foreach(var student in _gameState.Students.OrderByDescending(s => s.Year).ThenByDescending(s => s.GPA))
            {
                int credits = 0;
                while( credits < 9 )
                {
                    ICourseInstance course = null;
                    foreach(Year year in Enum.GetValues(typeof(Year)))
                    {
                        if( student.Enrolments.Where(e => e.Course.Template.Year == year).Sum(e => e.Course.Template.RequiredHours)
                                >= GameState.CreditsPerYear )
                        {
                            continue;
                        }

                        course = SelectCourseFromDepartment(student.Major, semester, student, year);
                        if( course != null ) break;
                        course = SelectCourseFromDepartment(_gameState.Departments.FirstOrDefault(d => d != student.Major && d.GetCourses().Count > 0),
                            semester, student, year);
                        if( course != null ) break;
                    }
                    if( course == null ) break;                    

                    var enrolment = new CourseEnrolment(student, course);
                    credits += course.Template.RequiredHours;
                }
            }
        }

        private ICourseInstance SelectCourseFromDepartment(IDepartment department, Semester semester, IStudent student, Year year)
        {
            if( department == null ) return null;
            if( department == student.Major 
                && student.Enrolments.Where(e => e.Course.Template.Year == year && e.Course.Template.Department == student.Major)
                    .Sum(e => e.Course.Template.RequiredHours)
                                >= student.Major.RequiredCreditsPerYear )
            {
                return null;
            }

            var courses = department.GetScheduledCourses(semester).Where(
                c => c.Template.Year == year
                && c.IsReady()
                && c.Enrolments.Count < c.GetCapacity()
                && student.Enrolments.FirstOrDefault(e => e.Course.Template == c.Template) == null
                );

            return courses.OrderBy(c => c.Template.Year).FirstOrDefault();
        }

        public TimeUnit GetRescheduleDate()
        {
            return Date.AddSemesters(1);
        }
    }
}
