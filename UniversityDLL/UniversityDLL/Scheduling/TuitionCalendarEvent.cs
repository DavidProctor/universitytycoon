﻿using System;
using System.Linq;
using UniversityDLL.GameMangement;

namespace UniversityDLL.Scheduling
{
    [Serializable]
    public class TuitionCalendarEvent : ICalendarEvent
    {
        public TimeUnit Date { get; set; }
        public IScore Score { get; }

        private readonly IGameState _gameState;

        public TuitionCalendarEvent(TimeUnit date, IGameState gameState)
        {
            Date = date;
            Score = gameState.Score;
            _gameState = gameState;
        }

        public void Execute()
        {
            Score.AddMoney(_gameState.Enrolments.Count() * 450);
        }

        public TimeUnit GetRescheduleDate()
        {
            return Date.AddSemesters(1);
        }
    }
}
