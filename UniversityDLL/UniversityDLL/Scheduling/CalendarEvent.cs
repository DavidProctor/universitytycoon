﻿namespace UniversityDLL.Scheduling
{
    public interface ICalendarEvent
    {
        TimeUnit Date { get; set; }
        void Execute();
        TimeUnit GetRescheduleDate();
    }
}
