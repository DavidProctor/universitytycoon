﻿using System;
using UniversityDLL.Academics;

namespace UniversityDLL.Scheduling
{
    [Serializable]
    public enum QuarterHour
    {
        Zero,
        Fifteen,
        Thirty,
        Fortyfive
    }

    [Serializable]
    public class TimeUnit : IComparable
    {
        public const int WeeksPerSemester = 6;
        public const int HoursPerDay = 24;

        public const int TicksPerHour = 4;
        public const int TicksPerDay = TicksPerHour * HoursPerDay;
        public const int TicksPerWeek = TicksPerDay * 7;
        public const int TicksPerSeason = TicksPerWeek * WeeksPerSemester;
        public const int TicksPerYear = TicksPerSeason * 3;

        public Semester Semester
        {
            get
            {
                var year = (int)_ticks / TicksPerYear;
                var yearTicks = _ticks % TicksPerYear;
                var season = (Season)(yearTicks / TicksPerSeason);
                return Semester.GetSemester(year, season);
            }
        } 

        public int Week
        {
            get
            {
                var ticks = _ticks % TicksPerSeason;
                return (int)ticks / TicksPerWeek;
            }
        }

        public DayOfWeek Day
        {
            get
            {
                var ticks = _ticks % TicksPerWeek;
                return (DayOfWeek)(ticks / TicksPerDay);
            }
        }

        public int Hour
        {
            get
            {
                var ticks = _ticks % TicksPerDay;
                var hours = ticks / TicksPerHour;
                return (int) hours;
            }
        }

        public long Ticks { get { return _ticks; } }

        public DayAndTime DayAndTime
        {
            get
            {
                return DayAndTime.FromDetails(Day, Hour);
            }
        }

        public QuarterHour QuarterHour { get { return (QuarterHour) (_ticks % 4); } }
        
        private readonly long _ticks;

        public TimeUnit(long ticks)
        {
            _ticks = ticks;
        }

        public TimeUnit(Semester semester, int week, DayOfWeek day)
        {
            _ticks = (TicksPerYear * semester.Year)
                + (TicksPerSeason * (int)semester.Season)
                + (TicksPerWeek * week)
                + (TicksPerDay * (int)day);
        }

        public TimeUnit(int year, Season season, int week, DayOfWeek day)
        {
            _ticks = (TicksPerYear * year)
                + (TicksPerSeason * (int)season)
                + (TicksPerWeek * week)
                + (TicksPerDay * (int)day);
        }

        public TimeUnit(Semester semester, int week, DayOfWeek day, int hour, QuarterHour quarterHour)
        {
            _ticks = (TicksPerYear * semester.Year)
                + (TicksPerSeason * (int)semester.Season)
                + (TicksPerWeek * week)
                + (TicksPerDay * (int)day)
                + (TicksPerHour * hour)
                + ((int)quarterHour);
        }

        public TimeUnit Tick()
        {
            return new TimeUnit(_ticks + 1);
        }

        public TimeUnit AddTicks(int ticks)
        {
            return new TimeUnit(_ticks + ticks);
        }

        public TimeUnit AddHours(int hours)
        {
            return new TimeUnit(_ticks + TicksPerHour * hours);
        }

        public TimeUnit AddDays(int days)
        {
            return new TimeUnit(_ticks + TicksPerDay * days);
        }

        public TimeUnit AddSemesters(int semesters)
        {
            return new TimeUnit(_ticks + TicksPerSeason * semesters);
        }

        public bool DateEquals(TimeUnit other)
        {
            return Math.Abs(_ticks - other._ticks) < TicksPerDay;
        }

        public string ToDateString()
        {
            return $"{Day}, week {Week}, {Semester}";
        }

        public string ToTimeString()
        {
            var formattedHour = Hour % 12;
            if( formattedHour == 0 )
            {
                formattedHour = 12; 
            }
            var ampm = Hour < 12 ? "AM" : "PM";
            var formattedQuarter = QuarterHour == QuarterHour.Zero ? "00"
                : QuarterHour == QuarterHour.Fifteen ? "15"
                : QuarterHour == QuarterHour.Thirty ? "30"
                : "45";

            return $"{formattedHour}:{formattedQuarter} {ampm}";
        }

        public override string ToString()
        {
            return $"{ToDateString()} {ToTimeString()}";
        }

        public override bool Equals(object obj)
        {
            if( obj == this ) return true;
            if( obj.GetType() != typeof(TimeUnit) ) return false;
            var comparer = obj as TimeUnit;
            return comparer?._ticks == _ticks;
        }

        public override int GetHashCode()
        {
            return _ticks.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            TimeUnit other = obj as TimeUnit;
            if (other == null)
            {
                throw new ArgumentException($"Cannot compare TimeUnits to type {obj.GetType()}");
            }
            return Compare(this, other);
        }

        public static int Compare(TimeUnit left, TimeUnit right)
        {
            if (left == null && right == null) return 0;
            if (left == null) return -1;
            if (right == null) return 1;

            return left._ticks.CompareTo(right._ticks);
        }

        public static bool operator >(TimeUnit left, TimeUnit right)
        {
            return Compare(left, right) > 0;
        }

        public static bool operator <(TimeUnit left, TimeUnit right)
        {
            return Compare(left, right) < 0;
        }

        public static bool operator >=(TimeUnit left, TimeUnit right)
        {
            return Compare(left, right) >= 0;
        }

        public static bool operator <=(TimeUnit left, TimeUnit right)
        {
            return Compare(left, right) <= 0;
        }
    }
}
