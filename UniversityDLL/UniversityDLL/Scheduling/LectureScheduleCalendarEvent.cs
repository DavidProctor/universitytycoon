﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.Rooms;

namespace UniversityDLL.Scheduling
{
    [Serializable]
    public class LectureScheduleCalendarEvent : ICalendarEvent
    {
        private struct Session
        {
            public DayAndTime DayAndTime;
            public BookableRoom Room;
        }

        public TimeUnit Date { get; set; }

        private readonly IGameState _gameState;
        private readonly ICharacterFactory _characterFactory;

        public LectureScheduleCalendarEvent(TimeUnit time, IGameState gameState, ICharacterFactory characterFactory)
        {
            Date = time;
            _gameState = gameState;
            _characterFactory = characterFactory;
        }

        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public void Execute()
        {
            var departments = _gameState.Departments;
            while (departments.Any())
            {
                var removedDepartments = _gameState.Departments.Where(department => !TryScheduleSingleCourse(department));
                departments = departments.Except(removedDepartments).ToList();
            }
        }

        private bool TryScheduleSingleCourse(IDepartment department)
        {
            Semester semester = Date.Semester;
            var courseTemplate = SelectCourse(semester, department);

            if (courseTemplate == null)
            {
                return false;
            }
            var courseInstance = new CourseInstance(semester, courseTemplate);

            var scheduledSessions = new Stack<Session>();
            foreach (var room in _gameState.GetLectureHalls())
            {
                DayAndTime dayAndTime;
                while (TryFindOpenSession(room, scheduledSessions.Select(s => s.DayAndTime), out dayAndTime))
                {
                    scheduledSessions.Push(new Session { DayAndTime = dayAndTime, Room = room });
                    if (scheduledSessions.Count == courseTemplate.RequiredHours)
                    {
                        CommitCourseSchedule(department, scheduledSessions, courseInstance, semester);
                        return true;
                    }
                }
            }

            return false;
        }

        private ICourseTemplate SelectCourse(Semester semester, IDepartment department)
        {
            var scheduledCourses = department.GetScheduledCourses(semester);
            IEnumerable<ICourseTemplate> courses = department.GetCourses();
            if (scheduledCourses.Any())
            {
                courses = courses.Except(scheduledCourses.Select(c => c.Template));
            }
            var courseTemplate = courses.FirstOrDefault();
            return courseTemplate;
        }

        private void CommitCourseSchedule(IDepartment department, 
            Stack<Session> scheduledSessions, 
            CourseInstance courseInstance,
            Semester semester)
        {
            foreach (var scheduledSession in scheduledSessions)
            {
                scheduledSession.Room.AddBooking(scheduledSession.DayAndTime, courseInstance, semester);
                department.AddScheduledCourse(courseInstance);
            }

            var prof = _characterFactory.MakeProfessor();
            courseInstance.Professor = prof;
            prof.SetCourse(courseInstance);
        }

        private bool TryFindOpenSession(BookableRoom room, IEnumerable<DayAndTime> invalidSessions, out DayAndTime session)
        {
            var semester = Date.Semester;
            var schedule = room.GetSchedule(semester);
            foreach (DayOfWeek day in Enum.GetValues(typeof (DayOfWeek)))
            {
                if (day == DayOfWeek.Sunday || day == DayOfWeek.Saturday)
                {
                    continue;
                }

                for (int hour = 9; hour < 17; ++hour)
                {
                    session = DayAndTime.FromDetails(day, hour);
                    if (invalidSessions.Contains(session))
                    {
                        continue;
                    }
                    if (schedule.GetBooking(session) == null)
                    {
                        return true;
                    }
                }
            }
            session = DayAndTime.FromDetails(DayOfWeek.Sunday, 0);
            return false;
        }

        public TimeUnit GetRescheduleDate()
        {
            return Date.AddSemesters(1);
        }
    }
}
