﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UniversityDLL
{
    public static class Extensions
    {
        private static readonly Random Randy = new Random();

        public static T GetRandom<T>(this IList<T> source)
        {
            return source[Randy.Next(0, source.Count)];
        }

        public static T GetRandom<T>(this IEnumerable<T> source)
        {
            return source.DefaultIfEmpty().ElementAt(Randy.Next(0, source.Count()));
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
            {
                action(item);
            }
        }
    }
}
