﻿using System;

namespace UniversityDLL
{
    [Serializable]
    public enum Orientation
    {
        Forward,
        Right,
        Reverse,
        Left
    }

    [Serializable]
    public class OrientationTracker
    {
        public Orientation Orientation { get; private set; }
        public Orientation BaseOrientation { get; }

        public OrientationTracker(Orientation baseOrientation = Orientation.Forward)
        {
            BaseOrientation = baseOrientation;
        }

        public Orientation NextRotation()
        {
            switch (Orientation)
            {
                case Orientation.Forward:
                    Orientation = Orientation.Right;
                    return Orientation;
                case Orientation.Right:
                    Orientation = Orientation.Reverse;
                    return Orientation;
                case Orientation.Reverse:
                    Orientation = Orientation.Left;
                    return Orientation;
                case Orientation.Left:
                    Orientation = Orientation.Forward;
                    return Orientation;
                default:
                    throw new NotImplementedException($"Unknown orientation: {Orientation}");
            }
        }

        public void SetOrientation(Orientation newOrientation)
        {
            Orientation = newOrientation;
        }

        public void SetOrientationFromParent(Orientation parentOrientation)
        {
            Orientation = (Orientation) ( ((int)parentOrientation + (int)BaseOrientation) % 4 );
        }
    }
}