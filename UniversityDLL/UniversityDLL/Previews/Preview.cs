﻿using System.Collections.Generic;
using System.Linq;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLL.Previews
{
    public interface IPreview
    {
        ISelection Selection { get; }
        IGameObject EdgeObject { get; }
        IGameObject MiddleObject { get; }

        void StartPreview(ITile tile);
        void ChangePreview(ITile tile);
        void RenderPreview();

        void Destroy();
    }

    public class Preview : IPreview
    {
        public ISelection Selection { get; }
        public IGameObject EdgeObject { get; }
        public IGameObject MiddleObject { get; }
        
        private readonly Dictionary<ITile, IGameObject> _renderedTiles;
        private readonly List<ITile> _hiddenContent;
        private readonly TileLayer _layer;

        public Preview(ISelection selection, IGameObject edge, IGameObject middle, TileLayer layer)
        {
            Selection = selection;
            EdgeObject = edge;
            MiddleObject = middle;
            _renderedTiles = new Dictionary<ITile, IGameObject>();
            _hiddenContent = new List<ITile>();
            _layer = layer;
        }

        public void StartPreview(ITile tile)
        {
            Selection.StartSelection(tile);
            ClearDeselectedRenders();
            RenderPreview();
        }

        public void ChangePreview(ITile tile)
        {
            Selection.ChangeSelection(tile);
            ClearDeselectedRenders();
            RenderPreview();
        }

        public void RenderPreview()
        {
            var edgeTiles = Selection.GetEdgeTiles();
            foreach(var tile in Selection.GetSelectedTiles())
            {
                var tileToRender = edgeTiles.Contains(tile) ? EdgeObject : MiddleObject;
                RenderSingleObject(tile, tileToRender);
            }
        }

        public void Destroy()
        {
            foreach( var tile in _renderedTiles )
            {
                tile.Value.Destroy();
                tile.Key.TileContentCollection.GetTileContent(TileLayer.Ground).GameObject.Show();
            }

            foreach( var tile in _hiddenContent )
            {
                tile.TileContentCollection.Render();
            }
            _hiddenContent.Clear();
        }

        private void RenderSingleObject(ITile tile, IGameObject content)
        {
            tile.TileContentCollection.HideLayersObscuredByLayer(_layer);
            _hiddenContent.Add(tile);

            if(_renderedTiles.ContainsKey(tile))
            {
                _renderedTiles[tile].Destroy();
                _renderedTiles.Remove(tile);
            }

            var rendered = content.GetClone();
            rendered.Render(tile.X, 0f, tile.Z);
            _renderedTiles.Add(tile, rendered);
        }

        private void ClearDeselectedRenders()
        {
            var selectedTiles = Selection.GetSelectedTiles();
            var deselectedTiles = _renderedTiles.Keys.Except(selectedTiles).ToArray();

            foreach( var tile in deselectedTiles )
            {
                _renderedTiles[tile].Destroy();
                _renderedTiles.Remove(tile);
                if (_hiddenContent.Contains(tile))
                {
                    tile.TileContentCollection.Render();
                    _hiddenContent.Remove(tile);
                }
            }
        }
    }
}
