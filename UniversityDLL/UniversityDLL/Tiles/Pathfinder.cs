﻿using System;
using System.Collections.Generic;

namespace UniversityDLL.Tiles
{
    public interface IPathfinder<T> where T : IWaypoint
    {
        List<T> TryFindPath(T start, T end);
    }

    [Serializable]
    public class NoPathExistsException : Exception
    {
        public NoPathExistsException() { }

        public NoPathExistsException(string message)
            : base(message)
        { }
    }

    public class Pathfinder<T> : IPathfinder<T> where T : class, IWaypoint
    {
        private static Pathfinder<T> _instance;

        private Pathfinder() { }

        public static Pathfinder<T> GetInstance()
        {
            return _instance ?? (_instance = new Pathfinder<T>());
        }

        public List<T> TryFindPath(T start, T end)
        {
            var path = new List<T>();
            if(!start.IsWalkable() || !end.IsWalkable())
            {
                throw new NoPathExistsException("Could not plot a path between these points - one of the points is not walkable");
            }

            var invalidMoves = new List<T>();
            var source = new Dictionary<T, T>();
            var queue = new Queue<T>();

            invalidMoves.Add(start);
            source.Add(start, null);
            queue.Enqueue(start);

            while(queue.Count > 0)
            {
                var tile = queue.Dequeue();
                if( tile == end )
                {
                    path = FinalizePath(path, source, tile);
                    return path;
                }
                foreach( T neighbor in tile.Neighbors )
                {
                    if( !invalidMoves.Contains(neighbor) && neighbor.IsWalkable() )
                    {
                        invalidMoves.Add(neighbor);
                        source.Add(neighbor, tile);
                        queue.Enqueue(neighbor);
                    }
                }
            }

            throw new NoPathExistsException($"Could not find a valid path between points [{start}] and [{end}]");
        }

        private static List<T> FinalizePath(List<T> path, Dictionary<T, T> source, T tile)
        {
            while( tile != null )
            {
                path.Add(tile);
                tile = source[tile];
            }
            path.Reverse();
            return path;
        }
    }
}
