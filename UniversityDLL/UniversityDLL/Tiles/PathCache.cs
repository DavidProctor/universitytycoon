﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace UniversityDLL.Tiles
{
    public interface IPathCache<T> where T : IWaypoint
    {
        List<T> GetPath(T start, T end);
        void CachePathAsync(T start, T end);
    }

    public class PathCache<T> : IDisposable, IPathCache<T> where T : IWaypoint
    {
        private struct PathEndpoints
        {
            public T Start;
            public T End;
        }

        private readonly IPathfinder<T> _pathfinder;
        private readonly Dictionary<PathEndpoints, List<T>> _cachedPaths = new Dictionary<PathEndpoints, List<T>>();
        private readonly BackgroundWorker _worker = new BackgroundWorker();
        private readonly Queue<PathEndpoints> _workQueue = new Queue<PathEndpoints>();

        public PathCache(IPathfinder<T> pathfinder)
        {
            _pathfinder = pathfinder;
            _worker.DoWork += ConsumeWorkQueue;
            _worker.RunWorkerAsync();
        }

        public List<T> GetPath(T start, T end)
        {
            var endpoints = new PathEndpoints {Start = start, End = end};
            if (!_cachedPaths.ContainsKey(endpoints))
            {
                CachePath(endpoints);
            }
            return _cachedPaths[endpoints];
        }

        public void CachePathAsync(T start, T end)
        {
            var endpoints = new PathEndpoints { Start = start, End = end };
            if( !_cachedPaths.ContainsKey(endpoints) )
            {
                _workQueue.Enqueue(endpoints);
                if (!_worker.IsBusy)
                {
                    _worker.RunWorkerAsync();
                }
            }
        }

        private void CachePath(PathEndpoints endpoints)
        {
            var path = _pathfinder.TryFindPath(endpoints.Start, endpoints.End);
            _cachedPaths.Add(endpoints, path);

            var reverseEndpoints = new PathEndpoints { Start = endpoints.End, End = endpoints.Start };
            if (reverseEndpoints.Equals(endpoints))
            {
                return;
            }
            var reversePath = _cachedPaths[endpoints].ToList();
            reversePath.Reverse();
            _cachedPaths.Add(reverseEndpoints, reversePath);
        }

        private void ConsumeWorkQueue(object sender, DoWorkEventArgs e)
        {
            while ( _workQueue.Any() )
            {
                var work = _workQueue.Dequeue();
                CachePath(work);
            }
        }

        public void Dispose()
        {
            _worker.Dispose();
        }
    }
}