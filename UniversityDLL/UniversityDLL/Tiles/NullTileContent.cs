﻿using UnityEngine;
using UniversityDLL.GameMangement;

namespace UniversityDLL.Tiles
{
    public class NullTileContent : ITileContent
    {
        public IGameObject GameObject { get { return new NullGameObject(); } }
        public long PurchaseCost { get { return default(long); } }
        public Orientation Orientation { get { return default(Orientation); } }
        public TileLayer Layer { get { return default(TileLayer); } }

        public IGameObject Render(float x, float y, float z)
        {
            return new NullGameObject();
        }

        public IGameObject Render(float x, float y, float z, Color color)
        {
            return new NullGameObject();
        }

        public ITileContent GetClone()
        {
            return new NullTileContent();
        }

        public void Show()
        {
        }

        public void Hide()
        {
        }

        public void Destroy()
        {
        }

        public void SetOrientation(Orientation orientation)
        {
        }

        public void Rotate()
        {
        }
    }
}