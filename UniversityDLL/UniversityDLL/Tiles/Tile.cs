﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniversityDLL.ActivityPoints;
using UniversityDLL.Rooms;

namespace UniversityDLL.Tiles
{
    public interface IWaypoint
    {
        bool IsWalkable();
        void AddConnection(IWaypoint neighbor);
        void RemoveConnection(IWaypoint neighbor);
        IList<IWaypoint> Neighbors { get; }
        IEnumerable<IPathConnection> Connections { get; }
    }

    public interface ITile : IWaypoint, IGameEntity
    {
        Room Room { get; set; }
        ITileContentCollection TileContentCollection { get; }
        void Destroy();

        int X { get; }
        int Z { get; }
        bool BuildIsValid(ITileContent tileContent);
        bool HasLayer(TileLayer layer);
        ActivityPoint ActivityPoint { get; }

        void SetPosition(int x, int z);

        void AddTileContent(ITileContent tileContent);
        void SetActivityPoint(ActivityPoint activityPoint);
    }

    [Serializable]
    public class Tile : ITile
    {
        public IEnumerable<IPathConnection> Connections { get { return _pathConnections; } }

        public IList<IWaypoint> Neighbors
        {
            get { return _pathConnections.SelectMany(c => c.Nodes)
                                         .Where(t => !t.Equals(this))
                                         .ToList();
            }
        }

        public ITileContentCollection TileContentCollection { get; private set; }
        public ActivityPoint ActivityPoint { get; private set; }

        public int X { get; private set; }
        public int Z { get; private set; }
        public Room Room { get; set; }
        public string Name { get { return ToString(); } }

        private readonly List<IPathConnection> _pathConnections = new List<IPathConnection>();

        public Tile(ITileContentCollection tileContentCollection, Room room)
        {
            TileContentCollection = tileContentCollection;
            TileContentCollection.Tile = this;
            Room = room;
        }

        public void SetPosition(int x, int z)
        {
            if(x < 0 || z < 0)
            {
                throw new ArgumentOutOfRangeException($"Invalid tile position: ({x}, {z})");
            }

            X = x;
            Z = z;
        }

        public bool IsWalkable()
        {
            return TileContentCollection.IsWalkable();
        }

        public bool BuildIsValid(ITileContent tileContent)
        {
            return TileContentCollection.BuildIsValid(tileContent);
        }

        public bool HasLayer(TileLayer layer)
        {
            return TileContentCollection.HasLayer(layer);
        }

        public void AddConnection(IWaypoint neighbor)
        {
            var connection = PathConnection.GetConnection(this, neighbor);
            _pathConnections.Add(connection);
        }

        public void RemoveConnection(IWaypoint neighbor)
        {
            var connection = PathConnection.GetConnection(this, neighbor);
            _pathConnections.Remove(connection);
        }

        public void AddTileContent(ITileContent tileContent)
        {
            TileContentCollection.AddTileContent(tileContent);
        }

        public void SetActivityPoint(ActivityPoint activityPoint)
        {
            ActivityPoint = activityPoint;
        }

        public void Destroy()
        {
            TileContentCollection.Destroy();
            TileContentCollection = null;
            _pathConnections.Clear();
        }

        public override string ToString()
        {
            return $"Tile at ({X}, {Z})";
        }
    }
}
