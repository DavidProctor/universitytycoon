﻿using System;
using System.Collections.Generic;

namespace UniversityDLL.Tiles
{
    public interface ILayerDisplayRules
    {
        bool ShouldHide(TileLayer existing, TileLayer additional);
        HashSet<TileLayer> GetLayersObscuredByPreview(TileLayer layer);
    }

    public class LayerDisplayRules : ILayerDisplayRules
    {
        private static readonly LayerDisplayRules Instance = new LayerDisplayRules();

        public static ILayerDisplayRules GetInstance()
        {
            return Instance;
        }

        private LayerDisplayRules() { }

        public bool ShouldHide(TileLayer existing, TileLayer additional)
        {
            var addingGroundLayer = additional == TileLayer.Ground
                    || additional == TileLayer.Basement
                    || additional == TileLayer.Floor;

            if (existing == TileLayer.Ground
                || existing == TileLayer.Basement
                || existing == TileLayer.Floor)
            {
                return addingGroundLayer;
            }

            return !addingGroundLayer;
        }

        public HashSet<TileLayer> GetLayersObscuredByPreview(TileLayer layer)
        {
            switch (layer)
            {
                case TileLayer.Door:
                case TileLayer.Wall:
                case TileLayer.ActivityPoint:
                case TileLayer.Structure:
                    return new HashSet<TileLayer>
                    {
                        TileLayer.Door,
                        TileLayer.Wall,
                        TileLayer.ActivityPoint,
                        TileLayer.Structure
                    };
                case TileLayer.Floor:
                case TileLayer.Basement:
                case TileLayer.Ground:
                    return new HashSet<TileLayer>
                    {
                        TileLayer.Ground,
                        TileLayer.Basement,
                        TileLayer.Floor
                    };
                case TileLayer.NullLayer:
                    return new HashSet<TileLayer>();
                default:
                    throw new ArgumentOutOfRangeException($"No preview rendering rules for layer {layer}");
            }
        }
    }
}