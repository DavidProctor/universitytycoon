﻿using System;
using UniversityDLL.GameMangement;

namespace UniversityDLL.Tiles
{
    public class TileContentBuilder
    {
        private IGameObject _prototype;
        private long _cost;
        private TileLayer _layer;
        private Orientation _orientation;

        public ITileContent Build()
        {
            if(_prototype == null) throw new InvalidOperationException("Cannot build a TileContent without setting a GameObject");

            return new TileContent(_prototype, _cost, _layer, _orientation);
        }

        public TileContentBuilder SetPrototype(IGameObject gameObject)
        {
            _prototype = gameObject;
            return this;
        }

        public TileContentBuilder SetCost(long cost)
        {
            if (cost < 0) throw new ArgumentOutOfRangeException();

            _cost = cost;
            return this;
        }

        public TileContentBuilder SetLayer(TileLayer layer)
        {
            _layer = layer;
            return this;
        }

        public TileContentBuilder SetRotation(Orientation rotation)
        {
            _orientation = rotation;
            return this;
        }
    }
}
