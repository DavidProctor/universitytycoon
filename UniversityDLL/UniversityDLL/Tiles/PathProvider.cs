﻿using System.Collections.Generic;
using UniversityDLL.Rooms;

namespace UniversityDLL.Tiles
{
    public interface IPathProvider
    {
        List<ITile> GetPath(ITile start, ITile end);
        void AddPathAsync(ITile start, ITile end);
        void AddDestinationTile(ITile waypoint);
    }

    public class PathProvider : IPathProvider
    {
        private static PathProvider _instance;

        public static PathProvider GetInstance()
        {
            if (_instance == null)
            {
                var tilePathfinder = Pathfinder<ITile>.GetInstance();
                var roomPathfinder = Pathfinder<Room>.GetInstance();
                var tilePathCache = new PathCache<ITile>(tilePathfinder);
                var roomPathCache = new PathCache<Room>(roomPathfinder);
                _instance = new PathProvider(tilePathCache, roomPathCache);
            }
            return _instance;
        }

        private readonly IPathCache<ITile> _tilePathCache;
        private readonly IPathCache<Room> _roomPathCache;

        protected PathProvider(IPathCache<ITile> tilePathCache, IPathCache<Room> roomPathCache)
        {
            _tilePathCache = tilePathCache;
            _roomPathCache = roomPathCache;
        }

        public List<ITile> GetPath(ITile start, ITile end)
        {
            if (start.Room == end.Room)
            {
                return _tilePathCache.GetPath(start, end);
            }

            List<ITile> path = new List<ITile>();
            var reverseRoomPath = _roomPathCache.GetPath(start.Room, end.Room);
            reverseRoomPath.Reverse();
            var roomPath = new Stack<Room>(reverseRoomPath);
            var entry = start;
            while ( roomPath.Count != 0)
            {
                var room = roomPath.Pop();
                if ( roomPath.Count != 0)
                {
                    var nextRoom = roomPath.Peek();
                    var exits = room.GetExitsToRoom(nextRoom);
                    var exit = exits.GetRandom();
                    var pathThroughRoom = _tilePathCache.GetPath(entry, exit);
                    pathThroughRoom.Remove(exit);
                    path.AddRange(pathThroughRoom);
                    entry = exit;
                }
            }
            var pathToEnd = _tilePathCache.GetPath(entry, end);
            path.AddRange(pathToEnd);
            return path;
        }

        public void AddPathAsync(ITile start, ITile end)
        {
            if( start.Room == end.Room )
            {
                _tilePathCache.CachePathAsync(start, end);
                return;
            }

            var reverseRoomPath = _roomPathCache.GetPath(start.Room, end.Room);
            reverseRoomPath.Reverse();
            var roomPath = new Stack<Room>(reverseRoomPath);
            var entry = start;
            while( roomPath.Count != 0 )
            {
                var room = roomPath.Pop();
                if( roomPath.Count != 0 )
                {
                    var nextRoom = roomPath.Peek();
                    var exits = room.GetExitsToRoom(nextRoom);
                    var exit = exits.GetRandom();
                    _tilePathCache.CachePathAsync(entry, exit);
                    entry = exit;
                }
            }
            _tilePathCache.CachePathAsync(entry, end);
        }

        public void AddDestinationTile(ITile waypoint)
        {
            var room = waypoint.Room;
            foreach (var exit in room.GetAllExits())
            {
                _tilePathCache.CachePathAsync(exit, waypoint);
            }
        }
    }
}