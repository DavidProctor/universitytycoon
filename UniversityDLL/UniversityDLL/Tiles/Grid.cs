﻿using System;
using System.Linq;

namespace UniversityDLL.Tiles
{
    public interface IGrid
    {
        ITile GetTile(int x, int z);
        int Width { get; }
        int Height { get; }
        void ConnectNeighbors();
        void Destroy();
        bool Any(Func<ITile, bool> predicate);
        int Count(Func<ITile, bool> predicate);
    }

    public interface IGridGenerator
    {
        int Width { get; }
        int Height { get; }
        IGrid GenerateGrid();
    }

    [Serializable]
    public class Grid : IGrid
    {
        private readonly ITile[,] _tileArray;

        public int Width { get{ return _tileArray.GetLength(0); } }
        public int Height { get { return _tileArray.GetLength(1); } }

        public Grid(int width, int height)
        {
            _tileArray = new ITile[width, height];
        }

        public void SetTile(int x, int z, ITile tile)
        {
            _tileArray[x, z] = tile;
            tile.SetPosition(x, z);
        }

        public ITile GetTile(int x, int z)
        {
            return _tileArray[x, z];
        }

        public void ConnectNeighbors()
        {
            foreach(var tile in _tileArray)
            {
                ConnectNeighborsOfTile(tile);
            }
        }

        public void Destroy()
        {
            for(int x = 0;x < Width; ++x )
            {
                for(int z = 0; z < Height; ++z )
                {
                    _tileArray[x, z].Destroy();
                    _tileArray[x, z] = null;
                }
            }
        }

        public bool Any(Func<ITile, bool> predicate)
        {
            for (int x = 0; x < Width; ++x)
            {
                for (int z = 0; z < Height; z++)
                {
                    if (predicate(_tileArray[x, z]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public int Count(Func<ITile, bool> predicate)
        {
            int count = 0;
            for( int x = 0; x < Width; ++x )
            {
                for( int z = 0; z < Height; z++ )
                {
                    if( predicate(_tileArray[x, z]) )
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        private void ConnectNeighborsOfTile(ITile tile)
        {
            for(int x = tile.X - 1; x <= tile.X + 1; ++x)
            {
                if(x < 0 || x >= Width)
                    continue;

                for(int z = tile.Z - 1; z <= tile.Z + 1; ++z)
                {
                    if(z < 0 || z >= Height)
                        continue;

                    if(!(x == tile.X && z == tile.Z))
                        tile.AddConnection(_tileArray[x, z]);
                }
            }
        }
    }
}
