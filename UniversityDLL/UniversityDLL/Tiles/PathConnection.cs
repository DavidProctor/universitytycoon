﻿using System;
using System.Linq;

namespace UniversityDLL.Tiles
{
    public interface IPathConnection
    {
        IWaypoint[] Nodes { get; }
    }

    [Serializable]
    public class PathConnection : IPathConnection
    {
        public static IPathConnection GetConnection(IWaypoint node1, IWaypoint node2)
        {
            return new PathConnection(node1, node2);
        }

        public IWaypoint[] Nodes { get; }

        private PathConnection(IWaypoint node1, IWaypoint node2)
        {
            Nodes = new[] {node1, node2};
        }

        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            var other = obj as PathConnection;
            if (other == null) return false;

            return other.Nodes.Contains(Nodes[0]) && other.Nodes.Contains(Nodes[1]);
        }

        public override int GetHashCode()
        {
            return CalculateHash(Nodes[0], Nodes[1]);
        }

        private static int CalculateHash(IWaypoint node1, IWaypoint node2)
        {
            var hashcode = 31;
            hashcode += node1.GetHashCode() * 17;
            hashcode += node2.GetHashCode() * 17;
            return hashcode;
        }

        public override string ToString()
        {
            return $"PathConnection between {Nodes[0]} and {Nodes[1]}";
        }
    }
}