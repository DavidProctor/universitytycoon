﻿using System;
using System.Collections.Generic;

namespace UniversityDLL.Tiles
{
    public enum TileLayer
    {
        NullLayer,
        Ground,
        Basement,
        Floor,
        ActivityPoint,
        Structure,
        Door,
        Wall
    }

    public interface ITileContentCollection
    {
        void AddTileContent(ITileContent content);
        void RemoveTileContent(TileLayer layer);
        ITileContent GetTileContent(TileLayer layer);
        bool HasLayer(TileLayer layer);
        bool IsWalkable();
        bool BuildIsValid(ITileContent tileContent);
        ITile Tile { get; set; }
        void Render();
        void HideLayer(TileLayer layer);
        void ShowLayer(TileLayer layer);
        void Destroy();
        void HideLayersObscuredByLayer(TileLayer layer);
    }

    [Serializable]
    public class TileContentCollection : ITileContentCollection
    {
        private ITileContent _groundContent;
        private ITileContent _basementContent;
        private ITileContent _floorContent;
        private ITileContent _activityPointContent;
        private ITileContent _structureContent;
        private ITileContent _wallContent;
        private ITileContent _doorContent;
        private readonly List<ITileContent> _layerContents;

        public ITile Tile { get; set; }

        public TileContentCollection(ITileContent ground)
        {
            if (ground.Layer != TileLayer.Ground)
            {
                throw new InvalidOperationException($"TileContentCollection must be constructed with ground content - received {ground.Layer}");
            }
            _layerContents = new List<ITileContent>();
            SetContentAtLayer(TileLayer.Ground, ground);
        }

        public void AddTileContent(ITileContent content)
        {
            if(Tile == null)
            {
                throw new InvalidOperationException("Tried to add TileContent to TileContentCollection with null Tile");
            }
            if (!BuildIsValid(content))
            {
                throw new InvalidOperationException($"Cannot build a {content.Layer} over tile {Tile.X}, {Tile.Z}");
            }

            if(content.Layer == TileLayer.Basement)
            {
                _groundContent.Destroy();
            }
            else if (content.Layer == TileLayer.Floor)
            {
                _basementContent.Destroy();
            }
            else if (content.Layer == TileLayer.Door && HasLayer(TileLayer.Wall))
            {
                _wallContent.Destroy();
            }

            DestroyContentAtLayer(content.Layer);
            SetContentAtLayer(content.Layer, content);
            content.Render(Tile.X, 0f, Tile.Z);
        }

        public void RemoveTileContent(TileLayer layer)
        {
            DestroyContentAtLayer(layer);

            if(layer == TileLayer.Floor
                || layer == TileLayer.Wall && !HasLayer(TileLayer.Floor))
            {
                _basementContent.Render(Tile.X, 0f, Tile.Z);
            }
            else if (layer == TileLayer.Basement)
            {
                _groundContent.Render(Tile.X, 0f, Tile.Z);
            }
        }

        public ITileContent GetTileContent(TileLayer layer)
        {
            switch( layer )
            {
                case TileLayer.NullLayer:
                    return new NullTileContent();
                case TileLayer.Ground:
                    return _groundContent;
                case TileLayer.Basement:
                    return _basementContent;
                case TileLayer.Floor:
                    return _floorContent;
                case TileLayer.Structure:
                    return _structureContent;
                case TileLayer.ActivityPoint:
                    return _activityPointContent;
                case TileLayer.Wall:
                    return _wallContent;
                case TileLayer.Door:
                    return _doorContent;
                default:
                    throw new IndexOutOfRangeException($"Unknown tile layer: {layer}");
            }
        }

        public bool HasLayer(TileLayer layer)
        {
            return GetTileContent(layer) != null;
        }

        public bool IsWalkable()
        {
            return (!HasLayer(TileLayer.Wall) || HasLayer(TileLayer.Door))
                && !HasLayer(TileLayer.Structure);
        }

        public void Render()
        {
            var layersToHide = new List<TileLayer>();
            if (HasLayer(TileLayer.Door))
            {
                layersToHide.Add(TileLayer.Wall);
            }
            if (HasLayer(TileLayer.Floor))
            {
                layersToHide.Add(TileLayer.Basement);
                layersToHide.Add(TileLayer.Ground);
            }
            if (HasLayer(TileLayer.Basement))
            {
                layersToHide.Add(TileLayer.Ground);
            }

            foreach (TileLayer layer in Enum.GetValues(typeof(TileLayer)))
            {
                if (!HasLayer(layer))
                {
                    continue;
                }

                if (layersToHide.Contains(layer))
                {
                    GetTileContent(layer).Hide();
                    continue;
                }

                GetTileContent(layer).Render(Tile.X, 0, Tile.Z);
            }
        }

        public void Destroy()
        {
            DestroyContentAtLayer(TileLayer.Ground);
            DestroyContentAtLayer(TileLayer.Floor);
            DestroyContentAtLayer(TileLayer.Structure);
            DestroyContentAtLayer(TileLayer.Wall);
            DestroyContentAtLayer(TileLayer.Door);

            Tile = null;
        }

        public void HideLayersObscuredByLayer(TileLayer obscuringLayer)
        {
            foreach (var hiddenLayer in LayerDisplayRules.GetInstance().GetLayersObscuredByPreview(obscuringLayer))
            {
                if(HasLayer(hiddenLayer) )
                {
                    HideLayer(hiddenLayer);
                }
            }
        }

        public bool BuildIsValid(ITileContent tileContent)
        {
            if (tileContent.Layer == TileLayer.NullLayer)
            {
                return true;
            }

            if (tileContent.Layer == TileLayer.Basement)
            {
                return !HasLayer(TileLayer.Floor)
                    && !HasLayer(TileLayer.Structure)
                    && !HasLayer(TileLayer.ActivityPoint)
                    && !HasLayer(TileLayer.Wall)
                    && !HasLayer(TileLayer.Door);
            }

            if (tileContent.Layer == TileLayer.Floor)
            {
                return HasLayer(TileLayer.Basement)
                       && !HasLayer(TileLayer.Wall)
                       && !HasLayer(TileLayer.Door);
            }

            if (tileContent.Layer == TileLayer.Structure)
            {
                return !HasLayer(TileLayer.Wall)
                       && !HasLayer(TileLayer.Door)
                       && !HasLayer(TileLayer.ActivityPoint);
            }

            if (tileContent.Layer == TileLayer.ActivityPoint)
            {
                return !HasLayer(TileLayer.Wall)
                    && !HasLayer(TileLayer.Structure);
            }

            if (tileContent.Layer == TileLayer.Wall)
            {
                return HasLayer(TileLayer.Basement)
                       && !HasLayer(TileLayer.Structure)
                       && !HasLayer(TileLayer.Door)
                       && !HasLayer(TileLayer.ActivityPoint);
            }

            if (tileContent.Layer == TileLayer.Door)
            {
                return HasLayer(TileLayer.Basement)
                       && HasLayer(TileLayer.Wall);
            }

            throw new InvalidOperationException($"Unknown tile layer: {tileContent.Layer}");
        }
        
        public void HideLayer(TileLayer layer)
        {
            GetTileContent(layer)?.Hide();
        }

        public void ShowLayer(TileLayer layer)
        {
            GetTileContent(layer)?.Show();
        }

        private void DestroyContentAtLayer(TileLayer layer)
        {
            var content = GetTileContent(layer);
            content?.Destroy();
            if (content != null)
            {
                _layerContents.Remove(content);
            }
            SetContentAtLayer(layer, null);
        }

        private void SetContentAtLayer(TileLayer layer, ITileContent content)
        {
            if (content != null)
            {
                _layerContents.Add(content);
            }

            switch( layer )
            {
                case TileLayer.Ground:
                    _groundContent = content;
                    break;
                case TileLayer.Basement:
                    _basementContent = content;
                    break;
                case TileLayer.Floor:
                    _floorContent = content;
                    break;
                case TileLayer.Structure:
                    _structureContent = content;
                    break;
                case TileLayer.ActivityPoint:
                    _activityPointContent = content;
                    break;
                case TileLayer.Wall:
                    _wallContent = content;
                    break;
                case TileLayer.Door:
                    _doorContent = content;
                    break;
                default:
                    throw new IndexOutOfRangeException($"Unknown tile layer: {layer}");
            }
        }
    }
}
