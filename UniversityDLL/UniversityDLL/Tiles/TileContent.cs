﻿using System;
using UnityEngine;
using UniversityDLL.GameMangement;

namespace UniversityDLL.Tiles
{
    public interface ITileContent
    {
        IGameObject GameObject { get; }
        IGameObject Render(float x, float y, float z);
        IGameObject Render(float x, float y, float z, Color color);
        long PurchaseCost { get; }
        Orientation Orientation { get; }
        TileLayer Layer { get; }
        ITileContent GetClone();
        void Show();
        void Hide();
        void Destroy();
        void SetOrientation(Orientation orientation);
        void Rotate();
    }

    [Serializable]
    public class TileContent : ITileContent
    {
        public IGameObject GameObject { get; }
        public long PurchaseCost { get; }
        public Orientation Orientation { get { return _orientationTracker.Orientation; } }
        public TileLayer Layer { get; }

        private readonly OrientationTracker _orientationTracker = new OrientationTracker();

        public TileContent(IGameObject gameObject, long cost, TileLayer layer, Orientation orientation = Orientation.Forward)
        {
            GameObject = gameObject;
            PurchaseCost = cost;
            Layer = layer;
            _orientationTracker.SetOrientation(orientation);
            GameObject.SetOrientation(Orientation);
        }

        public IGameObject Render(float x, float y, float z)
        {
            var result = GameObject.Render(x, y, z);
            return result;
        }

        public IGameObject Render(float x, float y, float z, Color color)
        {
            var result = GameObject.Render(x, y, z, color);
            return result;
        }

        public ITileContent GetClone()
        {
            return new TileContent(GameObject.GetClone(), PurchaseCost, Layer, Orientation);
        }

        public void Hide()
        {
            GameObject.Hide();
        }

        public void Show()
        {
            GameObject.Show();
        }

        public void Destroy()
        {
            GameObject.Destroy();
        }

        public void SetOrientation(Orientation orientation)
        {
            _orientationTracker.SetOrientation(orientation);
            GameObject.SetOrientation(orientation);
        }

        public void Rotate()
        {
            _orientationTracker.NextRotation();
            GameObject.SetOrientation(_orientationTracker.Orientation);
        }
    }
}
