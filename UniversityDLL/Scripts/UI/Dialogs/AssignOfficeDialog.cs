﻿using System;
using System.Collections.Generic;
using Scripts.UI.Primitives;
using SLS.Widgets.Table;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.People;
using UniversityDLL.Rooms;

namespace Scripts.UI.Dialogs
{
    class AssignOfficeDialog : Dialog
    {
        private class UnassignedAdminsTablePanel : TablePanel<IAdministrator>
        {
            public UnassignedAdminsTablePanel(
                Action<IAdministrator, Column> clickFunction,
                IEnumerable<ColumnConfig<IAdministrator>> columns,
                RectTransform parent = null)
                : base("UnassignedAdminsTablePanel", clickFunction, columns, parent)
            {
            }
        }

        private readonly IGameState _gameState = GameState.GetInstance();
        private readonly UnassignedAdminsTablePanel _unassignedAdminsTablePanel;
        private readonly TextPanel _selectedAdminPanel;
        private readonly ButtonWrapper _assignAdminButton;
        private readonly RAdminOffice _office;

        private IAdministrator _selectedAdministrator;

        public AssignOfficeDialog(RAdminOffice office) 
            : base($"{office.Name} Dialog", "Administrator's office")
        {
            _office = office;

            var contentPanel = new ContainerPanel("Assign admin office panel", RectTransform.Edge.Left, Transform);
            SetContentPanel(contentPanel);
            
            var panelWidth = Mathf.RoundToInt(contentPanel.Transform.rect.width / 3 - contentPanel.Margin * 2);
            var panelHeight = contentPanel.Transform.rect.height;

            var availableAdminsColumnConfig = new ColumnConfig<IAdministrator>
            {
                DisplayFunction = administrator => administrator.Name,
                Header = "Unassigned administrators",
                Type = Column.ColumnType.TEXT,
            };
            _unassignedAdminsTablePanel = new UnassignedAdminsTablePanel(OnSelectUnassignedAdmin, new[] { availableAdminsColumnConfig }, contentPanel.Transform);
            _gameState.Administrators.ForEach(admin => _unassignedAdminsTablePanel.AddRow(admin));
            _unassignedAdminsTablePanel.SetAbsoluteDimensions(panelWidth, panelHeight);
            PopulateUnassignedAdminsTable();
            contentPanel.AddContent(_unassignedAdminsTablePanel);

            var actionsPanelWith = panelWidth*2 + Margin;
            var actionsPanel = new ContainerPanel("AssignOfficeActionsPanel", RectTransform.Edge.Top, contentPanel.Transform);
            actionsPanel.SetAbsoluteDimensions(actionsPanelWith, panelHeight);
            contentPanel.AddContent(actionsPanel);

            var headerSize = 30f;
            var deptDetailsHeader = new TextPanel("AssignOfficeHeaderPanel", actionsPanel.Transform)
            {
                TextContent = "Unoccupied office"
            };
            deptDetailsHeader.SetAbsoluteDimensions(actionsPanelWith, headerSize);
            actionsPanel.AddContent(deptDetailsHeader);

            var detailSize = 60f;
            var textPanel = new TextPanel("AssignOfficeTextPanel", actionsPanel.Transform)
            {
                TextContent = "This office is not yet occupied.\nSelect an administrator from the list to assign them."
            };
            textPanel.TextComponent.alignment = TextAnchor.UpperLeft;
            textPanel.SetAbsoluteDimensions(panelWidth, detailSize * 3);
            actionsPanel.AddContent(textPanel);

            _selectedAdminPanel = new TextPanel("SelectedAdminPanel", actionsPanel.Transform)
            {
                TextContent = string.Empty
            };
            _selectedAdminPanel.TextComponent.alignment = TextAnchor.UpperLeft;
            _selectedAdminPanel.SetAbsoluteDimensions(panelWidth, detailSize);
            actionsPanel.AddContent(_selectedAdminPanel);

            _assignAdminButton = new TextButton("AssignAdminButton", "Assign", OnAssignButton, actionsPanel.Transform);
            _assignAdminButton.Button.enabled = false;
            actionsPanel.AddContent(_assignAdminButton);
        }

        private void OnSelectUnassignedAdmin(IAdministrator admin, Column column)
        {
            _selectedAdministrator = admin;
            _selectedAdminPanel.TextContent = $"{admin.Name}";
            _assignAdminButton.Button.enabled = true;
        }

        private void OnAssignButton()
        {
            _office.SetOwner(_selectedAdministrator);
            _assignAdminButton.Button.enabled = false;
            _selectedAdminPanel.TextContent = string.Empty;
            _unassignedAdminsTablePanel.ResetTable();
            GameObject.Destroy(GameObject);
            DialogFactory.GetInstance().GetInfoDialog(_office);
        }

        private void PopulateUnassignedAdminsTable()
        {
            _unassignedAdminsTablePanel.ResetTable();
            _gameState.Administrators.ForEach(admin => _unassignedAdminsTablePanel.AddRow(admin));
        }
    }
}
