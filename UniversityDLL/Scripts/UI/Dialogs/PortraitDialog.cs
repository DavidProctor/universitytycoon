﻿using System;
using Scripts.UI.Primitives;
using UnityEngine;

namespace Scripts.UI.Dialogs
{
    public class PortraitDialog : ContainerPanel
    {
        public PortraitDialog(string name, string imagePath, string textContent, Action onButton, string buttonText="OK") 
            : base($"{name}PortraitDialog", RectTransform.Edge.Left)
        {
            var panelWidth = 450f;
            var panelHeight = 275f;
            var contentHeight = panelHeight - Margin * 2;
            SetBackgroundColor(Palette.PanelBackground);
            base.SetAbsoluteDimensions(panelWidth, panelHeight);

            var portait = new ImagePanel($"{Name}PortraitPanel", Transform);
            portait.SetImage(UiSprites.FindSprite(imagePath));
            portait.SetAbsoluteDimensions(Width / 3f, contentHeight);
            AddContent(portait);

            var contentPanel = new ContainerPanel($"{Name}TextPanel", RectTransform.Edge.Top, Transform);
            var contentPanelWidth = panelWidth - portait.Width - Margin * 3;
            contentPanel.SetAbsoluteDimensions(contentPanelWidth, contentHeight);
            AddContent(contentPanel);

            var lineHeight = 25f;
            var namePanel = new TextPanel($"{Name}NameLabel", contentPanel.Transform);
            namePanel.SetAbsoluteDimensions(contentPanelWidth, lineHeight);
            namePanel.TextContent = name;
            namePanel.TextComponent.alignment = TextAnchor.MiddleLeft;
            namePanel.TextComponent.fontStyle = FontStyle.Bold;
            contentPanel.AddContent(namePanel);

            var textPanel = new TextPanel($"{Name}TextLabel", contentPanel.Transform);
            textPanel.SetAbsoluteDimensions(contentPanelWidth, contentHeight - (lineHeight * 2) - (Margin * 4));
            textPanel.TextContent = textContent;
            textPanel.TextComponent.alignment = TextAnchor.UpperLeft;
            contentPanel.AddContent(textPanel);

            Action onButtonWithDialogClose = () =>
            {
                Destroy();
                onButton();
            };

            var button = new TextButton($"{Name}NextButton", buttonText, onButtonWithDialogClose, contentPanel.Transform);
            button.SetPositionFromEdge(RectTransform.Edge.Right, 0);
            contentPanel.AddContent(button);
        }
    }
}