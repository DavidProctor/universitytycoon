﻿using System;
using System.IO;
using Scripts.Loading;
using Scripts.UI.Primitives;
using SLS.Widgets.Table;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UniversityDLL.GameMangement;

namespace Scripts.UI.Dialogs
{
    class SaveLoadDialog : MonoBehaviour
    {
        private GameObject _canvas;
        private UIObject _panel;
        private UIObject _mainPanel;
        private Table _saveLoadTable;
        private UIObject _saveNamePopup;
        private InputField _saveNameInputField;

        public void Start()
        {
            validateSaveFolder();

            _canvas = GameObject.Find("Canvas");
            _panel = UIComponent.AddChildObject(_canvas, "SaveLoadDialog");
            _panel.RectTransform.anchorMax = new Vector2(0.75f, 0.75f);
            _panel.RectTransform.anchorMin = new Vector2(0.25f, 0.25f);
            var panelImage = _panel.GameObject.AddComponent<Image>();
            panelImage.color = Color.gray;

            var header = UIComponent.AddText(_panel.GameObject, "Save / Load", "Header");
            header.RectTransform.anchorMax = new Vector2(0.95f, 0.95f);
            header.RectTransform.anchorMin = new Vector2(0.05f, 0.05f);
            header.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0.05f, 40f);
            header.Text.fontSize = 32;

            _mainPanel = UIComponent.AddChildObject(_panel.GameObject, "MainPanel");
            _mainPanel.RectTransform.anchorMin = new Vector2(0.05f, 0.1f);
            _mainPanel.RectTransform.anchorMax = new Vector2(0.95f, 0.87f);

            SetupSaveFileTable();

            var buttonPanel = UIComponent.AddChildObject(_mainPanel.GameObject, "ButtonPanel");
            buttonPanel.RectTransform.anchorMin = new Vector2(0.7f, 0f);

            var newSaveButton = UIComponent.AddButton(buttonPanel.GameObject, "New Save", OnSaveButton, "NewSaveButton");
            newSaveButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 5f, 35f);
            var saveButton = UIComponent.AddButton(buttonPanel.GameObject, "Overwrite", OnOverwriteButton, "OverwriteButton");
            saveButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 45f, 35f);
            var loadButton = UIComponent.AddButton(buttonPanel.GameObject, "Load", OnLoadButton, "LoadButton");
            loadButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 95f, 35f);
            var deleteButton = UIComponent.AddButton(buttonPanel.GameObject, "Delete", OnDeleteButton, "DeleteButton");
            deleteButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 135f, 35f);
            var doneButton = UIComponent.AddButton(buttonPanel.GameObject, "Done", OnDoneButton, "DoneButton");
            doneButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 5f, 35f);
        }

        private void SetupSaveFileTable()
        {
            var tableObject = UIComponent.AddChildObject(_mainPanel.GameObject, "DepartmentsTable");
            tableObject.RectTransform.anchorMax = new Vector2(0.65f, 1f);
            _saveLoadTable = tableObject.GameObject.AddComponent<Table>();

            _saveLoadTable.reset();
            _saveLoadTable.addTextColumn();
            _saveLoadTable.initialize();
            _saveLoadTable.cellSelectColor = Color.magenta;

            var saveDirectory = new DirectoryInfo(getSaveFolder());

            foreach( FileInfo saveFile in saveDirectory.GetFiles("*.usv") )
            {
                Datum row = Datum.Body(saveFile.Name);
                row.rawObject = saveFile;
                row.elements.Add(saveFile.Name);
                _saveLoadTable.data.Add(row);
            }

            _saveLoadTable.startRenderEngine();
        }

        private void OnSaveButton()
        {
            var saver = new GameSaver();
            var targetFile = string.Format("{0}/{1}.usv", 
                getSaveFolder(),
                DateTime.Now.ToString("MMM dd yyyy h-mm-ss tt"));
            saver.Save(GameState.GetInstance(), targetFile);
            SetupSaveFileTable();
        }

        private void OnNewSaveButton()
        {
            var canvas = GameObject.Find("Canvas");
            _saveNamePopup = UIComponent.AddChildObject(canvas, "NewSaveDialog");
            _saveNamePopup.RectTransform.anchorMin = new Vector2(0.35f, 0.45f);
            _saveNamePopup.RectTransform.anchorMax = new Vector2(0.65f, 0.55f);
            var saveImage = _saveNamePopup.GameObject.AddComponent<Image>();
            saveImage.color = Color.red;

            var saveName = Instantiate(Resources.Load("InputField")) as GameObject;
            saveName.transform.SetParent(_saveNamePopup.GameObject.transform);
            var saveNameTransform = saveName.GetComponent<RectTransform>();
            saveNameTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0.1f, 30f);
            saveNameTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0.2f, 300f);
            _saveNameInputField = saveName.GetComponent<InputField>();
            _saveNameInputField.gameObject.SetActive(false);
            _saveNameInputField.gameObject.SetActive(true);
            Debug.Log(_saveNameInputField);
            _saveNameInputField.onValueChanged.AddListener(value => Type(value));

            var commitButton = UIComponent.AddButton(_saveNamePopup.GameObject, "Save", OnCommitNewSaveButton, "CommitNewSaveButton");
            commitButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0.1f, 35f);
            commitButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0.1f, 100f);
            var cancelButton = UIComponent.AddButton(_saveNamePopup.GameObject, "Cancel", OnCancelNewSaveButton, "CancelNewSaveButton");
            cancelButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0.1f, 35f);
            cancelButton.RectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0.1f, 100f);
        }

        private void Type(string value)
        {
            Debug.Log(_saveNameInputField.text);
        }

        private void OnCommitNewSaveButton()
        { 
            var saver = new GameSaver();
            var targetFile = string.Format("{0}/{1}.usv", getSaveFolder(), _saveNameInputField.text);
            saver.Save(GameState.GetInstance(), targetFile);
            SetupSaveFileTable();
            Destroy(_saveNamePopup.GameObject);
            _saveNameInputField = null;
            _saveNamePopup = null;
        }

        private void OnCancelNewSaveButton()
        {
            Destroy(_saveNamePopup.GameObject);
            _saveNamePopup = null;
        }

        private void OnOverwriteButton()
        {
            var saver = new GameSaver();
            var targetFile = _saveLoadTable.selectedDatum.rawObject as FileInfo;
            saver.Save(GameState.GetInstance(), targetFile.FullName);
            SetupSaveFileTable();
        }

        private void OnLoadButton()
        {
            var loadFile = _saveLoadTable.selectedDatum.rawObject as FileInfo;

            var loadPointerObject = new GameObject("LoadFilePointer");
            var loadPointer = loadPointerObject.AddComponent<LoadFilePointer>();
            loadPointer.FileToLoad = loadFile.FullName;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        }

        private void OnDeleteButton()
        {
            var file = _saveLoadTable.selectedDatum.rawObject as FileInfo;
            file.Delete();
            SetupSaveFileTable();
        }

        private string getSaveFolder()
        {
            var appdataFolder = Environment.GetEnvironmentVariable("LocalAppData");
            return appdataFolder + "/UniversityTycoon/saves";
        }

        private void validateSaveFolder()
        {
            var path = getSaveFolder();
            var folder = new DirectoryInfo(path);
            if( !folder.Exists )
            {
                Directory.CreateDirectory(path);
            }
        }

        public void OnDoneButton()
        {
            Destroy(_panel.GameObject);
        }
    }
}
