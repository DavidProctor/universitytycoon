﻿using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.UI;

namespace Scripts.UI.Dialogs
{
    public class Dialog : ImagePanel, IDialog
    {
        public TextPanel TitleText { get; }
        public GuiComponent ContentPanel { get; private set; }
        public ContainerPanel BottomButtonsPanel { get; }
        public TextButton CloseTextButton { get; }

        public Dialog(string name, string title, RectTransform parent = null)
            : base (name, parent)
        {
            TitleText = new TextPanel($"{name}TitleText", Transform)
            {
                TextContent = title
            };
            TitleText.SetAutoSizeText();
            BottomButtonsPanel = new ContainerPanel($"{name}BottomButtonsPanel", RectTransform.Edge.Right, Transform);

            CloseTextButton = new TextButton($"{name}CloseButton", 
                                            UiText.Get(this, "Close"), 
                                            () => GameObject.SetActive(false), 
                                            BottomButtonsPanel.Transform);
            BottomButtonsPanel.AddContent(CloseTextButton);

            SetBackgroundColor(Palette.PanelBackground);

            base.SetAbsoluteDimensions(640, 480);
        }

        public override void SetAbsoluteDimensions(float width, float height)
        {
            base.SetAbsoluteDimensions(width, height);

            var widthMinusMargins = width - (Margin * 2);
            var titleHeight = 40f;
            var bottomButtonsHeight = 35f;

            TitleText.SetAbsoluteDimensions(widthMinusMargins, titleHeight);
            TitleText.SetPositionFromEdge(RectTransform.Edge.Top, Margin);

            ContentPanel?.SetAbsoluteDimensions(widthMinusMargins, height - (Margin * 4 + titleHeight + bottomButtonsHeight));
            ContentPanel?.SetPositionFromEdge(RectTransform.Edge.Top, Margin * 2 + titleHeight);

            BottomButtonsPanel.SetAbsoluteDimensions(widthMinusMargins, bottomButtonsHeight);
            BottomButtonsPanel.SetPositionFromEdge(RectTransform.Edge.Bottom, Margin);
        }

        public void SetContentPanel(GuiComponent contentPanel)
        {
            ContentPanel = contentPanel;
            SetAbsoluteDimensions(Transform.rect.width, Transform.rect.height);
        }
    }
}