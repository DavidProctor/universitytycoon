﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Loading;
using Scripts.UI.Primitives;
using SLS.Widgets.Table;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;

namespace Scripts.UI.Dialogs
{
    public class CreateDepartmentDialog : Dialog
    {
        private class ActiveDeptsTablePanel : TablePanel<IDepartment>
        {
            public ActiveDeptsTablePanel(
                Action<IDepartment, Column> clickFunction, 
                IEnumerable<ColumnConfig<IDepartment>> columns,
                RectTransform parent = null) 
                : base("ActiveDeptsTablePanel", clickFunction, columns, parent)
            {
            }
        }

        private class AvailableDeptsPanel : TablePanel<IDepartment>
        {
            public AvailableDeptsPanel(
                Action<IDepartment, Column> clickFunction, 
                IEnumerable<ColumnConfig<IDepartment>> columns, 
                RectTransform parent = null) 
                : base("ActiveDeptsTablePanel", clickFunction, columns, parent)
            {
            }
        }

        private class CoursesPanel : TablePanel<ICourseTemplate>
        {
            public CoursesPanel(Action<ICourseTemplate, Column> clickFunction, 
                IEnumerable<ColumnConfig<ICourseTemplate>> columns, 
                RectTransform parent) 
                : base("CoursesPanel", clickFunction, columns, parent)
            {
            }
        }

        private readonly TablePanel<IDepartment> _activeDepartmentsPanel;
        private readonly TablePanel<IDepartment> _availableDepartmentsTable;
        private readonly TextPanel _departmentDetailsText;
        private readonly TablePanel<ICourseTemplate> _coursesPanel;
        private readonly TextButton _createDepartmentTextButton;
        private readonly List<IDepartment> _availableDepartments = new TestDataGenerator().CreateDepartments();
        private readonly IGameState _gameState = GameState.GetInstance();

        private IDepartment _selectedDepartment;

        public CreateDepartmentDialog() 
            : base("CreateDepartmentDialog", "Manage Departments", null)
        {
            var contentPanel = new ContainerPanel("CreateDeptContainer", RectTransform.Edge.Left, Transform);
            SetContentPanel(contentPanel);

            var panelWidth = Mathf.RoundToInt(contentPanel.Transform.rect.width / 3 - contentPanel.Margin * 2);
            var panelHeight = contentPanel.Transform.rect.height;

            var activeDeptsColumnConfig = new ColumnConfig<IDepartment>
            {
                DisplayFunction = department => department.ToString(),
                Header = "Active departments",
                Type = Column.ColumnType.TEXT,
            };
            _activeDepartmentsPanel = new ActiveDeptsTablePanel(OnSelectActiveDepartment, new [] {activeDeptsColumnConfig}, contentPanel.Transform);
            _gameState.Departments.ForEach(d => _activeDepartmentsPanel.AddRow(d));
            _activeDepartmentsPanel.SetAbsoluteDimensions(panelWidth, panelHeight);
            PopulateActiveDepartmentsTable();
            contentPanel.AddContent(_activeDepartmentsPanel);

            var availableDeptsColumnConfig = new ColumnConfig<IDepartment>
            {
                DisplayFunction = department => department.ToString(),
                Header = "Available departments",
                Type = Column.ColumnType.TEXT,
            };
            _availableDepartmentsTable = new AvailableDeptsPanel(OnSelectAvailableDepartment, new [] {availableDeptsColumnConfig}, contentPanel.Transform);
            PopulateAvailableDepartmentsTable();
            _availableDepartmentsTable.SetAbsoluteDimensions(panelWidth, panelHeight);
            contentPanel.AddContent(_availableDepartmentsTable);

            var deptDetailsPanel = new ContainerPanel("DeptDetailsPanel", RectTransform.Edge.Top, contentPanel.Transform);
            deptDetailsPanel.SetAbsoluteDimensions(panelWidth, panelHeight);

            var headerSize = 20f;
            var deptDetailsHeader = new TextPanel("DeptDetailsHeaderLabel", deptDetailsPanel.Transform)
            {
                TextContent = "Department details"
            };
            deptDetailsHeader.SetAbsoluteDimensions(panelWidth, headerSize);
            deptDetailsPanel.AddContent(deptDetailsHeader);

            var detailSize = 60f;
            _departmentDetailsText = new TextPanel("DepartmentDetailsLabel", deptDetailsPanel.Transform)
            {
                TextContent = string.Empty
            };
            _departmentDetailsText.TextComponent.alignment = TextAnchor.UpperLeft;
            _departmentDetailsText.SetAbsoluteDimensions(panelWidth, detailSize);
            deptDetailsPanel.AddContent(_departmentDetailsText);

            _createDepartmentTextButton = new TextButton("CreateDepartmentButton", "Create department", OnCreateDepartmentButton, deptDetailsPanel.Transform);
            _createDepartmentTextButton.Button.enabled = false;
            
            var coursesColumnConfig = new[]
            {
                new ColumnConfig<ICourseTemplate>
                {
                    DisplayFunction = courseTemplate => courseTemplate.ToString(),
                    Header = "Sample courses",
                    Type = Column.ColumnType.TEXT
                },
            };
            _coursesPanel = new CoursesPanel((course, column) => { }, coursesColumnConfig, deptDetailsPanel.Transform);
            _coursesPanel.SetAbsoluteDimensions(panelWidth, panelHeight - _createDepartmentTextButton.Transform.rect.height - headerSize - detailSize - deptDetailsPanel.Margin * 4);
            deptDetailsPanel.AddContent(_coursesPanel);
            deptDetailsPanel.AddContent(_createDepartmentTextButton);

            contentPanel.AddContent(deptDetailsPanel);
        }

        private void OnCreateDepartmentButton()
        {
            if (_selectedDepartment == null)
            {
                return;
            }
            _gameState.AddDepartment(_selectedDepartment);
            PopulateActiveDepartmentsTable();
            PopulateAvailableDepartmentsTable();
            OnSelectActiveDepartment(null, null);

            _selectedDepartment = null;
            _createDepartmentTextButton.Button.enabled = false;
        }

        private void OnSelectActiveDepartment(IDepartment department, Column column)
        {
            _departmentDetailsText.TextContent = string.Empty;
            _createDepartmentTextButton.Button.enabled = false;
            _selectedDepartment = null;
            _coursesPanel.ResetTable();
        }

        private void OnSelectAvailableDepartment(IDepartment department, Column column)
        {
            _selectedDepartment = department;
            if (_gameState.Score.Prestige > department.PrestigeCost)
            {
                _createDepartmentTextButton.Button.enabled = true;
            }
            PopulateCoursesPanel(department);
        }

        private void PopulateCoursesPanel(IDepartment department)
        {
            _coursesPanel.ResetTable();
            department.GetCourses().ForEach(course => _coursesPanel.AddRow(course));
            _departmentDetailsText.TextContent = $"Department of {department}\nFour-year degree";
        }

        private void PopulateActiveDepartmentsTable()
        {
            _activeDepartmentsPanel.ResetTable();
            _gameState.Departments.ForEach(department => _activeDepartmentsPanel.AddRow(department));
        }

        private void PopulateAvailableDepartmentsTable()
        {
            _availableDepartmentsTable.ResetTable();
            var activeDepartments = _gameState.Departments;
            foreach (var department in _availableDepartments)
            {
                if (activeDepartments.Contains(department))
                {
                    continue;
                }

                _availableDepartmentsTable.AddRow(department);
            }
        }
    }
}