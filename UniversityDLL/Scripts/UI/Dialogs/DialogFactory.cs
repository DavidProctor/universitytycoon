﻿using System.Collections.Generic;
using System.Linq;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;
using UniversityDLL.UI;

namespace Scripts.UI.Dialogs
{
    public class DialogFactory : IDialogFactory
    {
        private static readonly DialogFactory Instance =  new DialogFactory();

        public static DialogFactory GetInstance()
        {
            return Instance;
        }

        private DialogFactory() { }

        private readonly Dictionary<IGameEntity, Dialog> _dialogs = new Dictionary<IGameEntity, Dialog>();

        public IDialog GetInfoDialog(IGameEntity subject)
        {
            if (subject == null)
            {
                return null;
            }

            if (_dialogs.ContainsKey(subject))
            {
                _dialogs[subject].Transform.gameObject.SetActive(true);
                return _dialogs[subject];
            }

            if (subject is IStudent)
            {
                return MakeStudentDialog(subject as IStudent);
            }
            if (subject is ITile)
            {
                var tile = subject as ITile;
                var room = tile.Room;
                if (_dialogs.ContainsKey(room) )
                {
                    _dialogs[room].Transform.gameObject.SetActive(true);
                    return _dialogs[room];
                }

                if ( room is RLectureHall)
                {
                    return MakeLectureHallDialog(room as RLectureHall);
                }
                if ( room is RPub)
                {
                    return MakePubDialog(room as RPub);
                }
                if (room is RAdminOffice)
                {
                    return MakeAdminOfficeDialog(room as RAdminOffice);
                }
            }

            return null;
        }

        private Dialog MakeStudentDialog(IStudent student)
        {
            var dlg = new Dialog($"{student.Name} Character Dialog", student.Name);
            dlg.SetAbsoluteDimensions(450f, 275f);

            var contentPanel = new ContainerPanel($"{dlg.Name} Content Panel", RectTransform.Edge.Left, dlg.Transform);
            dlg.SetContentPanel(contentPanel);

            var photo = new ImagePanel($"{student.Name} Character Dialog Photo", contentPanel.Transform);
            photo.SetBackgroundColor(Palette.BlueLight);
            photo.SetAbsoluteDimensions(contentPanel.Transform.rect.width / 3, contentPanel.Transform.rect.height);
            contentPanel.AddContent(photo);
            
            var detailsPanel = new TextPanel($"{student.Name} Character Dialog Name Panel", contentPanel.Transform);
            detailsPanel.TextComponent.alignment = TextAnchor.UpperLeft;
            var gpa = student.Enrolments.Average(e => e.StudyValue);
            detailsPanel.TextContent = $"NAME: {student.Name}\nMAJOR: {student.Major.Name}\nGPA: {gpa:f2}";
            detailsPanel.SetAbsoluteDimensions(contentPanel.Transform.rect.width - photo.Transform.rect.width - contentPanel.Margin,
                contentPanel.Transform.rect.height);
            contentPanel.AddContent(detailsPanel);

            _dialogs.Add(student, dlg);

            return dlg;
        }

        private Dialog MakeLectureHallDialog(RLectureHall lectureHall)
        {
            var dlg = new Dialog($"{lectureHall.Name} Lecture Hall Dialog", lectureHall.Name);

            var contentPanel = new ContainerPanel($"{dlg.Name} Content Panel", RectTransform.Edge.Left, dlg.Transform);
            dlg.SetContentPanel(contentPanel);

            var detailsPanel = new TextPanel($"{dlg.Name} Details Panel", contentPanel.Transform);
            detailsPanel.TextComponent.alignment = TextAnchor.UpperLeft;
            detailsPanel.TextContent = $"{lectureHall.Name}\nCapacity: {lectureHall.Capacity}";
            detailsPanel.SetAbsoluteDimensions(contentPanel.Transform.rect.width / 4, contentPanel.Transform.rect.height);
            contentPanel.AddContent(detailsPanel);

            var schedulePanel = new ScheduleTablePanel($"{dlg.Name} Schedule Panel", 
                lectureHall.GetSchedule(GameState.GetInstance().Calendar.CurrentTime.Semester),
                contentPanel.Transform);
            schedulePanel.SetAbsoluteDimensions(contentPanel.Transform.rect.width - detailsPanel.Transform.rect.width - contentPanel.Margin,
                contentPanel.Transform.rect.height);
            contentPanel.AddContent(schedulePanel);

            _dialogs.Add(lectureHall, dlg);

            return dlg;
        }

        private Dialog MakePubDialog(RPub pub)
        {
            var dlg = new Dialog($"{pub.Name} Pub Dialog", pub.Name);

            var contentPanel = new TextPanel($"{dlg.Name} Content Panel", dlg.Transform);
            contentPanel.TextComponent.alignment = TextAnchor.UpperCenter;
            contentPanel.TextContent = $"It's {pub.Name}, the campus pub.";
            dlg.SetContentPanel(contentPanel);

            _dialogs.Add(pub, dlg);

            return dlg;
        }

        private Dialog MakeAdminOfficeDialog(RAdminOffice office)
        {
            if (office.Owner == null)
            {
                return new AssignOfficeDialog(office);
            }

            var dlg = new Dialog($"{office.Name} Dialog", office.Name);

            var contentPanel = new TextPanel($"{dlg.Name} Content Panel", dlg.Transform);
            contentPanel.TextComponent.alignment = TextAnchor.UpperCenter;
            contentPanel.TextContent = $"This office belongs to {office.Owner.Name}.";
            dlg.SetContentPanel(contentPanel);

            _dialogs.Add(office, dlg);

            return dlg;
        }
    }
}