﻿using Scripts.UI.Primitives;
using UnityEngine;
using UnityEngine.UI;
using UniversityDLL.GameMangement;

namespace Scripts.UI.Hud
{
    class HudTopPanel : HudComponent
    {
        private class SeparatorPanel : ImagePanel
        {
            public SeparatorPanel(string name, RectTransform transform) 
                : base(name, transform)
            {
                SetBackgroundColor(Palette.BlueVeryLight);
                base.SetAbsoluteDimensions(2, Parent.rect.height);
            }
        }

        private readonly int _panelHeight = 22;

        public HudTopPanel() 
            : base("HudTopPanel", RectTransform.Edge.Left)
        {
            SetImage(UiSprites.FindSprite("topstrip"));
            Image.type = Image.Type.Tiled;
            
            base.SetAbsoluteDimensions(Parent.GetComponent<RectTransform>().rect.width, _panelHeight);
            base.SetPositionFromEdge(RectTransform.Edge.Top, 0);

            var resourcesPanel = new HudResourcesPanel(Transform);

            var calendar = GameState.GetInstance().Calendar;

            var datePanel = new TextPanel("HudDatePanel", Transform);
            datePanel.SetUpdateAction(() => datePanel.TextContent = calendar.CurrentTime.ToDateString());
            datePanel.SetAbsoluteDimensions(225, _panelHeight);
            TextStyler.ApplyHudStyle(datePanel.TextComponent);

            var timePanel = new TextPanel("HudTimePanel", Transform);
            timePanel.SetUpdateAction(() => timePanel.TextContent = calendar.CurrentTime.ToTimeString());
            timePanel.SetAbsoluteDimensions(100, _panelHeight);
            TextStyler.ApplyHudStyle(timePanel.TextComponent);

            //var timeControlPanel = new HudTimeButtons("HudTimeButtons", GameController.GetInstance().TimeControls, Transform);

            var blankPanel = new ImagePanel("HudBlankPanel", Transform);
            var blankPanelWidth = Width - (resourcesPanel.Width + datePanel.Width + timePanel.Width //+ timeControlPanel.Width
                + 12 + (Margin*11));
            blankPanel.SetAbsoluteDimensions(blankPanelWidth, _panelHeight);

            AddContent(new SeparatorPanel("Separator1", Transform));
            AddContent(resourcesPanel);
            AddContent(new SeparatorPanel("Separator2", Transform));
            AddContent(blankPanel);
            AddContent(new SeparatorPanel("Separator3", Transform));
            AddContent(datePanel);
            AddContent(new SeparatorPanel("Separator4", Transform));
            AddContent(timePanel);
            AddContent(new SeparatorPanel("Separator5", Transform));
            //AddContent(timeControlPanel);
            //AddContent(new SeparatorPanel("Separator6", Transform));
        }
    }
}
