﻿using System;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.GameMangement;

namespace Scripts.UI.Hud
{
    public class HudResourcesPanel : HudComponent
    {
        private readonly int _cashPanelWidth = 100;
        private readonly int _prestigePanelWidth = 40;
        private readonly int _culturePanelWidth = 40;

        public HudResourcesPanel(RectTransform parent) 
            : base("HudResourcesPanel", RectTransform.Edge.Left, parent)
        {
            var score = GameState.GetInstance().Score;

            AddIndicator("HudCashIndicator", "cashicon", () => $"{score.Money:N0}", _cashPanelWidth);
            AddIndicator("HudPrestigeIndicator", "prestigeicon", () => $"{score.Prestige:N0}", _prestigePanelWidth);
            AddIndicator("HudCultureIndicator", "cultureicon", () => "300", _culturePanelWidth);
            
            base.SetAbsoluteDimensions(GetWidthFromChildren(), parent.rect.height);
        }

        private void AddIndicator(string name, string iconPath, Func<string> displayFunciton, int textWidth)
        {
            var panel = new ContainerPanel(name, RectTransform.Edge.Left, Transform);
            var icon = MakeIcon($"{name}Icon", iconPath, panel.Transform);
            var text = MakeText($"{name}Text", displayFunciton, textWidth, panel.Transform);
            panel.AddContent(icon);
            panel.AddContent(text);
            panel.SetAbsoluteDimensions(icon.Width + textWidth + panel.Margin * 3, Parent.rect.height);
            AddContent(panel);
        }

        private GuiComponent MakeIcon(string name, string path, RectTransform parent)
        {
            var panel = new ImagePanel($"{name}", parent);
            var image = UiSprites.FindSprite(path);
            panel.SetImage(image);
            panel.SetAbsoluteDimensions(image.rect.width, image.rect.height);
            return panel;
        }

        private GuiComponent MakeText(string name, Func<string> displayFunction, int width, RectTransform parent)
        {
            var panel = new TextPanel($"{name}", parent);
            panel.SetUpdateAction(() => panel.TextContent = displayFunction());
            TextStyler.ApplyHudStyle(panel.TextComponent);
            panel.SetAbsoluteDimensions(width, 20);
            return panel;
        }
    }
}