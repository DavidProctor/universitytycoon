﻿using System.Collections;
using Scripts.ControlMediation;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.GameMangement;

namespace Scripts.UI.Hud
{
    public class TurnActionsPanel : HudComponent
    {
        // ReSharper disable once ClassNeverInstantiated.Local
        private class CoroutineRunner : MonoBehaviour { }

        private readonly CoroutineRunner _coroutineRunner;

        public TurnActionsPanel() 
            : base("TurnActionsHudPanel", RectTransform.Edge.Right)
        {
            _coroutineRunner = GameObject.AddComponent<CoroutineRunner>();

            base.SetPositionFromEdge(RectTransform.Edge.Bottom, Margin);
            base.SetPositionFromEdge(RectTransform.Edge.Right, Margin);

            var nextTurnButton = new TextButton("NextTurnButton", "Next Turn", OnNextTurnButton, Transform);
            AddContent(nextTurnButton);
        }

        private void OnNextTurnButton()
        {
            _coroutineRunner.StartCoroutine(ProcessTurn());
        }

        private IEnumerator ProcessTurn()
        {
            GameController.GetInstance().SetControlMode(ControlMode.Wait);
            var turnCamera = GameObject.Find("TurnCamera").GetComponent<Camera>();
            var gameCamera = GameObject.Find("GameCamera").GetComponent<Camera>();
            turnCamera.enabled = true;
            gameCamera.enabled = false;
            yield return 0;

            yield return GameState.GetInstance().ProcessTurn();

            gameCamera.enabled = true;
            turnCamera.enabled = false;
            GameController.GetInstance().SetControlMode(ControlMode.Standard);
        }
    }
}