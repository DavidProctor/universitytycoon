﻿using System.Linq;
using Scripts.UI.Hud;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;

namespace Scripts.UI
{
    public class ObjectivesHudPanel : HudComponent
    {
        private readonly int _panelWidth = 175;
        private readonly int _panelHeight = 20;
        private readonly Color _backgroundColor = Palette.TransparentPanelBackground;
        
        private readonly IGameState _gameState = GameState.GetInstance();

        private int _lastObjectiveCount = 0;

        public ObjectivesHudPanel() 
            : base("ObjectivesHudPanel", RectTransform.Edge.Top, null)
        {
            SetBackgroundColor(_backgroundColor);
            ReDraw();
            GuiComponentBehavior.SetOnUpdateAction(OnUpdate);
        }

        private void MakeObjectivePanel(IObjective objective, int level)
        {
            AddSingleObjectivePanel(objective, level);
            foreach (var subObjective in objective.SubObjectives)
            {
                MakeObjectivePanel(subObjective, level + 1);
            }
        }

        private void AddSingleObjectivePanel(IObjective objective, int level)
        {
            var text = objective.IsComplete() ? $"[X] {objective}" : $"[ ] {objective}";
            var objectivePanel = new TextPanel($"{objective}ObjectivePanel", Transform)
            {
                TextContent = text
            };
            objectivePanel.SetAbsoluteDimensions(_panelWidth - Margin * level, _panelHeight);
            objectivePanel.SetPositionFromEdge(RectTransform.Edge.Left, Margin * level);
            objectivePanel.TextComponent.alignment = TextAnchor.MiddleLeft;
            AddContent(objectivePanel);
        }

        private void ReDraw()
        {
            ResetContents();
             _gameState.Objectives.ForEach(objective => MakeObjectivePanel(objective, 1));
            SetAbsoluteDimensions(_panelWidth, _panelHeight * Contents.Count + Margin * Contents.Count + Margin);
            base.SetPositionFromEdge(RectTransform.Edge.Top, 50);
            base.SetPositionFromEdge(RectTransform.Edge.Left, 0);
        }

        private void OnUpdate()
        {
            var count = _gameState.Objectives.Count();
            if (count != _lastObjectiveCount)
            {
                ReDraw();
                _lastObjectiveCount = count;
            }

            if (_gameState.Objectives.Any(objective => objective.IsComplete()))
            {
                _gameState.ClearCompletedObjectives();
                ReDraw();
            }
        }
    }
}