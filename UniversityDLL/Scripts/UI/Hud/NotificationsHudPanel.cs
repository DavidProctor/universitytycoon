﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.UI.Hud;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.UI;

namespace Scripts.UI
{
    public class NotificationsHudPanel : HudComponent
    {
        private readonly NotificationCollection _notifications = NotificationCollection.Instance;
        private readonly float _notificationDuration = 10f;

        private int _lastNotificationCount = int.MaxValue;
        private bool _isExpanded;

        public NotificationsHudPanel() 
            : base("NotificationsHudPanel", RectTransform.Edge.Top)
        {
            SetBackgroundColor(Palette.TransparentPanelBackground);
            SetUpdateAction(OnUpdate);
        }

        private void OnUpdate()
        {
            _notifications.ClearElapsedNotifications();
            if( _notifications.Count != _lastNotificationCount )
            {
                _lastNotificationCount = _notifications.Count;
                Draw();
            }
        }

        private void Draw()
        {
            if (_isExpanded)
            {
                DrawExpanded();
            }
            else
            {
                DrawCollapsed();
            }
        }

        private void DrawExpanded()
        {
            DrawRange(_notifications, "Collapse", DrawCollapsed);
            _isExpanded = true;
        }

        private void DrawCollapsed()
        {
            DrawRange(_notifications.Where(n => n.SpawnTime > Time.time - _notificationDuration), 
                        "Show All", 
                        DrawExpanded);
            _isExpanded = false;
        }

        private void DrawRange(IEnumerable<Notification> notifications, string buttonText, Action buttonAction)
        {
            ResetContents();

            if( !_notifications.Any() )
            {
                SetAbsoluteDimensions(0, 0);
                return;
            }

            var height = Margin;
            foreach( var notification in notifications )
            {
                var notificationPanel = new NotificationPanel($"{notification.Text}HudPanel", notification, Transform);
                AddContent(notificationPanel);
                height += notificationPanel.Height + Margin;
            }

            var button = new TextButton($"NotificationHudPanel{buttonText}Button", buttonText, buttonAction, Transform);
            button.SetAbsoluteDimensions(185, 35);
            AddContent(button);
            height += button.Height + Margin;

            SetAbsoluteDimensions(button.Width + Margin * 2, height);
            base.SetPositionFromEdge(RectTransform.Edge.Top, 75f);
            base.SetPositionFromEdge(RectTransform.Edge.Right, 0);
        }
    }
}