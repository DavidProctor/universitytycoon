﻿using System.Collections.Generic;
using Scripts.UI.Primitives;

namespace Scripts.UI.Hud
{
    public class Hud
    {
        public IEnumerable<HudComponent> HudComponents { get { return _hudComponents; } }

        private readonly List<HudComponent> _hudComponents;
        private readonly DebugPanel _debugPanel = new DebugPanel();

        public Hud()
        {
            _hudComponents = new List<HudComponent>
            {
                new HudTopPanel(),
                new HudBottomPanel(),
                new ObjectivesHudPanel(),
                new NotificationsHudPanel(),
                new TurnActionsPanel(),
            };
        }

        public void Hide()
        {
            foreach (var hudComponent in _hudComponents)
            {
                hudComponent.CanvasGroup.alpha = 0f;
            }
        }

        public void Show()
        {
            foreach( var hudComponent in _hudComponents )
            {
                hudComponent.CanvasGroup.alpha = 1f;
            }
        }
    }
}