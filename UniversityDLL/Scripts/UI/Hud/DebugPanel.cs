﻿using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL;

namespace Scripts.UI.Hud
{
    class DebugPanel : TextPanel
    {
        public DebugPanel() 
            : base("DebugPanel")
        {
            TextComponent.alignment = TextAnchor.MiddleRight;
            base.SetPositionFromEdge(RectTransform.Edge.Top, 50);
            base.SetPositionFromEdge(RectTransform.Edge.Right, Margin);

            SetUpdateAction(UpdateText);
        }

        private void UpdateText()
        {
            TextContent = Utils.DebugText;
        }
    }
}
