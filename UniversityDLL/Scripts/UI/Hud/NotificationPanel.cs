﻿using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.UI;

namespace Scripts.UI
{
    public class NotificationPanel : ContainerPanel
    {
        public NotificationPanel(string name, Notification notification, RectTransform parent = null) 
            : base(name, RectTransform.Edge.Left, parent)
        {
            var textPanel = new TextPanel($"{name}TextPanel", Transform)
            {
                TextContent = notification.Text
            };
            textPanel.SetAbsoluteDimensions(125, 35);
            AddContent(textPanel);

            var dismissButton = new TextButton($"{name}DismissButton", 
                                                  "X",
                                                  () => NotificationCollection.Instance.Remove(notification), 
                                                  Transform);
            dismissButton.SetAbsoluteDimensions(35, 35);
            AddContent(dismissButton);

            base.SetAbsoluteDimensions(185, 45);
            SetBackgroundColor(Palette.PanelBackground);
        }
    }
}