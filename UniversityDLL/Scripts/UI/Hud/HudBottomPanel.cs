﻿using System;
using System.Collections.Generic;
using Scripts.ControlMediation;
using Scripts.Loading;
using Scripts.UI.Dialogs;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Previews;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace Scripts.UI.Hud
{
    class HudBottomPanel : HudComponent
    {
        private class TextMenuOption
        {
            public string Text { get; }
            private readonly Action _action;

            public TextMenuOption(string text, Action action)
            {
                Text = text;
                _action = action;
            }

            public TextButton GetButton(string name, RectTransform parent)
            {
                return new TextButton(name, Text, _action, parent);
            }
        }

        private class FrameMenuOption
        {
            public string SpritePath { get; }

            private readonly string _text;
            private readonly Sprite _sprite;
            private readonly Action _action;

            public FrameMenuOption(string text, string spritePath, Action action)
            {
                _text = text;
                SpritePath = SpritePath;
                _sprite = UiSprites.FindSprite(spritePath);
                _action = action;
            }

            public FrameButton GetButton(string name, RectTransform parent)
            {
                return new FrameButton(name, _sprite, _text, _action, parent);
            }
        }

        private ImageButton _universityLogoTextButton;
        private IEnumerable<TextMenuOption> _subMenuOptions;
        private IEnumerable<FrameMenuOption> _tertiaryMenuOptions;

        private readonly IEnumerable<TextMenuOption> _mainMenuOptions;

        public HudBottomPanel() 
            : base("HudBottomPanel", RectTransform.Edge.Left)
        {
            Margin *= 3;
            base.SetPositionFromEdge(RectTransform.Edge.Left, 0);
            base.SetPositionFromEdge(RectTransform.Edge.Bottom, 0);

            _mainMenuOptions = new[]
            {
                new TextMenuOption("Build", OnBuildButton),
                new TextMenuOption("Purchase", OnPurchaseButton),
                new TextMenuOption("Organize", OnOrganizeButton), 
                new TextMenuOption("Debug", OnDebugButton), 
            };

            ReDraw();
        }

        private void ReDraw()
        {
            ResetContents();
            MakeLogoButton();

            AddContent(_universityLogoTextButton);
            AddContent(MakeTextButtonMenu("HudMainMenu", _mainMenuOptions));
            if(_subMenuOptions != null) AddContent(MakeTextButtonMenu("HudSubMenu", _subMenuOptions));
            if(_tertiaryMenuOptions != null) AddContent(MakeFrameButtonMenu("HudTertiaryMenu", _tertiaryMenuOptions));
        }

        private void MakeLogoButton()
        {
            // ReSharper disable once ObjectCreationAsStatement
            _universityLogoTextButton = new ImageButton("HudBottomLogoButton", "shield-small-red", () => new CreateDepartmentDialog(), Transform);
            _universityLogoTextButton.SetAbsoluteDimensions(125, 125);
            _universityLogoTextButton.SetPositionFromEdge(RectTransform.Edge.Left, Margin);
            _universityLogoTextButton.SetPositionFromEdge(RectTransform.Edge.Bottom, Margin);
        }

        private ContainerPanel MakeTextButtonMenu(string menuName, IEnumerable<TextMenuOption> options)
        {
            var menu = new ContainerPanel(menuName, RectTransform.Edge.Top, Transform);
            foreach (var option in options)
            {
                menu.AddContent(option.GetButton($"{menuName}{option.Text}Button", menu.Transform));
            }
            menu.SetAbsoluteDimensions(TextButton.DefaultWidth, menu.GetHeightFromChildren());
            menu.SetPositionFromEdge(RectTransform.Edge.Bottom, Margin - menu.Margin);

            return menu;
        }

        private ContainerPanel MakeFrameButtonMenu(string menuName, IEnumerable<FrameMenuOption> options)
        {
            var menu = new ContainerPanel(menuName, RectTransform.Edge.Left);
            foreach (var option in options)
            {
                menu.AddContent(option.GetButton($"{menuName}{option.SpritePath}FrameButton", menu.Transform));
            }
            menu.SetAbsoluteDimensions(menu.GetWidthFromChildren(), FrameButton.DefaultHeight);
            menu.SetPositionFromEdge(RectTransform.Edge.Bottom, Margin);

            return menu;
        }

        private void OnBuildButton()
        {
            _subMenuOptions = new List<TextMenuOption>
            {
                new TextMenuOption("Basement", OnBasementButton),
                new TextMenuOption("Floors", OnFloorsButton),
                new TextMenuOption("Walls", OnWallsButton),
                new TextMenuOption("Doors", OnDoorsButton)
            };
            ReDraw();
        }

        private void OnPurchaseButton()
        {
            _subMenuOptions = new[]
            {
                new TextMenuOption("Lecture Seat", MakeStampButton(StampFactory.MakeLectureSeatStamp())),
                new TextMenuOption("Pub Booth", MakeStampButton(StampFactory.MakePubBoothStamp())),
                new TextMenuOption("Lectern", MakeStampButton(StampFactory.MakeLecternStamp())), 
                new TextMenuOption("Office Desk", MakeStampButton(StampFactory.MakeOfficeDeskStamp())), 
                new TextMenuOption("Bookshelf", MakeStampButton(StampFactory.MakeBookshelf1Stamp())), 
                new TextMenuOption("Study Booth", MakeStampButton(StampFactory.MakeStudyBoothStamp())), 
            };
            _tertiaryMenuOptions = null;
            ReDraw();
        }

        private void OnOrganizeButton()
        {
            _subMenuOptions = new[]
            {
                new TextMenuOption("LectureHall", MakeRoomButtonAction(() => new RLectureHall())), 
                new TextMenuOption("Pub", MakeRoomButtonAction(() => new RPub())), 
                new TextMenuOption("Professor's Office", MakeRoomButtonAction(() => new RProfessorOffice())), 
                new TextMenuOption("Administrator's office", MakeRoomButtonAction(() => new RAdminOffice())), 
            };
            _tertiaryMenuOptions = null;
            ReDraw();
        }

        private void OnDebugButton()
        {
            var gameState = GameState.GetInstance();
            foreach (var professor in gameState.Professors)
            {
                Debug.Log($"Professor {professor.Name}");
                foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
                {
                    foreach (var booking in professor.PersonalSchedule.GetBookingsByDay(day))
                    {
                        Debug.Log(booking);
                    }
                }
            }
        }

        private void OnBasementButton()
        {
            _tertiaryMenuOptions = null;
            ReDraw();

            var controls = GameController.GetInstance().Controls;
            var mode = MouseModeFactory.GetBoxBrush("Floors/ConcreteFloor", TileLayer.Basement, 10);
            controls.SetMouseMode(mode);
            controls.SetAltMouseMode(mode);
        }

        private void OnFloorsButton()
        {
            _tertiaryMenuOptions = new []
            {
                new FrameMenuOption("$10", "ConcreteFloorIcon", MakeFloorButtonAction("ConcreteFloor", 10)),
                new FrameMenuOption("$25", "BadLinoleumIcon", MakeFloorButtonAction("BadLinoleumFloor", 25)),
                new FrameMenuOption("$35", "BlueCarpetIcon", MakeFloorButtonAction("BlueCarpetFloor", 35)),
                new FrameMenuOption("$45", "HardwoodIcon", MakeFloorButtonAction("HardwoodFloor", 45)),
            };
            ReDraw();
        }

        private void OnWallsButton()
        {
            _tertiaryMenuOptions = new[]
            {
                new FrameMenuOption("$20", "concrete-wall-icon", MakeWallButtonAction("ConcreteWall", 20)),
                new FrameMenuOption("$25", "boring-wall-icon", MakeWallButtonAction("BoringWall", 25)),
                new FrameMenuOption("$30", "wood-wall-icon", MakeWallButtonAction("WoodWall", 30)),
                new FrameMenuOption("$50", "red-brick-icon", MakeWallButtonAction("BrickWall", 50)),
            };
            ReDraw();
        }

        private void OnDoorsButton()
        {
            _tertiaryMenuOptions = new[]
            {
                new FrameMenuOption("$35", "door-icon", MakeDoorButtonAction("WoodDoor", 35)),
            };
            ReDraw();
        }

        private Action MakeFloorButtonAction(string prototypeName, int price)
        {
            var controls = GameController.GetInstance().Controls;
            return () =>
            {
                controls.SetMouseMode(MouseModeFactory.GetBoxBrush($"Floors/{prototypeName}", TileLayer.Floor, price));
                controls.SetAltMouseMode(MouseModeFactory.GetFloodBrush($"Floors/{prototypeName}", TileLayer.Floor, price));
            };
        }

        private Action MakeWallButtonAction(string prototypeName, int price)
        {
            var controls = GameController.GetInstance().Controls;
            return () =>
            {
                controls.SetMouseMode(MouseModeFactory.GetLineBrush($"Walls/{prototypeName}", TileLayer.Wall, price));
                controls.SetAltMouseMode(MouseModeFactory.GetRectangleBrush($"Walls/{prototypeName}", TileLayer.Wall, price));
            };
        }

        private Action MakeDoorButtonAction(string prototypeName, int price)
        {
            var controls = GameController.GetInstance().Controls;
            var mode = MouseModeFactory.GetDoorLineBrush($"Doors/{prototypeName}", price);
            return () =>
            {
                controls.SetMouseMode(mode);
                controls.SetAltMouseMode(mode);
            };
        }

        private Action MakeStampButton(MouseMode stampMode)
        {
            var controls = GameController.GetInstance().Controls;
            return () =>
            {
                controls.SetMouseMode(stampMode);
                controls.SetAltMouseMode(stampMode);
            };
        }

        private Action MakeRoomButtonAction(Func<Room> defineRoomFunc)
        {
            var controls = GameController.GetInstance().Controls;
            return () =>
            {
                var selection = new FloodSelection((s, r) => s.Room == r.Room && !r.HasLayer(TileLayer.Wall));
                var prototype = new PrefabWrapper("GUIPreview");
                var preview = new Preview(selection, prototype, prototype, TileLayer.NullLayer);
                var brush = new DefineRoomBrush(selection, defineRoomFunc, GameState.GetInstance());

                var mode = new BrushMouseMode(brush, preview);

                controls.SetMouseMode(mode);
                controls.SetAltMouseMode(mode);
            };
        }
    }
}
