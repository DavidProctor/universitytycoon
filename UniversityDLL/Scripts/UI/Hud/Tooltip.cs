﻿using System;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.UI;

namespace Scripts.UI.Hud
{
    public class Tooltip : ContainerPanel
    {
        private static readonly float MouseMargin = 20f;
        private static readonly Color BackgroundColor = Palette.BlueVeryDark;

        private readonly TextPanel _text;

        private IGameEntity _entity;

        public Tooltip()
            : base("Tooltip", RectTransform.Edge.Top)
        {
            _text = new TextPanel($"{Name}TitlePanel", Transform);
            _text.TextComponent.alignment = TextAnchor.MiddleLeft;
            AddContent(_text);

            SetBackgroundColor(BackgroundColor);
            base.SetAbsoluteDimensions(_text.Width + Margin * 2, _text.Height + Margin * 2);
        }

        public void Update(IGameEntity entity, Vector3 position)
        {
            if (entity != _entity)
            {
                _entity = entity;
                UpdateContent(entity);
            }

            UpdateDisplay(position);
        }

        private void UpdateContent(IGameEntity entity)
        {
            _entity = entity;
            var content = TooltipContentFactory.MakeContent(entity);

            if (content == null)
            {
                SetBackgroundColor(new Color(0, 0, 0, 0));
                _text.TextContent = String.Empty;
                return;
            }

            var text = content.Content == null
                ? content.Title
                : $"{content.Title}\n\n{content.Content}";

            SetBackgroundColor(BackgroundColor);
            _text.TextContent = text;
        }

        private void UpdateDisplay(Vector3 position)
        {
            Resize();

            var xPos = GetXPosition(position.x);
            var yPos = GetYPosition(position.y);
            Transform.position = new Vector3(xPos, yPos, 0);
        }

        private float GetXPosition(float mouseX)
        {
            if (Width + MouseMargin + mouseX > Screen.width)
            {
                return mouseX - MiddleX - MouseMargin;
            }
            return mouseX + MiddleX + MouseMargin;
        }

        private float GetYPosition(float mouseY)
        {
            if (mouseY - Height < 0)
            {
                return mouseY + MiddleY;
            }
            return mouseY - MiddleY;
        }

        private void Resize()
        {
            SetAbsoluteDimensions(_text.Width + Margin * 2, _text.Height + Margin * 2);
            _text.Transform.localPosition = Vector3.zero;
        }
    }
}