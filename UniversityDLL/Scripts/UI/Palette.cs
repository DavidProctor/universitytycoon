﻿//#####  Palette URL: http://paletton.com/#uid=75B0D0kllll9Ys0fEoGr1h+y8eF

using UnityEngine;

namespace Scripts.UI
{
    public static class Palette
    {
        public static readonly Color Red = new Color(0.655f, 0.22f, 0.239f); // A7383D
        public static readonly Color RedVeryLight = new Color(0.859f, 0.592f, 0.604f); // DB979A
        public static readonly Color RedLight = new Color(0.757f, 0.388f, 0.404f); // C16367
        public static readonly Color RedDark = new Color(0.553f, 0.086f, 0.11f); // 8D161C
        public static readonly Color RedVeryDark = new Color(0.451f, 0f, 0.024f); // 730006

        public static readonly Color Brown = new Color(0.667f, 0.455f, 0.224f); // AA7439
        public static readonly Color BrownVeryLight = new Color(0.875f, 0.745f, 0.604f); // DFBE9A
        public static readonly Color BrownLight = new Color(0.773f, 0.592f, 0.396f); // C59765
        public static readonly Color BrownDark = new Color(0.561f, 0.333f, 0.086f); // 8F5516
        public static readonly Color BrownVeryDark = new Color(0.459f, 0.239f, 0f); // 753D00

        public static readonly Color Blue = new Color(0.153f, 0.345f, 0.42f); // 27586B
        public static readonly Color BlueVeryLight = new Color(0.384f, 0.506f, 0.549f); // 62818C
        public static readonly Color BlueLight = new Color(0.259f, 0.42f, 0.482f); // 426B7B
        public static readonly Color BlueDark = new Color(0.067f, 0.275f, 0.353f); // 11465A
        public static readonly Color BlueVeryDark = new Color(0.012f, 0.212f, 0.286f); // 033649

        public static readonly Color Green = new Color(0.216f, 0.545f, 0.18f); // 378B2E
        public static readonly Color GreenVeryLight = new Color(0.514f, 0.718f, 0.494f); // 83B77E
        public static readonly Color GreenLight = new Color(0.353f, 0.631f, 0.322f); // 5AA152
        public static readonly Color GreenDark = new Color(0.11f, 0.459f, 0.071f); // 1C7512
        public static readonly Color GreenVeryDark = new Color(0.035f, 0.376f, 0f); // 096000

        public static readonly Color PanelBackground = BlueVeryDark;
        public static readonly Color SecondaryPanelBackground = BlueDark;
        public static readonly Color TransparentPanelBackground = new Color(0.259f, 0.42f, 0.482f, 0.3f);
        public static readonly Color Highlight = Color.yellow;
        public static readonly Color ActiveButton = BlueVeryLight;
        public static readonly Color Text = Color.white;
    }
}