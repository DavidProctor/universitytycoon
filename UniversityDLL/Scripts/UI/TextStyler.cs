﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI
{
    public static class TextStyler
    {
        public static Font HudFont = Resources.Load<Font>("Fonts/Arimo-BoldItalic");

        public static void ApplyHudStyle(Text text)
        {
            text.font = Resources.Load<Font>("Fonts/Arimo-BoldItalic");
            text.fontSize = 14;
            text.color = Palette.Text;
        }
    }
}