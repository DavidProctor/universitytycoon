﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.UI.Primitives
{
    public class UiSprites : MonoBehaviour
    {
        public List<Sprite> Sprites;

        private static UiSprites Instance { get { return GameObject.Find("GameStarter").GetComponent<UiSprites>(); } }

        public static Sprite FindSprite(string name)
        {
            return Instance.Sprites.Find(s => s.name == name);
        }
    }
}