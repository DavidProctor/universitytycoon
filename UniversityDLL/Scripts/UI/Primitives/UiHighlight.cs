﻿using UnityEngine;

namespace Scripts.UI.Primitives
{
    public class UiHighlight : ImagePanel
    {
        private readonly float _minWidth;
        private readonly float _minHeight;

        public UiHighlight(string name, RectTransform parent, RectTransform target) 
            : base(name, parent)
        {
            _minWidth = target.rect.width + 20f;
            _minHeight = target.rect.height + 20f;

            base.SetAbsoluteDimensions(_minWidth, _minHeight);
            Transform.position = target.position;
            Transform.SetAsFirstSibling();

            SetBackgroundColor(Palette.Highlight);
            SetUpdateAction(UpdateSize);
        }

        private void UpdateSize()
        {
            var width = _minWidth + Mathf.Sin(Time.time * 2) * 10f;
            var height = _minHeight + Mathf.Sin(Time.time * 2) * 10f;
            SetAbsoluteDimensions(width, height);
        }
    }
}