﻿using System;
using UnityEngine;

namespace Scripts.UI.Primitives
{
    public sealed class TextButton : ButtonWrapper
    {
        public static readonly int DefaultWidth = 125;
        public static readonly int DefaultHeight = 35;

        public TextPanel TextPanel { get; }

        public TextButton(string name, string text, Action action, RectTransform parent = null)
            : base(name, action, parent)
        {
            base.SetAbsoluteDimensions(DefaultWidth, DefaultHeight);

            TextPanel = new TextPanel($"{name}Text", Transform) {TextContent = text};
            TextPanel.SetAbsoluteDimensions(DefaultWidth, DefaultHeight);

            TextPanel.SetTextColor(Palette.Text);
            TextPanel.SetFontSize(14);
            TextStyler.ApplyHudStyle(TextPanel.TextComponent);
        }

        public override void SetAbsoluteDimensions(float width, float height)
        {
            base.SetAbsoluteDimensions(width, height);
            TextPanel?.SetAbsoluteDimensions(width, height);
        }
    }
}