﻿using System;
using SLS.Widgets.Table;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.Scheduling;

namespace Scripts.UI.Primitives
{
    public class ScheduleTablePanel : GuiComponent
    {
        public ScheduleTablePanel(string name, IRoomSchedule schedule, RectTransform parent = null) 
            : base(name, parent)
        {
            var table = GameObject.AddComponent<Table>();
            table.reset();
            table.addTextColumn("Hour");

            foreach (DayOfWeek day in Calendar.Weekdays)
            {
                table.addTextColumn(day.ToString());
            }
            table.initialize();

            for( int hour = 9; hour < 18; ++hour )
            {
                var row = Datum.Body(hour.ToString());
                row.rawObject = hour;
                row.elements.Add($"{hour:D2}:00");
                foreach( var day in Calendar.Weekdays )
                {
                    var dayAndTime = DayAndTime.FromDetails(day, hour);
                    var scheduled = schedule.GetBooking(dayAndTime);
                    row.elements.Add(scheduled == null ? "FREE" : scheduled.Course.Template.Name);
                }
                table.data.Add(row);
            }
            
            table.startRenderEngine();
        }
    }
}