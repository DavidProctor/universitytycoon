﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.Primitives
{
    public abstract class ButtonWrapper : ImagePanel
    {
        public Button Button { get; protected set; }

        protected ButtonWrapper(string name, Action action, RectTransform parent = null)
            : base(name, parent)
        {
            Button = GameObject.AddComponent<Button>();
            Button.onClick.AddListener(() => action());

            Image.color = Color.white;
            Image.type = Image.Type.Sliced;
            Image.sprite = UiSprites.FindSprite("button-base");
            GuiComponentBehavior.SetOnMouseEnterAction(() => Image.sprite = UiSprites.FindSprite("button-hover"));
            GuiComponentBehavior.SetOnMouseLeaveAction(() => Image.sprite = UiSprites.FindSprite("button-base"));
        }
    }
}
