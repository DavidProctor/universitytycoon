﻿using UnityEngine;

namespace Scripts.UI.Primitives
{
    public class HudComponent : ContainerPanel
    {
        public CanvasGroup CanvasGroup { get; }

        public HudComponent(string name, RectTransform.Edge justification, RectTransform parent = null) 
            : base(name, justification, parent)
        {
            CanvasGroup = GameObject.AddComponent<CanvasGroup>();
        }
    }
}