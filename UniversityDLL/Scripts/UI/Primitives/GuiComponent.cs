﻿using System;
using UnityEngine;

namespace Scripts.UI.Primitives
{
    public abstract class GuiComponent
    {
        public static float DefaultMargin = 10f;

        public string Name { get; }
        public RectTransform Parent { get; set; }
        public GameObject GameObject { get; }
        public RectTransform Transform { get; }
        public GuiComponentBehavior GuiComponentBehavior { get; }
        public float Margin { get; set; }
        public int MiddleX { get { return Width/2; } }
        public int MiddleY { get { return Height/2; } }
        public int Width { get { return Mathf.RoundToInt(Transform.rect.width); } }
        public int Height { get { return Mathf.RoundToInt(Transform.rect.height); } }

        protected GuiComponent(string name, RectTransform parent = null)
        {
            Name = name;
            GameObject = new GameObject(name);
            GameObject.AddComponent<CanvasRenderer>();
            Transform = GameObject.AddComponent<RectTransform>();
            GuiComponentBehavior = GameObject.AddComponent<GuiComponentBehavior>();
            GuiComponentBehavior.GuiComponent = this;

            Margin = DefaultMargin;

            Parent = parent ?? GameObject.Find("Canvas").GetComponent<RectTransform>();
            GameObject.transform.SetParent(Parent);

            Transform.anchoredPosition = Vector2.one;
        }

        public virtual void SetAbsoluteDimensions(float width, float height)
        {
            Transform.anchorMin = new Vector2(0.5f, 0.5f);
            Transform.anchorMax = new Vector2(0.5f, 0.5f);
            Transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            Transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
        }

        public virtual void SetPositionFromEdge(RectTransform.Edge edge, float distance)
        {
            var size = edge == RectTransform.Edge.Bottom || edge == RectTransform.Edge.Top
                                ? Transform.rect.height
                                : Transform.rect.width;

            Transform.SetInsetAndSizeFromParentEdge(edge, distance, size);
        }

        public void SetUpdateAction(Action onUpdateAction)
        {
            GuiComponentBehavior.SetOnUpdateAction(onUpdateAction);
        }

        public void Destroy()
        {
            GameObject.Destroy(GameObject);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}