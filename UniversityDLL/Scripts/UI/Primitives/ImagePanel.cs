﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.Primitives
{
    public class ImagePanel : GuiComponent
    {
        public Image Image { get; }

        public ImagePanel(string name, RectTransform transform = null)
            : base(name, transform)
        {
            Image = GameObject.AddComponent<Image>();
            Image.color = Color.clear;
        }

        public void SetImage(Sprite sprite)
        {
            Image.sprite = sprite;
            Image.color = Color.white;
            Image.preserveAspect = true;
        }

        public void SetBackgroundColor(Color color)
        {
            Image.color = color;
        }
    }
}