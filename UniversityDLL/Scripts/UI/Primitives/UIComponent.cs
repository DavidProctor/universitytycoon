﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.Primitives
{
    // ReSharper disable once InconsistentNaming
    class UIObject
    {
        public GameObject GameObject;
        public RectTransform RectTransform;
    }

    class TextObject : UIObject
    {
        public Text Text;
    }

    class ButtonObject : UIObject
    {
        public Button Button;
    }

    // ReSharper disable once InconsistentNaming
    static class UIComponent
    {
        public static UIObject AddChildObject(GameObject parent, string name)
        {
            var child = new GameObject();
            child.name = name;
            child.transform.SetParent(parent.transform);
            child.AddComponent<CanvasRenderer>();

            var transform = child.AddComponent<RectTransform>();
            transform.anchorMax = Vector2.one;
            transform.anchorMin = Vector2.zero;
            transform.anchoredPosition = Vector3.zero;
            transform.sizeDelta = Vector2.zero;

            return new UIObject
            {
                GameObject = child,
                RectTransform = transform
            };
        }

        public static ButtonObject AddButton(GameObject parent, string text, Action onPressAction, string name)
        {
            var button = AddChildObject(parent, name);
            var buttonComponent = button.GameObject.AddComponent<Button>();
            var buttonImage = button.GameObject.AddComponent<Image>();
            buttonImage.color = new Color(0.9f, 0.9f, 0.9f);

            button.RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 150f);
            button.RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 35f);

            AddText(button.GameObject, text, name + "Text");

            buttonComponent.onClick.AddListener(() => onPressAction());

            return new ButtonObject()
            {
                GameObject = button.GameObject,
                RectTransform = button.RectTransform,
                Button = buttonComponent
            };
        }

        public static TextObject AddText(GameObject parent, string text, string name)
        {
            var textObject = AddChildObject(parent, name);
            var textComponent = textObject.GameObject.AddComponent<Text>();
            textComponent.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
            textComponent.text = text;
            textComponent.alignment = TextAnchor.MiddleCenter;
            textComponent.color = Palette.Text;

            return new TextObject
            {
                GameObject = textObject.GameObject,
                RectTransform = textObject.RectTransform,
                Text = textComponent
            };
        }


        public static void uGUIAnchorAroundObject(GameObject o)
        {
            if( o != null && o.GetComponent<RectTransform>() != null )
            {
                var r = o.GetComponent<RectTransform>();
                var p = o.transform.parent.GetComponent<RectTransform>();

                var offsetMin = r.offsetMin;
                var offsetMax = r.offsetMax;
                var _anchorMin = r.anchorMin;
                var _anchorMax = r.anchorMax;

                var parent_width = p.rect.width;
                var parent_height = p.rect.height;

                var anchorMin = new Vector2(_anchorMin.x + (offsetMin.x / parent_width),
                                            _anchorMin.y + (offsetMin.y / parent_height));
                var anchorMax = new Vector2(_anchorMax.x + (offsetMax.x / parent_width),
                                            _anchorMax.y + (offsetMax.y / parent_height));

                r.anchorMin = anchorMin;
                r.anchorMax = anchorMax;

                r.offsetMin = new Vector2(0, 0);
                r.offsetMax = new Vector2(1, 1);
                r.pivot = new Vector2(0.5f, 0.5f);

            }
        }
    }
}
