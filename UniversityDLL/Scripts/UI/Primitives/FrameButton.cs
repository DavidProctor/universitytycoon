﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.Primitives
{
    public class FrameButton : ButtonWrapper
    {
        public static readonly int DefaultWidth = 104;
        public static readonly int DefaultHeight = 104;

        public FrameButton(string name, Sprite image, string text, Action action, RectTransform parent = null) 
            : base(name, action, parent)
        {
            base.SetAbsoluteDimensions(DefaultWidth, DefaultHeight);

            Image.sprite = image;
            
            var frameObject = new GameObject($"{name}FrameImage");
            var frameTransform = frameObject.AddComponent<RectTransform>();
            var frameImage = frameObject.AddComponent<Image>();
            frameObject.transform.SetParent(Transform);
            frameTransform.anchorMin = new Vector2(0.5f, 0.5f);
            frameTransform.anchorMax = new Vector2(0.5f, 0.5f);
            frameTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, DefaultWidth);
            frameTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, DefaultHeight);
            frameImage.type = Image.Type.Sliced;
            frameImage.fillCenter = false;
            frameImage.sprite = UiSprites.FindSprite("button-transparent");
            frameObject.transform.localPosition = Vector3.zero;
            
            var insetImage = new ImagePanel($"{name}FrameInsetImage", Transform);
            insetImage.SetImage(UiSprites.FindSprite("button-price-corner"));
            insetImage.SetAbsoluteDimensions(47, 20);
            insetImage.SetPositionFromEdge(RectTransform.Edge.Right, 2);
            insetImage.SetPositionFromEdge(RectTransform.Edge.Bottom, 2);

            var insetText = new TextPanel($"{name}FrameInsetText", Transform);
            TextStyler.ApplyHudStyle(insetText.TextComponent);
            insetText.SetAbsoluteDimensions(38, 16);
            insetText.SetPositionFromEdge(RectTransform.Edge.Right, 2);
            insetText.SetPositionFromEdge(RectTransform.Edge.Bottom, 4);
            insetText.TextComponent.resizeTextForBestFit = true;
            insetText.TextContent = text;

            GuiComponentBehavior.SetOnMouseEnterAction(() => { });
            GuiComponentBehavior.SetOnMouseLeaveAction(() => { });
        }
    }
}