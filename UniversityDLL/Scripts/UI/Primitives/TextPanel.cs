﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.Primitives
{
    public class TextPanel : GuiComponent
    {
        public Text TextComponent { get; }
        public ContentSizeFitter ContentSizeFitter { get; }

        public string TextContent
        {
            get { return TextComponent.text; }
            set { TextComponent.text = value; }
        }

        public TextPanel(string name, RectTransform parent = null)
            : base(name, parent)
        {
            ContentSizeFitter = GameObject.AddComponent<ContentSizeFitter>();
            TextComponent = GameObject.AddComponent<Text>();
            TextContent = "";
            TextComponent.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            SetFontSize(14);
            SetTextColor(Palette.Text);
            TextComponent.alignment = TextAnchor.MiddleCenter;
            ContentSizeFitter.SetLayoutHorizontal();
            ContentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            ContentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        }

        public void SetTextColor(Color color)
        {
            TextComponent.color = color;
        }

        public void SetAutoSizeText()
        {
            TextComponent.resizeTextForBestFit = true;
        }

        public void SetFontSize(int size)
        {
            TextComponent.resizeTextForBestFit = false;
            TextComponent.fontSize = size;
        }
    }
}