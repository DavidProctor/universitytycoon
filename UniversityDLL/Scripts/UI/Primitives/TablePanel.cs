﻿using System;
using System.Collections.Generic;
using SLS.Widgets.Table;
using UnityEngine;

namespace Scripts.UI.Primitives
{
    public abstract class TablePanel<T> : GuiComponent
    {
        private readonly Table _table;
        private readonly List<Func<T, string>> _displayFunctions = new List<Func<T, string>>();
        private readonly IEnumerable<ColumnConfig<T>> _columnConfigs;
        private readonly Action<T, Column> _clickFunction;

        protected TablePanel(string name, Action<T, Column> clickFunction, IEnumerable<ColumnConfig<T>> columns, RectTransform parent = null) 
            : base(name, parent)
        {
            _table = GameObject.AddComponent<Table>();
            _columnConfigs = columns;
            _clickFunction = clickFunction;
            ResetTable();
        }

        private void AddColumn(ColumnConfig<T> config)
        {
            switch (config.Type)
            {
                case Column.ColumnType.TEXT:
                    _table.addTextColumn(config.Header);
                    break;
                case Column.ColumnType.IMAGE:
                    _table.addTextColumn(config.Header);
                    break;
            }
            
            _displayFunctions.Add(config.DisplayFunction);
        }

        public void AddRow(T datum)
        {
            Datum row = Datum.Body(Guid.NewGuid().ToString());
            row.rawObject = datum;

            foreach (var displayFunction in _displayFunctions)
            {
                row.elements.Add(displayFunction(datum));
            }

            _table.data.Add(row);
        }

        public void ResetTable()
        {
            _displayFunctions.Clear();
            _table.reset();

            foreach (var columnConfig in _columnConfigs)
            {
                AddColumn(columnConfig);
            }

            _table.initialize((datum, column) => _clickFunction((T) datum.rawObject, column));
            _table.startRenderEngine();
        }
    }
}