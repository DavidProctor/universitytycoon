﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts.UI.Primitives
{
    class ImageButton : ButtonWrapper
    {
        public Image ButtonImage { get; }

        private readonly Transform _transform;

        public ImageButton(string name, string spritePath, Action action, RectTransform parent = null) 
            : base(name, action, parent)
        {
            var imageObject = new GameObject($"{name}Image");
            ButtonImage = imageObject.AddComponent<Image>();
            _transform = imageObject.transform;
            ButtonImage.sprite = UiSprites.FindSprite(spritePath);

            _transform.SetParent(Transform);
            base.SetAbsoluteDimensions(ButtonImage.sprite.rect.width + Margin * 2, ButtonImage.sprite.rect.height + Margin * 2);
            _transform.localPosition = Vector3.zero;
        }

        public override void SetAbsoluteDimensions(float width, float height)
        {
            base.SetAbsoluteDimensions(width, height);
            _transform.localPosition = Vector3.zero;
        }
    }
}
