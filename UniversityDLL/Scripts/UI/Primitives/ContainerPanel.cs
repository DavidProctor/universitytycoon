﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.UI.Primitives
{
    public class ContainerPanel : ImagePanel
    {
        public RectTransform.Edge Justification { get; }
        public List<GuiComponent> Contents { get; }

        private float _currentSize = 0f;

        public ContainerPanel(string name, RectTransform.Edge justification, RectTransform parent = null)
            : base (name, parent)
        {
            Contents = new List<GuiComponent>();
            Justification = justification;
        }

        public void AddContent(GuiComponent content)
        {
            _currentSize += Margin;
            Contents.Add(content);
            content.Parent = Transform;
            content.SetPositionFromEdge(Justification, _currentSize);
            if (Justification == RectTransform.Edge.Bottom || Justification == RectTransform.Edge.Top)
            {
                _currentSize += content.Transform.rect.height;
            }
            else
            {
                _currentSize += content.Transform.rect.width;
            }
        }

        public int GetMaximumSizeForContent(GuiComponent content)
        {
            var otherSizes = Margin;
            foreach (var guiComponent in Contents)
            {
                if(guiComponent == content) continue;

                if (Justification == RectTransform.Edge.Bottom || Justification == RectTransform.Edge.Top)
                {
                    otherSizes += guiComponent.Transform.rect.height + Margin;
                }
                else
                {
                    otherSizes += guiComponent.Transform.rect.width + Margin;
                }
            }
            return Mathf.RoundToInt(Transform.rect.height - otherSizes);
        }

        public int GetWidthFromChildren()
        {
            return GetDimensionFromChildren(guiComponent => guiComponent.Width);
        }

        public int GetHeightFromChildren()
        {
            return GetDimensionFromChildren(guiComponent => guiComponent.Height);
        }

        private int GetDimensionFromChildren(Func<GuiComponent, int> getMarginalValue)
        {
            var marginInt = Mathf.RoundToInt(Margin);
            var result = marginInt;
            foreach (var guiComponent in Contents)
            {
                result += getMarginalValue(guiComponent) + marginInt;
            }
            return result;
        }

        protected void ResetContents()
        {
            foreach( var button in Contents )
            {
                UnityEngine.Object.Destroy(button.GameObject);
            }
            Contents.Clear();
            _currentSize = 0f;
        }
    }
}