﻿using UnityEngine;

namespace Scripts.UI.Primitives
{
    public class RadioButtonPanel : ContainerPanel
    {
        private TextButton _selected;
        private Color _selectedColor = Palette.Highlight;
        private Color _unselectedColor = Palette.ActiveButton;

        public RadioButtonPanel(string name, RectTransform.Edge justification, RectTransform parent = null) 
            : base(name, justification, parent)
        {
        }

        public void SetSelected(TextButton selected)
        {
            _selected?.SetBackgroundColor(_unselectedColor);
            _selected = selected;
            _selected.SetBackgroundColor(_selectedColor);
        }

        public void Deselect()
        {
            _selected?.SetBackgroundColor(_unselectedColor);
            _selected = null;
        }

        public void SetSelectedColor(Color color)
        {
            _selectedColor = color;
        }

        public void SetUnselectedColor(Color color)
        {
            _unselectedColor = color;
        }
    }
}