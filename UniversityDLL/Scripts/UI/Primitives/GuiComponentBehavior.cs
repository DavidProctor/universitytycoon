﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scripts.UI.Primitives
{
    public class GuiComponentBehavior : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public GuiComponent GuiComponent { get; set; }

        private Action _onUpdate = () => { };
        private Action _onMouseEnter = () => { };
        private Action _onMouseLeave = () => { };

        public void Update()
        {
            _onUpdate();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _onMouseEnter();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _onMouseLeave();
        }

        public void SetOnUpdateAction(Action onUpdateAction)
        {
            if(onUpdateAction == null) throw new ArgumentNullException(nameof(onUpdateAction));

            _onUpdate = onUpdateAction;
        }

        public void SetOnMouseEnterAction(Action onMouseEnterAction)
        {
            if(onMouseEnterAction == null) throw new ArgumentNullException(nameof(onMouseEnterAction));

            _onMouseEnter = onMouseEnterAction;
        }

        public void SetOnMouseLeaveAction(Action onMouseLeaveAction)
        {
            if(onMouseLeaveAction == null) throw new ArgumentNullException(nameof(onMouseLeaveAction));

            _onMouseLeave = onMouseLeaveAction;
        }
    }
}