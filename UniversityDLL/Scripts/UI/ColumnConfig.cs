﻿using System;
using SLS.Widgets.Table;

namespace Scripts.UI
{
    public class ColumnConfig<T>
    {
        public Column.ColumnType Type { get; set; }
        public string Header { get; set; }
        public Func<T, string> DisplayFunction { get; set; }

        public override string ToString()
        {
            return $"Column '{Header}'";
        }
    }
}