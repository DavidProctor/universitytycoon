﻿using UnityEngine;
using UniversityDLL.People;

namespace Scripts.People
{
    public class PathfinderTestCharacterBehavior : MonoBehaviour, ICharacterBehavior
    {
        public ICharacter Character { get; set; }

        private PersonMovement _personMovement;

        public void Start()
        {
            _personMovement = gameObject.AddComponent<PersonMovement>();
        }

        public void Update()
        {
            
        }
    }
}