﻿using System.Linq;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.ActivityPoints;
using UniversityDLL.GameMangement;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;

namespace Scripts.People
{
    class CharacterBehavior : MonoBehaviour, ICharacterBehavior
    {
        public ICharacter Character { get; set; }
        private ICalendar _calendar;
        private PersonMovement _personMovement;
        private TimeUnit _stayUntil;

        public void Start()
        {
            _personMovement = gameObject.AddComponent<PersonMovement>();
            _calendar = GameState.GetInstance().Calendar;
            _stayUntil = _calendar.CurrentTime;
        }

        private ActivityPoint _point;

        public void Update()
        {
            if (Character == null) return;

            var dayAndTime = _calendar.CurrentTime.DayAndTime;
            var location = Character.PersonalSchedule.GetLocationAtTime(dayAndTime);
            if (location != null)
            {
                if (!_personMovement.IsSpawned)
                {
                    var seat = location.GetAvailableActivityPoint<ClassSeatActivityPoint>();
                    if (seat.TrySetOccupant(Character))
                    {
                        _point = seat;
                        Character.Spawn(seat.Tile);
                    }
                }
            }
            else if (_personMovement.IsSpawned)
            {
                _point?.Vacate();
                _point = null;
                Character.Despawn();
            }
        }

        public void Update2()
        {
            if(!_personMovement.IsMoving && _stayUntil.CompareTo(_calendar.CurrentTime) < 0)
            {
                TrySetDestination();
            }
        }

        private void TrySetDestination()
        {
            if( Character == null ) return;
                
            var dayAndTime = _calendar.CurrentTime.DayAndTime;
            var location = Character.PersonalSchedule.GetLocationAtTime(dayAndTime);
            if(location == null)
            {
                if (!_personMovement.IsSpawned)
                {
                    return;
                }

                var pubs = GameState.GetInstance().Rooms.Where(r => r is RPub);

                if( pubs.Any() && Utils.IsTrueWithChance(20) && _personMovement.IsSpawned)
                {
                    var pub = pubs.GetRandom();
                    _stayUntil = _calendar.CurrentTime.AddHours(new[] {1, 2, 3, 4}.GetRandom());
                    var activityPoint = pub.GetAvailableActivityPoint<PubSeatActivityPoint>();
                    Debug.Log("Available activity point: " + activityPoint);
                    if (!activityPoint.TrySetOccupant(Character))
                    {
                        return;
                    }
                    _personMovement.SetDestination(activityPoint);
                    return;
                }
                
                Debug.Log("About to despawn");
                _personMovement.GetCurrentTile().ActivityPoint?.Vacate();
                Debug.Log("Vacated");
                Character.Despawn();
                return;
            }
            
            var seat = location.GetAvailableActivityPoint<ClassSeatActivityPoint>();
            Debug.Log("Got seat: " + seat);
            if (!seat.TrySetOccupant(Character))
            {
                Debug.Log("Fucked up");
                return;
            }
            _personMovement.SetDestination(seat);
        }
    }
}
