﻿using UnityEngine;

namespace Scripts
{
    public class Prototypes : MonoBehaviour
    {
        public static GameObject Get(string path)
        {
            return Resources.Load(path) as GameObject;
        }

        public static GameObject GetMesh(string path)
        {
            var gameObject = new GameObject(path);
            var model = Instantiate(Resources.Load(path) as GameObject);
            model.transform.SetParent(gameObject.transform);

            gameObject.layer = LayerMask.NameToLayer("ClickTarget");
            var collider = gameObject.AddComponent<CapsuleCollider>();
            collider.center = new Vector3(0, 1f, 0);

            return gameObject;
        }
    }
}
