﻿using System;
using Scripts.ControlMediation;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;
using Object = UnityEngine.Object;

namespace Scripts
{
    public class ModelWrapper : IGameObject
    {
        public string Name { get; set; }

        public GameObject GameObject
        {
            get
            {
                if( _instance == null )
                {
                    _instance = Prototypes.GetMesh(_prototypePath);
                    if( _instance == null )
                    {
                        throw new NullReferenceException("NullLayer: " + _prototypePath);
                    }
                }
                return _instance;
            }
        }

        public Orientation Orientation { get { return _orientationTracker.Orientation; } }

        private readonly string _prototypePath;
        private readonly OrientationTracker _orientationTracker = new OrientationTracker();

        [NonSerialized]
        private GameObject _instance;

        public ModelWrapper(string prototypePath)
        {
            _prototypePath = prototypePath;
        }

        public IGameObject GetClone()
        {
            var clone = new ModelWrapper(_prototypePath) { Name = Name };
            clone.SetOrientation(Orientation);
            return clone;
        }

        public void SetClickableObject(IGameEntity entity)
        {
            if( _instance == null )
            {
                Render(0, 10000f, 0);
                Hide();
            }

            if( _instance?.GetComponent<Clickable>() != null )
            {
                throw new InvalidOperationException("Instance already has a Clickable component");
            }

            var clickable = _instance?.AddComponent<Clickable>();
            clickable?.SetGameEntity(entity);
        }

        public IGameObject Render(float x, float y, float z)
        {
            Show();
            if( _instance == null )
            {
                _instance = Prototypes.GetMesh(_prototypePath);
                RotateToOrientation();
            }
            if( InstanceHasMoved(x, y, z) )
            {
                _instance.transform.position = new Vector3(x, y, z);
            }
            return this;
        }

        public IGameObject Render(float x, float y, float z, Color color)
        {
            Render(x, y, z);
            foreach( var renderer in _instance.GetComponentsInChildren<Renderer>() )
            {
                renderer.material.color = color;
            }
            return this;
        }

        public void SetOrientation(Orientation orientation)
        {
            _orientationTracker.SetOrientation(orientation);
            RotateToOrientation();
        }

        public void Destroy()
        {
            Object.Destroy(_instance);
        }

        private bool InstanceHasMoved(float x, float y, float z)
        {
            return !Utils.FloatEquals(x, _instance.transform.position.x)
                || !Utils.FloatEquals(z, _instance.transform.position.z)
                || !Utils.FloatEquals(y, _instance.transform.position.y);
        }

        public void Hide()
        {
            if( _instance != null )
            {
                _instance.SetActive(false);
            }
        }

        public void Show()
        {
            if( _instance != null )
            {
                _instance.SetActive(true);
            }
            var layer = LayerMask.NameToLayer("ClickTarget");
            GameObject.ActOnGameObjectAndChildren(o => o.layer = layer);
        }

        private void RotateToOrientation()
        {
            if( _instance == null )
            {
                return;
            }

            switch( Orientation )
            {
                case Orientation.Forward:
                    _instance.transform.rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case Orientation.Right:
                    _instance.transform.rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case Orientation.Reverse:
                    _instance.transform.rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case Orientation.Left:
                    _instance.transform.rotation = Quaternion.Euler(0, 270, 0);
                    break;
                default:
                    throw new InvalidOperationException($"Cannot set rotation to value {Orientation}");
            }
        }
    }
}