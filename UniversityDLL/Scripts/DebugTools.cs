﻿using System;
using UnityEngine;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace Scripts
{
    public class DebugTools : MonoBehaviour
    {
        public void OnDrawGizmos()
        {
            DrawTileConnectionGizmos();
        }

        private void ForEachTile(Action<ITile> action)
        {
            var gameState = GameState.GetInstance();
            for( int x = 0; x < gameState.Grid.Width; ++x )
            {
                for( int z = 0; z < gameState.Grid.Height; z++ )
                {
                    action(gameState.Grid.GetTile(x, z));
                }
            }
        }

        private void DrawTileConnectionGizmos()
        {
            ForEachTile(t => {
                Gizmos.color = Color.cyan;
                foreach( var connection in t.Connections )
                {
                    var other = connection.Nodes[0] == t ? connection.Nodes[1] : connection.Nodes[0];
                    var otherTile = other as ITile;
                    var thisPos = new Vector3(t.X, 0, t.Z);
                    var otherPos = new Vector3(otherTile.X, 0, otherTile.Z);

                    Gizmos.DrawLine(thisPos, otherPos);
                }
            });
        }

        private void DrawTileNeighborGizmos()
        {
            ForEachTile(t => {
                Gizmos.color = Color.magenta;
                foreach( var neighbor in t.Neighbors )
                {
                    var thisPos = new Vector3(t.X, 0, t.Z);
                    var neighboringTile = neighbor as ITile;
                    var otherPos = new Vector3(neighboringTile.X, 0, neighboringTile.Z);
                    var end = LerpByDistance(thisPos, otherPos, 0.3f);

                    Gizmos.DrawLine(thisPos, end);
                }
            });
        }

        private void DrawLongConnectionGizmos()
        {
            ForEachTile(t => {
                Gizmos.color = Color.cyan;
                foreach( var connection in t.Connections )
                {
                    var other = connection.Nodes[0] == t ? connection.Nodes[1] : connection.Nodes[0];
                    var otherTile = other as ITile;
                    var thisPos = new Vector3(t.X, 0, t.Z);
                    var otherPos = new Vector3(otherTile.X, 0, otherTile.Z);
                    if (Vector3.Distance(thisPos, otherPos) > 1.1f)
                    {
                        Gizmos.DrawLine(thisPos, otherPos);
                    }
                }
            });
        }

        public Vector3 LerpByDistance(Vector3 start, Vector3 end, float x)
        {
            Vector3 P = x * Vector3.Normalize(end - start) + start;
            return P;
        }
    }
}