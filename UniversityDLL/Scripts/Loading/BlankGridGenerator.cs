﻿using Scripts.UI;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace Scripts.Loading
{
    public class BlankGridGenerator : IGridGenerator
    {
        public int Width { get; }
        public int Height { get; }

        public BlankGridGenerator(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public IGrid GenerateGrid()
        {
            var grid = new Grid(Width, Height);
            var grassTiles = new[]
            {
                new PrefabWrapper("Grass/Grass1"),
                new PrefabWrapper("Grass/Grass2"),
                new PrefabWrapper("Grass/Grass3")
            };
            var outdoors = ROutdoors.GetInstance();

            for( int x = 0; x < Width; ++x )
            {
                for( int z = 0; z < Height; ++z )
                {
                    var tileContent = new TileContentBuilder()
                                            .SetLayer(TileLayer.Ground)
                                            .SetPrototype(grassTiles.GetRandom().GetClone())
                                            .Build();
                    tileContent.GameObject.GameObject.GetComponentInChildren<MeshRenderer>().sharedMaterial.color =
                        Palette.Green;
                    var tile = new Tile(new TileContentCollection(tileContent), outdoors);
                    grid.SetTile(x, z, tile);
                    tile.TileContentCollection.Render();
                }
            }

            return grid;
        }
    }
}
