﻿using System;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;

namespace Scripts.Loading
{
    [Serializable]
    public class GameObjectFactory : IGameObjectFactory
    {
        public IGameObject MakeModelWrapper(string modelPath)
        {
            return new ModelWrapper(modelPath);
        }
    }
}