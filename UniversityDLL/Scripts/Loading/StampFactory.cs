﻿using UniversityDLL;
using UniversityDLL.ActivityPoints;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Previews;
using UniversityDLL.Tiles;

namespace Scripts.Loading
{
    public static class StampFactory
    {
        private static readonly IGrid Grid = GameState.GetInstance().Grid;
        private static readonly IPathProvider PathProvider = UniversityDLL.Tiles.PathProvider.GetInstance();

        public static StampMouseMode MakePubBoothStamp()
        {
            var mouseMode = new StampMouseMode(Grid);

            var seat1Content = TileContentFactory.MakePubBoothSeat(Orientation.Forward);
            mouseMode.SetMouseModeAtPosition(0, 0, MakeSeatBrushMouseMode(Grid, seat1Content, PathProvider));
            var seat2Content = TileContentFactory.MakePubBoothSeat(Orientation.Forward);
            mouseMode.SetMouseModeAtPosition(1, 0, MakeSeatBrushMouseMode(Grid, seat2Content, PathProvider));
            var seat3Content = TileContentFactory.MakePubBoothSeat(Orientation.Reverse);
            mouseMode.SetMouseModeAtPosition(0, 2, MakeSeatBrushMouseMode(Grid, seat3Content, PathProvider));
            var seat4Content = TileContentFactory.MakePubBoothSeat(Orientation.Reverse);
            mouseMode.SetMouseModeAtPosition(1, 2, MakeSeatBrushMouseMode(Grid, seat4Content, PathProvider));

            var table1Content = TileContentFactory.MakePubBoothTable(Orientation.Forward);
            mouseMode.SetMouseModeAtPosition(0, 1, MakeTableBrushMouseMode(Grid, table1Content));
            var table2Content = TileContentFactory.MakePubBoothTable(Orientation.Forward);
            mouseMode.SetMouseModeAtPosition(1, 1, MakeTableBrushMouseMode(Grid, table2Content));

            return mouseMode;
        }

        public static StampMouseMode MakeLectureSeatStamp()
        {
            var mouseMode = new StampMouseMode(Grid);

            var prefab = new PrefabWrapper("Objects/LectureHallSeat/object");
            var tileContent = new TileContentBuilder()
                .SetLayer(TileLayer.ActivityPoint)
                .SetPrototype(prefab)
                .Build();
            var selection = new BoxSelection(Grid);
            var activityPoint = new ClassSeatActivityPoint(tileContent);
            var brush = new AddActivityPointBrush(selection, tileContent, PathProvider, activityPoint);
            var preview = new Preview(selection, tileContent.GameObject, tileContent.GameObject, tileContent.Layer);
            var mode = new BrushMouseMode(brush, preview);

            mouseMode.SetMouseModeAtPosition(0, 0, mode);

            return mouseMode;
        }

        public static StampMouseMode MakeLecternStamp()
        {
            var mouseMode = new StampMouseMode(Grid);

            var emptyObject = new PrefabWrapper("EmptyGameObject");
            var nullTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.ActivityPoint)
                .SetPrototype(emptyObject)
                .SetRotation(Orientation.Reverse)
                .Build();
            var activityPoint = new LecternActivityPoint(GameState.GetInstance().Calendar, nullTileContent);
            mouseMode.SetMouseModeAtPosition(0, 1, MakeActivityPointBrushMouseMode(nullTileContent, activityPoint));

            var lecternObject = new PrefabWrapper("Objects/Lectern/object");
            var lecternTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.Structure)
                .SetPrototype(lecternObject)
                .Build();
            mouseMode.SetMouseModeAtPosition(0, 0, MakeBuildBrushMouseMode(lecternTileContent));

            return mouseMode;
        }

        public static StampMouseMode MakeBookshelf1Stamp()
        {
            var mouseMode = new StampMouseMode(Grid);

            var bookshelfObject = new PrefabWrapper("Objects/Bookshelves/boolshelf1object");
            var bookshelfTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.Structure)
                .SetPrototype(bookshelfObject)
                .Build();
            var mode = MakeActivityPointBrushMouseMode(bookshelfTileContent, new SpawnerActivityPoint());
            mouseMode.SetMouseModeAtPosition(0, 0, mode);

            return mouseMode;
        }

        public static StampMouseMode MakeStudyBoothStamp()
        {
            var mouseMode = new StampMouseMode(Grid);

            var emptyObject = new PrefabWrapper("EmptyGameObject");
            var nullTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.ActivityPoint)
                .SetPrototype(emptyObject)
                .SetRotation(Orientation.Reverse)
                .Build();
            mouseMode.SetMouseModeAtPosition(0, 1, MakeActivityPointBrushMouseMode(nullTileContent, new StudyBoothActivityPoint(nullTileContent)));

            var boothObject = new PrefabWrapper("Objects/StudyBooth/object");
            var boothTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.Structure)
                .SetPrototype(boothObject)
                .Build();
            mouseMode.SetMouseModeAtPosition(0, 0, MakeBuildBrushMouseMode(boothTileContent));

            return mouseMode;
        }

        public static StampMouseMode MakeOfficeDeskStamp()
        {
            var mouseMode = new StampMouseMode(Grid);
            var nullTileContent = MakeEmptyTileContent();

            mouseMode.SetMouseModeAtPosition(0, 1, MakeBuildBrushMouseMode(nullTileContent));
            mouseMode.SetMouseModeAtPosition(1, 1, MakeActivityPointBrushMouseMode(nullTileContent, new OfficeDeskActivityPoint(nullTileContent)));
            mouseMode.SetMouseModeAtPosition(0, 0, MakeBuildBrushMouseMode(nullTileContent));

            var deskObject = new PrefabWrapper("Objects/OfficeDesk/object");
            var officeDeskTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.ActivityPoint)
                .SetPrototype(deskObject)
                .Build();
            mouseMode.SetMouseModeAtPosition(1, 0, MakeBuildBrushMouseMode(officeDeskTileContent));

            return mouseMode;
        }

        private static BrushMouseMode MakeActivityPointBrushMouseMode(ITileContent content, ActivityPoint activityPoint)
        {
            var selection = new LineSelection(Grid);
            var brush = new AddActivityPointBrush(selection, content, PathProvider, activityPoint);
            var preview = new Preview(selection, content.GameObject, content.GameObject, content.Layer);
            return new BrushMouseMode(brush, preview);
        }

        private static BrushMouseMode MakeBuildBrushMouseMode(ITileContent content)
        {
            var selection = new LineSelection(Grid);
            var score = GameState.GetInstance().Score;
            var brush = new BuildBrush(selection, content, content, score);
            var preview = new Preview(selection, content.GameObject, content.GameObject, content.Layer);
            return new BrushMouseMode(brush, preview);
        }


        private static BrushMouseMode MakeSeatBrushMouseMode(IGrid grid, ITileContent content, IPathProvider pathProvider)
        {
            var selection = new LineSelection(grid);
            var brush = new AddActivityPointBrush(selection, content, pathProvider, new PubSeatActivityPoint(content));
            var preview = new Preview(selection, content.GameObject, content.GameObject, content.Layer);
            return new BrushMouseMode(brush, preview);
        }

        private static BrushMouseMode MakeTableBrushMouseMode(IGrid grid, ITileContent content)
        {
            var selection = new LineSelection(grid);
            var score = GameState.GetInstance().Score;
            var brush = new BuildBrush(selection, content, content, score);
            var preview = new Preview(selection, content.GameObject, content.GameObject, content.Layer);
            return new BrushMouseMode(brush, preview);
        }

        private static ITileContent MakeEmptyTileContent()
        {
            var emptyObject = new PrefabWrapper("EmptyGameObject");
            var nullTileContent = new TileContentBuilder()
                .SetLayer(TileLayer.ActivityPoint)
                .SetPrototype(emptyObject)
                .Build();
            return nullTileContent;
        }
    }
}