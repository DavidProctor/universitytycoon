﻿using System.Collections.Generic;
using UniversityDLL.Academics;

namespace Scripts.Loading
{
    public class TestDataGenerator
    {
        public List<IDepartment> CreateDepartments()
        {
            return new List<IDepartment>
            {
                MakeEnglishDepartment(),
                MakePoliSciDepartment(),
                MakeEngineeringDepartment(),
                MakePhysicsDepartment(),
                MakeVcrRepairDepartment()
            };
        }

        public IDepartment MakeEnglishDepartment()
        {
            var english = new Department("English");

            AddFirstYearCourse(english, "English 101 - Classic Literature");
            AddFirstYearCourse(english, "English 102 - Poetry");
            AddFirstYearCourse(english, "English 103 - Harry Potter in Life and Culture");
            AddFirstYearCourse(english, "English 104 - Drama");
            AddFirstYearCourse(english, "English 120 - Remedial English");
            AddFirstYearCourse(english, "English 121 - University Writing");

            AddSecondYearCourse(english, "English 200 - Medieval Literature");
            AddSecondYearCourse(english, "English 201 - Twentieth Century Literature");
            AddSecondYearCourse(english, "English 202 - Nineteenth Century Literature");
            AddSecondYearCourse(english, "English 203 - Modern Drama");
            AddSecondYearCourse(english, "English 210 - History and Principles of Rhetoric");
            AddSecondYearCourse(english, "English 211 - History and Principles of Criticism");
            AddSecondYearCourse(english, "English 220 - History and Principles of Clickbait");
            AddSecondYearCourse(english, "English 230 - Metrics and Prosody");
            AddSecondYearCourse(english, "English 240 - English for Scientists");

            AddThirdYearCourse(english, "English 300 - Premodern English");
            AddThirdYearCourse(english, "English 310 - Chaucer");
            AddThirdYearCourse(english, "English 311 - Milton");
            AddThirdYearCourse(english, "English 312 - Shakespeare");
            AddThirdYearCourse(english, "English 331 - Literature of Popular Visible Minorities");
            AddThirdYearCourse(english, "English 336 - Literature of English as a Second Language");
            AddThirdYearCourse(english, "English 341 - Eighteeth Century British Novels");
            AddThirdYearCourse(english, "English 343 - Eighteenth Century American Literature");
            AddThirdYearCourse(english, "English 344 - Diasporic Literature");
            AddThirdYearCourse(english, "English 370 - Stephen King and the Human Condition");
            AddThirdYearCourse(english, "English 371 - Pretentious Modern Literature");
            AddThirdYearCourse(english, "English 360 - Poetry, 1600 - present");
            AddThirdYearCourse(english, "English 352 - Women in Drama");
            AddThirdYearCourse(english, "English 353 - Drama in Women");

            AddFourthYearCourse(english, "English 400 - Advanced Old English");
            AddFourthYearCourse(english, "English 410 - Advanced Chaucer");
            AddFourthYearCourse(english, "English 411 - Advanced Milton");
            AddFourthYearCourse(english, "English 412 - Advanced Shakespeare");
            AddFourthYearCourse(english, "English 420 - Poetry Involving Snakes");
            AddFourthYearCourse(english, "English 421 - Poetry Involving the Moon");
            AddFourthYearCourse(english, "English 425 - Pre-modern Poetry");
            AddFourthYearCourse(english, "English 430 - Slavery Narratives");
            AddFourthYearCourse(english, "English 440 - Advanced North American Literature");
            AddFourthYearCourse(english, "English 441 - Advanced Harry Potter");
            AddFourthYearCourse(english, "English 442 - Writing About Television");
            AddFourthYearCourse(english, "English 462 - Topics in Critical Theory");
            AddFourthYearCourse(english, "English 463 - Professor Takes You For Coffee");
            AddFourthYearCourse(english, "English 470 - Directed Studies");
            AddFourthYearCourse(english, "English 471 - Field School 1");
            AddFourthYearCourse(english, "English 472 - Field School 2");

            return english;
        }

        public IDepartment MakePoliSciDepartment()
        {
            var poliSci = new Department("Political Science");
            AddFirstYearCourse(poliSci, "Introduction to Politics and Government");
            AddFirstYearCourse(poliSci, "Introduction to Justice");
            AddFirstYearCourse(poliSci, "Principles of Left-Wing Third Parties");

            AddSecondYearCourse(poliSci, "Quantitative Research in Political Science");
            AddSecondYearCourse(poliSci, "Soft Research in Political Science");
            AddSecondYearCourse(poliSci, "Introduction to Political Philosophy");
            AddSecondYearCourse(poliSci, "Introduction to Pinkoism");
            AddSecondYearCourse(poliSci, "Introduction to Amoral Warmongering");
            AddSecondYearCourse(poliSci, "Comparative Politics");
            AddSecondYearCourse(poliSci, "National Politics");
            AddSecondYearCourse(poliSci, "International Politics During the Cold War and Today");
            AddSecondYearCourse(poliSci, "Introduction to Social Engineering");
            AddSecondYearCourse(poliSci, "Introduction to Electioneering");

            AddThirdYearCourse(poliSci, "Advanced Quantitative Research in Political Science");
            AddThirdYearCourse(poliSci, "Advanced Soft Research in Political Science");
            AddThirdYearCourse(poliSci, "Ancient Political Philosophy");
            AddThirdYearCourse(poliSci, "Political Philosophy, 1600 to Today");
            AddThirdYearCourse(poliSci, "Practical Euphemisms for Marxism");
            AddThirdYearCourse(poliSci, "Supply-Side Political Economy");
            AddThirdYearCourse(poliSci, "Conflict Studies");
            AddThirdYearCourse(poliSci, "Ethics in National Politics");
            AddThirdYearCourse(poliSci, "Ethics in International Politics");
            AddThirdYearCourse(poliSci, "Chinese Politics");
            AddThirdYearCourse(poliSci, "British Politics");
            AddThirdYearCourse(poliSci, "US Politics");
            AddThirdYearCourse(poliSci, "Advanced International Politics");
            AddThirdYearCourse(poliSci, "Advanced Social Engineering");
            AddThirdYearCourse(poliSci, "Advanced Electioneering");

            AddFourthYearCourse(poliSci, "Flat-out Advanced Statistics");
            AddFourthYearCourse(poliSci, "Jurgen Habermas Consumes You");
            AddFourthYearCourse(poliSci, "Platonic Studies");
            AddFourthYearCourse(poliSci, "Marxist Studies");
            AddFourthYearCourse(poliSci, "H.L. Mencken Studies");
            AddFourthYearCourse(poliSci, "Practical Champagne Socialism");
            AddFourthYearCourse(poliSci, "Economic Oppression: Theory and Best Practices");
            AddFourthYearCourse(poliSci, "Nuclear Strategy and Arms Control");
            AddFourthYearCourse(poliSci, "Political Islam");
            AddFourthYearCourse(poliSci, "Special Topics in Comparative Politics 1");
            AddFourthYearCourse(poliSci, "Special Topics in Cmpmarative Politics 2");
            AddFourthYearCourse(poliSci, "Field School 1");
            AddFourthYearCourse(poliSci, "Field School 2");
            AddFourthYearCourse(poliSci, "Directed Study");
            AddFourthYearCourse(poliSci, "Internationalism");
            AddFourthYearCourse(poliSci, "Advanced Globalization");
            AddFourthYearCourse(poliSci, "Fussing About the Culture");
            AddFourthYearCourse(poliSci, "Social Engineering for Electoral Gain");

            return poliSci;
        }

        public IDepartment MakeEngineeringDepartment()
        {
            var engi = new Department("Engineering");

            AddFirstYearCourse(engi, "Engineering Technology and Society");
            AddFirstYearCourse(engi, "Enginering, Science, and Society");
            AddFirstYearCourse(engi, "Process, Form, and Convention in Provessional Genres");
            AddFirstYearCourse(engi, "Introduction to Instruments and Measurement");
            AddFirstYearCourse(engi, "Introduction to Engineering Analysis");

            AddSecondYearCourse(engi, "Graphical COmmunication for ENgineering");
            AddSecondYearCourse(engi, "Electric Circuits I");
            AddSecondYearCourse(engi, "Mircroelectronics I");
            AddSecondYearCourse(engi, "Software Design and Analysis for Engineers");
            AddSecondYearCourse(engi, "Fundamentals of DIgital Logic & Design");
            AddSecondYearCourse(engi, "Introduction to Computer Design");
            AddSecondYearCourse(engi, "Engineering Measurement and Data Analysis");

            AddThirdYearCourse(engi, "Directed Studies");
            AddThirdYearCourse(engi, "Human Factors and Usability");
            AddThirdYearCourse(engi, "Project Documentation and Group Dynamics");
            AddThirdYearCourse(engi, "Electric Circuits II");
            AddThirdYearCourse(engi, "Electronic Devices");
            AddThirdYearCourse(engi, "Microelectronics II");
            AddThirdYearCourse(engi, "Random Processes");
            AddThirdYearCourse(engi, "Engineering Materials");
            AddThirdYearCourse(engi, "Digital Systems Design");
            AddThirdYearCourse(engi, "Embedded and Real Time System Software");
            AddThirdYearCourse(engi, "Linear Systems");
            AddThirdYearCourse(engi, "Feedback Control Systems");
            AddThirdYearCourse(engi, "Statics and Strength of Materials");
            AddThirdYearCourse(engi, "Introduction to Mechanical Design");
            AddThirdYearCourse(engi, "Introduction to Electro-Mechanical Sensors and Actuators");

            AddFourthYearCourse(engi, "Directed Studies II");
            AddFourthYearCourse(engi, "Documentation, UI Design, and Groups");
            AddFourthYearCourse(engi, "Engineering Ethics, Law, and Professional Practice");
            AddFourthYearCourse(engi, "The Business of Engineering");
            AddFourthYearCourse(engi, "Technologies, CUltures, and a SUstainable World");
            AddFourthYearCourse(engi, "Multimedia COmmunications Engineering");
            AddFourthYearCourse(engi, "Electronic System Design");
            AddFourthYearCourse(engi, "High-Frequency Electronics");
            AddFourthYearCourse(engi, "Digital signal processing");
            AddFourthYearCourse(engi, "Capstone Project");
            AddFourthYearCourse(engi, "VLSI Systems Design");
            AddFourthYearCourse(engi, "Optical and Laser Engineering");
            AddFourthYearCourse(engi, "Orthopaedic and Rehabilitation ENgineering");
            AddFourthYearCourse(engi, "Digita/Medical Image Processing");
            AddFourthYearCourse(engi, "Designing for Reliability");
            AddFourthYearCourse(engi, "Decision Making in Engineering");
            AddFourthYearCourse(engi, "Introduction to Robotics");

            return engi;
        }

        public IDepartment MakePhysicsDepartment()
        {
            var phys = new Department("Physics");

            AddFirstYearCourse(phys, "Introudction to Physics");
            AddFirstYearCourse(phys, "Mechanics and Modern Physics");
            AddFirstYearCourse(phys, "Optics, Electricity and Magnetism");
            AddFirstYearCourse(phys, "Mechanics and Special Relativity");
            AddFirstYearCourse(phys, "Elecricity, Magnetism, and Light");
            AddFirstYearCourse(phys, "Introduction to Astronomy");

            AddSecondYearCourse(phys, "Intermediate Mechanics");
            AddSecondYearCourse(phys, "Electromagnetics");
            AddSecondYearCourse(phys, "Vibrations and Waves");

            AddThirdYearCourse(phys, "Relativity and Quantum Mechanics");
            AddThirdYearCourse(phys, "Intermediate Electricity and Magnetism");
            AddThirdYearCourse(phys, "Electronics and Instrumentation");
            AddThirdYearCourse(phys, "Thermal Physics");
            AddThirdYearCourse(phys, "Energy and the Environment");
            AddThirdYearCourse(phys, "Introduction to Biologics Physics");
            AddThirdYearCourse(phys, "Semiconductor Physics");
            AddThirdYearCourse(phys, "Quantum Mechanics I");
            AddThirdYearCourse(phys, "Introduction to Astrophyics");
            AddThirdYearCourse(phys, "Methods of Theoretical Physics");
            AddThirdYearCourse(phys, "Computational Physics");

            AddFourthYearCourse(phys, "Advanced Mechanics");
            AddFourthYearCourse(phys, "Quantum Mechanics II");
            AddFourthYearCourse(phys, "Electromagnetic Waves");
            AddFourthYearCourse(phys, "Biological Physics");
            AddFourthYearCourse(phys, "Statistical Physics");
            AddFourthYearCourse(phys, "Modern Optics");
            AddFourthYearCourse(phys, "Solid State Physics");
            AddFourthYearCourse(phys, "Particle Physics");
            AddFourthYearCourse(phys, "General Relativity and Gravitation");
            AddFourthYearCourse(phys, "Advanced Quantum Mechanics");
            AddFourthYearCourse(phys, "Introduction to Quantum Field Theory");
            AddFourthYearCourse(phys, "Advanced Electromagnetism");
            AddFourthYearCourse(phys, "Nonlinear Physics");

            return phys;
        }

        public IDepartment MakeVcrRepairDepartment()
        { 
            var vcr = new Department("VCR Repair");

            AddFirstYearCourse(vcr, "History of the Moving Image");
            AddFirstYearCourse(vcr, "VCRs and Culture");
            AddFirstYearCourse(vcr, "Introduction to VCR Repair Instrumentation");
            AddFirstYearCourse(vcr, "Remedial VCR Repair");

            AddSecondYearCourse(vcr, "Introduction to Philosophy of VCR Repair");
            AddSecondYearCourse(vcr, "Professionalism in VCR Repair");
            AddSecondYearCourse(vcr, "Introduction to Ethics in VCR Repair");
            AddSecondYearCourse(vcr, "The Physics of VCR Repair");
            AddSecondYearCourse(vcr, "Special Topics: VHS");
            AddSecondYearCourse(vcr, "Globalization and VCR Repair");
            AddSecondYearCourse(vcr, "VCR Repair Strategy and Arms Control");
            AddSecondYearCourse(vcr, "Introduction to the Business of VCR Repair");

            AddThirdYearCourse(vcr, "Intermediate Philosophy of VCR Repair");
            AddThirdYearCourse(vcr, "Quantitative Methods in VCR Repair");
            AddThirdYearCourse(vcr, "VCR Repair: Marxist Perspectives");
            AddThirdYearCourse(vcr, "The Gender Politics of VCR Repair");
            AddThirdYearCourse(vcr, "VCR Repair in Diaspora");
            AddThirdYearCourse(vcr, "The Biology of VCR Repair");
            AddThirdYearCourse(vcr, "Preventive VCR Maintenance");
            AddThirdYearCourse(vcr, "Intermediate Business of VCR Repair");
            AddThirdYearCourse(vcr, "VCR Repair Since 1945");
            AddThirdYearCourse(vcr, "VCR Repair in the Asian Tradition");
            AddThirdYearCourse(vcr, "Intermediate Ethics of VCR Repair");
            AddThirdYearCourse(vcr, "Intermediate VCR Repair Instrumentation");

            AddFourthYearCourse(vcr, "Advanced Philosophy of VCR Repair");
            AddFourthYearCourse(vcr, "Advanced Marxism in VCR Repair");
            AddFourthYearCourse(vcr, "Advanced Preventive VCR Maintenance");
            AddFourthYearCourse(vcr, "VHS Recovery");
            AddFourthYearCourse(vcr, "Beta and Society");
            AddFourthYearCourse(vcr, "Advanced Business of VCR Repair");
            AddFourthYearCourse(vcr, "Advanced Ethics of VCR Repair");
            AddFourthYearCourse(vcr, "Advanced VCR Repair Instrumentation");
            AddFourthYearCourse(vcr, "VCR Sales Since 2001");
            AddFourthYearCourse(vcr, "VCR Repair and the Internet");
            AddFourthYearCourse(vcr, "VCR Repair in the Developing World");
            AddFourthYearCourse(vcr, "VCR Repair and Political Revolution");
            AddFourthYearCourse(vcr, "Special Topics in VCR Repair");
            AddFourthYearCourse(vcr, "Directed Study");
            AddFourthYearCourse(vcr, "Field School");
            AddFourthYearCourse(vcr, "Practicum");

            return vcr;
        }

        private void AddFirstYearCourse(IDepartment department, string name)
        {
            AddCourse(department, name, Year.First, 3);
        }

        private void AddSecondYearCourse(IDepartment department, string name)
        {
            AddCourse(department, name, Year.Second, 3);
        }

        private void AddThirdYearCourse(IDepartment department, string name)
        {
            AddCourse(department, name, Year.Third, 4);
        }

        private void AddFourthYearCourse(IDepartment department, string name)
        {
            AddCourse(department, name, Year.Fourth, 4);
        }

        private void AddCourse(IDepartment department, string name, Year year, int credits)
        {
            var course = new CourseTemplate(name, credits, year, department);
            department.AddCourse(course);
        }
    }
}