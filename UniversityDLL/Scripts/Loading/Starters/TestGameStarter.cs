﻿using System;
using UnityEngine;

namespace Scripts.Loading.Starters
{
    public class TestGameStarter : BaseGameStarter
    {
        public TestGameStarter()
        {
            var loadPointerGameObject = new GameObject("LoadFilePointer");
            var loadPointer = loadPointerGameObject.AddComponent<LoadFilePointer>();

            var path = $"{Environment.GetEnvironmentVariable("LocalAppData")}/UniversityTycoon/saves/pathtest.usv";
            if (System.IO.File.Exists(path))
            {
                loadPointer.FileToLoad = path;
            }
        }
    }
}