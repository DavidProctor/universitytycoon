﻿using Scripts.Prototype;
using Scripts.ScriptedEvents;
using UnityEngine;

namespace Scripts.Loading.Starters
{
    public class DemoGameStarter : BaseGameStarter
    {
        public override void AddControllerComponents(GameObject controllerObject)
        {
            controllerObject.AddComponent<Tutorial>();
            controllerObject.AddComponent<TuitionScheduler>();
            controllerObject.AddComponent<ProfessorPayScheduler>();
        }
    }
}