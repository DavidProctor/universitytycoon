﻿using UnityEngine;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.People;
using UniversityDLL.Tiles;

namespace Scripts.Loading.Starters
{
    public abstract class BaseGameStarter
    {
        public virtual IGridGenerator GetGridGenerator()
        {
            return new BlankGridGenerator(100, 100);
        }

        public virtual void AddControllerComponents(GameObject controllerObject)
        {
            
        }

        public virtual ObjectFactoryFacade GetFactoryFacade()
        {
            var characterFactory = new CharacterFactory(GameState.GetInstance(),
                                                        new GameObjectFactory(), 
                                                        new NameGenerator());
            return new ObjectFactoryFacade(characterFactory);
        }
    }
}