﻿using Scripts.Prototype;
using UnityEngine;

namespace Scripts.Loading.Starters
{
    public class RegularGameStarter : BaseGameStarter
    {
        public override void AddControllerComponents(GameObject controllerObject)
        {
            controllerObject.AddComponent<TuitionScheduler>();
            controllerObject.AddComponent<ProfessorPayScheduler>();
        }
    }
}