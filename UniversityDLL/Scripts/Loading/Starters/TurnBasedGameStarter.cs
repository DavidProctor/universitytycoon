﻿using Scripts.ControlMediation;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;

namespace Scripts.Loading.Starters
{
    public class TurnBasedGameStarter : MonoBehaviour
    {
        public string StartFile;
        public GameState.GameStarterMode Mode;

        public void Start()
        {
            var canvas = new GameObject("Canvas");
            canvas.AddComponent<RectTransform>();
            var canvasComponent = canvas.AddComponent<Canvas>();
            canvasComponent.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.AddComponent<CanvasScaler>();
            canvas.AddComponent<GraphicRaycaster>();
            canvas.AddComponent<CanvasRenderer>();

            var starter = GetStarterMode();
            var saveFileName = GetSaveFileName();

            var controllerGameObject = new GameObject("Controller")
            {
                layer = LayerMask.NameToLayer("ClickTarget")
            };
            var controllerWrapper = controllerGameObject.AddComponent<GameStateWrapper>();

            if( saveFileName == null )
            {
                controllerWrapper.InitNewGame(starter.GetGridGenerator(), new TestDataGenerator(), starter.GetFactoryFacade());
            }
            else
            {
                controllerWrapper.Init(saveFileName);
            }

            controllerGameObject.AddComponent<DebugTools>();

            var timeControls = controllerGameObject.AddComponent<TimeControls>();
            GameController.GetInstance().TimeControls = timeControls;

            var gameState = GameState.GetInstance();
            gameState.RegisterRoom(ROutdoors.GetInstance());

            var eventSystem = new GameObject("EventSystem");
            eventSystem.AddComponent<EventSystem>();
            eventSystem.AddComponent<StandaloneInputModule>();

            var dayNightCycle = new GameObject("DayNightCycle");
            dayNightCycle.AddComponent<DayNightCycle>();

            var cameraTargetObject = new GameObject("CameraTarget");
            cameraTargetObject.AddComponent<CameraControl>();

            starter.AddControllerComponents(controllerGameObject);
        }

        private BaseGameStarter GetStarterMode()
        {
            BaseGameStarter starter;
            switch( Mode )
            {
                case GameState.GameStarterMode.Demo:
                    starter = new DemoGameStarter();
                    break;
                case GameState.GameStarterMode.Test:
                    starter = new TestGameStarter();
                    break;
                default:
                    starter = new RegularGameStarter();
                    break;
            }
            return starter;
        }

        private string GetSaveFileName()
        {
            var loadPointer = GameObject.Find("LoadFilePointer");

            if( loadPointer == null )
            {
                return null;
            }

            var saveFileName = loadPointer.GetComponent<LoadFilePointer>().FileToLoad;
            Destroy(loadPointer);
            return saveFileName;
        }
    }
}