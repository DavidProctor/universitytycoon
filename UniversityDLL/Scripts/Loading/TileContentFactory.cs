﻿using UniversityDLL;
using UniversityDLL.Tiles;

namespace Scripts.Loading
{
    public static class TileContentFactory
    {
        public static ITileContent MakePubBoothSeat(Orientation rotation)
        {
            var gameObject = new PrefabWrapper("Objects/PubBoothSeat");
            var content = new TileContentBuilder()
                .SetCost(60)
                .SetLayer(TileLayer.ActivityPoint)
                .SetPrototype(gameObject)
                .SetRotation(rotation)
                .Build();
            return content;
        }

        public static ITileContent MakePubBoothTable(Orientation orientation)
        {
            var gameObject = new PrefabWrapper("Objects/PubBoothTable");
            return new TileContentBuilder()
                .SetCost(40)
                .SetLayer(TileLayer.Structure)
                .SetPrototype(gameObject)
                .SetRotation(orientation)
                .Build();
        }
    }
}