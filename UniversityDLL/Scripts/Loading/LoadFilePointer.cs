﻿using UnityEngine;

namespace Scripts.Loading
{
    class LoadFilePointer : MonoBehaviour
    {
        public string FileToLoad;

        public void Start()
        {
            DontDestroyOnLoad(this);
        }
    }
}
