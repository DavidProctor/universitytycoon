﻿using System;
using System.Linq;
using Scripts.UI.Dialogs;
using Scripts.UI.Primitives;
using UnityEngine;
using UniversityDLL.ActivityPoints;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace Scripts.ScriptedEvents
{
    public class Tutorial : MonoBehaviour
    {
        private class DialogContents
        {
            public Func<string> GetText { get; }
            public string ButtonText { get; }
            public Action Action { get; }
            public bool IsLastDialog { get; }

            public DialogContents(string text, string buttonText, Action action = null, bool isLastDialog = false)
                : this(() => text, buttonText, action, isLastDialog)
            { }

            public DialogContents(Func<string> getTextFunc, string buttonText, Action action = null, bool isLastDialog = false)
            {
                GetText = getTextFunc;
                ButtonText = buttonText;
                Action = action ?? (() => { });
                IsLastDialog = isLastDialog;
            }
        }

        private readonly string _portraitPath = "petter";
        private readonly string _name = "Andrew Petter";
        private UiHighlight _currentHighlight;

        private DialogContents[] _dialogs;

    public void Start()
    {
        Func<string> getDepartmentChoiceResponse = () =>
        {
            var department = GameState.GetInstance().Departments.FirstOrDefault()?.Name;
            return department == "English" ?
                "Ah! English! A very popular subject! The default major, if you will! Excellent choice!" :
                department == "Engineering" ?
                "Hm. I'm not sure if you're fully grasping my point about attracting large volumes of students.\n\nNo matter - Engineering it is." :
                department == "Physics" ?
                "Physics. A STEM field. It'll do, but try to remember that we're not trying to create geniuses here - we're trying to create dollars." :
                department == "Political Science" ?
                "Good old poli sci. That was my major, you know!" :
                department == "VCR Repair" ?
                "VCR repair. A field so hopelessly obsolete that it simply cannot be considered ethical " +
                  "to accept payment for teaching it.\n\nThis is an excellent first step - don't ever let things like that stand in your way!" :
                "Hm. I'm not familiar with that one. Hopefully this works out!";
        };
        Func<string> getBasementResponse = () =>
        {
            var size = GameState.GetInstance().Grid.Count(t => t.HasLayer(TileLayer.Basement));
            if (size == 0)
            {
                return "Well, this won't do at all. Make sure you build a basement at some point or we'll be stuck here forever.";
            }
            if (size < 30)
            {
                return
                    "Hmm. A very small foundation. We'll only be able to fit a dozen or so tuition-payers in here. I suppose we'll have to add to it later.";
            }
            if (size < 100)
            {
                return "A respectable foundation! Don't forget that we'll have to build more later, though.";
            }
            return "That's a nice, big footprint! Think of all the trust fund vessels we'll be able to fit in here!";
        };

            _dialogs = new[]
            {
                new DialogContents("Hi, I'm Andrew Petter, noted perjurer and abuser of the public trust!",
                    "Hello, Petter"),
                new DialogContents(
                    "After I resigned in disgrace from my position as Minister of Finance, I was offered a series of sinecures before " +
                    "being placed in charge of my own university.\n\nI'm here to teach you how to do the same!",
                    "Whatever you say",
                    () => SetHighlight("HudCashIndicator")),
                new DialogContents("Highlighted at your upper left is the only thing you really have to care about: your liquid assets!\n\n" +
                                   "Your number one priority will always be to make this number as big as possible!",
                    "Jesus christ"),
                new DialogContents("You make money by attracting lots of students who are eligible for those sweet, nourishing student loans.\n\n" +
                    "There are plenty of other creative ways to make money, but that's your bread and butter.",
                    "But that's uneth--",
                    () => SetHighlight("HudPrestigeIndicator")),
                new DialogContents("Next is your prestige score!\n\nThis measures how respected our institution is, and allows us to attract " +
                    "students with deeper pockets.",
                    "..."),  
                new DialogContents("There are many ways to gain prestige, such as building statues, hiring famous professors, " +
                    "and producing graduates who go on to make a really significant mark in their field.",
                    "Makes sense"), 
                new DialogContents("Don't hold your breath for that last one, though. I haven't seen it happen very often.",
                    "Uh-huh",
                    ClearHighlight),
                new DialogContents("Prestige is very important! Without it, you cannot expand your university, and if you don't expand, " +
                    "you can't make more money!",
                    "All right"),
                new DialogContents("For example, you need to spend prestige to open new departments.\n\n" +
                "Right now this university only exists on paper, so let's spend some prestige to open a department!",
                "Sure",
                    () =>
                    {
                        SetHighlight("HudBottomLogoButton");
                        var obj = new Objective("Found a department", 
                            () => GameState.GetInstance().Departments.Any(),
                            () => { MakeDialog(10); });
                        GameState.GetInstance().AddObjective(obj);
                    }), 
                new DialogContents("You can manage your departments by clicking on your university's logo in the bottom left.\n\n" + 
                    "I've also added an objective for you, which you can see on your left - I'll be back when you're done!",
                    "Sure thing",
                    ClearHighlight,
                    isLastDialog: true),
                new DialogContents(getDepartmentChoiceResponse,
                    "..."), 
                new DialogContents("Now, in order to make money, you want to keep your costs as low as possible.\n\n" +
                    "Sadly, there are some things that we simply can't go without paying for while calling ourselves a university.",
                    "Such as?"), 
                new DialogContents("In particular, we're definitely going to need to build at least one classroom.",
                    "Of course",
                    () => SetHighlight("HudMainMenuBuildButton")),
                new DialogContents("Let's start by constructing a foundation. See the Build button at the bottom of your screen? " +
                                   "Click it and select the Basement tool, and click and drag to build.",
                    "Got it",
                    () =>
                    {
                        ClearHighlight();
                        var obj = new Objective("Build a basement",
                            () => GameState.GetInstance().Grid.Any(t => t.HasLayer(TileLayer.Basement)),
                            () => MakeDialog(14));
                        GameState.GetInstance().AddObjective(obj);
                    },
                    isLastDialog: true), 
                new DialogContents(getBasementResponse,
                    "..."),
                new DialogContents("All right, let's get some walls and flooring in there! Pick something nice-looking, or something cheap!\n\n"+
                    "Remember: you can often hold SHIFT to make building easier!",
                    "All right",
                    () =>
                    {
                        ClearHighlight();
                        var obj = new Objective("Build walls and floors",
                            () =>
                            {
                                var grid = GameState.GetInstance().Grid;
                                return grid.Any(t => t.HasLayer(TileLayer.Floor)) &&
                                       grid.Any(t => t.HasLayer(TileLayer.Wall));
                            },
                            () => MakeDialog(16));
                        GameState.GetInstance().AddObjective(obj);
                    },
                    isLastDialog: true), 
                new DialogContents("What style! What pizazz!",
                    "...Thanks?",
                    () => SetHighlight("HudMainMenuOrganizeButton")), 
                new DialogContents("Now, I understand that there was once a time when you could assume some intelligence in the people" +
                                   "at a university. I assure you that this time is over.\n\n" +
                                   "Use the Organize menu to designate this as a lecture hall so that nobody mistakes it for something else.",
                    "Okay, okay",
                    () =>
                    {
                        ClearHighlight();
                        var obj = new Objective("Define a lecture hall",
                            () => GameState.GetInstance().Rooms.Any(r => r is RLectureHall),
                            () => MakeDialog(18));
                        GameState.GetInstance().AddObjective(obj);
                    },
                    isLastDialog: true), 
                new DialogContents("Beautiful job!\n\nFinish her off by adding a door and a few seats using the tools at the bottom of your screen." +
                                   "Remember that you can often press R to rotate objects before placing them!",
                    "OK",
                    () =>
                    {
                        var obj = new Objective("Finish the lecture hall",
                            () => true,
                            () => MakeDialog(19));
                        obj.AddSubObjective(new Objective("Add some seating",
                            () => GameState.GetInstance().Rooms.Any(r => r.GetAvailableActivityPoint<ClassSeatActivityPoint>() != null),
                            () => { }));
                        obj.AddSubObjective(new Objective("Add a door",
                            () => GameState.GetInstance().Rooms.Any(r => r.GetAllExits().Count > 0),
                            () => { }));
                        GameState.GetInstance().AddObjective(obj);
                    },
                    isLastDialog: true),
                new DialogContents("It's beautiful. I couldn't be more proud.",
                    "...Thanks?"), 
                new DialogContents("Well, I've taught you all I know. It's time for you to go make some money of your own.",
                    "I guess",
                    () => SetHighlight("HudTimeButtons")), 
                new DialogContents("The only thing left to do is to start time moving using the time controls at the top right of your screen!",
                    "Got it",
                    ClearHighlight), 
                new DialogContents("Remember the oath of the university administrator:\n\nNo matter your family or your race, " +
                    "your color or your creed, your wealth or your poverty...\n\nEveryone deserves the chance to spend their money here.",
                    "Grim."), 
                new DialogContents("Farewell! Enjoy!\n\nAnd remember, any similarities between me and any actual persons is " +
                    "entirely coincidental!",
                    "Right")
            };
            MakeDialog(0);
        }

        private void MakeDialog(int index)
        {
            var contents = _dialogs[index];
            Action onButton;
            if (index < _dialogs.Length - 1 && !contents.IsLastDialog)
            {
                onButton = () =>
                {
                    contents.Action();
                    MakeDialog(index + 1);
                };
            }
            else
            {
                onButton = () => contents.Action();
            }
            
            var dialog = new PortraitDialog(_name, _portraitPath, contents.GetText(), onButton, contents.ButtonText);
        }

        private void SetHighlight(string componentName)
        {
            ClearHighlight();
            var highlightedComponent = GameObject.Find(componentName).GetComponent<GuiComponentBehavior>().GuiComponent;
            _currentHighlight = new UiHighlight($"{componentName}Highlight", highlightedComponent.Parent, highlightedComponent.Transform);
        }

        private void ClearHighlight()
        {
            _currentHighlight?.Destroy();
            _currentHighlight = null;
        }
    }
}