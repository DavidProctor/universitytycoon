﻿using System;
using Scripts.ControlMediation;
using UniversityDLL;
using UnityEngine;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;
using Object = UnityEngine.Object;

namespace Scripts
{
    class PrimitiveWrapper : IGameObject
    {
        private readonly GameObject _instance;
        private readonly PrimitiveType _primitiveType;
        private readonly Vector3 _offsetPosition;

        public string Name { get; set; }
        public GameObject GameObject { get { return _instance; } }
        public Orientation Orientation { get; private set; }

        public PrimitiveWrapper(string name, PrimitiveType primitiveType)
        {
            Name = name;
            _primitiveType = primitiveType;
            _instance = GameObject.CreatePrimitive(_primitiveType);
            _instance.gameObject.layer = LayerMask.NameToLayer("ClickTarget");
            _offsetPosition = Vector3.zero;
        }

        public PrimitiveWrapper(string name, PrimitiveType primitiveType, Vector3 offsetPosition)
            : this(name, primitiveType)
        {
            _offsetPosition = offsetPosition;
        }

        public void SetOrientation(Orientation orientation)
        {
            Orientation = orientation;
            RotateToOrientation();
        }

        public void Destroy()
        {
            Object.Destroy(_instance);
        }

        public IGameObject GetClone()
        {
            var wrapper = new PrimitiveWrapper(Name, _primitiveType);
            wrapper.SetOrientation(Orientation);
            return wrapper;
        }

        public void SetClickableObject(IGameEntity entity)
        {
            if( _instance.GetComponent<Clickable>() != null )
            {
                throw new InvalidOperationException("Instance already has a Clickable component");
            }

            if( _instance == null )
            {
                Render(0, 10000f, 0);
                Hide();
            }

            var clickable = _instance.AddComponent<Clickable>();
            clickable.SetGameEntity(entity);
        }

        public void Hide()
        {
            _instance.SetActive(false);
        }

        public IGameObject Render(float x, float y, float z)
        {
            _instance.transform.position = new Vector3(x, y, z) + _offsetPosition;
            _instance.transform.localScale = new Vector3(0.7f, 0.8f, 0.7f);
            Show();
            RotateToOrientation();
            return this;
        }

        public IGameObject Render(float x, float y, float z, Color color)
        {
            var renderer = _instance.GetComponent<MeshRenderer>();
            renderer.material.color = color;
            return Render(x, y, z);
        }

        public void Show()
        {
            _instance.SetActive(true);
        }

        private void RotateToOrientation()
        {
            if( _instance == null )
            {
                return;
            }

            switch( Orientation )
            {
                case Orientation.Forward:
                    _instance.transform.rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case Orientation.Right:
                    _instance.transform.rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case Orientation.Reverse:
                    _instance.transform.rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case Orientation.Left:
                    _instance.transform.rotation = Quaternion.Euler(0, 270, 0);
                    break;
                default:
                    throw new InvalidOperationException($"Cannot set rotation to value {Orientation}");
            }
        }
    }
}
