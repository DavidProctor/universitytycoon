﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.Scheduling;

namespace Scripts
{
    public class DayNightCycle : MonoBehaviour
    {
        private IGameState _gameState;
        private Transform _sunPivot;
        private Transform _sunTransform;
        private Light _sunLight;
        private Light _northeastLight;
        private Light _northwestLight;
        private Light _southeastLight;
        private Light _southwestLight;
        private Light _moonLight;
        private float _ambientLightIntensity;

        public void Start()
        {
            _gameState = GameState.GetInstance();

            var spinner = GameObject.Find("DayNightCycle");
            _sunPivot = spinner.transform;

            var sun = new GameObject("Sun");
            _sunTransform = sun.transform;
            _sunTransform.SetParent(spinner.transform);
            _sunTransform.localRotation = Quaternion.LookRotation(Vector3.up);

            _sunLight = sun.AddComponent<Light>();
            _sunLight.type = LightType.Directional;
            _sunLight.shadows = LightShadows.Hard;
            _sunLight.transform.localPosition = new Vector3(0, -100f, 0);
            
            _sunPivot.transform.Rotate(Vector3.left, (360f / 24) * _gameState.Calendar.CurrentTime.Ticks % TimeUnit.TicksPerDay);
            _sunPivot.transform.Rotate(Vector3.forward, 15f);

            var ambientAnchor = new GameObject("AmbientLightAnchor");
            ambientAnchor.transform.position = new Vector3(50, 10, 50);

            _northeastLight = AddLightToGameObject("NortheastAmbientLight", ambientAnchor);
            _northeastLight.transform.localPosition = new Vector3(150, 0, 0);
            _northeastLight.transform.localRotation = Quaternion.LookRotation(Vector3.left);
            _northwestLight = AddLightToGameObject("NorthwestAmbientLight", ambientAnchor);
            _northwestLight.transform.localPosition = new Vector3(0, 0, 150);
            _northwestLight.transform.localRotation = Quaternion.LookRotation(Vector3.back);
            _southeastLight = AddLightToGameObject("SoutheastAmbientLight", ambientAnchor);
            _southeastLight.transform.localPosition = new Vector3(0, 0, -150);
            _southwestLight = AddLightToGameObject("SouthwestAmbientLight", ambientAnchor);
            _southwestLight.transform.localPosition = new Vector3(-150, 0, 0);
            _southwestLight.transform.localRotation = Quaternion.LookRotation(Vector3.right);
            _moonLight = AddLightToGameObject("MoonLight", ambientAnchor);
            _moonLight.transform.localPosition = new Vector3(0, 150, 0);
            _moonLight.transform.localRotation = Quaternion.LookRotation(Vector3.down);
            _moonLight.intensity = 0.4f;
        }

        public void Update()
        {
            var rotation = (360f/TimeUnit.TicksPerDay)*(_gameState.Calendar.CurrentTime.Ticks%TimeUnit.TicksPerDay);
            _sunPivot.transform.rotation = Quaternion.AngleAxis(rotation, Vector3.forward);

            _sunLight.intensity = Mathf.InverseLerp(0, -10, -_sunTransform.position.y);
            _ambientLightIntensity = Mathf.InverseLerp(0, -15, -_sunTransform.position.y);
            _northeastLight.intensity = _ambientLightIntensity;
            _northwestLight.intensity = _ambientLightIntensity;
            _southeastLight.intensity = _ambientLightIntensity;
            _southwestLight.intensity = _ambientLightIntensity;
        }

        private Light AddLightToGameObject(string objectName, GameObject parent)
        {
            var lightObject = new GameObject(objectName);
            lightObject.transform.SetParent(parent.transform);

            var lightComponent = lightObject.AddComponent<Light>();
            lightComponent.type = LightType.Directional;
            lightComponent.shadows = LightShadows.None;

            return lightComponent;
        }
    }
}
