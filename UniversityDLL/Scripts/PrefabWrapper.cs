﻿using System;
using Scripts.ControlMediation;
using UniversityDLL;
using UnityEngine;
using UniversityDLL.GameMangement;
using Object = UnityEngine.Object;

namespace Scripts
{
    [Serializable]
    public class PrefabWrapper : IGameObject
    {
        [NonSerialized]
        private GameObject _instance;
        [NonSerialized]
        private GameObject _prototype;

        public Orientation Orientation { get { return _orientationTracker.Orientation; } }
        
        private readonly string _prototypePath;
        private readonly OrientationTracker _orientationTracker = new OrientationTracker();

        public GameObject GameObject
        {
            get
            {
                if( _prototype == null )
                {
                    _prototype = Prototypes.Get(_prototypePath);
                    if(_prototype == null)
                    {
                        throw new NullReferenceException("NullLayer: " + _prototypePath);
                    }
                }
                return _prototype;
            }
        }
        public string Name { get; set; }

        public PrefabWrapper(string prototypePath)
        {
            _prototypePath = prototypePath;
        }

        public IGameObject GetClone()
        {
            var clone = new PrefabWrapper(_prototypePath) { Name = Name };
            clone.SetOrientation(Orientation);
            return clone;
        }

        public void SetClickableObject(IGameEntity entity)
        {
            if( _instance == null )
            {
                Render(0, 10000f, 0);
                Hide();
            }

            if( _instance.GetComponent<Clickable>() != null )
            {
                throw new InvalidOperationException("Instance already has a Clickable component");
            }

            var clickable = _instance.AddComponent<Clickable>();
            clickable.SetGameEntity(entity);
        }

        public IGameObject Render(float x, float y, float z)
        {
            Show();
            if( _instance == null )
            {
                _instance = Object.Instantiate(GameObject, new Vector3(x, y, z), Quaternion.identity) as GameObject;
                RotateToOrientation();
            }
            if( InstanceHasMoved(x, y, z) )
            {
                _instance.transform.position = new Vector3(x, y, z);
            }
            return this;
        }

        public IGameObject Render(float x, float y, float z, Color color)
        {
            Render(x, y, z);
            foreach( var renderer in _instance.GetComponentsInChildren<Renderer>() )
            {
                renderer.material.color = color;
            }
            return this;
        }

        public void SetOrientation(Orientation orientation)
        {
            _orientationTracker.SetOrientation(orientation);
            RotateToOrientation();
        }

        public void Destroy()
        {
            GameObject.Destroy(_instance);
        }

        private bool InstanceHasMoved(float x, float y, float z)
        {
            return !Utils.FloatEquals(x, _instance.transform.position.x)
                || !Utils.FloatEquals(z, _instance.transform.position.z)
                || !Utils.FloatEquals(y, _instance.transform.position.y);
        }

        public void Hide()
        {
            if (_instance != null)
            {
                _instance.SetActive(false);
            }
        }

        public void Show()
        {
            if( _instance != null )
            {
                _instance.SetActive(true);
            }
        }

        private void RotateToOrientation()
        {
            if( _instance == null )
            {
                return;
            }

            switch( Orientation )
            {
                case Orientation.Forward:
                    _instance.transform.rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case Orientation.Right:
                    _instance.transform.rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case Orientation.Reverse:
                    _instance.transform.rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case Orientation.Left:
                    _instance.transform.rotation = Quaternion.Euler(0, 270, 0);
                    break;
                default:
                    throw new InvalidOperationException($"Cannot set rotation to value {Orientation}");
            }
        }
    }
}
