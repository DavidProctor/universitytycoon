﻿using System;
using System.Linq;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Scheduling;
using UniversityDLL.UI;

namespace Scripts.Prototype
{
    public class TuitionScheduler : MonoBehaviour
    {
        [Serializable]
        private class CollectTuitionCalendarEvent : ICalendarEvent
        {
            private readonly int _tuitionPerCourse = 500 / TimeUnit.WeeksPerSemester;

            public TimeUnit Date { get; set; }
            public void Execute()
            {
                var gameState = GameState.GetInstance();
                var currentSemester = gameState.Calendar.CurrentTime.Semester;
                var enrolmentCount= gameState.Enrolments.Count(e => e.Course.Semester.Equals(currentSemester));
                var revenue = enrolmentCount*_tuitionPerCourse;

                gameState.Score.AddMoney(revenue);
                NotificationCollection.Instance.AddNotification(Notification.WithExpiry($"${revenue} in tuition collected", Date.AddDays(1)));
            }

            public TimeUnit GetRescheduleDate()
            {
                return Date.AddDays(7);
            }
        }

        public void Start()
        {
            var calendar = GameState.GetInstance().Calendar;
            var tuitionTime = new TimeUnit(2006, Season.Fall, 1, DayOfWeek.Tuesday);
            var tuitionEvent = new CollectTuitionCalendarEvent
            {
                Date = tuitionTime
            };
            calendar.AddEvent(tuitionEvent);
        }
    }
}