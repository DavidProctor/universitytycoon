﻿using System;
using System.Linq;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Scheduling;
using UniversityDLL.UI;

namespace Scripts.Prototype
{
    public class ProfessorPayScheduler : MonoBehaviour
    {
        [Serializable]
        private class ProfessorPayCalendarEvent : ICalendarEvent
        {
            private readonly int _biWeeklyProfessorPay = 1000;

            public TimeUnit Date { get; set; }
            public void Execute()
            {
                var gameState = GameState.GetInstance();
                var profCount = gameState.Professors.Count();
                var debit = profCount* _biWeeklyProfessorPay;
                gameState.Score.SubtractMoney(debit);
                NotificationCollection.Instance.AddNotification(Notification.WithExpiry($"${debit} in professor salaries paid", Date.AddDays(1)));
            }

            public TimeUnit GetRescheduleDate()
            {
                return Date.AddDays(14);
            }
        }

        public void Start()
        {
            var payEvent = new ProfessorPayCalendarEvent
            {
                Date = new TimeUnit(2006, Season.Fall, 1, DayOfWeek.Wednesday)
            };
            GameState.GetInstance().Calendar.AddEvent(payEvent);
        }
    }
}