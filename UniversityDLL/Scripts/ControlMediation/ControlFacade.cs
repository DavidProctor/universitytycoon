﻿using Scripts.UI.Dialogs;
using Scripts.UI.Hud;
using UnityEngine;
using UniversityDLL.ControlManagement;
using UniversityDLL.ControlManagement.MouseModes;

namespace Scripts.ControlMediation
{
    public class ControlFacade : MonoBehaviour
    {
        public Controls Controls { get; set; }

        private readonly ControlMappings _controlMappings = ControlMappings.GetInstance();
        private Tooltip _tooltip;

        public void Start()
        {
            _tooltip = new Tooltip();
        }
    
        public void Update()
        {
            var entity = Controls.GetGameEntityAtMouse();
            if( entity == null )
            {
                _tooltip.GameObject.SetActive(false);
                return;
            }

            _tooltip.GameObject.SetActive(true);
            _tooltip.Update(entity, Controls.GetMousePosition());
            
            if( _controlMappings.DeselectButtonReleased() && !(Controls.MouseMode is StandardMouseMode))
            {
                Controls.MouseMode.Destroy();
                Controls.AltMouseMode.Destroy();
                var standardMouseMode = new StandardMouseMode(DialogFactory.GetInstance());
                Controls.SetMouseMode(standardMouseMode);
                Controls.SetAltMouseMode(standardMouseMode);
            }

            MouseMode mouseMode = _controlMappings.AltCursorModeButtonHeldDown() ? Controls.AltMouseMode : Controls.MouseMode;

            if( _controlMappings.AltCursorModeButtonHeldDown() )
            {
                Controls.MouseMode.Destroy();
                mouseMode = Controls.AltMouseMode;
            }
            else if( _controlMappings.AltCursorModeButtonReleased() )
            {
                Controls.AltMouseMode.Destroy();
                mouseMode = Controls.MouseMode;
            }

            if( _controlMappings.SelectButtonPressed() )
            {
                mouseMode.OnLeftMouseDown(entity);
            }
            else if( _controlMappings.SelectButtonReleased() )
            {
                mouseMode.OnLeftMouseUp(entity);
            }
            else if( _controlMappings.SelectButtonHeldDown() )
            {
                mouseMode.OnMoveWhileLeftMouseDown(entity);
            }
            else
            {
                mouseMode.OnMoveWhileLeftMouseUp(entity);
            }

            if ( _controlMappings.RotateButtonReleased() )
            {
                mouseMode.Rotate();
            }

            if (_controlMappings.SaveButtonReleased())
            {
                var canvas = GameObject.Find("Canvas");
                canvas.AddComponent<SaveLoadDialog>();
            }
        }
    }
}
