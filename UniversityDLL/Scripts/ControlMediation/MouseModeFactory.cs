﻿using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Previews;
using UniversityDLL.Tiles;

namespace Scripts.ControlMediation
{
    public static class MouseModeFactory
    {
        public static MouseMode GetBoxBrush(string prototype, TileLayer layer, long cost)
        {
            var wall = new TileContentBuilder()
                        .SetPrototype(new PrefabWrapper(prototype))
                        .SetLayer(layer)
                        .SetCost(cost)
                        .Build();

            var selection = new BoxSelection(GameState.GetInstance().Grid);
            var preview = new Preview(selection, wall.GameObject, wall.GameObject, layer);
            var score = GameState.GetInstance().Score;
            var brush = new BuildBrush(selection, wall, wall, score);

            return new BrushMouseMode(brush, preview);
        }

        public static MouseMode GetLineBrush(string prototype, TileLayer layer, long cost)
        {
            var wall = new TileContentBuilder()
                        .SetPrototype(new PrefabWrapper(prototype))
                        .SetLayer(layer)
                        .SetCost(cost)
                        .Build();

            var selection = new LineSelection(GameState.GetInstance().Grid);
            var preview = new Preview(selection, wall.GameObject, wall.GameObject, layer);
            var score = GameState.GetInstance().Score;
            var brush = new BuildBrush(selection, wall, wall, score);

            return new BrushMouseMode(brush, preview);
        }

        public static MouseMode GetDoorLineBrush(string prototype, long cost)
        {
            var door = new TileContentBuilder()
                .SetPrototype(new PrefabWrapper(prototype))
                .SetLayer(TileLayer.Door)
                .SetCost(cost)
                .Build();

            var selection = new LineSelection(GameState.GetInstance().Grid);
            var preview = new Preview(selection, door.GameObject, door.GameObject, TileLayer.Door);
            var brush = new BuildDoorBrush(selection, door, PathProvider.GetInstance());

            return new BrushMouseMode(brush, preview);
        }

        public static MouseMode GetRectangleBrush(string prototype, TileLayer layer, long cost)
        {
            var wall = new TileContentBuilder()
                        .SetPrototype(new PrefabWrapper(prototype))
                        .SetLayer(layer)
                        .SetCost(cost)
                        .Build();

            var selection = new FloodSelection((s, t) => s.Room == t.Room);
            var preview = new Preview(selection, wall.GameObject, new NullGameObject(), layer);
            var score = GameState.GetInstance().Score;
            var brush = new BuildBrush(selection, wall, new NullTileContent(), score);

            return new BrushMouseMode(brush, preview);
        }

        public static MouseMode GetFloodBrush(string prototype, TileLayer layer, long cost)
        {
            var content = new TileContentBuilder()
                                .SetPrototype(new PrefabWrapper(prototype))
                                .SetLayer(layer)
                                .SetCost(cost)
                                .Build();

            var selection = new FloodSelection((s, t) => t.Room == s.Room && !t.HasLayer(TileLayer.Wall));
            var preview = new Preview(selection, content.GameObject, content.GameObject, layer);
            var score = GameState.GetInstance().Score;
            var brush = new BuildBrush(selection, content, content, score);
            return new BrushMouseMode(brush, preview);
        }
    }
}