﻿using System;
using Scripts.Loading;
using UnityEngine;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace Scripts.ControlMediation
{
    public class GameStateWrapper : MonoBehaviour
    {
        public IGameState GameState;
        public float CameraMoveSpeed;

        public Prototypes Prototypes { get; set; }

        private BoxCollider _clickTarget;
        
        public void Init(string saveFile)
        {
            CameraMoveSpeed = 10f;

            GameState = UniversityDLL.GameMangement.GameState.LoadSave(saveFile);

            SetupPrototypes();
            SetupClickTarget();
            SetupCalendar();
            SetupScore();
        }

        public void InitNewGame(IGridGenerator gridGenerator, TestDataGenerator dataGenerator, ObjectFactoryFacade objectFactoryFacade)
        {
            CameraMoveSpeed = 10f;

            var loader = new GameLoader();
            GameState = loader.LoadGame(gridGenerator, objectFactoryFacade);

            SetupPrototypes();
            SetupClickTarget();
            SetupCalendar();
            SetupScore();
        }

        private void SetupClickTarget()
        {
            _clickTarget = gameObject.AddComponent<BoxCollider>();
            _clickTarget.size = new Vector3(GameState.Grid.Width, 0, GameState.Grid.Height);
            _clickTarget.transform.position = new Vector3(GameState.Grid.Width / 2f - 0.5f, 0, GameState.Grid.Height / 2f - 0.5f);
        }

        private void SetupCalendar()
        {
            GameState.Calendar.CurrentTime = new TimeUnit(Semester.GetSemester(2006, Season.Fall), 1, DayOfWeek.Sunday, 11, QuarterHour.Fifteen);
        }

        private void SetupScore()
        {
            GameState.Score.AddMoney(1000000);
            GameState.Score.AddPrestige(350);
        }

        private void SetupPrototypes()
        {
            Prototypes = gameObject.GetComponent<Prototypes>();
        }
    }
}
