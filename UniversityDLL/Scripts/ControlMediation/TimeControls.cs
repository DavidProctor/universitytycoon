﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniversityDLL.GameMangement;

namespace Scripts.ControlMediation
{
    public class TimeControls : MonoBehaviour
    {
        public enum SimulationSpeed
        {
            Normal,
            Fast,
            Fastest
        }

        private readonly IGameState _gameState = GameState.GetInstance();
        public bool IsPaused { get; private set; }
        public SimulationSpeed CurrentSpeed { get; private set; }

        private readonly Dictionary<SimulationSpeed, float> _speedMap = new Dictionary<SimulationSpeed, float>
        {
            { SimulationSpeed.Normal, 5f },
            { SimulationSpeed.Fast, 1f },
            { SimulationSpeed.Fastest, 0.05f }
        };

        public void Start()
        {
            CurrentSpeed = SimulationSpeed.Normal;
            IsPaused = true;
        }

        public void SetClockSpeed(SimulationSpeed speed)
        {
            CurrentSpeed = speed;
            _gameState.Calendar.SecondsPerTick = _speedMap[speed];
            StopAllCoroutines();
            StartCoroutine(TickCalendar());
        }

        public void TogglePaused()
        {
            IsPaused = !IsPaused;
            if (IsPaused)
            {
                StopAllCoroutines();
            }
            else
            {
                StartCoroutine(TickCalendar());
            }
        }

        private IEnumerator TickCalendar()
        {
            while( true )
            {
                _gameState.Calendar.Tick();
                yield return new WaitForSeconds(_gameState.Calendar.SecondsPerTick);
            }
        }
    }
}