﻿using System.Collections;
using UnityEngine;
using UniversityDLL.GameMangement;

namespace Scripts.ControlMediation
{
    public class CameraControl : MonoBehaviour
    {
        private static readonly float MoveSpeed = 10f;

        private readonly ControlMappings _controlMappings = ControlMappings.GetInstance();
        private Camera _camera;

        public void Start()
        {
            var cameraObject = new GameObject("GameCamera");
            cameraObject.transform.SetParent(transform);
            cameraObject.transform.position = new Vector3(0f, 0f, 0f);
            cameraObject.transform.RotateAround(transform.position, Vector3.left, -45);

            _camera = cameraObject.AddComponent<Camera>();
            cameraObject.tag = "MainCamera";
            _camera.orthographic = true;
            _camera.nearClipPlane = -25f;

            var grid = GameState.GetInstance().Grid;
            transform.position = new Vector3(grid.Width / 2f, 0f, grid.Height / 2f);
            transform.RotateAround(transform.position, Vector3.up, 45);
        }

        // Update is called once per frame
        public void Update()
        {
            Vector3 movement = (Vector3.forward * MoveSpeed * _controlMappings.CameraVerticalMovement * Time.deltaTime)
                + (Vector3.right * MoveSpeed * _controlMappings.CameraHorizontalMovement * Time.deltaTime);

            transform.Translate(movement);

            _camera.orthographicSize -= _controlMappings.CameraZoom * 3;
            if (_camera.orthographicSize < 3f) _camera.orthographicSize = 3f;
            if (_camera.orthographicSize > 20f) _camera.orthographicSize = 20f;

            if (_controlMappings.RotateCameraLeftButtonReleased())
            {
                StartCoroutine(RotateCamera(Vector3.up * 90));
            }
            if (_controlMappings.RotateCameraRightButtonReleased())
            {
                StartCoroutine(RotateCamera(Vector3.up * -90));
            }
        }

        IEnumerator RotateCamera(Vector3 angle)
        {
            var fromAngle = transform.rotation;
            var toAngle = Quaternion.Euler(transform.eulerAngles + angle);
            for( var t = 0f; t < 1; t += Time.deltaTime / 0.2f )
            {
                transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
                yield return null;
            }
            transform.rotation = toAngle;
        }
    }
}
