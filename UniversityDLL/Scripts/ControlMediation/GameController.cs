﻿using Scripts.UI.Dialogs;
using Scripts.UI.Hud;
using UnityEngine;
using UniversityDLL.ControlManagement;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.AsyncTasks;

namespace Scripts.ControlMediation
{

    public class GameController : IGameController
    {
        private static readonly GameController Instance = new GameController();

        private ControlMode _controlMode = ControlMode.Standard;

        public static GameController GetInstance()
        {
            return Instance;
        }

        public Controls Controls { get; }
        public TimeControls TimeControls { get; set; }
        public IAsyncExecutor AsyncExecutor => new AsyncExecutor();
        public ControlFacade ControlFacade { get; }
        public Hud Hud { get; }
        
        private GameController()
        {
            var starter = GameObject.Find("GameStarter");
            Hud = new Hud();
            Controls = new Controls(DialogFactory.GetInstance());
            ControlFacade = starter.AddComponent<ControlFacade>();
            ControlFacade.Controls = Controls;
        }

        public void SetControlMode(ControlMode mode)
        {
            _controlMode = mode;

            switch (_controlMode)
            {
                case ControlMode.Standard:
                    SetStandardControlMode();
                    break;
                case ControlMode.Wait:
                    SetWaitControlMode();
                    break;
                default:
                    SetStandardControlMode();
                    break;
            }
        }

        private void SetStandardControlMode()
        {
            ControlFacade.enabled = true;
            Hud.Show();
        }

        private void SetWaitControlMode()
        {
            ControlFacade.enabled = false;
            Hud.Hide();
        }
    }
}