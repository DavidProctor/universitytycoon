﻿using UnityEngine;

namespace Scripts.ControlMediation
{
    public class ControlMappings
    {
        private static readonly ControlMappings Instance = new ControlMappings();

        public static ControlMappings GetInstance()
        {
            return Instance;
        }

        private ControlMappings() { }

        public float CameraHorizontalMovement
        {
            get { return Input.GetAxis("Horizontal"); }
        }

        public float CameraVerticalMovement
        {
            get { return Input.GetAxis("Vertical"); }
        }

        public float CameraZoom
        {
            get { return Input.GetAxis("ScrollWheel"); }
        }

        public bool SelectButtonPressed()
        {
            return Input.GetMouseButtonDown(0);
        }

        public bool SelectButtonHeldDown()
        {
            return Input.GetMouseButton(0);
        }

        public bool SelectButtonReleased()
        {
            return Input.GetMouseButtonUp(0);
        }

        public bool DeselectButtonReleased()
        {
            return Input.GetMouseButtonUp(1);
        }

        public bool AltCursorModeButtonPressed()
        {
            return Input.GetKeyDown(KeyCode.LeftShift);
        }

        public bool AltCursorModeButtonHeldDown()
        {
            return Input.GetKey(KeyCode.LeftShift);
        }

        public bool AltCursorModeButtonReleased()
        {
            return Input.GetKeyUp(KeyCode.LeftShift);
        }

        public bool RotateButtonReleased()
        {
            return Input.GetKeyUp(KeyCode.R);
        }

        public bool RotateCameraLeftButtonReleased()
        {
            return Input.GetKeyUp(KeyCode.Q);
        }

        public bool RotateCameraRightButtonReleased()
        {
            return Input.GetKeyUp(KeyCode.E);
        }

        public bool SaveButtonReleased()
        {
            return Input.GetKeyUp(KeyCode.F5);
        }
    }
}