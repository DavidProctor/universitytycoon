﻿using UniversityDLL;
using UniversityDLL.ControlManagement;

namespace Scripts.ControlMediation
{
    public class Clickable : ClickableBase, IClickable
    {
        public override IGameEntity Entity { get; protected set; }

        public void SetGameEntity(IGameEntity entity)
        {
            Entity = entity;
        }
    }
}