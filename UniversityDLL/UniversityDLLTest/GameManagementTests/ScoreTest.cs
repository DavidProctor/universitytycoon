﻿using System;
using NUnit.Framework;
using UniversityDLL.GameMangement;

namespace UniversityDLLTest.GameManagementTests
{
    [TestFixture]
    class ScoreTest
    {
        private IScore _score;

        [SetUp]
        public void BeforeEachTest()
        {
            _score = new Score();
        }

        [Test]
        public void Money_DefaultsToZero()
        {
            Assert.AreEqual(0, _score.Money);
        }

        [Test]
        public void AddMoney_Works()
        {
            long figure = 10000;
            _score.AddMoney(figure);

            Assert.AreEqual(figure, _score.Money);
        }

        [Test]
        public void AddMoney_Twice_Works()
        {
            _score.AddMoney(500);
            _score.AddMoney(500);

            Assert.AreEqual(1000, _score.Money);
        }

        [Test]
        public void AddMoney_NegativeAmount_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _score.AddMoney(-500));
        }

        [Test]
        public void SubtractMoney_Works()
        {
            _score.AddMoney(1000);
            _score.SubtractMoney(500);

            Assert.AreEqual(500, _score.Money);
        }

        [Test]
        public void SubtractMoney_NegativeAmount_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                _score.SubtractMoney(-500));
        }

        [Test]
        public void SubtractMoney_GoesNegative_Throws()
        {
            _score.AddMoney(500);

            Assert.Throws<BankruptcyException>(() =>
                _score.SubtractMoney(501)
                );
        }

        [Test]
        public void AddPrestige_Works()
        {
            _score.AddPrestige(10);

            Assert.AreEqual(10, _score.Prestige);
        }

        [Test]
        public void AddPrestige_NegativeAmount_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _score.AddPrestige(-5));
        }

        [Test]
        public void SubtractPrestige_Works()
        {
            _score.AddPrestige(10);

            _score.SubtractPrestige(5);

            Assert.AreEqual(5, _score.Prestige);
        }

        [Test]
        public void SubtractPrestige_GoesNegative_Throws()
        {
            _score.AddPrestige(500);

            Assert.Throws<BankruptcyException>(() => _score.SubtractPrestige(501));
        }

        [Test]
        public void SubtractPrestige_NegativeAmount_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _score.SubtractPrestige(-5));
        }
    }
}
