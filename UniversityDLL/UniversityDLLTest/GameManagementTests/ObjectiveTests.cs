﻿using System;
using Moq;
using NUnit.Framework;
using UniversityDLL.GameMangement;

namespace UniversityDLLTest.GameManagementTests
{
    [TestFixture]
    public class ObjectiveTests
    {
        private IObjective _objective;

        private readonly string _objectiveText = "here is your objective";

        [SetUp]
        public void BeforeEachTest()
        {
            _objective = new Objective(_objectiveText);
        }

        [Test]
        public void Ctor_SetsText()
        {
            Assert.AreEqual(_objectiveText, _objective.ToString());
        }

        [Test]
        public void IsComplete_HasCompletionPredicate_ExecutesCompletionPredicate()
        {
            bool hasExecuted = false;
            Func<bool> completionPredicate = () => { hasExecuted = true; return true; };
            _objective = new Objective(_objectiveText, completionPredicate, () => { });

            _objective.IsComplete();

            Assert.IsTrue(hasExecuted);
        }

        [Test]
        public void IsComplete_HasCompletionPredicate_DelegatesToCompletionPredicate()
        {
            var complete = new Objective(_objectiveText, () => true, () => { });
            var incomplete = new Objective(_objectiveText, () => false, () => { });

            Assert.IsTrue(complete.IsComplete());
            Assert.IsFalse(incomplete.IsComplete());
        }

        [Test]
        public void AddSubObjective_ExecutesSubObjectiveWhenQueried()
        {
            var subObjective = MakeCompleteMockObjective();
            _objective.AddSubObjective(subObjective.Object);

            _objective.IsComplete();

            subObjective.Verify(o => o.IsComplete());
        }

        [Test]
        public void IsComplete_AnyIncompleteSubobjectives_ReturnsFalse()
        {
            _objective.AddSubObjective(MakeCompleteMockObjective().Object);
            _objective.AddSubObjective(MakeCompleteMockObjective().Object);
            _objective.AddSubObjective(MakeIncompleteMockObjective().Object);
            _objective.AddSubObjective(MakeCompleteMockObjective().Object);

            var result = _objective.IsComplete();

            Assert.IsFalse(result);
        }

        [Test]
        public void IsComplete_CompletionPredicateIncomplete_ReturnsFalse()
        {
            _objective = new Objective(_objectiveText, () => false, () => { });
            _objective.AddSubObjective(MakeCompleteMockObjective().Object);
            _objective.AddSubObjective(MakeCompleteMockObjective().Object);
            _objective.AddSubObjective(MakeCompleteMockObjective().Object);

            var result = _objective.IsComplete();

            Assert.IsFalse(result);
        }

        [Test]
        public void ExecuteCompletionAction_HasCompletionAction_ExecutesCompletionAction()
        {
            var executed = false;
            _objective = new Objective(_objectiveText, () => true, () => executed = true);

            _objective.ExecuteCompletionAction();

            Assert.IsTrue(executed);
        }

        [Test]
        public void ExecuteCompletionAction_NotComplete_DoesNotExecuteCompletionAction()
        {
            var executed = false;
            _objective = new Objective(_objectiveText, () => false, () => executed = true);

            _objective.ExecuteCompletionAction();

            Assert.IsFalse(executed);
        }

        private Mock<IObjective> MakeCompleteMockObjective()
        {
            var objective = new Mock<IObjective>();
            objective.Setup(o => o.IsComplete()).Returns(true);
            return objective;
        }

        private Mock<IObjective> MakeIncompleteMockObjective()
        {
            var objective = new Mock<IObjective>();
            objective.Setup(o => o.IsComplete()).Returns(false);
            return objective;
        }
    }
}