﻿using Moq;
using NUnit.Framework;
using UniversityDLL.GameMangement.AsyncTasks;

namespace UniversityDLLTest.GameManagementTests.AsyncTaskTests
{
    [TestFixture]
    public class AsyncExecutorTests
    {
        private IAsyncExecutor _asyncExecutor;

        [SetUp]
        public void BeforeEachTest()
        {
            _asyncExecutor = new AsyncExecutor();
        }

        [Test]
        public void AddTask_IncrementsCount()
        {
            var task = new Mock<IAsyncTask>();

            _asyncExecutor.AddTask(task.Object);

            Assert.AreEqual(1, _asyncExecutor.QueuedTasks);
        }

        [Test]
        public void ExecuteNextTask_CallsExecuteOnNextTask()
        {
            var task = new Mock<IAsyncTask>();
            _asyncExecutor.AddTask(task.Object);

            _asyncExecutor.ExecuteNextTask();

            task.Verify(t => t.Execute());
        }

        [Test]
        public void ExecuteNextTask_DecrementsCount()
        {
            var task = new Mock<IAsyncTask>();
            _asyncExecutor.AddTask(task.Object);

            _asyncExecutor.ExecuteNextTask();

            Assert.AreEqual(0, _asyncExecutor.QueuedTasks);
        }

        [Test]
        public void ExecuteNextTask_Empty_DoesNotThrow()
        {
            _asyncExecutor.ExecuteNextTask();
        }
    }
}