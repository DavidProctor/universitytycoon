﻿using System.Linq;
using Moq;
using NUnit.Framework;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.GameManagementTests
{
    [TestFixture]
    class GameLoaderTest
    {
        private GameLoader _gameLoader;
        private Mock<IGrid> _mockGrid;
        private Mock<IGridGenerator> _mockGridGenerator;
        private Mock<IObjectFactoryFacade> _mockObjectFactory;
        private Mock<ICharacterFactory> _mockCharacterFactory;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockGridGenerator = new Mock<IGridGenerator>();
            _mockGrid = new Mock<IGrid>();
            _mockGridGenerator.Setup(gg => gg.GenerateGrid()).Returns(_mockGrid.Object);

            _mockCharacterFactory = new Mock<ICharacterFactory>();

            _mockObjectFactory = new Mock<IObjectFactoryFacade>();
            _mockObjectFactory.Setup(f => f.CharacterFactory).Returns(_mockCharacterFactory.Object);

            _gameLoader = new GameLoader();
        }

        [Test]
        public void LoadGame_ReturnsGameController()
        {
            var actual = _gameLoader.LoadGame(_mockGridGenerator.Object, _mockObjectFactory.Object);

            Assert.IsNotNull(actual);
        }

        [Test]
        public void LoadGame_CreatesGrid()
        {
            var actual = _gameLoader.LoadGame(_mockGridGenerator.Object, _mockObjectFactory.Object);

            _mockGridGenerator.Verify(gg => gg.GenerateGrid());
            Assert.AreEqual(_mockGrid.Object, actual.Grid);
        }

        [Test]
        public void LoadGame_ConnectsGridNeighbors()
        {
            _gameLoader.LoadGame(_mockGridGenerator.Object, _mockObjectFactory.Object);

            _mockGrid.Verify(g => g.ConnectNeighbors());
        }

        [Test]
        public void LoadGame_CreatesCalendar()
        {
            var actual = _gameLoader.LoadGame(_mockGridGenerator.Object, _mockObjectFactory.Object);

            Assert.IsNotNull(actual.Calendar);
        }

        [Test]
        public void LoadGame_CreatesScore()
        {
            var actual = _gameLoader.LoadGame(_mockGridGenerator.Object, _mockObjectFactory.Object);

            Assert.IsNotNull(actual.Score);
        }

        [Test]
        public void LoadGame_AddsStandardCalendarEvents()
        {
            var actual = _gameLoader.LoadGame(_mockGridGenerator.Object, _mockObjectFactory.Object);

            var events = actual.Calendar.FutureEvents;
            Assert.IsNotNull(events.FirstOrDefault(e => e is AdmissionsCalendarEvent));
            Assert.IsNotNull(events.FirstOrDefault(e => e is TuitionCalendarEvent));
            Assert.IsNotNull(events.FirstOrDefault(e => e is CourseSelectionCalendarEvent));
        }
    }
}
