﻿using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.People;

namespace UniversityDLLTest.GameManagementTests.FactoryTests
{
    [TestFixture]
    class CharacterFactoryTests
    {
        private ICharacterFactory _characterFactory;

        private Mock<IGameState> _mockGameState;
        private Mock<IGameObjectFactory> _mockGameObjectFactory;
        private Mock<INameGenerator> _mockNameGenerator;
        private Mock<ICharacterBehavior> _mockCharacterBehavior;

        private class TestableCharacterFactory : CharacterFactory
        {
            private readonly ICharacterBehavior _characterBehavior;

            public TestableCharacterFactory(IGameState gameState, 
                                            IGameObjectFactory gameObjectFactory, 
                                            INameGenerator nameGenerator,
                                            ICharacterBehavior characterBehavior1) 
                : base(gameState, gameObjectFactory, nameGenerator)
            {
                _characterBehavior = characterBehavior1;
            }

            protected override ICharacterBehavior MakeCharacterBehavior(IGameObject gameObject, CharacterType type)
            {
                return _characterBehavior;
            }
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _mockGameState = new Mock<IGameState>();
            _mockGameObjectFactory = new Mock<IGameObjectFactory>();
            _mockNameGenerator = new Mock<INameGenerator>();
            _mockCharacterBehavior = new Mock<ICharacterBehavior>();

            _characterFactory = new TestableCharacterFactory(_mockGameState.Object, 
                                                             _mockGameObjectFactory.Object, 
                                                             _mockNameGenerator.Object,
                                                             _mockCharacterBehavior.Object);
        }

        [Test]
        public void MakeStudent_CreatesStudentObject()
        {
            var major = new Mock<IDepartment>();
            _mockGameState.SetupGet(s => s.Departments).Returns(new[] {major.Object});
            var model = new Mock<IGameObject>();
            _mockGameObjectFactory.Setup(f => f.MakeModelWrapper(It.IsAny<string>())).Returns(model.Object);
            var name = "Generated name";
            _mockNameGenerator.Setup(g => g.GetName(It.IsAny<Sex>())).Returns(name);

            var student = _characterFactory.MakeStudent();

            Assert.IsNotNull(student);
            Assert.AreEqual(name, student.Name);
            Assert.AreEqual(major.Object, student.Major);
            Assert.IsInstanceOf<CharacterRenderer>(student.Renderer);
            Assert.AreEqual(model.Object, student.Renderer.GameObject);
            Assert.AreEqual(_mockCharacterBehavior.Object, student.Behavior);
            Assert.IsInstanceOf<PersonalSchedule>(student.PersonalSchedule);
            _mockCharacterBehavior.VerifySet(c => c.Character = student);
            model.Verify(m => m.SetClickableObject(student));
        }
    }
}
