﻿using Moq;
using NUnit.Framework;
using UniversityDLL.GameMangement.Factories;

namespace UniversityDLLTest.GameManagementTests.FactoryTests
{
    [TestFixture]
    public class ObjectFactoryFacadeTests
    {
        [Test]
        public void MakeStudent_DelegatesToCharacterFactory()
        {
            var mockCharacterFactory = new Mock<ICharacterFactory>();
            var objectFactoryFacade = new ObjectFactoryFacade(mockCharacterFactory.Object);

            objectFactoryFacade.MakeStudent();

            mockCharacterFactory.Verify(f => f.MakeStudent());
        }
    }
}