﻿using NUnit.Framework;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    public class NullTileContentTest
    {
        private ITileContent _nullTileContent;

        [SetUp]
        public void BeforeEachTest()
        {
            _nullTileContent = new NullTileContent();
        }

        [Test]
        public void GameObject_ReturnsNullGameObject()
        {
            var actual = _nullTileContent.GameObject;
            
            Assert.IsTrue(actual is NullGameObject);
        }

        [Test]
        public void Render_ReturnsNullGameObject()
        {
            Assert.IsTrue(_nullTileContent.Render(1f, 2f, 3f) is NullGameObject);
            Assert.IsTrue(_nullTileContent.Render(100f, 200f, 300f, Color.black) is NullGameObject);
        }

        [Test]
        public void PurchaseCose_ReturnsZero()
        {
            Assert.AreEqual(0, _nullTileContent.PurchaseCost);
        }

        [Test]
        public void Layer_ReturnsNullLayer()
        {
            Assert.AreEqual(TileLayer.NullLayer, _nullTileContent.Layer);
        }

        [Test]
        public void GetClone_ReturnsClone()
        {
            var actual = _nullTileContent.GetClone();
            Assert.IsTrue(actual is NullTileContent);
            Assert.AreNotSame(actual, _nullTileContent);
        }

        [Test]
        public void Show_DoesNotThrow()
        {
            _nullTileContent.Show();
        }

        [Test]
        public void Hide_DoesNotThrow()
        {
            _nullTileContent.Hide();
        }

        [Test]
        public void Destroy_DoesNotThrow()
        {
            _nullTileContent.Destroy();
        }

        [Test]
        public void Orientation_IsForward()
        {
            Assert.AreEqual(Orientation.Forward, _nullTileContent.Orientation);
        }

        [Test]
        public void SetOrientation_DoesNotThrow()
        {
            _nullTileContent.SetOrientation(Orientation.Right);
        }

        [Test]
        public void Rotate_DoesNotThrow()
        {
            _nullTileContent.Rotate();
        }
    }
}