﻿using System;
using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    class TileContentBuilderTest
    {
        private TileContentBuilder _tileContentBuilder;

        private Mock<IGameObject> _mockGameObject;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockGameObject = DataFactory.MakeSelfReplicatingGameObject();

            _tileContentBuilder = new TileContentBuilder();
        }

        [Test]
        public void Build_WithoutGameObject_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => _tileContentBuilder.Build());
        }

        [Test]
        public void Build_ReturnsTileContent()
        {
            _tileContentBuilder.SetPrototype(_mockGameObject.Object);

            var actual = _tileContentBuilder.Build();

            Assert.IsInstanceOf<TileContent>(actual);
        }

        [Test]
        public void SetPrototype_ReturnedContentHasGameObject()
        {
            var name = "Test name";
            _mockGameObject.Setup(g => g.Name).Returns(name);
            _tileContentBuilder.SetPrototype(_mockGameObject.Object);

            var actual = _tileContentBuilder.Build();

            Assert.AreEqual(name, actual.GameObject.Name);
        }

        [Test]
        public void SetPrototype_FluentInterface()
        {
            var actual = _tileContentBuilder.SetPrototype(_mockGameObject.Object);

            Assert.AreSame(actual, _tileContentBuilder);
        }

        [Test]
        public void SetCost_SetsCost()
        {
            _tileContentBuilder.SetPrototype(_mockGameObject.Object);
            var cost = 100L;

            _tileContentBuilder.SetCost(cost);

            var actual = _tileContentBuilder.Build();
            Assert.AreEqual(cost, actual.PurchaseCost);
        }

        [Test]
        public void SetCost_FluentInterface()
        {
            var actual = _tileContentBuilder.SetCost(100L);

            Assert.AreEqual(actual, _tileContentBuilder);
        }

        [Test]
        public void SetCost_NegativeFigure_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                _tileContentBuilder.SetCost(-50)
                );
        }

        [Test]
        public void SetLayer_SetsLayer()
        {
            _tileContentBuilder.SetPrototype(_mockGameObject.Object);
            var layer = TileLayer.Structure;

            _tileContentBuilder.SetLayer(layer);

            var actual = _tileContentBuilder.Build();
            Assert.AreEqual(layer, actual.Layer);
        }

        [Test]
        public void SetLayer_FluentInterface()
        {
            var actual = _tileContentBuilder.SetLayer(TileLayer.Structure);

            Assert.AreEqual(actual, _tileContentBuilder);
        }

        [Test]
        public void SetOrientation_SetsRotation()
        {
            _tileContentBuilder.SetPrototype(_mockGameObject.Object);
            var orientation = Orientation.Right;

            _tileContentBuilder.SetRotation(orientation);

            var actual = _tileContentBuilder.Build();
            Assert.AreEqual(orientation, actual.Orientation);
        }

        [Test]
        public void SetRotation_FluentInterface()
        {
            var actual = _tileContentBuilder.SetRotation(Orientation.Reverse);

            Assert.AreSame(_tileContentBuilder, actual);
        }

        [Test]
        public void Build_RotationDefaultsToForward()
        {
            var actual = _tileContentBuilder
                .SetPrototype(_mockGameObject.Object)
                .Build();

            Assert.AreEqual(Orientation.Forward, actual.Orientation);
        }
    }
}
