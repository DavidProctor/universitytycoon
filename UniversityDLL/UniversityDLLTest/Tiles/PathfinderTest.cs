﻿using Moq;
using NUnit.Framework;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    class PathfinderTest
    {
        private IPathfinder<IWaypoint> _pathfinder;
        private CompleteMockTile[,] _tiles;
        private Mock<IGrid> _grid;

        // Tile field is set up with _ being walkable and X not:
        //    0 1 2 3 4
        // 0  _ _ _ X _
        // 1  X _ _ X _
        // 2  _ _ _ X _
        // 3  _ _ _ X _
        // 4  _ _ _ X _
        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            _grid = new Mock<IGrid>();
            _tiles = DataFactory.MakeMockTileGrid(5, 5, _grid);

            MakeTileNonWalkable(_tiles[3, 0]);
            MakeTileNonWalkable(_tiles[3, 1]);
            MakeTileNonWalkable(_tiles[3, 2]);
            MakeTileNonWalkable(_tiles[3, 3]);
            MakeTileNonWalkable(_tiles[3, 4]);
            MakeTileNonWalkable(_tiles[0, 1]);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _pathfinder = Pathfinder<IWaypoint>.GetInstance();
        }

        [Test]
        public void SeparateInstancesForDifferentTypeParamaters()
        {
            var waypointPathfinder = Pathfinder<IWaypoint>.GetInstance();
            var tilePathfinder = Pathfinder<ITile>.GetInstance();

            Assert.AreNotSame(waypointPathfinder, tilePathfinder);
        }

        [Test]
        public void TryFindPath_StartIsNotWalkable_Throws()
        {
            Assert.Throws<NoPathExistsException>(() => _pathfinder.TryFindPath(TileAt(3, 0), TileAt(2, 0)));
        }

        [Test]
        public void TryFindPath_EndIsNotWalkable_ReturnsFalse()
        {
            Assert.Throws<NoPathExistsException>(() => _pathfinder.TryFindPath(TileAt(0, 0), TileAt(3, 0)));
        }

        [Test]
        public void TryFindPath_StraightLine_ReturnsTrueAndStraightLine()
        {
            var path = _pathfinder.TryFindPath(TileAt(0, 0), TileAt(2, 0));
            
            Assert.AreEqual(3, path.Count);
            Assert.AreEqual(TileAt(0, 0), path[0]);
            Assert.AreEqual(TileAt(1, 0), path[1]);
            Assert.AreEqual(TileAt(2, 0), path[2]);
        }

        [Test]
        public void TryFindPath_WindsAroundObstacle_ReturnsTrueAndCorrectPath()
        {
            var path = _pathfinder.TryFindPath(TileAt(0, 0), TileAt(0, 3));

            Assert.AreEqual(4, path.Count);
            Assert.AreEqual(TileAt(0, 0), path[0]);
            Assert.AreEqual(TileAt(1, 1), path[1]);
            Assert.AreEqual(TileAt(0, 2), path[2]);
            Assert.AreEqual(TileAt(0, 3), path[3]);
        }

        [Test]
        public void TryFindPath_NoPathExists_Throws()
        {
            Assert.Throws<NoPathExistsException>(() => _pathfinder.TryFindPath(TileAt(0, 0), TileAt(4, 0)));
        }

        private void MakeTileNonWalkable(CompleteMockTile tile)
        {
            tile.Tile.Setup(t => t.IsWalkable()).Returns(false);
        }

        private ITile TileAt(int x, int z)
        {
            return _tiles[x, z].Tile.Object;
        }
    }
}
