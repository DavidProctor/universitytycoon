﻿using Moq;
using NUnit.Framework;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    public class PathConnectionTest
    {
        private IPathConnection _pathConnection;

        private Mock<IWaypoint> _node1;
        private Mock<IWaypoint> _node2;

        [SetUp]
        public void BeforeEachTest()
        {
            _node1 = new Mock<IWaypoint>();
            _node2 = new Mock<IWaypoint>();

            _pathConnection = PathConnection.GetConnection(_node1.Object, _node2.Object);
        }

        [Test]
        public void Nodes_Works()
        {
            var actual = _pathConnection.Nodes;

            CollectionAssert.Contains(actual, _node1.Object);
            CollectionAssert.Contains(actual, _node2.Object);
        }

        [Test]
        public void Equals_ReturnsTrueForEquivalent()
        {
            var equivalent = PathConnection.GetConnection(_node2.Object, _node1.Object);

            Assert.AreEqual(_pathConnection, equivalent);
        }

        [Test]
        public void GetHashCode_SameForEquivalent()
        {
            var equivalent = PathConnection.GetConnection(_node2.Object, _node1.Object);

            Assert.AreEqual(equivalent.GetHashCode(), _pathConnection.GetHashCode());
        }

        [Test]
        public void GetHashCode_DifferentForDifferentNodes()
        {
            var different = PathConnection.GetConnection(new Mock<IWaypoint>().Object, _node1.Object);

            Assert.AreNotEqual(different.GetHashCode(), _pathConnection.GetHashCode());
        }
    }
}