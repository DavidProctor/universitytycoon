﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    public class PathProviderTest
    {
        class TestablePathProvider : PathProvider
        {
            public TestablePathProvider(IPathCache<ITile> tilePathCache, IPathCache<Room> roomPathCache)
                : base(tilePathCache, roomPathCache)
            { }
        }

        private IPathProvider _pathProvider;
        private CompleteMockTile[,] _tiles;

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            var grid = new Mock<IGrid>();
            _tiles = DataFactory.MakeMockTileGrid(5, 5, grid);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _pathProvider = PathProvider.GetInstance();
        }

        [Test]
        public void GetPath_TilesAreSame_ReturnsSingleTile()
        {
            var path = _pathProvider.GetPath(_tiles[0, 0].Tile.Object, _tiles[0, 0].Tile.Object);

            CollectionAssert.Contains(path, _tiles[0,0].Tile.Object);
        }

        [Test]
        public void GetPath_StartNotWalkable_Throws()
        {
            _tiles[0, 0].Tile.Setup(t => t.IsWalkable()).Returns(false);

            Assert.Throws<NoPathExistsException>(
                () => _pathProvider.GetPath(_tiles[0, 0].Tile.Object, _tiles[0, 1].Tile.Object));

            //Cleanup
            _tiles[0, 0].Tile.Setup(t => t.IsWalkable()).Returns(true);
        }

        [Test]
        public void GetPath_EndNotWalkable_Throws()
        {
            _tiles[0, 0].Tile.Setup(t => t.IsWalkable()).Returns(false);

            Assert.Throws<NoPathExistsException>(
                () => _pathProvider.GetPath(_tiles[0, 1].Tile.Object, _tiles[0, 0].Tile.Object));

            //Cleanup
            _tiles[0, 0].Tile.Setup(t => t.IsWalkable()).Returns(true);
        }

        [Test]
        public void GetPath_TilesAreInSameRoom_ReturnsPath()
        {
            SetupRoomWithTiles(_tiles[0, 0], _tiles[0, 2]);

            var path = _pathProvider.GetPath(_tiles[0, 0].Tile.Object, _tiles[0, 2].Tile.Object);

            Assert.AreEqual(3, path.Count);
            Assert.AreEqual(_tiles[0,0].Tile.Object, path[0]);
            Assert.AreEqual(_tiles[0,1].Tile.Object, path[1]);
            Assert.AreEqual(_tiles[0,2].Tile.Object, path[2]);
        }

        [Test]
        public void GetPath_DifferentRoom_ReturnsPath()
        {
            var room1 = SetupRoomWithTiles(_tiles[0, 0]);
            var room2 = SetupRoomWithTiles(_tiles[2, 1]);
            PlaceExitBetweenRooms(room1, room2, _tiles[1,1]);

            var path = _pathProvider.GetPath(_tiles[0, 0].Tile.Object, _tiles[2, 1].Tile.Object);

            Assert.AreEqual(3, path.Count);
            Assert.AreEqual(_tiles[0, 0].Tile.Object, path[0]);
            Assert.AreEqual(_tiles[1, 1].Tile.Object, path[1]);
            Assert.AreEqual(_tiles[2, 1].Tile.Object, path[2]);
        }

        [Test]
        public void GetPath_MultipleRooms_ReturnsPath()
        {
            var room1 = SetupRoomWithTiles(_tiles[0, 0]);
            var room2 = SetupRoomWithTiles(_tiles[2, 1]);
            var room3 = SetupRoomWithTiles(_tiles[3, 1], _tiles[4,4]);
            PlaceExitBetweenRooms(room1, room2, _tiles[1,1]);
            PlaceExitBetweenRooms(room2, room3, _tiles[3,1]);

            var path = _pathProvider.GetPath(_tiles[0, 0].Tile.Object, _tiles[4, 4].Tile.Object);

            CollectionAssert.AllItemsAreUnique(path);
            Assert.AreEqual(_tiles[0, 0].Tile.Object, path[0]);
            Assert.AreEqual(_tiles[4, 4].Tile.Object, path[path.Count - 1]);
            CollectionAssert.Contains(path, _tiles[1,1].Tile.Object);
            CollectionAssert.Contains(path, _tiles[3,1].Tile.Object);
        }

        [Test]
        public void AddDestinationTile_CreatesConnectionsToAllDoors()
        {
            var tilePathCache = new Mock<IPathCache<ITile>>();
            var roomPathCache = new Mock<IPathCache<Room>>();
            var testablePathProvider = new TestablePathProvider(tilePathCache.Object, roomPathCache.Object);
            var room = SetupRoomWithTiles(_tiles[2, 0]);
            var connection1 = new Mock<Room>();
            var connection2 = new Mock<Room>();
            PlaceExitBetweenRooms(room, connection1, _tiles[0,1]);
            PlaceExitBetweenRooms(room, connection2, _tiles[2,3]);

            testablePathProvider.AddDestinationTile(_tiles[2,0].Tile.Object);

            tilePathCache.Verify(c => c.CachePathAsync(_tiles[0,1].Tile.Object, _tiles[2,0].Tile.Object));
            tilePathCache.Verify(c => c.CachePathAsync(_tiles[2,3].Tile.Object, _tiles[2,0].Tile.Object));
        }

        //TODO Use the exit that is nearest

        private Mock<Room> SetupRoomWithTiles(params CompleteMockTile[] tiles)
        {
            var neighbors = new List<IWaypoint>();
            var exits = new List<ITile>();
            var room = new Mock<Room>();
            room.Setup(r => r.IsWalkable()).Returns(true);
            room.SetupGet(r => r.Neighbors).Returns(neighbors);
            room.Setup(r => r.GetAllExits()).Returns(exits);
            room.Setup(r => r.AddConnection(It.IsAny<IWaypoint>()))
                .Callback<IWaypoint>(r => neighbors.Add(r));
            room.Setup(r => r.AddExit(It.IsAny<ITile>(), It.IsAny<Room>()))
                .Callback<ITile, Room>((t, r) => exits.Add(t));
            foreach (var tile in tiles)
            {
                tile.Tile.SetupGet(t => t.Room).Returns(room.Object);
            }
            return room;
        }

        private void PlaceExitBetweenRooms(Mock<Room> room1, Mock<Room> room2, CompleteMockTile tile)
        {
            room1.Object.AddExit(tile.Tile.Object, room2.Object);
            room2.Object.AddExit(tile.Tile.Object, room1.Object);

            room1.Setup(r => r.GetExitsToRoom(room2.Object)).Returns(new List<ITile> { tile.Tile.Object });
            room2.Setup(r => r.GetExitsToRoom(room1.Object)).Returns(new List<ITile> { tile.Tile.Object });

            room1.Object.AddConnection(room2.Object);
            room2.Object.AddConnection(room1.Object);
        }
    }
}