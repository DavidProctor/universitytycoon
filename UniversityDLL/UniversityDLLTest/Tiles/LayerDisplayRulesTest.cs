﻿using NUnit.Framework;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    public class LayerDisplayRulesTest
    {
        private readonly ILayerDisplayRules _layerDisplayRules = LayerDisplayRules.GetInstance();

        [Test]
        public void GetLayersObscuredByPreview_ByDoor_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.Door);

            CollectionAssert.DoesNotContain(result, TileLayer.Ground);
            CollectionAssert.DoesNotContain(result, TileLayer.Basement);
            CollectionAssert.DoesNotContain(result, TileLayer.Floor);
            CollectionAssert.Contains(result, TileLayer.Structure);
            CollectionAssert.Contains(result, TileLayer.ActivityPoint);
            CollectionAssert.Contains(result, TileLayer.Wall);
            CollectionAssert.Contains(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByWall_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.Wall);

            CollectionAssert.DoesNotContain(result, TileLayer.Ground);
            CollectionAssert.DoesNotContain(result, TileLayer.Basement);
            CollectionAssert.DoesNotContain(result, TileLayer.Floor);
            CollectionAssert.Contains(result, TileLayer.Structure);
            CollectionAssert.Contains(result, TileLayer.ActivityPoint);
            CollectionAssert.Contains(result, TileLayer.Wall);
            CollectionAssert.Contains(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByActivityPoint_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.ActivityPoint);

            CollectionAssert.DoesNotContain(result, TileLayer.Ground);
            CollectionAssert.DoesNotContain(result, TileLayer.Basement);
            CollectionAssert.DoesNotContain(result, TileLayer.Floor);
            CollectionAssert.Contains(result, TileLayer.Structure);
            CollectionAssert.Contains(result, TileLayer.ActivityPoint);
            CollectionAssert.Contains(result, TileLayer.Wall);
            CollectionAssert.Contains(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByStructure_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.Structure);

            CollectionAssert.DoesNotContain(result, TileLayer.Ground);
            CollectionAssert.DoesNotContain(result, TileLayer.Basement);
            CollectionAssert.DoesNotContain(result, TileLayer.Floor);
            CollectionAssert.Contains(result, TileLayer.Structure);
            CollectionAssert.Contains(result, TileLayer.ActivityPoint);
            CollectionAssert.Contains(result, TileLayer.Wall);
            CollectionAssert.Contains(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByFloor_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.Floor);

            CollectionAssert.Contains(result, TileLayer.Ground);
            CollectionAssert.Contains(result, TileLayer.Basement);
            CollectionAssert.Contains(result, TileLayer.Floor);
            CollectionAssert.DoesNotContain(result, TileLayer.Structure);
            CollectionAssert.DoesNotContain(result, TileLayer.ActivityPoint);
            CollectionAssert.DoesNotContain(result, TileLayer.Wall);
            CollectionAssert.DoesNotContain(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByBasement_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.Basement);

            CollectionAssert.Contains(result, TileLayer.Ground);
            CollectionAssert.Contains(result, TileLayer.Basement);
            CollectionAssert.Contains(result, TileLayer.Floor);
            CollectionAssert.DoesNotContain(result, TileLayer.Structure);
            CollectionAssert.DoesNotContain(result, TileLayer.ActivityPoint);
            CollectionAssert.DoesNotContain(result, TileLayer.Wall);
            CollectionAssert.DoesNotContain(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByGround_HidesCorrectLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.Ground);

            CollectionAssert.Contains(result, TileLayer.Ground);
            CollectionAssert.Contains(result, TileLayer.Basement);
            CollectionAssert.Contains(result, TileLayer.Floor);
            CollectionAssert.DoesNotContain(result, TileLayer.Structure);
            CollectionAssert.DoesNotContain(result, TileLayer.ActivityPoint);
            CollectionAssert.DoesNotContain(result, TileLayer.Wall);
            CollectionAssert.DoesNotContain(result, TileLayer.Door);
        }

        [Test]
        public void GetLayersObscuredByPreview_ByNull_HidesNoLayers()
        {
            var result = _layerDisplayRules.GetLayersObscuredByPreview(TileLayer.NullLayer);

            CollectionAssert.DoesNotContain(result, TileLayer.Ground);
            CollectionAssert.DoesNotContain(result, TileLayer.Basement);
            CollectionAssert.DoesNotContain(result, TileLayer.Floor);
            CollectionAssert.DoesNotContain(result, TileLayer.Structure);
            CollectionAssert.DoesNotContain(result, TileLayer.ActivityPoint);
            CollectionAssert.DoesNotContain(result, TileLayer.Wall);
            CollectionAssert.DoesNotContain(result, TileLayer.Door);
        }
    }
}