﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    public class PathCacheTest
    {
        private IPathCache<IWaypoint> _waypointPathCache;
        private Mock<IWaypoint> _mockWaypoint1;
        private Mock<IWaypoint> _mockWaypoint2;
        private Mock<IWaypoint> _mockWaypoint3;
        private Mock<IWaypoint> _mockWaypoint4;
        private List<IWaypoint> _forwardPath;
        private List<IWaypoint> _reversePath;
        private Mock<IPathfinder<IWaypoint>> _mockWaypointPathfinder;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockWaypoint1 = new Mock<IWaypoint>();
            _mockWaypoint2 = new Mock<IWaypoint>();
            _mockWaypoint3 = new Mock<IWaypoint>();
            _mockWaypoint4 = new Mock<IWaypoint>();

            _forwardPath = new List<IWaypoint>
            {
                _mockWaypoint1.Object,
                _mockWaypoint2.Object,
                _mockWaypoint3.Object,
                _mockWaypoint4.Object
            };
            _reversePath = new List<IWaypoint>
            {
                _mockWaypoint4.Object,
                _mockWaypoint3.Object,
                _mockWaypoint2.Object,
                _mockWaypoint1.Object
            };

            _mockWaypointPathfinder = new Mock<IPathfinder<IWaypoint>>();

            _mockWaypointPathfinder.Setup(p => p.TryFindPath(_mockWaypoint1.Object, _mockWaypoint4.Object)).Returns(_forwardPath);
            _mockWaypointPathfinder.Setup(p => p.TryFindPath(_mockWaypoint4.Object, _mockWaypoint1.Object)).Returns(_reversePath);

            _waypointPathCache = new PathCache<IWaypoint>(_mockWaypointPathfinder.Object);
        }

        [Test]
        public void GetPath_FirstTime_ObtainsFromPathfinder()
        {
            var path = _waypointPathCache.GetPath(_mockWaypoint1.Object, _mockWaypoint4.Object);

            _mockWaypointPathfinder.Verify(p => p.TryFindPath(It.IsAny<IWaypoint>(), It.IsAny<IWaypoint>()), Times.Once);
            Assert.AreEqual(_forwardPath[0], path[0]);
            Assert.AreEqual(_forwardPath[1], path[1]);
            Assert.AreEqual(_forwardPath[2], path[2]);
            Assert.AreEqual(_forwardPath[3], path[3]);
        }

        [Test]
        public void GetPath_SecondTime_UsesCachedValue()
        {
            _waypointPathCache.GetPath(_mockWaypoint1.Object, _mockWaypoint4.Object);
            _waypointPathCache.GetPath(_mockWaypoint1.Object, _mockWaypoint4.Object);

            _mockWaypointPathfinder.Verify(p => p.TryFindPath(It.IsAny<IWaypoint>(), It.IsAny<IWaypoint>()), Times.Once);
        }

        [Test]
        public void GetPath_Reversed_UsesCachedValue()
        {
            _waypointPathCache.GetPath(_mockWaypoint1.Object, _mockWaypoint4.Object);
            var path = _waypointPathCache.GetPath(_mockWaypoint4.Object, _mockWaypoint1.Object);

            _mockWaypointPathfinder.Verify(p => p.TryFindPath(It.IsAny<IWaypoint>(), It.IsAny<IWaypoint>()), Times.Once);
            Assert.AreEqual(_reversePath[0], path[0]);
            Assert.AreEqual(_reversePath[1], path[1]);
            Assert.AreEqual(_reversePath[2], path[2]);
            Assert.AreEqual(_reversePath[3], path[3]);
        }

        [Test]
        public void GetPath_NoPathExists_Throws()
        {
            _mockWaypointPathfinder.Setup(t => t.TryFindPath(It.IsAny<IWaypoint>(), It.IsAny<IWaypoint>()))
                .Throws(new NoPathExistsException());

            Assert.Throws<NoPathExistsException>(() => _waypointPathCache.GetPath(_mockWaypoint1.Object, _mockWaypoint4.Object));
        }
    }
}