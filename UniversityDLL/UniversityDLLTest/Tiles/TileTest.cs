﻿using System;
using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    public class TileTest
    {
        private Tile _tile;
        private Mock<ITileContent> _tileContent;
        private Mock<ITileContentCollection> _tileContentCollection;
        private Mock<Room> _room;

        private int testXpos = 10;
        private int testZpos = 20;

        [SetUp]
        public void BeforeEachTest()
        {
            _tileContent = new Mock<ITileContent>();
            _tileContentCollection = new Mock<ITileContentCollection>();
            _room = new Mock<Room>();

            _tile = new Tile(_tileContentCollection.Object, _room.Object);
        }

        [Test]
        public void Ctor_SetsTileOnTileContentCollection()
        {
            _tileContentCollection.VerifySet(c => c.Tile = It.IsAny<ITile>());
        }

        [Test]
        public void Ctor_SetsRoom()
        {
            Assert.AreEqual(_room.Object, _tile.Room);
        }

        [Test]
        public void SetPosition_Works()
        {
            _tile.SetPosition(testXpos, testZpos);

            Assert.AreEqual(testXpos, _tile.X);
            Assert.AreEqual(testZpos, _tile.Z);
        }

        [Test]
        public void SetPosition_NegativeXValue_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _tile.SetPosition(-1, 10));
        }

        [Test]
        public void SetPosition_NegativeYValue_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _tile.SetPosition(10, -1));
        }

        [Test]
        public void AddNeighbor_AddsNeighbor()
        {
            var neighbor = new Tile(_tileContentCollection.Object, _room.Object);

            _tile.AddConnection(neighbor);

            CollectionAssert.Contains(_tile.Neighbors, neighbor);
        }

        [Test]
        public void RemoveNeighbor_RemovesNeighbor()
        {
            var neighbor = new Tile(_tileContentCollection.Object, _room.Object);
            _tile.AddConnection(neighbor);

            _tile.RemoveConnection(neighbor);

            CollectionAssert.DoesNotContain(_tile.Neighbors, neighbor);
        }

        [Test]
        public void Connections_ReturnsPathConnectionsToNeighbors()
        {
            var neighbor = new Tile(_tileContentCollection.Object, _room.Object);
            _tile.AddConnection(neighbor);
            var expectedConnection = PathConnection.GetConnection(_tile, neighbor);

            CollectionAssert.Contains(_tile.Connections, expectedConnection);
        }

        [Test]
        public void AddTileContent_Works()
        {
            _tile.AddTileContent(_tileContent.Object);

            _tileContentCollection.Verify(c => c.AddTileContent(_tileContent.Object));
        }

        [Test]
        public void AddTileContent_AddsToTileContentCollection()
        {
            _tile.AddTileContent(_tileContent.Object);

            _tileContentCollection.Verify(c => c.AddTileContent(_tileContent.Object));
        }

        [Test]
        public void Destroy_DestroysTileContentCollectionAndRemovesPointers()
        {
            var neighbor = new Tile(_tileContentCollection.Object, _room.Object);
            _tile.AddConnection(neighbor);

            _tile.Destroy();

            _tileContentCollection.Verify(c => c.Destroy());
            Assert.IsNull(_tile.TileContentCollection);
            CollectionAssert.IsEmpty(_tile.Neighbors);
        }

        [Test]
        public void IsWalkable_QueriesTileContentCollection()
        {
            _tileContentCollection.Setup(t => t.IsWalkable()).Returns(true);

            var result = _tile.IsWalkable();

            Assert.IsTrue(result);
            _tileContentCollection.Verify(c => c.IsWalkable());
        }

        [Test]
        public void IsBuildValid_QueriesTileContentCollection()
        {
            _tileContentCollection.Setup(t => t.BuildIsValid(_tileContent.Object)).Returns(true);

            var result = _tile.BuildIsValid(_tileContent.Object);

            Assert.IsTrue(result);
            _tileContentCollection.Verify(c => c.BuildIsValid(_tileContent.Object));
        }

        [Test]
        public void HasLayer_QueriesTileContentCollection()
        {
            _tileContentCollection.Setup(c => c.HasLayer(It.IsAny<TileLayer>())).Returns(true);

            var result = _tile.HasLayer(TileLayer.Wall);

            Assert.IsTrue(result);
            _tileContentCollection.Verify(c => c.HasLayer(TileLayer.Wall));
        }

        [Test]
        public void SetActivityPoint_Works()
        {
            var point = new Mock<ActivityPoint>();

            _tile.SetActivityPoint(point.Object);

            Assert.AreEqual(point.Object, _tile.ActivityPoint);
        }

        [Test]
        public void GetActivityPoint_NoActivityPoint_ReturnsNull()
        {
            Assert.IsNull(_tile.ActivityPoint);
        }
    }
}