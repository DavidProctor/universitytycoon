﻿using Moq;
using NUnit.Framework;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    class TileContentTest
    {
        private ITileContent _tileContent;
        private Mock<IGameObject> _gameObject;

        private const float TestX = 11f;
        private const float TestY = 0f;
        private const float TestZ = -3.5f;
        private const long TestCost = 100L;
        private const TileLayer TestLayer = TileLayer.Structure;
        private readonly Color _testColor = new Color(1f, 0.5f, 0.3f, 0.111111f);

        [SetUp]
        public void BeforeEachTest()
        {
            _gameObject = DataFactory.MakeSelfReplicatingGameObject();

            _tileContent = new TileContent(_gameObject.Object, TestCost, TestLayer);
        }

        [Test]
        public void Ctor_SetsGameObjectCostAndOrientation()
        {
            long cost = 10;
            var orientation = Orientation.Forward;
            _tileContent = new TileContent(_gameObject.Object, cost, TestLayer, orientation);

            Assert.AreEqual(cost, _tileContent.PurchaseCost);
            Assert.AreEqual(_gameObject.Object, _tileContent.GameObject);
            Assert.AreEqual(orientation, _tileContent.Orientation);

            _gameObject.Verify(g => g.SetOrientation(orientation));
        }

        [Test]
        public void GetClone_Works()
        {
            var actual = _tileContent.GetClone();

            Assert.AreEqual(actual.PurchaseCost, _tileContent.PurchaseCost);
            Assert.AreEqual(actual.Layer, _tileContent.Layer);
            Assert.AreEqual(actual.Orientation, _tileContent.Orientation);
            Assert.AreNotSame(_tileContent, actual);
        }

        [Test]
        public void Render_Works()
        {
            _tileContent.Render(TestX, TestY, TestZ);

            _gameObject.Verify(g => g.Render(TestX, TestY, TestZ));
        }

        [Test]
        public void Render_WithColor_Works()
        {
            _tileContent.Render(TestX, TestY, TestZ, _testColor);

            _gameObject.Verify(g => g.Render(TestX, TestY, TestZ, _testColor));
        }

        [Test]
        public void Render_WithOrientation_Works()
        {
            var orientation = Orientation.Left;
            _tileContent = new TileContent(_gameObject.Object, 0, TestLayer, orientation);

            _tileContent.Render(TestX, TestY, TestZ);
            
            _gameObject.Verify(c => c.SetOrientation(orientation));
        }

        [Test]
        public void Render_WithRotationAndColor_Works()
        {
            var orientation = Orientation.Forward;
            _tileContent = new TileContent(_gameObject.Object, 0, TestLayer, orientation);

            _tileContent.Render(TestX, TestY, TestZ, _testColor);

            _gameObject.Verify(g => g.SetOrientation(orientation));
        }

        [Test]
        public void Render_ReturnsRenderedObject()
        {
            _gameObject.Setup(g => g.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>())).Returns(_gameObject.Object);

            var actual = _tileContent.Render(TestX, TestY, TestZ);

            Assert.AreEqual(_gameObject.Object, actual);
        }

        [Test]
        public void Render_WithColor_ReturnsRenderedObject()
        {
            _gameObject.Setup(g => g.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>(), It.IsAny<Color>())).Returns(_gameObject.Object);

            var actual = _tileContent.Render(TestX, TestY, TestZ, _testColor);

            Assert.AreEqual(_gameObject.Object, actual);
        }

        [Test]
        public void Hide_PassesToGameObject()
        {
            _tileContent.Hide();

            _gameObject.Verify(g => g.Hide());
        }

        [Test]
        public void Show_PassesToGameObject()
        {
            _tileContent.Show();

            _gameObject.Verify(g => g.Show());
        }

        [Test]
        public void Destroy_DestroysGameObjectAndClearsReferences()
        {
            _tileContent.Destroy();

            _gameObject.Verify(g => g.Destroy());
        }

        [Test]
        public void SetOrientation_SetsGameObjectRotation()
        {
            _tileContent.SetOrientation(Orientation.Reverse);

            Assert.AreEqual(Orientation.Reverse, _tileContent.Orientation);
            _gameObject.Verify(g => g.SetOrientation(Orientation.Reverse));
        }

        [Test]
        public void Rotate_RotatesToNext()
        {
            _tileContent.SetOrientation(Orientation.Left);

            _tileContent.Rotate();

            Assert.AreEqual(Orientation.Forward, _tileContent.Orientation);
            _gameObject.Verify(g => g.SetOrientation(Orientation.Forward));
        }
    }
}
