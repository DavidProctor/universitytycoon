﻿using System;
using Moq;
using NUnit.Framework;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Tiles
{
    [TestFixture]
    class TileContentCollectionTest
    {
        private ITileContentCollection _tileContentCollection;

        private Mock<ITileContent> _ground;
        private Mock<ITileContent> _basement;
        private Mock<ITileContent> _floor;
        private Mock<ITileContent> _structure;
        private Mock<ITileContent> _activityPoint;
        private Mock<ITileContent> _wall;
        private Mock<ITileContent> _door;
        private Mock<IGameObject> _gameObject;
        private Mock<ITile> _tile;

        [SetUp]
        public void BeforeEachTest()
        {
            _tile = new Mock<ITile>();
            _gameObject = new Mock<IGameObject>();
            _ground = MakeMockTileContentWithLayer(TileLayer.Ground);
            _basement = MakeMockTileContentWithLayer(TileLayer.Basement);
            _floor = MakeMockTileContentWithLayer(TileLayer.Floor);
            _activityPoint = MakeMockTileContentWithLayer(TileLayer.ActivityPoint);
            _structure = MakeMockTileContentWithLayer(TileLayer.Structure);
            _door = MakeMockTileContentWithLayer(TileLayer.Door);
            _wall = MakeMockTileContentWithLayer(TileLayer.Wall);

            _tile.SetupGet(t => t.X).Returns(111);
            _tile.SetupGet(t => t.Z).Returns(222);

            _tileContentCollection = new TileContentCollection(_ground.Object);
            _tileContentCollection.Tile = _tile.Object;
        }

        [Test]
        public void Ctor_ConstructedWithNonGround_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => new TileContentCollection(_basement.Object));
        }

        [Test]
        public void BuildIsValid_NullLayer_ReturnsTrue()
        {
            var nullLayerContent = new Mock<ITileContent>();
            nullLayerContent.SetupGet(c => c.Layer).Returns(TileLayer.NullLayer);

            Assert.IsTrue(_tileContentCollection.BuildIsValid(nullLayerContent.Object));
        }

        [Test]
        public void AddTileContent_EmptyTile_Throws()
        {
            _tileContentCollection.Tile = null;

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_floor.Object));
        }

        [Test]
        public void AddTileContent_BasementOverGround_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Basement));
        }

        [Test]
        public void AddTileContent_FloorOverGround_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_floor.Object));
        }

        [Test]
        public void AddTileContent_WallOverGround_Throws()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.AreEqual(_structure.Object, _tileContentCollection.GetTileContent(TileLayer.Structure));
        }

        [Test]
        public void AddTileContent_ActivityPointOverGround_Works()
        {
            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.AreEqual(_activityPoint.Object, _tileContentCollection.GetTileContent(TileLayer.ActivityPoint));
        }

        [Test]
        public void AddTileContent_DoorOverGround_Throws()
        {
            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_door.Object));
        }

        [Test]
        public void AddTileContent_FloorOverBasement_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _tileContentCollection.AddTileContent(_floor.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Floor));
        }

        [Test]
        public void AddTileContent_StructureOverBasement_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Structure));
        }

        [Test]
        public void AddTileContent_ActivityPointOverBasement_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.ActivityPoint));
        }

        [Test]
        public void AddTileContent_WallOverBasement_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _tileContentCollection.AddTileContent(_wall.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Wall));
        }

        [Test]
        public void AddTileContent_DoorOverBasement_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_door.Object));
        }

        [Test]
        public void AddTileContent_BasementOverFloor_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);
            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_basement.Object));
        }

        [Test]
        public void AddTileContent_BasementOverStructure_Throws()
        {
            _tileContentCollection.AddTileContent(_structure.Object);
            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_basement.Object));
        }

        [Test]
        public void AddTileContent_BasementOverActivityPoint_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_activityPoint.Object);
            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_basement.Object));
        }

        [Test]
        public void AddTileContent_BasementOverDoor_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);
            _tileContentCollection.AddTileContent(_door.Object);
            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_basement.Object));
        }

        [Test]
        public void AddTileContent_StructureOverGround_Works()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Structure));
        }

        [Test]
        public void AddTileContent_StructureOverFloor_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Structure));
        }

        [Test]
        public void AddTileContent_StructureOverActivityPoint_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_structure.Object));
        }

        [Test]
        public void AddTileContent_StructureOverWall_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_structure.Object));
        }

        [Test]
        public void AddTileContent_StructureOverDoor_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);
            _tileContentCollection.AddTileContent(_door.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_structure.Object));
        }

        [Test]
        public void AddTileContent_ActivityPointOverFloor_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.AreEqual(_activityPoint.Object, _tileContentCollection.GetTileContent(TileLayer.ActivityPoint));
        }

        [Test]
        public void AddTIleContent_ActivityPointOverStructure_Throws()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_activityPoint.Object));
        }

        [Test]
        public void AddTileContent_ActivityPointOverWall_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_activityPoint.Object));
        }

        [Test]
        public void AddTileContent_ActivityPointOverDoor_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);
            _tileContentCollection.AddTileContent(_door.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_activityPoint.Object));
        }

        [Test]
        public void AddTileContent_WallOverFloor_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            _tileContentCollection.AddTileContent(_wall.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Wall));
        }

        [Test]
        public void AddTileContent_WallOverStructure_Throws()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_wall.Object));
        }

        [Test]
        public void AddTileContent_WallOverActivityPoint_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_wall.Object));
        }

        [Test]
        public void AddTileContent_WallOverDoor_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);
            _tileContentCollection.AddTileContent(_door.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_wall.Object));
        }

        [Test]
        public void AddTileContent_DoorOverFloor_Throws()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_door.Object));
        }

        [Test]
        public void AddTileContent_DoorOverStructure_Throws()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_door.Object));
        }

        [Test]
        public void AddTileContent_DoorOverActivityPoint_Throws()
        {
            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.Throws<InvalidOperationException>(() => _tileContentCollection.AddTileContent(_door.Object));
        }

        [Test]
        public void AddTileContent_DoorOverWall_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            _tileContentCollection.AddTileContent(_door.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Door));
        }

        [Test]
        public void AddTileContent_Fresh_AddsTileContentWithLayer()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            var actual = _tileContentCollection.GetTileContent(TileLayer.Floor);

            Assert.AreEqual(_floor.Object, actual);
        }

        [Test]
        public void AddTileContent_Fresh_Renders()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            _floor.Verify(g => g.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void AddTileContent_FloorOverOldFloor_ReplacesOldTileContent()
        {
            var original = MakeMockTileContentWithLayer(TileLayer.Floor);
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(original.Object);

            _tileContentCollection.AddTileContent(_floor.Object);

            var actual = _tileContentCollection.GetTileContent(TileLayer.Floor);
            Assert.AreEqual(_floor.Object, actual);
        }

        [Test]
        public void AddTileContent_BasementOverGround_ReplacesOldTileContent()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _ground.Verify(g => g.Destroy());
            _basement.Verify(f => f.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void AddTileContent_FloorOverGround_ReplacesOldTileContent()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _tileContentCollection.AddTileContent(_floor.Object);

            _ground.Verify(g => g.Destroy());
            _basement.Verify(f => f.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void RemoveTileContent_FloorOverBasement_ReRendersOldTileContent()
        {
            var floor = DataFactory.MakeCompleteMockTileContentWithLayer(TileLayer.Floor);
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(floor.TileContent.Object);

            _tileContentCollection.RemoveTileContent(TileLayer.Floor);

            _basement.Verify(g => g.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()), Times.Exactly(2));
            floor.TileContent.Verify(f => f.Destroy());
        }

        [Test]
        public void RemoveTileContent_BasementOverGround_ReRendersGround()
        {
            _tileContentCollection.AddTileContent(_basement.Object);

            _tileContentCollection.RemoveTileContent(TileLayer.Basement);

            _basement.Verify(b => b.Destroy());
            _ground.Verify(g => g.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void AddTileContent_DoorOverWall_ReplacesWall()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            _tileContentCollection.AddTileContent(_door.Object);

            _wall.Verify(w => w.Destroy());
            _door.Verify(d => d.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void RemoveTileContent_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            _tileContentCollection.RemoveTileContent(TileLayer.Floor);

            Assert.IsNull(_tileContentCollection.GetTileContent(TileLayer.Floor));
        }

        [Test]
        public void GetTileContent_Empty_ReturnsNull()
        {
            var actual = _tileContentCollection.GetTileContent(TileLayer.Wall);

            Assert.IsNull(actual);
        }

        [Test]
        public void GetTileContent_NullLayer_ReturnsNullTileContent()
        {
            var actual = _tileContentCollection.GetTileContent(TileLayer.NullLayer);

            Assert.IsTrue(actual is NullTileContent);
        }

        [Test]
        public void HasLayer_True_ReturnsTrue()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.IsTrue(_tileContentCollection.HasLayer(TileLayer.Structure));
        }

        [Test]
        public void HasLayer_False_ReturnsFalse()
        {
            Assert.IsFalse(_tileContentCollection.HasLayer(TileLayer.Floor));
        }

        [Test]
        public void Destroy_CallsDestroyOnAllTileContents()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);
            _tileContentCollection.AddTileContent(_structure.Object);

            _tileContentCollection.Destroy();

            _floor.Verify(f => f.Destroy());
            _structure.Verify(s => s.Destroy());
            Assert.IsFalse(_tileContentCollection.HasLayer(TileLayer.Floor));
            Assert.IsFalse(_tileContentCollection.HasLayer(TileLayer.Structure));
            Assert.IsNull(_tileContentCollection.Tile);
        }

        [Test]
        public void Render_CallsRenderOnAllTileContents()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);
            _tileContentCollection.AddTileContent(_structure.Object);

            _tileContentCollection.Render();

            _floor.Verify(f => f.Render(111, 0, 222));
            _structure.Verify(s => s.Render(111, 0, 222));
        }

        [Test]
        public void Render_WallAndDoor_RendersOnlyDoor()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);
            _tileContentCollection.AddTileContent(_door.Object);

            _tileContentCollection.Render();

            _door.Verify(d => d.Render(111, 0, 222));
            _wall.Verify(w => w.Render(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Never);
        }

        [Test]
        public void IsWalkable_GroundOnly_ReturnsTrue()
        {
            Assert.IsTrue(_tileContentCollection.IsWalkable());
        }

        [Test]
        public void IsWalkable_Floor_ReturnsTrue()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_floor.Object);

            Assert.IsTrue(_tileContentCollection.IsWalkable());
        }

        [Test]
        public void IsWalkable_Door_ReturnsTrue()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);
            _tileContentCollection.AddTileContent(_door.Object);

            Assert.IsTrue(_tileContentCollection.IsWalkable());
        }

        [Test]
        public void IsWalkable_ActivityPoint_ReturnsTrue()
        {
            _tileContentCollection.AddTileContent(_activityPoint.Object);

            Assert.IsTrue(_tileContentCollection.IsWalkable());
        }

        [Test]
        public void IsWalkable_Wall_ReturnsFalse()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            Assert.IsFalse(_tileContentCollection.IsWalkable());
        }

        [Test]
        public void IsWalkable_Structure_ReturnsFalse()
        {
            _tileContentCollection.AddTileContent(_structure.Object);

            Assert.IsFalse(_tileContentCollection.IsWalkable());
        }

        [Test]
        public void HideLayer_Works()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            _tileContentCollection.HideLayer(TileLayer.Wall);

            _wall.Verify(w => w.Hide());
        }

        [Test]
        public void HideLayer_LayerIsNull_DoesNotThrow()
        {
            _tileContentCollection.HideLayer(TileLayer.Wall);
        }

        [Test]
        public void ShowLayer_PassesOnToTileContent()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            _tileContentCollection.ShowLayer(TileLayer.Wall);

            _wall.Verify(w => w.Show());
        }

        [Test]
        public void ShowLayer_LayerIsNull_DoesNotThrow()
        {
            _tileContentCollection.ShowLayer(TileLayer.Wall);
        }

        [Test]
        public void HideLayersObscuredByLayer_HidesObscuredLayers()
        {
            _tileContentCollection.AddTileContent(_basement.Object);
            _tileContentCollection.AddTileContent(_wall.Object);

            _tileContentCollection.HideLayersObscuredByLayer(TileLayer.Door);
            
            _basement.Verify(b => b.Hide(), Times.Never);
            _wall.Verify(w => w.Hide());
        }

        [Test]
        public void HideLayersObscuredByLayer_BasementOverGround_Works()
        {
            _tileContentCollection.HideLayersObscuredByLayer(TileLayer.Basement);

            _ground.Verify(g => g.Hide());
        }

        private Mock<ITileContent> MakeMockTileContentWithLayer(TileLayer layer)
        {
            var content = new Mock<ITileContent>();
            content.Setup(c => c.Layer).Returns(layer);
            content.SetupGet(c => c.GameObject).Returns(_gameObject.Object);
            return content;
        }
    }
}
