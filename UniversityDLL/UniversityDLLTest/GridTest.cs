﻿using NUnit.Framework;
using Moq;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest
{
    [TestFixture]
    public class GridTest
    {
        private Grid Q;

        private const int width = 10;
        private const int height = 15;

        [SetUp]
        public void BeforeEachTest()
        {
            Q = new Grid(width, height);
        }

        [Test]
        public void Ctor_WidthIsSet()
        {
            Assert.AreEqual(width, Q.Width);
        }

        [Test]
        public void Ctor_HeightIsSet()
        {
            Assert.AreEqual(height, Q.Height);
        }

        [Test]
        public void SetAndGetTile_Works()
        {
            var tile = new Tile(new Mock<ITileContentCollection>().Object, new Mock<Room>().Object);

            Q.SetTile(3, 5, tile);

            Assert.AreEqual(tile, Q.GetTile(3, 5));
        }

        [Test]
        public void SetTile_SetsPosition()
        {
            var tile = new Mock<ITile>();
            var xpos = 3;
            var ypos = 5;

            Q.SetTile(xpos, ypos, tile.Object);

            tile.Verify(x => x.SetPosition(xpos, ypos));
        }

        [Test]
        public void ConnectNeighbors_ConnectsNeighbors()
        {
            Q = new Grid(3, 3);
            var tile00 = MockTileAtLocation(0, 0);
            var tile01 = MockTileAtLocation(0, 1);
            var tile02 = MockTileAtLocation(0, 2);
            var tile10 = MockTileAtLocation(1, 0);
            var tile11 = MockTileAtLocation(1, 1);
            var tile12 = MockTileAtLocation(1, 2);
            var tile20 = MockTileAtLocation(2, 0);
            var tile21 = MockTileAtLocation(2, 1);
            var tile22 = MockTileAtLocation(2, 2);

            Q.ConnectNeighbors();

            tile11.Verify(x => x.AddConnection(tile00.Object));
            tile11.Verify(x => x.AddConnection(tile21.Object));
            tile00.Verify(x => x.AddConnection(tile22.Object), Times.Never);
        }

        [Test]
        public void Destroy_DestroysAllTiles()
        {
            Q = new Grid(3, 3);
            var tile00 = MockTileAtLocation(0, 0);
            var tile01 = MockTileAtLocation(0, 1);
            var tile02 = MockTileAtLocation(0, 2);
            var tile10 = MockTileAtLocation(1, 0);
            var tile11 = MockTileAtLocation(1, 1);
            var tile12 = MockTileAtLocation(1, 2);
            var tile20 = MockTileAtLocation(2, 0);
            var tile21 = MockTileAtLocation(2, 1);
            var tile22 = MockTileAtLocation(2, 2);

            Q.Destroy();

            tile00.Verify(t => t.Destroy());
            tile01.Verify(t => t.Destroy());
            tile02.Verify(t => t.Destroy());
            tile10.Verify(t => t.Destroy());
            tile11.Verify(t => t.Destroy());
            tile12.Verify(t => t.Destroy());
            tile20.Verify(t => t.Destroy());
            tile21.Verify(t => t.Destroy());
            tile22.Verify(t => t.Destroy());

            Assert.IsNull(Q.GetTile(0, 0));
            Assert.IsNull(Q.GetTile(0, 1));
            Assert.IsNull(Q.GetTile(0, 2));
            Assert.IsNull(Q.GetTile(1, 0));
            Assert.IsNull(Q.GetTile(1, 1));
            Assert.IsNull(Q.GetTile(1, 2));
            Assert.IsNull(Q.GetTile(2, 0));
            Assert.IsNull(Q.GetTile(2, 1));
            Assert.IsNull(Q.GetTile(2, 2));
        }

        private Mock<ITile> MockTileAtLocation(int x, int z)
        {
            var tile = new Mock<ITile>();
            tile.SetupGet(t => t.X).Returns(x);
            tile.SetupGet(t => t.Z).Returns(z);
            Q.SetTile(x, z, tile.Object);

            return tile;
        }
    }
}
