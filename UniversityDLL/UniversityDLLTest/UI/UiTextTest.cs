﻿using NUnit.Framework;
using UniversityDLL.UI;

namespace UniversityDLLTest.UI
{
    [TestFixture]
    public class UiTextTest
    {
        [Test]
        public void Get_ReturnsEnglish()
        {
            var result = UiText.Get(this, "Hey howdy hey");
            
            Assert.AreEqual("Hey howdy hey", result);
        }
    }
}