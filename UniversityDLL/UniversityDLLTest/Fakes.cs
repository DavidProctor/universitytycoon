﻿using Moq;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Tiles;

namespace UniversityDLLTest
{
    public class Fakes
    {
        public class TestableClassSeatActivityPoint : ClassSeatActivityPoint
        {
            public TestableClassSeatActivityPoint() :
                base(new Mock<ITileContent>().Object)
            {
            }

            public override bool TrySetOccupant(ICharacter character)
            {
                Occupant = character;
                return true;
            }
        }

        public class TestableOfficeDeskActivityPoint : OfficeDeskActivityPoint
        {
            public TestableOfficeDeskActivityPoint() 
                : base(new Mock<ITileContent>().Object)
            {
            }

            public override bool TrySetOccupant(ICharacter character)
            {
                Occupant = character;
                return true;
            }
        }

        public class TestablePubSeatActivityPoint : PubSeatActivityPoint
        {
            public TestablePubSeatActivityPoint()
                : base(new Mock<ITileContent>().Object)
            {
            }

            public override bool TrySetOccupant(ICharacter character)
            {
                Occupant = character;
                return true;
            }
        }
    }
}