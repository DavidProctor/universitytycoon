﻿using System;
using System.Collections.Generic;

using Moq;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest
{
    internal struct MockCoursePair
    {
        public Mock<ICourseInstance> Instance;
        public Mock<ICourseTemplate> Template;
        public List<ICourseEnrolment> Enrolments;
    }

    struct CompleteMockTile
    {
        public Mock<ITile> Tile;
        public Mock<IGameObject> RenderedObject;
        public Mock<ITileContent> Content;
        public Mock<ITileContentCollection> ContentCollection;
    }

    struct CompleteMockTileContent
    {
        public Mock<ITileContent> TileContent;
        public Mock<IGameObject> GameObject;
    }

    internal struct CompleteMockDepartment
    {
        public Mock<IDepartment> Department;
        public List<MockCoursePair> Courses;
    }

    static class DataFactory
    {
        public static CompleteMockTile MakeCompleteMockTile(long contentCost = 0L)
        {
            var tile = new Mock<ITile>();
            var renderedObject = new Mock<IGameObject>();
            var contentCollection = new Mock<ITileContentCollection>();
            var content = new Mock<ITileContent>();

            tile.SetupGet(t => t.TileContentCollection).Returns(contentCollection.Object);
            tile.SetupGet(t => t.Neighbors).Returns(new List<IWaypoint>());

            contentCollection.Setup(c => c.GetTileContent(It.IsAny<TileLayer>())).Returns(content.Object);
            content.SetupGet(c => c.GameObject).Returns(renderedObject.Object);
            content.SetupGet(c => c.PurchaseCost).Returns(contentCost);
            content.Setup(c => c.GetClone()).Returns(content.Object);

            renderedObject.Setup(r => r.GetClone()).Returns(renderedObject.Object);

            return new CompleteMockTile
            {
                Tile = tile,
                RenderedObject = renderedObject,
                Content = content,
                ContentCollection = contentCollection,
            };
        }

        public static CompleteMockTile[,] MakeMockTileGrid(int width, int height, Mock<IGrid> grid)
        {
            var tiles = new CompleteMockTile[width, height];
            grid.SetupGet(g => g.Width).Returns(width);
            grid.SetupGet(g => g.Height).Returns(height);

            for (int x = 0; x < width; ++x)
            {
                for (int z = 0; z < height; ++z)
                {
                    tiles[x, z] = MakeCompleteMockTile();
                    tiles[x, z].Tile.SetupGet(t => t.X).Returns(x);
                    tiles[x, z].Tile.SetupGet(t => t.Z).Returns(z);
                    tiles[x, z].Tile.Setup(t => t.IsWalkable()).Returns(true);
                    tiles[x, z].Tile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);

                    var neighbors = new List<IWaypoint>();
                    tiles[x, z].Tile.Setup(t => t.AddConnection(It.IsAny<IWaypoint>())).Callback<IWaypoint>(t => neighbors.Add(t));
                    tiles[x, z].Tile.Setup(t => t.RemoveConnection(It.IsAny<IWaypoint>())).Callback<IWaypoint>(t => neighbors.Remove(t));
                    tiles[x, z].Tile.SetupGet(t => t.Neighbors).Returns(neighbors);
                    tiles[x, z].Tile.SetupGet(t => t.Room).Returns(new Mock<Room>().Object);

                    grid.Setup(g => g.GetTile(x, z)).Returns(tiles[x, z].Tile.Object);
                }
            }

            foreach(var tile in tiles) { 
                for( int x = tile.Tile.Object.X - 1; x <= tile.Tile.Object.X + 1; ++x )
                {
                    if( x < 0 || x >= width )
                        continue;

                    for( int z = tile.Tile.Object.Z - 1; z <= tile.Tile.Object.Z + 1; ++z )
                    {
                        if( z < 0 || z >= height )
                            continue;

                        if( !(x == tile.Tile.Object.X && z == tile.Tile.Object.Z) )
                            tile.Tile.Object.AddConnection(tiles[x, z].Tile.Object);
                    }
                }
            }

            return tiles;
        }

        public static CompleteMockTile[,] MakeMockTileGridWithWall(int width, int height, Mock<IGrid> grid)
        {
            var tiles = MakeMockTileGrid(width, height, grid);
            for(int x = 0; x < width; ++x)
            {
                for(int z = 0; z < height; ++z)
                {
                    SetupNeighborsOnTile(tiles[x, z].Tile, grid.Object, width, height);

                    var isEdge = x == 0 || x == width - 1 || z == 0 || z == height - 1;
                    tiles[x, z].ContentCollection.Setup(c => c.HasLayer(TileLayer.Wall)).Returns(isEdge);
                    tiles[x, z].ContentCollection.Setup(c => c.HasLayer(TileLayer.Floor)).Returns(!isEdge);
                    tiles[x, z].Content.SetupGet(c => c.Layer).Returns(isEdge ? TileLayer.Wall : TileLayer.Floor);
                }
            }
            var door = tiles[0, height/2];
            door.ContentCollection.Setup(c => c.HasLayer(TileLayer.Door)).Returns(true);
            door.Content.SetupGet(c => c.Layer).Returns(TileLayer.Door);
            return tiles;
        }

        private static void SetupNeighborsOnTile(Mock<ITile> tile, IGrid grid, int gridWidth, int gridHeight)
        {
            var neighbors = new List<IWaypoint>();
            for (int x = tile.Object.X - 1; x <= tile.Object.X + 1; ++x)
            {
                if (x < 0 || x >= gridWidth)
                    continue;

                for (int z = tile.Object.Z - 1; z <= tile.Object.Z + 1; ++z)
                {
                    if (z < 0 || z >= gridHeight)
                        continue;

                    if (!(x == tile.Object.X && z == tile.Object.Z))
                        neighbors.Add(grid.GetTile(x, z));
                }
            }
            tile.SetupGet(t => t.Neighbors).Returns(neighbors);
        }

        public static CompleteMockTileContent MakeCompleteMockTileContentWithLayer(TileLayer layer)
        {
            var gameObject = new Mock<IGameObject>();
            gameObject.Setup(g => g.GetClone()).Returns(gameObject.Object);

            var content = new Mock<ITileContent>();
            content.SetupGet(c => c.Layer).Returns(layer);
            content.SetupGet(c => c.GameObject).Returns(gameObject.Object);

            return new CompleteMockTileContent
            {
                TileContent = content,
                GameObject = gameObject
            };
        }

        public static Mock<IGameObject> MakeSelfReplicatingGameObject()
        {
            var gameObject = new Mock<IGameObject>();
            gameObject.Setup(g => g.GetClone()).Returns(gameObject.Object);
            return gameObject;
        }

        public static MockCoursePair MakeMockCoursePair(int capacity, Year year, bool isReady, IDepartment department, int requiredHours = 3)
        {
            var courseTemplate = new Mock<ICourseTemplate>();
            courseTemplate.SetupGet(c => c.Year).Returns(year);
            courseTemplate.SetupGet(c => c.RequiredHours).Returns(requiredHours);
            courseTemplate.SetupGet(c => c.Department).Returns(department);

            var courseInstance = new Mock<ICourseInstance>();
            courseInstance.Setup(c => c.GetCapacity()).Returns(capacity);
            courseInstance.Setup(c => c.IsReady()).Returns(isReady);
            courseInstance.SetupGet(c => c.Template).Returns(courseTemplate.Object);

            var enrolments = new List<ICourseEnrolment>();
            courseInstance.SetupGet(i => i.Enrolments).Returns(enrolments);
            courseInstance.Setup(i => i.AddEnrolment(It.IsAny<ICourseEnrolment>()))
                .Callback<ICourseEnrolment>(e => enrolments.Add(e));

            return new MockCoursePair()
            {
                Template = courseTemplate,
                Instance = courseInstance,
                Enrolments = enrolments
            };
        }

        public static CompleteMockDepartment MakeCompleteMockDepartment(int classCapacity, int coursesPerYear)
        {
            var department = new Mock<IDepartment>();
            department.SetupGet(d => d.RequiredCreditsPerYear).Returns(12);

            var courses = new List<MockCoursePair>();
            var courseInstances = new List<ICourseInstance>();
            var courseTemplates = new List<ICourseTemplate>();

            foreach( Year year in Enum.GetValues(typeof(Year)) )
            {
                for( int courseNum = 0; courseNum < coursesPerYear; ++courseNum )
                {
                    var course = MakeMockCoursePair(classCapacity, year, true, department.Object);
                    courses.Add(course);
                    courseInstances.Add(course.Instance.Object);
                    courseTemplates.Add(course.Template.Object);
                }
            }

            department.Setup(d => d.GetCourses()).Returns(courseTemplates);
            department.Setup(d => d.GetScheduledCourses(It.IsAny<Semester>())).Returns(courseInstances);

            return new CompleteMockDepartment
            {
                Department = department,
                Courses = courses
            };
        }
    }
}
