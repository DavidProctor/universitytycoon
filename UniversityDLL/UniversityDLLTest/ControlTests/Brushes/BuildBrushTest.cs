﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Brushes
{
    [TestFixture]
    class BuildBrushTest
    {
        private BuildBrush _buildBrush;

        private Mock<ISelection> _selection;
        private Mock<ITileContent> _edgeContent;
        private Mock<ITileContent> _middleContent;
        private Mock<ITile> _edgeTile;
        private Mock<ITile> _middleTile;
        private Mock<IScore> _mockScore;
        private List<ITile> _edgeTiles;
        private List<ITile> _allTiles;

        [SetUp]
        public void BeforeEachTest()
        {
            _selection = new Mock<ISelection>();
            _edgeContent = new Mock<ITileContent>();
            _middleContent = new Mock<ITileContent>();
            _edgeTile = new Mock<ITile>();
            _middleTile = new Mock<ITile>();
            _mockScore = new Mock<IScore>();
            _edgeTiles = new List<ITile> { _edgeTile.Object };
            _allTiles = new List<ITile> { _edgeTile.Object, _middleTile.Object };

            _selection.Setup(s => s.GetEdgeTiles()).Returns(_edgeTiles);
            _selection.Setup(s => s.GetSelectedTiles()).Returns(_allTiles);
            _edgeContent.Setup(e => e.GetClone()).Returns(_edgeContent.Object);
            _edgeContent.SetupGet(e => e.Layer).Returns(TileLayer.ActivityPoint);
            _middleContent.Setup(m => m.GetClone()).Returns(_middleContent.Object);
            _middleContent.SetupGet(m => m.Layer).Returns(TileLayer.Basement);
            _edgeTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);
            _middleTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);

            _buildBrush = new BuildBrush(_selection.Object, _edgeContent.Object, _middleContent.Object, _mockScore.Object);
        }

        [Test]
        public void Ctor_setsTileAndContent()
        {
            Assert.AreEqual(_selection.Object, _buildBrush.Selection);
            Assert.AreEqual(_edgeContent.Object, _buildBrush.EdgeContent);
            Assert.AreEqual(_middleContent.Object, _buildBrush.MiddleContent);
        }

        [Test]
        public void Execute_SetsTileContent()
        {
            _buildBrush.Execute();

            _edgeTile.Verify(t => t.AddTileContent(_edgeContent.Object));
            _middleTile.Verify(t => t.AddTileContent(_middleContent.Object));
        }

        [Test]
        public void Execute_ClonesTileContent()
        {
            _buildBrush.Execute();

            _edgeContent.Verify(e => e.GetClone());
            _middleContent.Verify(m => m.GetClone());
        }

        [Test]
        public void Execute_AnyBuildIsInvalid_DoesNotBuild()
        {
            _edgeTile.Setup(t => t.BuildIsValid(_edgeContent.Object)).Returns(false);

            _buildBrush.Execute();

            _edgeContent.Verify(e => e.GetClone(), Times.Never);
            _middleContent.Verify(e => e.GetClone(), Times.Never);
        }

        [Test]
        public void CanExecute_AnyBuildIsInvalid_ReturnsFalse()
        {
            _edgeTile.Setup(t => t.BuildIsValid(_edgeContent.Object)).Returns(false);

            Assert.IsFalse(_buildBrush.CanExecute());
        }

        [Test]
        public void Execute_BasementOrFloorOutdoors_SetsUndefinedRoom()
        {
            _edgeContent.SetupGet(c => c.Layer).Returns(TileLayer.Floor);
            _middleContent.SetupGet(c => c.Layer).Returns(TileLayer.Basement);
            _edgeTile.SetupProperty(t => t.Room);
            _edgeTile.Object.Room = ROutdoors.GetInstance();
            _middleTile.SetupProperty(t => t.Room);
            _middleTile.Object.Room = ROutdoors.GetInstance();

            _buildBrush.Execute();

            Assert.IsTrue(_edgeTile.Object.Room is RUndefined);
            Assert.IsTrue(_middleTile.Object.Room is RUndefined);
        }

        [Test]
        public void Execute_NullTileLayer_Skips()
        {
            _middleContent.SetupGet(c => c.Layer).Returns(TileLayer.NullLayer);

            _buildBrush.Execute();

            _middleTile.Verify(t => t.AddTileContent(It.IsAny<ITileContent>()), Times.Never);
        }

        [Test]
        public void Execute_ChargesMoney()
        {
            _middleContent.SetupGet(c => c.PurchaseCost).Returns(10);

            _buildBrush.Execute();

            _mockScore.Verify(s => s.SubtractMoney(10));
        }

        [Test]
        public void Rotate_SeparateTileContents_RotatesTileContent()
        {
            _buildBrush.Rotate();

            _edgeContent.Verify(c => c.Rotate(), Times.Once);
            _middleContent.Verify(c => c.Rotate(), Times.Once);
        }

        [Test]
        public void Rotate_SingleTileContent_RotatesTileContent()
        {
            var tileContent = new Mock<ITileContent>();
            var score = GameState.GetInstance().Score;
            _buildBrush = new BuildBrush(_selection.Object, tileContent.Object, tileContent.Object, score);

            _buildBrush.Rotate();

            tileContent.Verify(c => c.Rotate(), Times.Once);
        }
    }
}
