﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.ActivityPoints;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Brushes
{
    [TestFixture]
    class DefineRoomBrushTest
    {
        private DefineRoomBrush _defineRoomBrush;

        private Mock<ISelection> _selection;
        private Mock<ITile> _tile1;
        private Mock<ITile> _tile2;
        private Mock<ITile> _tile3;
        private List<ITile> _tiles;
        private Mock<Room> _newRoom;
        private Mock<Room> _oldRoom;
        private Mock<IGameState> _gameState;

        [SetUp]
        public void BeforeEachTest()
        {
            _newRoom = MakeRoomWithActivityPoints();
            _oldRoom = MakeRoomWithActivityPoints();
            _selection = new Mock<ISelection>();
            _tile1 = new Mock<ITile>();
            _tile1.SetupGet(t => t.Room).Returns(_oldRoom.Object);
            _tile2 = new Mock<ITile>();

            _tile2.SetupGet(t => t.Room).Returns(_oldRoom.Object);
            _tile3 = new Mock<ITile>();
            _tile3.SetupGet(t => t.Room).Returns(_oldRoom.Object);
            _tiles = new List<ITile> {_tile1.Object, _tile2.Object, _tile3.Object};
            _gameState = new Mock<IGameState>();
            
            _selection.Setup(s => s.GetSelectedTiles()).Returns(_tiles);

            _defineRoomBrush = new DefineRoomBrush(_selection.Object, () => _newRoom.Object, _gameState.Object);
        }

        [Test]
        public void Ctor_SetsTileRoomAndGameState()
        {
            Assert.AreEqual(_selection.Object, _defineRoomBrush.Selection);
            Assert.AreEqual(_gameState.Object, _defineRoomBrush.GameState);
        }

        [Test]
        public void Execute_CallsCreateRoom()
        {
            var called = false;
            _defineRoomBrush = new DefineRoomBrush(_selection.Object, () => { called = true; return _oldRoom.Object; }, _gameState.Object);

            _defineRoomBrush.Execute();

            Assert.IsTrue(called);
        }

        [Test]
        public void Execute_AddsSelectionToRoom()
        {
            _defineRoomBrush.Execute();

            _newRoom.Verify(r => r.SetTiles(_tiles));
        }

        [Test]
        public void Execute_SetsRoomOnTiles()
        {
            _defineRoomBrush.Execute();

            _tile1.VerifySet(t => t.Room = _newRoom.Object);
            _tile2.VerifySet(t => t.Room = _newRoom.Object);
            _tile3.VerifySet(t => t.Room = _newRoom.Object);
        }

        [Test]
        public void Execute_MovesActivityPointsToNewRoom()
        {
            var activityPoint1 = AddActivityPointToTile(_tile1);
            var activityPoint2 = AddActivityPointToTile(_tile3);

            _defineRoomBrush.Execute();

            _newRoom.Verify(r => r.AddActivityPoint(activityPoint1.Object));
            _oldRoom.Verify(r => r.RemoveActivityPoint(activityPoint1.Object));
            _newRoom.Verify(r => r.AddActivityPoint(activityPoint2.Object));
            _oldRoom.Verify(r => r.RemoveActivityPoint(activityPoint2.Object));
        }

        [Test]
        public void Execute_RegistersRoom()
        {
            _defineRoomBrush.Execute();

            _gameState.Verify(s => s.RegisterRoom(_newRoom.Object));
        }

        [Test]
        public void CanExecute_ReturnsTrue()
        {
            Assert.IsTrue(_defineRoomBrush.CanExecute());
        }

        private Mock<Room> MakeRoomWithActivityPoints()
        {
            var room = new Mock<Room>();
            var newRoomActivityPoints = new List<ActivityPoint>();
            room.Setup(r => r.AddActivityPoint(It.IsAny<ActivityPoint>()))
                .Callback<ActivityPoint>(p => newRoomActivityPoints.Add(p));
            room.Setup(r => r.GetActivityPoints()).Returns(newRoomActivityPoints);
            return room;
        }

        private Mock<ActivityPoint> AddActivityPointToTile(Mock<ITile> tile)
        {
            var activityPoint = new Mock<ActivityPoint>();
            activityPoint.SetupGet(p => p.Tile).Returns(tile.Object);
            tile.Object.Room.AddActivityPoint(activityPoint.Object);
            tile.Setup(t => t.HasLayer(TileLayer.ActivityPoint)).Returns(true);
            return activityPoint;
        }
    }
}
