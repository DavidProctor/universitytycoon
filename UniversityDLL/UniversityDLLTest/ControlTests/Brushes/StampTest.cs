﻿using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Brushes
{
    [TestFixture]
    public class StampTest
    {
        private IStamp _stamp;


        [SetUp]
        public void BeforeEachTest()
        {
            _stamp = new Stamp(3, 4);
        }

        [Test]
        public void Ctor_SetsWidthHeightAndPivot()
        {
            Assert.AreEqual(3, _stamp.Width);
            Assert.AreEqual(4, _stamp.Height);
        }

        [Test]
        public void SetTileContent_Works()
        {
            Mock<ITileContent> tileContent = new Mock<ITileContent>();

            _stamp.SetTileContentAtPoint(1, 2, tileContent.Object);

            Assert.AreEqual(_stamp.GetContents()[1,2], tileContent.Object);
        }
    }
}