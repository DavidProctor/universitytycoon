﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Brushes
{
    [TestFixture]
    public class AddActivityPointBrushTest
    {
        private IBrush _addActivityPointBrush;

        private Mock<ActivityPoint> _mockActivityPoint;
        private Mock<ITileContent> _mockTileContent;
        private Mock<ITile> _mockTile;
        private Mock<Room> _mockRoom;
        private Mock<ISelection> _mockSelection;
        private Mock<IPathProvider> _mockPathProvider;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockTileContent = new Mock<ITileContent>();
            _mockActivityPoint = new Mock<ActivityPoint>();
            _mockTile = new Mock<ITile>();
            _mockSelection = new Mock<ISelection>();
            _mockPathProvider = new Mock<IPathProvider>();
            _mockRoom = new Mock<Room>();

            _mockTileContent.Setup(t => t.GetClone()).Returns(_mockTileContent.Object);
            _mockSelection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile> {_mockTile.Object});
            _mockTile.SetupGet(t => t.Room).Returns(_mockRoom.Object);
            _mockActivityPoint.Setup(p => p.GetNewInstance()).Returns(_mockActivityPoint.Object);
            _mockTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);

            _addActivityPointBrush = new AddActivityPointBrush(_mockSelection.Object, 
                                                                _mockTileContent.Object,
                                                                _mockPathProvider.Object,
                                                                _mockActivityPoint.Object);
        }

        [Test]
        public void Execute_AddsCloneOfTileContent()
        {
            _addActivityPointBrush.Execute();

            _mockTileContent.Verify(t => t.GetClone());
            _mockTile.Verify(t => t.AddTileContent(_mockTileContent.Object));
        }

        [Test]
        public void Execute_AddsCloneOfActivityPoint()
        {
            _addActivityPointBrush.Execute();

            _mockActivityPoint.Verify(p => p.GetNewInstance());
        }

        [Test]
        public void Execute_AddsActivityPointToRoom()
        {
            _addActivityPointBrush.Execute();

            _mockRoom.Verify(r => r.AddActivityPoint(_mockActivityPoint.Object));
        }

        [Test]
        public void Execute_AddsWaypointToPathProvider()
        {
            _addActivityPointBrush.Execute();

            _mockPathProvider.Verify(p => p.AddDestinationTile(_mockTile.Object));
        }

        [Test]
        public void Execute_AddsTileToActivityPoint()
        {
            _addActivityPointBrush.Execute();

            _mockActivityPoint.VerifySet(p => p.Tile = _mockTile.Object);
        }

        [Test]
        public void Execute_SetsActivityPointOnTile()
        {
            _addActivityPointBrush.Execute();

            _mockTile.Verify(t => t.SetActivityPoint(_mockActivityPoint.Object));
        }

        [Test]
        public void Execute_BuildIsInvalid_DoesNotBuild()
        {
            _mockTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(false);

            _addActivityPointBrush.Execute();

            _mockTile.Verify(t => t.AddTileContent(It.IsAny<ITileContent>()), Times.Never);
        }

        [Test]
        public void CanExecute_FailsIfBuildIsValid()
        {
            Assert.IsTrue(_addActivityPointBrush.CanExecute());
        }

        [Test]
        public void CanExecute_FailsIfBuildIsInvalid()
        {
            _mockTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>()));

            Assert.IsFalse(_addActivityPointBrush.CanExecute());
        }

        [Test]
        public void MultipleActivityPoints_Works()
        {
            var activityPoint2 = new Mock<ActivityPoint>();
            _addActivityPointBrush = new AddActivityPointBrush(_mockSelection.Object,
                                                                _mockTileContent.Object,
                                                                _mockPathProvider.Object,
                                                                _mockActivityPoint.Object,
                                                                activityPoint2.Object);
        }

        [Test]
        public void Rotate_RotatesTileContent()
        {
            _addActivityPointBrush.Rotate();

            _mockTileContent.Verify(c => c.Rotate());
        }
    }
}