﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Brushes
{
    [TestFixture]
    public class StampBrushTest
    {
        private IBrush _stampBrush;

        private Mock<IGrid> _mockGrid;
        private CompleteMockTile[,] _mockTiles;
        private Mock<ISelection> _selection;

        private IStamp _stamp;
        private Mock<ITileContent> _mockTileContent00;
        private Mock<ITileContent> _mockTileContent10;
        private Mock<ITileContent> _mockTileContent21;

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            _stamp = new Stamp(3, 2);

            _mockTileContent00 = new Mock<ITileContent>();
            _mockTileContent00.SetupGet(t => t.Layer).Returns(TileLayer.Floor);
            _stamp.SetTileContentAtPoint(0, 0, _mockTileContent00.Object);
            _mockTileContent10 = new Mock<ITileContent>();
            _mockTileContent10.SetupGet(t => t.Layer).Returns(TileLayer.Floor);
            _stamp.SetTileContentAtPoint(1, 0, _mockTileContent10.Object);
            _mockTileContent21 = new Mock<ITileContent>();
            _mockTileContent21.SetupGet(t => t.Layer).Returns(TileLayer.Floor);
            _stamp.SetTileContentAtPoint(2, 1, _mockTileContent21.Object);

            _selection = new Mock<ISelection>();
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _mockGrid = new Mock<IGrid>();
            _mockTiles = DataFactory.MakeMockTileGrid(5, 5, _mockGrid);

            _selection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile>
            {
                _mockTiles[1,1].Tile.Object,
                _mockTiles[2,1].Tile.Object,
                _mockTiles[3,1].Tile.Object,
                _mockTiles[1,2].Tile.Object,
                _mockTiles[2,2].Tile.Object,
                _mockTiles[3,2].Tile.Object
            });

            _stampBrush = new StampBrush(_stamp, _selection.Object);
        }

        [Test]
        public void Execute_AddsTileContent()
        {
            _stampBrush.Execute();
            
            _mockTiles[1,1].Tile.Verify(t => t.AddTileContent(_mockTileContent00.Object));
            _mockTiles[2,1].Tile.Verify(t => t.AddTileContent(_mockTileContent10.Object));
            _mockTiles[3,2].Tile.Verify(t => t.AddTileContent(_mockTileContent21.Object));

            _mockTiles[3,1].Tile.Verify(t => t.AddTileContent(It.IsAny<ITileContent>()), Times.Never);
            _mockTiles[1,2].Tile.Verify(t => t.AddTileContent(It.IsAny<ITileContent>()), Times.Never);
            _mockTiles[2,2].Tile.Verify(t => t.AddTileContent(It.IsAny<ITileContent>()), Times.Never);
        }

        [Test]
        public void Execute_SingleBlocker_NoBuilds()
        {
            _mockTiles[3, 2].Tile.Setup(t => t.BuildIsValid(_mockTileContent21.Object)).Returns(false);

            _stampBrush.Execute();

            _mockTiles[1, 1].Tile.Verify(t => t.AddTileContent(_mockTileContent00.Object), Times.Never);
            _mockTiles[2, 1].Tile.Verify(t => t.AddTileContent(_mockTileContent10.Object), Times.Never);
            _mockTiles[3, 2].Tile.Verify(t => t.AddTileContent(_mockTileContent21.Object), Times.Never);
        }

        [Test]
        public void CanExecute_SingleBlocker_ReturnsFalse()
        {
            _mockTiles[3, 2].Tile.Setup(t => t.BuildIsValid(_mockTileContent21.Object)).Returns(false);

            Assert.IsFalse(_stampBrush.CanExecute());
        }
    }
}