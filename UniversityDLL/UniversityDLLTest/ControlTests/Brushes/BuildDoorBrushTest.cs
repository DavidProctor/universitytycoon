﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Brushes
{
    [TestFixture]
    public class BuildDoorBrushTest
    {
        private IBrush _buildDoorBrush;

        private Mock<ITile> _mockDoorTile;
        private Mock<ISelection> _mockSelection;
        private Mock<ITileContent> _mockTileContent;
        private Mock<IPathProvider> _mockPathProvider;

        private Mock<Room> _mockRoom1;
        private Mock<ITile> _mockRoom1Tile;
        private Mock<Room> _mockRoom2;
        private Mock<ITile> _mockRoom2Tile;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockRoom1 = new Mock<Room>();
            _mockRoom1Tile = MakeMockTileInRoom(_mockRoom1.Object);

            _mockRoom2 = new Mock<Room>();
            _mockRoom2Tile = MakeMockTileInRoom(_mockRoom2.Object);

            _mockRoom1.SetupGet(r => r.Neighbors).Returns(new List<IWaypoint> {_mockRoom2.Object});
            _mockRoom2.SetupGet(r => r.Neighbors).Returns(new List<IWaypoint> {_mockRoom1.Object});
            
            _mockDoorTile = new Mock<ITile>();
            _mockDoorTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);
            _mockDoorTile.Setup(t => t.Neighbors).Returns(new List<IWaypoint> {_mockRoom1Tile.Object, _mockRoom2Tile.Object});
            _mockDoorTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);

            var room1Exits = new List<ITile>();
            _mockRoom1.Setup(r => r.GetExitsToRoom(It.IsAny<Room>())).Returns(room1Exits);
            _mockRoom1.Setup(r => r.AddExit(It.IsAny<ITile>(), It.IsAny<Room>()))
                .Callback(() => room1Exits.Add(_mockDoorTile.Object));
            var room2Exits = new List<ITile>();
            _mockRoom2.Setup(r => r.GetExitsToRoom(It.IsAny<Room>())).Returns(room2Exits);
            _mockRoom2.Setup(r => r.AddExit(It.IsAny<ITile>(), It.IsAny<Room>()))
                .Callback(() => room2Exits.Add(_mockDoorTile.Object));

            _mockSelection = new Mock<ISelection>();
            _mockSelection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile>{_mockDoorTile.Object});

            _mockTileContent = new Mock<ITileContent>();
            _mockTileContent.Setup(t => t.GetClone()).Returns(_mockTileContent.Object);

            _mockPathProvider = new Mock<IPathProvider>();

            _buildDoorBrush = new BuildDoorBrush(_mockSelection.Object, _mockTileContent.Object, _mockPathProvider.Object);
        }

        [Test]
        public void Execute_AddsCloneOfTileContent()
        {
            _buildDoorBrush.Execute();

            _mockTileContent.Verify(t => t.GetClone());
            _mockDoorTile.Verify(t => t.AddTileContent(_mockTileContent.Object));
        }

        [Test]
        public void Execute_InvalidPlacement_DoesNothing()
        {
            _mockDoorTile.Setup(t => t.BuildIsValid(_mockTileContent.Object)).Returns(false);

            _buildDoorBrush.Execute();

            _mockDoorTile.Verify(t => t.AddTileContent(It.IsAny<ITileContent>()), Times.Never);
            _mockTileContent.Verify(t => t.GetClone(), Times.Never);
        }

        [Test]
        public void Execute_AddsExitsToRooms()
        {
            _buildDoorBrush.Execute();

            _mockRoom1.Verify(r => r.AddExit(_mockDoorTile.Object, _mockRoom2.Object));
            _mockRoom2.Verify(r => r.AddExit(_mockDoorTile.Object, _mockRoom1.Object));
        }

        [Test]
        public void Execute_AddsNeighborsToRooms()
        {
            _buildDoorBrush.Execute();

            _mockRoom1.Verify(r => r.AddConnection(_mockRoom2.Object));
            _mockRoom2.Verify(r => r.AddConnection(_mockRoom1.Object));
        }

        [Test]
        public void Execute_AddsCachedPath()
        {
            var room3 = new Mock<Room>();
            var room3Tile = new Mock<ITile>();
            _mockRoom1.Setup(r => r.GetExitsToRoom(_mockRoom2.Object)).Returns(new List<ITile> { _mockDoorTile.Object });
            _mockRoom1.Setup(r => r.GetExitsToRoom(room3.Object)).Returns(new List<ITile> { room3Tile.Object });
            _mockRoom1.SetupGet(r => r.Neighbors).Returns(new List<IWaypoint> {_mockRoom2.Object, room3.Object});
            
            _buildDoorBrush.Execute();
            
            _mockPathProvider.Verify(p => p.AddPathAsync(_mockDoorTile.Object, room3Tile.Object));
        }

        [Test]
        public void CanExecute_InvalidPlacement_ReturnsFalse()
        {
            _mockDoorTile.Setup(t => t.BuildIsValid(_mockTileContent.Object)).Returns(false);

            Assert.IsFalse(_buildDoorBrush.CanExecute());
        }

        [Test]
        public void CanExecute_ValidPlacement_ReturnsTrue()
        {
            Assert.IsTrue(_buildDoorBrush.CanExecute());
        }

        [Test]
        public void Rotate_RotatesTileContent()
        {
            _buildDoorBrush.Rotate();

            _mockTileContent.Verify(c => c.Rotate());
        }

        private Mock<ITile> MakeMockTileInRoom(Room room)
        {
            var mockTile = new Mock<ITile>();
            mockTile.SetupGet(t => t.Room).Returns(room);
            mockTile.Setup(t => t.BuildIsValid(It.IsAny<ITileContent>())).Returns(true);
            return mockTile;
        }
    }
}