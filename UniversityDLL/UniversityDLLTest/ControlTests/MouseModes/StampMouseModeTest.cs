﻿using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.MouseModes
{
    [TestFixture]
    public class StampMouseModeTest
    {
        private StampMouseMode _stampMouseMode;
        private Mock<MouseMode> _mockMouseMode00;
        private Mock<MouseMode> _mockMouseMode11;
        private CompleteMockTile[,] _tiles;
        private Mock<IGrid> _mockGrid;
        private Mock<ITile> _pivot;

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            _mockGrid = new Mock<IGrid>();
            _tiles = DataFactory.MakeMockTileGrid(2, 2, _mockGrid);
            _pivot = _tiles[1, 1].Tile;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _stampMouseMode = new StampMouseMode(_mockGrid.Object);

            _mockMouseMode00 = new Mock<MouseMode>();
            _mockMouseMode00.Setup(m => m.CanExecute()).Returns(true);
            _mockMouseMode11 = new Mock<MouseMode>();
            _mockMouseMode11.Setup(m => m.CanExecute()).Returns(true);

            _stampMouseMode.SetMouseModeAtPosition(0, 0, _mockMouseMode00.Object);
            _stampMouseMode.SetMouseModeAtPosition(1, 1, _mockMouseMode11.Object);
        }

        [Test]
        public void SetMouseModeAtPosition_Works()
        {
            var mockMouseMode = new Mock<MouseMode>();

            _stampMouseMode.SetMouseModeAtPosition(1, 3, mockMouseMode.Object);

            var modes = _stampMouseMode.GetStampContents();
            Assert.AreEqual(8, modes.Length);
            Assert.AreEqual(mockMouseMode.Object, modes[1, 3]);
        }

        [Test]
        public void SetMouseModeAtPosition_MultipleModes_Works()
        {
            var modes = _stampMouseMode.GetStampContents();
            Assert.AreEqual(4, modes.Length);
            Assert.AreEqual(_mockMouseMode00.Object, modes[0, 0]);
            Assert.AreEqual(_mockMouseMode11.Object, modes[1, 1]);
            Assert.IsNull(modes[0, 1]);
            Assert.IsNull(modes[1, 0]);
        }

        [Test]
        public void GetStampContents_Empty_Works()
        {
            var stampMouseMode = new StampMouseMode(_mockGrid.Object);

            var actual = stampMouseMode.GetStampContents();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void OnLeftMouseDown_DelegatesToMouseModes()
        {
            _stampMouseMode.OnLeftMouseDown(_pivot.Object);

            _mockMouseMode00.Verify(m => m.OnLeftMouseDown(_tiles[0,0].Tile.Object));
            _mockMouseMode11.Verify(m => m.OnLeftMouseDown(_tiles[1,1].Tile.Object));
        }

        [Test]
        public void OnMoveWhileLeftMouseDown_DelegatesToMouseModes()
        {
            _stampMouseMode.OnMoveWhileLeftMouseDown(_pivot.Object);

            _mockMouseMode00.Verify(m => m.OnMoveWhileLeftMouseDown(_tiles[0, 0].Tile.Object));
            _mockMouseMode11.Verify(m => m.OnMoveWhileLeftMouseDown(_tiles[1, 1].Tile.Object));
        }

        [Test]
        public void OnLeftMouseUp_DelegatesToMouseModes()
        {
            _stampMouseMode.OnLeftMouseUp(_pivot.Object);

            _mockMouseMode00.Verify(m => m.OnLeftMouseUp(_tiles[0, 0].Tile.Object));
            _mockMouseMode11.Verify(m => m.OnLeftMouseUp(_tiles[1, 1].Tile.Object));
        }

        [Test]
        public void OnLeftMouseUp_AnyModeNotExecutable_ExecutesNoModes()
        {
            _mockMouseMode00.Setup(m => m.CanExecute()).Returns(false);

            _stampMouseMode.OnLeftMouseUp(_pivot.Object);

            _mockMouseMode00.Verify(m => m.OnLeftMouseUp(_tiles[0, 0].Tile.Object), Times.Never);
            _mockMouseMode11.Verify(m => m.OnLeftMouseUp(_tiles[1, 1].Tile.Object), Times.Never);
        }

        [Test]
        public void OnMoveWhileLeftMouseUp_DelegatesToMouseModes()
        {
            _stampMouseMode.OnMoveWhileLeftMouseUp(_pivot.Object);

            _mockMouseMode00.Verify(m => m.OnMoveWhileLeftMouseUp(_tiles[0, 0].Tile.Object));
            _mockMouseMode11.Verify(m => m.OnMoveWhileLeftMouseUp(_tiles[1, 1].Tile.Object));
        }

        [Test]
        public void Destroy_PropogatesToMouseModes()
        {
            _stampMouseMode.Destroy();

            _mockMouseMode00.Verify(m => m.Destroy());
            _mockMouseMode11.Verify(m => m.Destroy());
        }

        [Test]
        public void Rotate_Works()
        {
            var mockMouseMode21 = new Mock<MouseMode>();
            _stampMouseMode.SetMouseModeAtPosition(1, 2, mockMouseMode21.Object);

            _stampMouseMode.Rotate();

            var actual = _stampMouseMode.GetStampContents();
            Assert.IsNull(actual[0, 0]);
            Assert.AreEqual(_mockMouseMode00.Object, actual[0, 1]);
            Assert.AreEqual(_mockMouseMode11.Object, actual[1, 0]);
            Assert.IsNull(actual[1,1]);
            Assert.AreEqual(mockMouseMode21.Object, actual[2, 0]);
            Assert.IsNull(actual[2,1]);
            Assert.AreEqual(Orientation.Right, _stampMouseMode.Orientation);
        }

        [Test]
        public void Rotate_PropogatesToContents()
        {
            _stampMouseMode.Rotate();

            _mockMouseMode00.Verify(m => m.Rotate());
            _mockMouseMode11.Verify(m => m.Rotate());
        }
    }
}