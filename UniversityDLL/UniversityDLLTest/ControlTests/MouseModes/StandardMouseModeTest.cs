﻿using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.UI;

namespace UniversityDLLTest.ControlTests.MouseModes
{
    [TestFixture]
    public class StandardMouseModeTest
    {
        private MouseMode _standardMouseMode;

        private Mock<IDialogFactory> _mockDialogFactory;
        private Mock<IGameEntity> _mockGameEntity;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockDialogFactory = new Mock<IDialogFactory>();
            _mockGameEntity = new Mock<IGameEntity>();

            _standardMouseMode = new StandardMouseMode(_mockDialogFactory.Object);
        }

        [Test]
        public void OnMouseUp_CallsDialogFactory()
        {
            _standardMouseMode.OnLeftMouseUp(_mockGameEntity.Object);

            _mockDialogFactory.Verify(f => f.GetInfoDialog(_mockGameEntity.Object));
        }

        [Test]
        public void CanExecute_AlwaysReturnsTrue()
        {
            Assert.IsTrue(_standardMouseMode.CanExecute());
        }
    }
}