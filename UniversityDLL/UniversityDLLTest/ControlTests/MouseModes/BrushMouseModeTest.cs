﻿using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement.Brushes;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.Previews;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.MouseModes
{
    [TestFixture]
    public class BrushMouseModeTest
    {
        private MouseMode _brushMouseMode;

        private Mock<IBrush> _mockBrush;
        private Mock<IPreview> _mockPreview;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockBrush = new Mock<IBrush>();
            _mockPreview = new Mock<IPreview>();

            _brushMouseMode = new BrushMouseMode(_mockBrush.Object, _mockPreview.Object);
        }

        [Test]
        public void CanExecute_DelegatesToBrush()
        {
            _brushMouseMode.CanExecute();

            _mockBrush.Verify(b => b.CanExecute());
        }

        [Test]
        public void Rotate_RotatesBrushAndUpdatesPreview()
        {
            _brushMouseMode.Rotate();

            _mockBrush.Verify(b => b.Rotate());
            _mockPreview.Verify(p => p.RenderPreview());
        }
    }
}