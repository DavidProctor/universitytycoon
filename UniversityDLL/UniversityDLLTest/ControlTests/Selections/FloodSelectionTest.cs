﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Selections
{
    [TestFixture]
    class FloodSelectionTest
    {
        private FloodSelection _floodSelection;
        private Mock<IGrid> _grid;
        private CompleteMockTile[,] _tiles;
        private CompleteMockTile[,] _oldTiles;
        private List<Mock<ITile>> _expectedTiles;

        private const int TestWidth = 5;
        private const int TestHeight = 5;

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            _grid = new Mock<IGrid>();
            _tiles = DataFactory.MakeMockTileGridWithWall(TestWidth, TestHeight, _grid);
            _oldTiles = DataFactory.MakeMockTileGridWithWall(15, 5, _grid);

            _expectedTiles = new List<Mock<ITile>>
            {
                _tiles[1, 1].Tile,
                _tiles[1, 2].Tile,
                _tiles[1, 3].Tile,
                _tiles[2, 1].Tile,
                _tiles[2, 2].Tile,
                _tiles[2, 3].Tile,
                _tiles[3, 1].Tile,
                _tiles[3, 2].Tile,
                _tiles[3, 3].Tile
            };
            foreach (var tile in _tiles)
            {
                tile.ContentCollection.Setup(c => c.HasLayer(TileLayer.Ground)).Returns(true);
            }
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _floodSelection = new FloodSelection();
        }

        [Test]
        public void Ctor_SetsBlockingPredicate()
        {
            var expectedTile = _tiles[2,2].Tile;
            var predicate = new Func<ITile, ITile, bool>((s, t) => t == _tiles[2,2].Tile.Object);

            _floodSelection = new FloodSelection(predicate);
            _floodSelection.StartSelection(expectedTile.Object);

            AssertTilesAreSelected(new List<Mock<ITile>> { expectedTile });
        }

        [Test]
        public void Ctor_SetsBlockingLayerToWall()
        {
            var tiles = DataFactory.MakeMockTileGrid(TestWidth, TestHeight, _grid);
            var blocking = new List<CompleteMockTile> {
                tiles[1,1],
                tiles[1,2],
                tiles[1,3],
                tiles[2,1],
                tiles[2,3],
                tiles[3,1],
                tiles[3,2],
                tiles[3,3]
            };
            foreach (var tile in blocking)
            {
                tile.ContentCollection.Setup(c => c.HasLayer(TileLayer.Wall)).Returns(true);
            }
            tiles[2, 2].ContentCollection.Setup(c => c.HasLayer(TileLayer.Floor)).Returns(true);

            _floodSelection.StartSelection(tiles[2, 2].Tile.Object);
            var actual = _floodSelection.GetSelectedTiles();

            CollectionAssert.Contains(actual, tiles[2, 2].Tile.Object);
            Assert.AreEqual(1, actual.Count);
        }

        [Test]
        public void StartSelection_SelectsAllTilesInRoom()
        {
            _floodSelection.StartSelection(_tiles[3, 3].Tile.Object);

            AssertTilesAreSelected(_expectedTiles);
        }

        [Test]
        public void StartSelection_Outdoors_Fails()
        {
            var tile = DataFactory.MakeCompleteMockTile();
            tile.Tile.Setup(t => t.Room).Returns(ROutdoors.GetInstance());

            _floodSelection.StartSelection(tile.Tile.Object);

            CollectionAssert.IsEmpty(_floodSelection.GetSelectedTiles());
        }

        [Test]
        public void StartSelection_DiscardsOldSelection()
        {
            _floodSelection.StartSelection(_oldTiles[1, 1].Tile.Object);

            _floodSelection.StartSelection(_tiles[1, 1].Tile.Object);

            AssertTilesAreSelected(_expectedTiles);
        }

        [Test]
        public void StartSelection_InvalidTile_OnlyStartTileIncluded()
        {
            _floodSelection.StartSelection(_tiles[0, 0].Tile.Object);

            var actual = _floodSelection.GetSelectedTiles();
            Assert.AreEqual(1, actual.Count);
            CollectionAssert.Contains(actual, _tiles[0, 0].Tile.Object);
        }

        [Test]
        public void ChangeSelection_InSelection_KeepsOldSelection()
        {
            _floodSelection.StartSelection(_tiles[2, 2].Tile.Object);

            _floodSelection.ChangeSelection(_tiles[2, 3].Tile.Object);

            AssertTilesAreSelected(_expectedTiles);
        }

        [Test]
        public void ChangeSelection_OutOfSelection_GetsNewSelection()
        {
            _floodSelection.StartSelection(_oldTiles[7, 2].Tile.Object);

            _floodSelection.ChangeSelection(_tiles[3, 1].Tile.Object);

            AssertTilesAreSelected(_expectedTiles);
        }

        [Test]
        public void GetEdgeTiles_ReturnsEdges()
        {
            var expectedTiles = new List<ITile>
            {
                _tiles[1, 1].Tile.Object,
                _tiles[1, 2].Tile.Object,
                _tiles[1, 3].Tile.Object,
                _tiles[2, 1].Tile.Object,
                _tiles[2, 3].Tile.Object,
                _tiles[3, 1].Tile.Object,
                _tiles[3, 2].Tile.Object,
                _tiles[3, 3].Tile.Object,
            };

            _floodSelection.StartSelection(_tiles[2,2].Tile.Object);

            var actual = _floodSelection.GetEdgeTiles();
            CollectionAssert.AreEquivalent(expectedTiles, actual);
        }

        private void AssertTilesAreSelected(List<Mock<ITile>> tiles)
        {
            var actual = _floodSelection.GetSelectedTiles();
            Assert.AreEqual(tiles.Count, actual.Count);
            foreach (var tile in tiles)
            {
                CollectionAssert.Contains(actual, tile.Object);
            }
        }
    }
}
