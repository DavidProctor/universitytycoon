﻿using System.Linq;
using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Selections
{
    [TestFixture]
    class LineSelectionTest
    {
        private LineSelection Q;
        private CompleteMockTile[,] QTiles;
        private Mock<IGrid> QGrid;

        private const int TestGridWidth = 4;
        private const int TestGridHeight = 4;

        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            QGrid = new Mock<IGrid>();
            QTiles = DataFactory.MakeMockTileGrid(TestGridWidth, TestGridHeight, QGrid);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            Q = new LineSelection(QGrid.Object);
        }

        [Test]
        public void StartSelection_ReplacesContents()
        {
            Q.StartSelection(QTiles[0, 0].Tile.Object);

            Q.StartSelection(QTiles[0, 1].Tile.Object);

            AssertTilesAreSelected(QTiles[0, 1].Tile.Object);
        }

        [Test]
        public void ChangeSelection_DirectlyAdjacent_AddsTile()
        {
            Q.StartSelection(QTiles[0, 0].Tile.Object);

            Q.ChangeSelection(QTiles[0, 1].Tile.Object);

            AssertTilesAreSelected(QTiles[0, 0].Tile.Object, QTiles[0, 1].Tile.Object);
        }

        [Test]
        public void ChangeSelection_ChangesDirection_KeepsAlreadySelected()
        {
            Q.StartSelection(QTiles[0, 0].Tile.Object);
            Q.ChangeSelection(QTiles[0, 1].Tile.Object);

            Q.ChangeSelection(QTiles[1, 1].Tile.Object);

            AssertTilesAreSelected(
                QTiles[0, 0].Tile.Object,
                QTiles[0, 1].Tile.Object,
                QTiles[1, 1].Tile.Object);
        }

        [Test]
        public void ChangeSelection_Retreat_RemovesTileFromSelection()
        {
            Q.StartSelection(QTiles[0, 0].Tile.Object);
            Q.ChangeSelection(QTiles[0, 1].Tile.Object);

            Q.ChangeSelection(QTiles[0, 0].Tile.Object);

            AssertTilesAreSelected(QTiles[0, 0].Tile.Object);
        }

        [Test]
        public void ChangeSelection_ZPattern_CreatesLineWithOneBend()
        {
            Q.StartSelection(QTiles[0, 0].Tile.Object);
            Q.ChangeSelection(QTiles[0, 1].Tile.Object);
            Q.ChangeSelection(QTiles[1, 1].Tile.Object);

            Q.ChangeSelection(QTiles[1, 2].Tile.Object);

            AssertTilesAreSelected(
                QTiles[0, 0].Tile.Object,
                QTiles[0, 1].Tile.Object,
                QTiles[0, 2].Tile.Object,
                QTiles[1, 2].Tile.Object
                );
        }

        [Test]
        public void ChangeSelection_Angle_LengthensInOriginalDirection()
        {
            Q.StartSelection(QTiles[0, 0].Tile.Object);
            Q.ChangeSelection(QTiles[1, 0].Tile.Object);
            Q.ChangeSelection(QTiles[1, 1].Tile.Object);
            Q.ChangeSelection(QTiles[1, 2].Tile.Object);

            AssertTilesAreSelected(
                QTiles[0, 0].Tile.Object,
                QTiles[1, 0].Tile.Object,
                QTiles[1, 1].Tile.Object,
                QTiles[1, 2].Tile.Object);
        }

        private void AssertTilesAreSelected(params ITile[] tiles)
        {
            var actual = Q.GetSelectedTiles();
            Assert.AreEqual(tiles.Count(), actual.Count);
            foreach(var tile in tiles)
            {
                CollectionAssert.Contains(actual, tile);
            }
        }
    }
}
