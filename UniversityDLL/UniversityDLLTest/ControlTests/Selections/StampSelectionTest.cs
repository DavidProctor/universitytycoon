﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Selections
{
    [TestFixture]
    public class StampSelectionTest
    {
        private ISelection _stampSelection;

        private Mock<IGrid> _mockGrid;
        private IStamp _stamp;
        private CompleteMockTile[,] _tiles;

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            _mockGrid = new Mock<IGrid>();
            _tiles = DataFactory.MakeMockTileGrid(5, 5, _mockGrid);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            _stamp = new Stamp(3, 3);
            _stampSelection = new StampSelection(_stamp, _mockGrid.Object);
        }

        [Test]
        public void StartSelection_CenterOfGrid_SelectsTilesInStamp()
        {
            _stampSelection.StartSelection(_tiles[2,2].Tile.Object);

            var selected = _stampSelection.GetSelectedTiles();
            AssertSelectionIsInBounds(selected, 1, 3, 1, 3);
        }

        [Test]
        public void ChangeSelection_CenterOfGrid_SelectsTilesInStamp()
        {
            _stampSelection.StartSelection(_tiles[1,1].Tile.Object);

            _stampSelection.ChangeSelection(_tiles[2,2].Tile.Object);

            var selected = _stampSelection.GetSelectedTiles();
            AssertSelectionIsInBounds(selected, 1, 3, 1, 3);
        }

        [Test]
        public void GetSelectedTiles_OffTopLeftEdge_PlacesWithinBounds()
        {
            _stampSelection.StartSelection(GetMockTileAtPosition(0, 0).Object);

            var selected = _stampSelection.GetSelectedTiles();
            AssertSelectionIsInBounds(selected, 0, 2, 0, 2);
        }

        [Test]
        public void GetSelectedTiles_OffBottomRightEdge_PlacesWithinBounds()
        {
            _stampSelection.StartSelection(GetMockTileAtPosition(4, 4).Object);

            var selected = _stampSelection.GetSelectedTiles();
            AssertSelectionIsInBounds(selected, 2, 4, 2, 4);
        }

        [Test]
        public void GetSelectedTiles_StartSelectionNotCalled_ReturnsEmpty()
        {
            var actual = _stampSelection.GetSelectedTiles();

            CollectionAssert.IsEmpty(actual);
        }

        [Test]
        public void GetEdgeTiles_StartSelectionNotCalled_ReturnsEmpty()
        {
            var actual = _stampSelection.GetEdgeTiles();

            CollectionAssert.IsEmpty(actual);
        }

        private Mock<ITile> GetMockTileAtPosition(int x, int z)
        {
            return _tiles[x, z].Tile;
        }

        private void AssertSelectionIsInBounds(IList<ITile> selectedTiles, int minX, int maxX, int minZ, int maxZ)
        {
            Assert.AreEqual((maxX - minX + 1) * (maxZ - minZ + 1), selectedTiles.Count);

            for (int x = minX; x < maxX; ++x)
            {
                for (int z  = minZ; z < maxZ; ++z)
                {
                    CollectionAssert.Contains(selectedTiles, _tiles[x, z].Tile.Object);
                }
            }
        }
    }
}
