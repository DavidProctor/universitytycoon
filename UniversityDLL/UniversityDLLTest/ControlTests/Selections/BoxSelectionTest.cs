﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests.Selections
{
    [TestFixture]
    class BoxSelectionTest
    {
        private BoxSelection Q;

        private const int _testGridWidth = 4;
        private const int _testGridHeight = 4;
        private CompleteMockTile[,] QTiles;
        private Mock<IGrid> QGrid;

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            QGrid = new Mock<IGrid>();
            QTiles = DataFactory.MakeMockTileGrid(_testGridWidth, _testGridHeight, QGrid);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            Q = new BoxSelection(QGrid.Object);
        }

        [Test]
        public void StartSelection_AddsStartTile()
        {
            var startTile = QTiles[0, 0].Tile.Object;
            Q.StartSelection(startTile);

            CollectionAssert.Contains(Q.GetSelectedTiles(), startTile);
        }

        [Test]
        public void ChangeSelection_AddsNewTile()
        {
            var startTile = QTiles[0, 0].Tile.Object;
            var newTile = QTiles[0, 1].Tile.Object;
            Q.StartSelection(startTile);

            Q.ChangeSelection(newTile);

            CollectionAssert.Contains(Q.GetSelectedTiles(), newTile);
        }

        [Test]
        public void ChangeSelection_Diagonal_CreatesBox()
        {
            var startTile = QTiles[0, 0].Tile.Object;
            var newTile = QTiles[1, 1].Tile.Object;
            Q.StartSelection(startTile);

            Q.ChangeSelection(newTile);

            CollectionAssert.Contains(Q.GetSelectedTiles(), QTiles[0, 1].Tile.Object);
            CollectionAssert.Contains(Q.GetSelectedTiles(), QTiles[1, 0].Tile.Object);
        }

        [Test]
        public void ChangeSelection_LargeBox_ContainsCenter()
        {
            var startTile = QTiles[0, 0].Tile.Object;
            var endTile = QTiles[2, 2].Tile.Object;

            Q.StartSelection(startTile);
            Q.ChangeSelection(endTile);

            CollectionAssert.Contains(Q.GetSelectedTiles(), QTiles[1, 1].Tile.Object);
        }

        [Test]
        public void ChangeSelection_SelectionCrossesStartTile_DoesNotContainEarlyBox()
        {
            var startTile = QTiles[1, 1].Tile.Object;
            var midTile = QTiles[2, 2].Tile.Object;
            var endTile = QTiles[0, 0].Tile.Object;

            Q.StartSelection(startTile);
            Q.ChangeSelection(midTile);
            Q.ChangeSelection(endTile);

            CollectionAssert.Contains(Q.GetSelectedTiles(), endTile);
            CollectionAssert.DoesNotContain(Q.GetSelectedTiles(), midTile);
        }

        [Test]
        public void GetEdgeTiles_ReturnsEdgeTiles()
        {
            var startTile = QTiles[0, 0].Tile.Object;
            var endTile = QTiles[2, 2].Tile.Object;
            var expected = new List<ITile>
            {
                QTiles[0,0].Tile.Object,
                QTiles[0,1].Tile.Object,
                QTiles[0,2].Tile.Object,
                QTiles[1,0].Tile.Object,
                QTiles[1,2].Tile.Object,
                QTiles[2,0].Tile.Object,
                QTiles[2,1].Tile.Object,
                QTiles[2,2].Tile.Object,
            };
            Q.StartSelection(startTile);
            Q.ChangeSelection(endTile);

            var actual = Q.GetEdgeTiles();

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void GetSelection_SelectionNotStarted_ReturnsEmpty()
        {
            var actual = Q.GetSelectedTiles();

            CollectionAssert.IsEmpty(actual);
        }
    }
}
