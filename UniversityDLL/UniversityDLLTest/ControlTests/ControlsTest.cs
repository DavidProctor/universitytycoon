﻿using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.ControlManagement;
using UniversityDLL.ControlManagement.MouseModes;
using UniversityDLL.UI;

namespace UniversityDLLTest.ControlTests
{
    [TestFixture]
    public class ControlsTest
    {
        private Controls _controls;

        private Mock<MouseMode> _mockMouseMode;
        private Mock<IDialogFactory> _mockDialogFactory;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockMouseMode = new Mock<MouseMode>();
            _mockDialogFactory = new Mock<IDialogFactory>();

            _controls = new Controls(_mockDialogFactory.Object);

            _controls.SetMouseMode(_mockMouseMode.Object);
            _controls.SetAltMouseMode(_mockMouseMode.Object);
        }

        [Test]
        public void MouseMode_DefaultsToStandard()
        {
            _controls = new Controls(_mockDialogFactory.Object);

            Assert.IsInstanceOf<StandardMouseMode>(_controls.MouseMode);
        }

        [Test]
        public void SetMouseMode_SetsMouseMode()
        {
            _controls.SetMouseMode(_mockMouseMode.Object);

            Assert.AreEqual(_mockMouseMode.Object, _controls.MouseMode);
        }

        [Test]
        public void SetMouseMode_DestroysOldMouseMode()
        {
            _controls.SetMouseMode(_mockMouseMode.Object);

            _mockMouseMode.Verify(m => m.Destroy());
        }

        [Test]
        public void SetAltMouseMode_SetsAltMouseMode()
        {
            _controls.SetAltMouseMode(_mockMouseMode.Object);

            Assert.AreEqual(_mockMouseMode.Object, _controls.AltMouseMode);
        }

        [Test]
        public void SetAltMouseMode_DestroysOldMouseMode()
        {
            _controls.SetAltMouseMode(_mockMouseMode.Object);

            _mockMouseMode.Verify(m => m.Destroy());
        }
    }
}
