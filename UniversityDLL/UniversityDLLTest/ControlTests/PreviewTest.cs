﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL;
using UniversityDLL.ControlManagement.Selections;
using UniversityDLL.GameMangement;
using UniversityDLL.Previews;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ControlTests
{
    [TestFixture]
    class PreviewTest
    {
        private Preview _preview;
        private Mock<ISelection> _selection;
        private CompleteMockTile _tile;
        private Mock<IGameObject> _edge;
        private Mock<IGameObject> _middle;
        private TileLayer _previewLayer = TileLayer.Wall;

        [SetUp]
        public void BeforeEachTest()
        {
            _selection = new Mock<ISelection>();
            _selection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile>());
            _selection.Setup(s => s.GetEdgeTiles()).Returns(new List<ITile>());

            _tile = DataFactory.MakeCompleteMockTile();

            _edge = DataFactory.MakeSelfReplicatingGameObject();
            _middle = DataFactory.MakeSelfReplicatingGameObject();

            _preview = new Preview(_selection.Object, _edge.Object, _middle.Object, _previewLayer);
        }

        [Test]
        public void Ctor_SetsSelection()
        {
            Assert.AreEqual(_selection.Object, _preview.Selection);
        }

        [Test]
        public void Ctor_SetsEdge()
        {
            Assert.AreEqual(_edge.Object, _preview.EdgeObject);
        }

        [Test]
        public void StartPreview_StartsSelection()
        {
            _preview.StartPreview(_tile.Tile.Object);

            _selection.Verify(s => s.StartSelection(_tile.Tile.Object));
        }

        [Test]
        public void StartPreview_RendersPreview()
        {
            SetupSelectionToReturnTile();

            _preview.StartPreview(_tile.Tile.Object);

            _middle.Verify(o => o.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void StartPreview_Twice_DestroysOldPreview()
        {
            var firstTile = DataFactory.MakeCompleteMockTile();
            var secondTile = DataFactory.MakeCompleteMockTile();
            var secondRender = new Mock<IGameObject>();
            _middle.SetupSequence(m => m.GetClone())
                .Returns(_middle.Object)
                .Returns(secondRender.Object);
            _selection.SetupSequence(s => s.GetSelectedTiles())
                .Returns(new List<ITile>())
                .Returns(new List<ITile> { firstTile.Tile.Object })
                .Returns(new List<ITile> { secondTile.Tile.Object })
                .Returns(new List<ITile> { secondTile.Tile.Object });

            _preview.StartPreview(firstTile.Tile.Object);
            _preview.StartPreview(secondTile.Tile.Object);

            _middle.Verify(m => m.Destroy());
            secondRender.Verify(r => r.Destroy(), Times.Never);
        }

        [Test]
        public void StartPreview_DelegatesHidingObscuredLayersToTileContentCollection()
        {
            SetupSelectionToReturnTile();
            _preview.StartPreview(_tile.Tile.Object);

            _tile.ContentCollection.Verify(c => c.HideLayersObscuredByLayer(_previewLayer));
        }

        [Test]
        public void ChangePreview_ChangesSelection()
        {
            _preview.ChangePreview(_tile.Tile.Object);

            _selection.Verify(s => s.ChangeSelection(_tile.Tile.Object));
        }

        [Test]
        public void ChangePreview_SetsTemporaryOnTileRender()
        {
            SetupSelectionToReturnTile();

            _preview.ChangePreview(_tile.Tile.Object);

            _middle.Verify(o => o.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void ChangePreview_Twice_DestroysOldPreview()
        {
            var firstTile = DataFactory.MakeCompleteMockTile();
            var secondTile = DataFactory.MakeCompleteMockTile();
            var secondRender = new Mock<IGameObject>();
            _middle.SetupSequence(m => m.GetClone())
                .Returns(_middle.Object)
                .Returns(secondRender.Object);
            _selection.SetupSequence(s => s.GetSelectedTiles())
                .Returns(new List<ITile>())
                .Returns(new List<ITile> { firstTile.Tile.Object })
                .Returns(new List<ITile> { secondTile.Tile.Object })
                .Returns(new List<ITile> { secondTile.Tile.Object });
            _preview.StartPreview(firstTile.Tile.Object);
            _preview.ChangePreview(secondTile.Tile.Object);

            _middle.Verify(g => g.Destroy());
            secondRender.Verify(r => r.Destroy(), Times.Never);
        }

        [Test]
        public void ChangePreview_RemovedContentsAreShown()
        {
            _preview = new Preview(_selection.Object, _edge.Object, _middle.Object, TileLayer.Door);
            var firstTile = DataFactory.MakeCompleteMockTile();
            firstTile.ContentCollection.Setup(c => c.HasLayer(TileLayer.Wall));
            var secondTile = DataFactory.MakeCompleteMockTile();
            var secondRender = new Mock<IGameObject>();
            _middle.SetupSequence(m => m.GetClone())
                .Returns(_middle.Object)
                .Returns(secondRender.Object);
            _selection.SetupSequence(s => s.GetSelectedTiles())
                .Returns(new List<ITile>())
                .Returns(new List<ITile> { firstTile.Tile.Object })
                .Returns(new List<ITile> { secondTile.Tile.Object })
                .Returns(new List<ITile> { secondTile.Tile.Object });
            _preview.StartPreview(firstTile.Tile.Object);
            _preview.ChangePreview(secondTile.Tile.Object);

            firstTile.ContentCollection.Verify(c => c.Render());
        }

        [Test]
        public void ChangePreview_EdgeChangesToMiddle_DestroysOldPreviewAndCreatesNew()
        {

            _selection.SetupSequence(s => s.GetEdgeTiles())
                .Returns(new List<ITile> { _tile.Tile.Object })
                .Returns(new List<ITile>());
            _selection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile> { _tile.Tile.Object });
            _preview.StartPreview(_tile.Tile.Object);

            _preview.ChangePreview(_tile.Tile.Object);

            _edge.Verify(e => e.Destroy());
            _middle.Verify(m => m.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void RenderPreview_RendersTileContent()
        {
            SetupSelectionToReturnTile();

            _preview.RenderPreview();

            _middle.Verify(g => g.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void RenderPreview_RendersEdgesAndMiddle()
        {
            var edgeTile = DataFactory.MakeCompleteMockTile();
            var middleTile = DataFactory.MakeCompleteMockTile();
            _selection.Setup(s => s.GetEdgeTiles()).Returns(new List<ITile> { edgeTile.Tile.Object });
            _selection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile> { edgeTile.Tile.Object, middleTile.Tile.Object });

            _preview.RenderPreview();

            _edge.Verify(r => r.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
            _middle.Verify(r => r.Render(It.IsAny<float>(), It.IsAny<float>(), It.IsAny<float>()));
        }

        [Test]
        public void RenderPreview_Structure_DoesNotHideGround()
        {
            SetupSelectionToReturnTile();
            _preview = new Preview(_selection.Object, _edge.Object, _middle.Object, TileLayer.Structure);

            _preview.RenderPreview();

            _tile.ContentCollection.Verify(c => c.HideLayer(TileLayer.Ground), Times.Never);
        }

        [Test]
        public void Destroy_DestroysRenders()
        {
            _selection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile> { _tile.Tile.Object });
            _preview.RenderPreview();

            _preview.Destroy();

            _middle.Verify(g => g.Destroy());
        }

        [Test]
        public void Destroy_ShowsHiddenContents()
        {
            SetupSelectionToReturnTile();
            _preview = new Preview(_selection.Object, _edge.Object, _middle.Object, TileLayer.Door);
            _preview.RenderPreview();

            _preview.Destroy();

            _tile.ContentCollection.Verify(c => c.Render());
        }

        private void SetupSelectionToReturnTile()
        {
            _selection.Setup(s => s.GetSelectedTiles()).Returns(new List<ITile>() { _tile.Tile.Object });
        }
    }
}
