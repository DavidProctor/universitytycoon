﻿using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;
using UniversityDLLTest.ActivityPointTests;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    public class RoomTest
    {
        private class TestableRoom : Room
        { }

        private Room _room;

        [SetUp]
        public void BeforeEachTest()
        {
            _room = new TestableRoom();
        }

        [Test]
        public void GetAvailableActivityPoint_Works()
        {
            var point = new TestableActivityPoint();
            _room.AddActivityPoint(point);

            var result = _room.GetAvailableActivityPoint<TestableActivityPoint>();

            Assert.AreEqual(point, result);
        }

        [Test]
        public void GetAvailableActivityPoint_NoneAvailable_ReturnsNull()
        {
            var result = _room.GetAvailableActivityPoint<TestableActivityPoint>();

            Assert.IsNull(result);
        }

        [Test]
        public void GetAvailableActivityPoint_WrongType_ReturnsNull()
        {
            var mockTileContent = new Mock<ITileContent>();
            var point = new ClassSeatActivityPoint(mockTileContent.Object);
            _room.AddActivityPoint(point);

            var result = _room.GetAvailableActivityPoint<TestableActivityPoint>();

            Assert.IsNull(result);
        }
    }
}