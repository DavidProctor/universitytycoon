﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.ActivityPoints;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    class RLectureHallTest
    {
        class TestableLecternActivityPoint : LecternActivityPoint
        {
            public int ExecuteInvocations { get; private set; }

            public TestableLecternActivityPoint() 
                : base(new Mock<ICalendar>().Object, new Mock<ITileContent>().Object)
            {
            }

            public override void ExecuteActivity()
            {
                ExecuteInvocations++;
            }
        }

        private BookableRoom _lectureHall;
        private List<ITile> _tiles;
        private Mock<ITile> _tile;
        private TestableLecternActivityPoint _fakeLectern;
        private Fakes.TestableClassSeatActivityPoint _fakeSeat;

        [SetUp]
        public void BeforeEachTest()
        {
            _tile = new Mock<ITile>();
            _tiles = new List<ITile> { _tile.Object };
            _fakeLectern = new TestableLecternActivityPoint();
            _fakeSeat = new Fakes.TestableClassSeatActivityPoint();

            _lectureHall = new RLectureHall();
        }

        [Test]
        public void SetTiles_Works()
        {
            _lectureHall.SetTiles(_tiles);

            Assert.IsTrue(_lectureHall.Tiles.Contains(_tile.Object));
        }

        [Test]
        public void Capacity_IsCountOfSeats()
        {
            var mockTileContent = new Mock<ITileContent>();
            var countedPoint1 = new ClassSeatActivityPoint(mockTileContent.Object);
            var countedPoint2 = new ClassSeatActivityPoint(mockTileContent.Object);
            var uncountedPoint = new SpawnerActivityPoint();
            _lectureHall.AddActivityPoint(countedPoint1);
            _lectureHall.AddActivityPoint(countedPoint2);
            _lectureHall.AddActivityPoint(uncountedPoint);

            Assert.AreEqual(2, _lectureHall.Capacity);
        }

        [Test]
        public void HasCurrentSchedule()
        {
            var semester = Semester.GetSemester(2016, Season.Summer);
            Assert.IsNotNull(_lectureHall.GetSchedule(semester));
        }

        [Test]
        public void SchedulesDifferBySemester()
        {
            var s1 = Semester.GetSemester(2000, Season.Fall);
            var s2 = Semester.GetSemester(3000, Season.Winter);

            Assert.AreNotSame(_lectureHall.GetSchedule(s1), _lectureHall.GetSchedule(s2));
        }

        [Test]
        public void AddBooking_Works()
        {
            var semester = Semester.GetSemester(2000, Season.Fall);
            var dayAndTime = DayAndTime.FromDetails(DayOfWeek.Friday, 3);
            var course = new Mock<ICourseInstance>();

            _lectureHall.AddBooking(dayAndTime, course.Object, semester);

            var bookings = _lectureHall.GetSchedule(semester).Bookings;
            Assert.IsNotNull(bookings.FirstOrDefault(b => b.DayAndTime.Equals(dayAndTime)
                                                    && b.Course == course.Object
                                                    && b.Room == _lectureHall));
        }

        [Test]
        public void RemoveBooking_Works()
        {
            var semester = Semester.GetSemester(2000, Season.Fall);
            var dayAndTime = DayAndTime.FromDetails(DayOfWeek.Friday, 3);
            var course = new Mock<ICourseInstance>();
            _lectureHall.AddBooking(dayAndTime, course.Object, semester);

            _lectureHall.RemoveBooking(dayAndTime, semester);

            Assert.IsNull(_lectureHall.GetSchedule(semester).GetBooking(dayAndTime));
        }

        [Test]
        public void IsWalkable_ReturnsTrue()
        {
            Assert.IsTrue(_lectureHall.IsWalkable());
        }

        [Test]
        public void AddAndRemoveNeighbors_Works()
        {
            var neighbor = new Mock<Room>();

            CollectionAssert.IsEmpty(_lectureHall.Neighbors);

            _lectureHall.AddConnection(neighbor.Object);
            CollectionAssert.Contains(_lectureHall.Neighbors, neighbor.Object);

            _lectureHall.RemoveConnection(neighbor.Object);
            CollectionAssert.IsEmpty(_lectureHall.Neighbors);

            _lectureHall.RemoveConnection(neighbor.Object);
        }

        [Test]
        public void AddExit_OnePerRoom_Works()
        {
            var neighbor1 = new Mock<Room>();
            var exit1 = new Mock<ITile>();
            var neighbor2 = new Mock<Room>();
            var exit2 = new Mock<ITile>();

            _lectureHall.AddExit(exit1.Object, neighbor1.Object);
            _lectureHall.AddExit(exit2.Object, neighbor2.Object);

            var exit1List = _lectureHall.GetExitsToRoom(neighbor1.Object);
            Assert.AreEqual(1, exit1List.Count);
            CollectionAssert.Contains(exit1List, exit1.Object);
            var exit2List = _lectureHall.GetExitsToRoom(neighbor2.Object);
            Assert.AreEqual(1, exit2List.Count);
            CollectionAssert.Contains(exit2List, exit2.Object);
        }

        [Test]
        public void AddExit_MultipleExits_Works()
        {
            var neighbor = new Mock<Room>();
            var exit1 = new Mock<ITile>();
            var exit2 = new Mock<ITile>();

            _lectureHall.AddExit(exit1.Object, neighbor.Object);
            _lectureHall.AddExit(exit2.Object, neighbor.Object);

            var exits = _lectureHall.GetExitsToRoom(neighbor.Object);
            Assert.AreEqual(2, exits.Count);
            CollectionAssert.Contains(exits, exit1.Object);
            CollectionAssert.Contains(exits, exit2.Object);
        }

        [Test]
        public void GetExitToRoom_NoExits_ReturnsEmpty()
        {
            var missingNeighbor = new Mock<Room>();

            var exits = _lectureHall.GetExitsToRoom(missingNeighbor.Object);

            CollectionAssert.IsEmpty(exits);
        }

        [Test]
        public void RemoveExit_Works()
        {
            var neighbor = new Mock<Room>();
            var exit = new Mock<ITile>();
            _lectureHall.AddExit(exit.Object, neighbor.Object);

            _lectureHall.RemoveExit(exit.Object);

            CollectionAssert.DoesNotContain(_lectureHall.GetExitsToRoom(neighbor.Object), exit.Object);
        }

        [Test]
        public void GetAllExits_Works()
        {
            var exit1 = new Mock<ITile>();
            _lectureHall.AddExit(exit1.Object, new Mock<Room>().Object);
            var exit2 = new Mock<ITile>();
            _lectureHall.AddExit(exit2.Object, new Mock<Room>().Object);
            var exit3 = new Mock<ITile>();
            _lectureHall.AddExit(exit3.Object, new Mock<Room>().Object);

            var actual = _lectureHall.GetAllExits();

            Assert.AreEqual(3, actual.Count);
            CollectionAssert.Contains(actual, exit1.Object);
            CollectionAssert.Contains(actual, exit2.Object);
            CollectionAssert.Contains(actual, exit3.Object);
        }

        [Test]
        public void AddActivityPoint_Works()
        {
            var activityPoint = new Mock<ActivityPoint>();

            _lectureHall.AddActivityPoint(activityPoint.Object);

            CollectionAssert.Contains(_lectureHall.GetActivityPoints(), activityPoint.Object);
        }

        [Test]
        public void RemoveActivityPoint_Works()
        {
            var activityPoint = new Mock<ActivityPoint>();
            _lectureHall.AddActivityPoint(activityPoint.Object);

            _lectureHall.RemoveActivityPoint(activityPoint.Object);

            CollectionAssert.DoesNotContain(_lectureHall.GetActivityPoints(), activityPoint.Object);
        }

        [Test]
        public void IsReady_NoPoints_ReturnsFalse()
        {
            Assert.IsFalse(_lectureHall.IsReady());
        }

        [Test]
        public void IsReady_NoLectern_ReturnsFalse()
        {
            _lectureHall.AddActivityPoint(_fakeSeat);

            Assert.IsFalse(_lectureHall.IsReady());
        }

        [Test]
        public void IsReady_NoSeats_ReturnsFalse()
        {
            _lectureHall.AddActivityPoint(_fakeLectern);

            Assert.IsFalse(_lectureHall.IsReady());
        }

        [Test]
        public void IsReady_AllRequiredActivityPointsPresent_ReturnsTrue()
        {
            _lectureHall.AddActivityPoint(_fakeSeat);
            _lectureHall.AddActivityPoint(_fakeLectern);

            Assert.IsTrue(_lectureHall.IsReady());
        }

        [Test]
        public void HourlyUpdate_TriggersTeaching()
        {
            _lectureHall.AddActivityPoint(_fakeSeat);
            _lectureHall.AddActivityPoint(_fakeLectern);

            _lectureHall.HourlyUpdate();

            Assert.AreEqual(1, _fakeLectern.ExecuteInvocations);
        }

        [Test]
        public void HourlyUpdate_NotReady_DoesNothing()
        {
            _lectureHall.AddActivityPoint(_fakeLectern);

            _lectureHall.HourlyUpdate();

            Assert.AreEqual(0, _fakeLectern.ExecuteInvocations);
        }
    }
}
