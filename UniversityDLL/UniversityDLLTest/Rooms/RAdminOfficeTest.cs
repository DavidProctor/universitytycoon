﻿using Moq;
using NUnit.Framework;
using UniversityDLL.People;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    class RAdminOfficeTest
    {
        private RAdminOffice _adminOffice;
        private Mock<IAdministrator> _mockOwner;

        [SetUp]
        public void BeforeEachTest()
        {
            _adminOffice = new RAdminOffice();
            _mockOwner = new Mock<IAdministrator>();
        }

        [Test]
        public void IsReady_RequiresDesk()
        {
            Assert.IsFalse(_adminOffice.IsReady());

            var fakeDesk = new Fakes.TestableOfficeDeskActivityPoint();
            _adminOffice.AddActivityPoint(fakeDesk);

            Assert.IsTrue(_adminOffice.IsReady());
        }

        [Test]
        public void SetOwner_ChangesName()
        {
            Assert.AreEqual(_adminOffice.Name, "Empty administrator's office");
            
            _mockOwner.SetupGet(o => o.Name).Returns("Chudd Truckley");
            _adminOffice.SetOwner(_mockOwner.Object);

            Assert.AreEqual(_adminOffice.Owner, _mockOwner.Object);
            Assert.AreEqual(_adminOffice.Name, "Chudd Truckley's office");
        }

        [Test]
        public void SetOwner_SetsOfficeOnOwner()
        {
            _adminOffice.SetOwner(_mockOwner.Object);

            _mockOwner.Verify(o => o.SetOffice(_adminOffice));
        }
    }
}
