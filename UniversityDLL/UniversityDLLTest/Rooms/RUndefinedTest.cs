﻿using NUnit.Framework;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    public class RUndefinedTest
    {
        private Room _undefinedRoom;

        [SetUp]
        public void BeforeEachTest()
        {
            _undefinedRoom = RUndefined.GetInstance();
        }

        [Test]
        public void IsWalkable_ReturnsFalse()
        {
            Assert.IsTrue(_undefinedRoom.IsWalkable());
        }

        [Test]
        public void Name_IsFoundation()
        {
            Assert.AreEqual("Undefined Room", _undefinedRoom.Name);
        }
    }
}