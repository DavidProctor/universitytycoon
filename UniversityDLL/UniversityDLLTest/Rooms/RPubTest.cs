﻿using NUnit.Framework;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    public class RPubTest
    {
        private Room _pub;

        [SetUp]
        public void BeforeEachTest()
        {
            _pub = new RPub();
        }

        [Test]
        public void Ctor_SetsName()
        {
            Assert.IsFalse(string.IsNullOrEmpty(_pub.Name));
        }

        [Test]
        public void IsReady_NoSeating_ReturnsFalse()
        {
            Assert.IsFalse(_pub.IsReady());
        }

        [Test]
        public void IsReady_HasSeating_ReturnsTrue()
        {
            var fakeBooth = new Fakes.TestablePubSeatActivityPoint();
            _pub.AddActivityPoint(fakeBooth);

            Assert.IsTrue(_pub.IsReady());
        }
    }
}