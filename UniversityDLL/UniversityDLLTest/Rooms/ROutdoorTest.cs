﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    public class ROutdoorTest
    {
        private Room _outdoors;

        [SetUp]
        public void BeforeEachTest()
        {
            _outdoors = ROutdoors.GetInstance();
        }

        [Test]
        public void Ctor_SetsMembers()
        {
            Assert.AreEqual("Outdoors", _outdoors.Name);
            Assert.AreEqual(int.MaxValue, _outdoors.Capacity);
            CollectionAssert.IsEmpty(_outdoors.Tiles);
            CollectionAssert.IsEmpty(_outdoors.Neighbors);
        }

        [Test]
        public void SetTiles_Works()
        {
            var tile1 = DataFactory.MakeCompleteMockTile();
            var tile2 = DataFactory.MakeCompleteMockTile();
            var tiles = new List<ITile> {tile1.Tile.Object, tile2.Tile.Object};

            _outdoors.SetTiles(tiles);

            CollectionAssert.AreEquivalent(tiles, _outdoors.Tiles);
        }

        [Test]
        public void IsWalkable_ReturnsTrue()
        {
            Assert.IsTrue(_outdoors.IsWalkable());
        }

        [Test]
        public void AddAndRemoveNeighbors_Works()
        {
            var neighbor = new Mock<Room>();

            CollectionAssert.IsEmpty(_outdoors.Neighbors);

            _outdoors.AddConnection(neighbor.Object);
            CollectionAssert.Contains(_outdoors.Neighbors, neighbor.Object);

            _outdoors.RemoveConnection(neighbor.Object);
            CollectionAssert.IsEmpty(_outdoors.Neighbors);

            _outdoors.RemoveConnection(neighbor.Object);
        }

        [Test]
        public void AddExit_OnePerRoom_Works()
        {
            var neighbor1 = new Mock<Room>();
            var exit1 = new Mock<ITile>();
            var neighbor2 = new Mock<Room>();
            var exit2 = new Mock<ITile>();

            _outdoors.AddExit(exit1.Object, neighbor1.Object);
            _outdoors.AddExit(exit2.Object, neighbor2.Object);

            var exit1List = _outdoors.GetExitsToRoom(neighbor1.Object);
            Assert.AreEqual(1, exit1List.Count);
            CollectionAssert.Contains(exit1List, exit1.Object);
            var exit2List = _outdoors.GetExitsToRoom(neighbor2.Object);
            Assert.AreEqual(1, exit2List.Count);
            CollectionAssert.Contains(exit2List, exit2.Object);
        }

        [Test]
        public void AddExit_MultipleExits_Works()
        {
            var neighbor = new Mock<Room>();
            var exit1 = new Mock<ITile>();
            var exit2 = new Mock<ITile>();

            _outdoors.AddExit(exit1.Object, neighbor.Object);
            _outdoors.AddExit(exit2.Object, neighbor.Object);

            var exits = _outdoors.GetExitsToRoom(neighbor.Object);
            Assert.AreEqual(2, exits.Count);
            CollectionAssert.Contains(exits, exit1.Object);
            CollectionAssert.Contains(exits, exit2.Object);
        }

        [Test]
        public void GetExitToRoom_NoExits_ReturnsEmpty()
        {
            var missingNeighbor = new Mock<Room>();

            var exits = _outdoors.GetExitsToRoom(missingNeighbor.Object);

            CollectionAssert.IsEmpty(exits);
        }

        [Test]
        public void RemoveExit_Works()
        {
            var neighbor = new Mock<Room>();
            var exit = new Mock<ITile>();
            _outdoors.AddExit(exit.Object, neighbor.Object);

            _outdoors.RemoveExit(exit.Object);

            CollectionAssert.DoesNotContain(_outdoors.GetExitsToRoom(neighbor.Object), exit.Object);
        }

        [Test]
        public void GetAllExits_Works()
        {
            var exit1 = new Mock<ITile>();
            _outdoors.AddExit(exit1.Object, new Mock<Room>().Object);
            var exit2 = new Mock<ITile>();
            _outdoors.AddExit(exit2.Object, new Mock<Room>().Object);
            var exit3 = new Mock<ITile>();
            _outdoors.AddExit(exit3.Object, new Mock<Room>().Object);

            var actual = _outdoors.GetAllExits();

            Assert.IsTrue(actual.Count >= 3);
            CollectionAssert.Contains(actual, exit1.Object);
            CollectionAssert.Contains(actual, exit2.Object);
            CollectionAssert.Contains(actual, exit3.Object);
        }

        [Test]
        public void AddActivityPoint_Works()
        {
            var activityPoint = new Mock<ActivityPoint>();

            _outdoors.AddActivityPoint(activityPoint.Object);

            CollectionAssert.Contains(_outdoors.GetActivityPoints(), activityPoint.Object);
        }

        [Test]
        public void RemoveActivityPoint_Works()
        {
            var activityPoint = new Mock<ActivityPoint>();
            _outdoors.AddActivityPoint(activityPoint.Object);

            _outdoors.RemoveActivityPoint(activityPoint.Object);

            CollectionAssert.DoesNotContain(_outdoors.GetActivityPoints(), activityPoint.Object);
        }
    }
}