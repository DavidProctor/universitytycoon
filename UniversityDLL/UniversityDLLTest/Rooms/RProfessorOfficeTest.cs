﻿using NUnit.Framework;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Rooms
{
    [TestFixture]
    public class RProfessorOfficeTest
    {
        private Room _office;

        [SetUp]
        public void BeforeEachTest()
        {
            _office = new RProfessorOffice();
        }

        [Test]
        public void IsReady_NoDesk_ReturnsFalse()
        {
            Assert.IsFalse(_office.IsReady());
        }

        [Test]
        public void IsReady_HasDesk_ReturnsTrue()
        {
            var fakeDesk = new Fakes.TestableOfficeDeskActivityPoint();
            _office.AddActivityPoint(fakeDesk);

            Assert.IsTrue(_office.IsReady());
        }
    }
}