﻿using NUnit.Framework;
using UniversityDLL;
using Moq;
using UniversityDLL.GameMangement;
using UniversityDLL.People;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.People
{
    [TestFixture]
    class CharacterRendererTest
    {
        private ICharacterRenderer _characterRenderer;
        private Mock<IGameObject> _mockGameObject;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockGameObject = new Mock<IGameObject>();

            _characterRenderer = new CharacterRenderer(_mockGameObject.Object);
        }

        [Test]
        public void Spawn_RendersCharacterAtTilePosition()
        {
            var spawnTile = new Mock<ITile>();
            spawnTile.SetupGet(t => t.X).Returns(10);
            spawnTile.SetupGet(t => t.Z).Returns(15);

            _characterRenderer.Spawn(spawnTile.Object);

            _mockGameObject.Verify(g => g.Render(It.IsInRange(9.9f, 10.1f, Range.Inclusive),
                It.IsInRange(-.1f, 1f, Range.Inclusive),
                It.IsInRange(14.9f, 15.1f, Range.Inclusive)));
        }

        [Test]
        public void Despawn_HidesObject()
        {
            _characterRenderer.Despawn();

            _mockGameObject.Verify(c => c.Render(0f, It.Is<float>(y => y > 9000), 0f));
        }
    }
}
