﻿using System;
using NUnit.Framework;
using Moq;
using UniversityDLL;
using UniversityDLL.Academics;
using UniversityDLL.People;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.People
{
    [TestFixture]
    class PersonalScheduleTest
    {
        private IPersonalSchedule _personalSchedule;

        private readonly DayAndTime _dayAndTime = DayAndTime.FromDetails(DayOfWeek.Thursday, 10);
        private Mock<Room> _mockRoom;

        [SetUp]
        public void SetUp()
        {
            _mockRoom = new Mock<Room>();

            _personalSchedule = new PersonalSchedule();
        }

        [Test]
        public void AddBooking_AddsBooking()
        {
            _personalSchedule.AddBooking(_dayAndTime, _mockRoom.Object);

            Assert.AreEqual(_mockRoom.Object, _personalSchedule.GetLocationAtTime(_dayAndTime));
        }

        [Test]
        public void GetLocationAtTime_NoBooking_ReturnsNull()
        {
            Assert.IsNull(_personalSchedule.GetLocationAtTime(_dayAndTime));
        }

        [Test]
        public void GetBookingsByDay_Works()
        {
            _personalSchedule.AddBooking(_dayAndTime, _mockRoom.Object);
            var newRoom = new Mock<Room>();
            var newTime = DayAndTime.FromDetails(DayOfWeek.Saturday, 22);
            _personalSchedule.AddBooking(newTime, newRoom.Object);

            var actual = _personalSchedule.GetBookingsByDay(DayOfWeek.Saturday);

            Assert.AreEqual(newRoom.Object, actual[0].Room);
            Assert.AreEqual(1, actual.Count);
        }
    }
}
