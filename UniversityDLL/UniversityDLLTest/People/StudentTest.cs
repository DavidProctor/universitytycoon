﻿using System;
using System.Collections.Generic;

using NUnit.Framework;
using Moq;
using UniversityDLL.Academics;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.People
{
    [TestFixture]
    class StudentTest
    {
        private IStudent _student;

        private Mock<IDepartment> _major;
        private Mock<ICourseEnrolment> _enrolment;
        private Mock<ICharacterRenderer> _renderer;
        private Mock<ICharacterBehavior> _behavior;
        private Mock<IPersonalSchedule> _schedule;
        private Mock<Room> _room;

        private readonly DayAndTime _enrolmentDayAndTime = DayAndTime.FromDetails(DayOfWeek.Thursday, 11);
        private readonly string _name = "Bill Smith";

        [SetUp]
        public void BeforeEachTest()
        {
            _room = new Mock<Room>();
            _enrolment = MakeEnrolmentWithCredits(3);
            _renderer = new Mock<ICharacterRenderer>();
            _behavior = new Mock<ICharacterBehavior>();
            _schedule = new Mock<IPersonalSchedule>();

            _major = new Mock<IDepartment>();

            _student = new Student(_name, _major.Object, _renderer.Object, _behavior.Object, _schedule.Object);
        }

        [Test]
        public void Ctor_SetsMajorScheduleAndName_EmptyEnrolments()
        {
            Assert.AreEqual(_major.Object, _student.Major);
            Assert.AreEqual(_schedule.Object, _student.PersonalSchedule);
            Assert.AreEqual(_name, _student.Name);

            CollectionAssert.IsEmpty(_student.Enrolments);
        }

        [Test]
        public void AddEnrolment_Works()
        {
            _student.AddEnrolment(_enrolment.Object);

            CollectionAssert.Contains(_student.Enrolments, _enrolment.Object);
        }

        [Test]
        public void AddEnrolment_AddsToSchedule()
        {
            _student.AddEnrolment(_enrolment.Object);

            _schedule.Verify(s => s.AddBooking(_enrolmentDayAndTime, _room.Object));
        }

        [Test]
        public void Year_ReturnsFirstYearCorrectly()
        {
            _student.AddEnrolment(MakeEnrolmentWithCredits(3).Object);

            Assert.AreEqual(Year.First, _student.Year);
        }

        [Test]
        public void Year_ReturnsSecondYearIfOver18Credits()
        {
            _student.AddEnrolment(MakeEnrolmentWithCredits(17).Object);
            _student.AddEnrolment(MakeEnrolmentWithCredits(1).Object);

            Assert.AreEqual(Year.Second, _student.Year);
        }

        [Test]
        public void Year_ReturnsThirdYearIfOver36Credits()
        {
            _student.AddEnrolment(MakeEnrolmentWithCredits(15).Object);
            _student.AddEnrolment(MakeEnrolmentWithCredits(15).Object);
            _student.AddEnrolment(MakeEnrolmentWithCredits(15).Object);

            Assert.AreEqual(Year.Third, _student.Year);
        }

        [Test]
        public void Year_ReturnsFourthYearIfOver60Credits()
        {
            _student.AddEnrolment(MakeEnrolmentWithCredits(int.MaxValue).Object);

            Assert.AreEqual(Year.Fourth, _student.Year);
        }

        [Test]
        public void Spawn_DelegatesToCharacterRenderer()
        {
            var mockTile = new Mock<ITile>();
            _student.Spawn(mockTile.Object);

            _renderer.Verify(r => r.Spawn(mockTile.Object));
        }

        [Test]
        public void Despawn_DelegatesToCharacterRenderer()
        {
            _student.Despawn();

            _renderer.Verify(r => r.Despawn());
        }

        public void Study_AddsValueToEnrolment()
        {
            var enrolment = new Mock<ICourseEnrolment>();
            var course = new Mock<ICourseInstance>();
            _enrolment.SetupGet(e => e.Course).Returns(course.Object);
            _student.AddEnrolment(enrolment.Object);

            _student.Study(course.Object, 5);

            _enrolment.Verify(e => e.AddStudyValue(5));
        }

        private Mock<ICourseEnrolment> MakeEnrolmentWithCredits(int credits)
        {
            var booking = new Mock<IRoomBooking>();
            booking.SetupGet(b => b.DayAndTime).Returns(_enrolmentDayAndTime);
            booking.SetupGet(b => b.Room).Returns(_room.Object);
            var sessions = new List<IRoomBooking>() { booking.Object };

            var course = DataFactory.MakeMockCoursePair(2, Year.First, true, new Mock<IDepartment>().Object);
            course.Template.SetupGet(c => c.RequiredHours).Returns(credits);
            course.Instance.SetupGet(c => c.ScheduledSessions).Returns(sessions);

            var enrolment = new Mock<ICourseEnrolment>();
            enrolment.SetupGet(e => e.Course).Returns(course.Instance.Object);

            return enrolment;
        }
    }
}
