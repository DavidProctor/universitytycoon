﻿using System;

using NUnit.Framework;
using Moq;
using UniversityDLL;
using UniversityDLL.Academics;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class RoomScheduleTest
    {
        private RoomSchedule _schedule;

        private Mock<ICourseInstance> _course;
        private Mock<Room> _room;
        private Mock<IRoomBooking> _booking;
        private readonly Semester _semester = Semester.GetSemester(1901, Season.Winter);

        private const DayOfWeek Day = DayOfWeek.Monday;
        private const int Hour = 21;
        private readonly DayAndTime _dayAndTime = DayAndTime.FromDetails(Day, Hour);

        [SetUp]
        public void BeforeEachTest()
        {
            _course = new Mock<ICourseInstance>();
            _room = new Mock<Room>();
            
            _booking = new Mock<IRoomBooking>();
            _booking.SetupGet(b => b.Course).Returns(_course.Object);
            _booking.SetupGet(b => b.Room).Returns(_room.Object);
            _booking.SetupGet(b => b.DayAndTime).Returns(_dayAndTime);

            _schedule = new RoomSchedule(_room.Object, _semester);
        }

        [Test]
        public void Ctor_SetsRoomAndSemester()
        {
            Assert.AreEqual(_room.Object, _schedule.Room);
            Assert.AreEqual(_semester, _schedule.Semester);
        }

        [Test]
        public void Schedule_CreatesSchedule()
        {
            _schedule.Schedule(_dayAndTime, _course.Object);

            var actual = _schedule.GetBooking(_dayAndTime);
            Assert.AreEqual(_course.Object, actual.Course);
            Assert.AreEqual(_room.Object, actual.Room);
            Assert.AreEqual(_dayAndTime, actual.DayAndTime);
        }

        [Test]
        public void GetBooking_ReturnsNullWhenNoBooking()
        {
            Assert.IsNull(_schedule.GetBooking(_dayAndTime));
        }

        [Test]
        public void RemoveBooking_Destroys()
        {
            _schedule.Schedule(_dayAndTime, _course.Object);
            var actual = _schedule.GetBooking(_dayAndTime);

            _schedule.RemoveBooking(_dayAndTime);

            _course.Verify(c => c.RemoveRoomBooking(It.IsAny<IRoomBooking>()));
        }
    }
}
