﻿using NUnit.Framework;
using Moq;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.People;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class CourseEnrolmentTest
    {
        private CourseEnrolment _courseEnrolment;

        private Mock<IStudent> _mockStudent;
        private Mock<ICourseInstance> _mockCourse;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockStudent = new Mock<IStudent>();
            _mockCourse = new Mock<ICourseInstance>();

            _courseEnrolment = new CourseEnrolment(_mockStudent.Object, _mockCourse.Object);
        }

        [Test]
        public void Ctor_SetsStudentAndCourse()
        {
            Assert.AreEqual(_courseEnrolment.Student, _mockStudent.Object);
            Assert.AreEqual(_courseEnrolment.Course, _mockCourse.Object);
        }

        [Test]
        public void Ctor_NotifiesStudentCourseAndGameState()
        {
            _mockStudent.Verify(s => s.AddEnrolment(_courseEnrolment));
            _mockCourse.Verify(c => c.AddEnrolment(_courseEnrolment));
            CollectionAssert.Contains(GameState.GetInstance().Enrolments, _courseEnrolment);
        }

        [Test]
        public void AddStudyValue_Works()
        {
            Assert.AreEqual(0, _courseEnrolment.StudyValue);

            _courseEnrolment.AddStudyValue(5);
            Assert.AreEqual(5, _courseEnrolment.StudyValue);
        }
    }
}
