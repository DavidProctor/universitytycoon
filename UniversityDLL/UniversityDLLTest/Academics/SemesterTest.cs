﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Moq;
using UniversityDLL;
using UniversityDLL.Academics;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class SemesterTest
    {
        private const int Year = 1988;
        private readonly Season Season = Season.Fall;

        [Test]
        public void GetSemester_RecyclesReferences()
        {
            var ref1 = Semester.GetSemester(Year, Season);
            var ref2 = Semester.GetSemester(Year, Season);

            Assert.AreSame(ref1, ref2);
        }

        [Test]
        public void GetSemester_ByDateTime_RecyclesReferences()
        {
            var ref1 = Semester.GetSemester(Year, Season);
            var ref2 = new TimeUnit(1988, Season.Fall, 2, DayOfWeek.Friday).Semester;

            Assert.AreSame(ref1, ref2);
        }

        [Test]
        public void GetAllSemesters_ReturnsAllSemesters()
        {
            var sem1 = Semester.GetSemester(1988, Season.Winter);
            var sem2 = Semester.GetSemester(1988, Season.Summer);
            var sem3 = Semester.GetSemester(1988, Season.Fall);

            var actual = Semester.GetAllSemesters();
            CollectionAssert.Contains(actual, sem1);
            CollectionAssert.Contains(actual, sem2);
            CollectionAssert.Contains(actual, sem3);
        }

        [Test]
        public void GetNext_Works()
        {
            var semester = Semester.GetSemester(1860, Season.Winter);

            semester = semester.GetNext();
            Assert.AreEqual(semester, Semester.GetSemester(1860, Season.Summer));
            semester = semester.GetNext();
            Assert.AreEqual(semester, Semester.GetSemester(1860, Season.Fall));
            semester = semester.GetNext();
            Assert.AreEqual(semester, Semester.GetSemester(1861, Season.Winter));
        }

        [Test]
        public void ToString_Works()
        {
            var semester = Semester.GetSemester(1988, Season.Winter);

            Assert.AreEqual("Winter 1988", semester.ToString());
        }
    }
}
