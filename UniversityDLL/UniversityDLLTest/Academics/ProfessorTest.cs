﻿using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.People;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class ProfessorTest
    {
        private IProfessor _professor;
        private Mock<ICharacterBehavior> _mockCharacterBehavior;
        private Mock<IPersonalSchedule> _mockPersonalSchedule;
        private Mock<ICharacterRenderer> _mockCharacterRenderer;

        private const string Name = "Cmdr. Corky Wloderczak";

        [SetUp]
        public void BeforeEachTest()
        {
            _mockCharacterBehavior = new Mock<ICharacterBehavior>();
            _mockPersonalSchedule = new Mock<IPersonalSchedule>();
            _mockCharacterRenderer = new Mock<ICharacterRenderer>();

            _professor = new Professor(Name, 
                _mockCharacterBehavior.Object, 
                _mockPersonalSchedule.Object, 
                _mockCharacterRenderer.Object,
                5);
        }

        [Test]
        public void Ctor_SetsName()
        {
            Assert.AreEqual(Name, _professor.Name);
        }

        [Test]
        public void Ctor_RegistersProfWithGameState()
        {
            var profs = GameState.GetInstance().Professors;

            CollectionAssert.Contains(profs, _professor);
        }

        [Test]
        public void ToString_ReturnsName()
        {
            Assert.AreEqual(Name, _professor.ToString());
        }
    }
}
