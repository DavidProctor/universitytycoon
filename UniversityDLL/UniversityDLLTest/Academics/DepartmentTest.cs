﻿using NUnit.Framework;
using Moq;
using UniversityDLL.Academics;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class DepartmentTest
    {
        private IDepartment _department;

        private Mock<ICourseInstance> _courseInstance;
        private Mock<ICourseTemplate> _courseTemplate;

        private const string DepartmentName = "VCR Repair";
        private readonly Semester _semester = Semester.GetSemester(1989, Season.Summer);
        private readonly int PrestigeCost = 75;

        [SetUp]
        public void BeforeEachTest()
        {
            _courseInstance = new Mock<ICourseInstance>();
            _courseInstance.SetupGet(c => c.Semester).Returns(_semester);

            _courseTemplate = new Mock<ICourseTemplate>();

            _department = new Department(DepartmentName);
        }

        [Test]
        public void Ctor_SetsUpPublicFields()
        {
            Assert.AreEqual(DepartmentName, _department.Name);
            CollectionAssert.IsEmpty(_department.GetCourses());
            Assert.AreEqual(12, _department.RequiredCreditsPerYear);
            Assert.AreEqual(PrestigeCost, _department.PrestigeCost);
        }

        [Test]
        public void AddCourse_Works()
        {
            _department.AddCourse(_courseTemplate.Object);

            CollectionAssert.Contains(_department.GetCourses(), _courseTemplate.Object);
        }

        [Test]
        public void AddScheduledCourse_Works()
        {
            _department.AddScheduledCourse(_courseInstance.Object);

            CollectionAssert.Contains(_department.GetScheduledCourses(_semester), _courseInstance.Object);
        }

        [Test]
        public void SetAdmissionsPolicy_LimitsWork()
        {
            _department.AdmissionsCapacity = -1;
            Assert.AreEqual(10, _department.AdmissionsCapacity);

            _department.AdmissionsCapacity = 101;
            Assert.AreEqual(10, _department.AdmissionsCapacity);

            _department.AdmissionsCapacity = 50;
            Assert.AreEqual(50, _department.AdmissionsCapacity);
        }

        [Test]
        public void Equals_SameName_ReturnsTrue()
        {
            var d1 = new Department("TestDept");
            var d2 = new Department("TestDept");

            Assert.AreEqual(d1, d2);
        }
    }
}
