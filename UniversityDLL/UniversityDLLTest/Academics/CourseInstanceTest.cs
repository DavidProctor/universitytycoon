﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Moq;
using UniversityDLL;
using UniversityDLL.Academics;
using UniversityDLL.People;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class CourseInstanceTest
    {
        private CourseInstance Q;

        private Mock<IProfessor> QProfessor;
        private Mock<IStudent> QStudent1;
        private Mock<ICourseEnrolment> QCourseEnrolment;
        private Mock<ICourseTemplate> QCourse;

        private readonly Semester Semester = Semester.GetSemester(1932, Season.Summer);

        [OneTimeSetUp]
        public void BeforeAnyTest()
        {
            QProfessor = new Mock<IProfessor>();
            QStudent1 = new Mock<IStudent>();
            QCourseEnrolment = new Mock<ICourseEnrolment>();
            QCourse = new Mock<ICourseTemplate>();
            QCourse.SetupGet(c => c.RequiredHours).Returns(2);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            Q = new CourseInstance(Semester, QCourse.Object);
        }

        [Test]
        public void Ctor_SetsSemesterAndCourse_NullProfessor()
        {
            Assert.AreEqual(Semester, Q.Semester);
            Assert.AreEqual(QCourse.Object, Q.Template);
            
            Assert.IsNull(Q.Professor);
        }

        [Test]
        public void Enrol_AddsEnrolment()
        {
            AddRequiredHours();

            Q.AddEnrolment(QCourseEnrolment.Object);

            CollectionAssert.Contains(Q.Enrolments, QCourseEnrolment.Object);
        }

        [Test]
        public void Enrol_ThrowsWhenOverCapacity()
        {
            AddRequiredHours();

            Q.AddEnrolment(QCourseEnrolment.Object);
            Q.AddEnrolment(new Mock<ICourseEnrolment>().Object);

            Assert.Throws<ArgumentOutOfRangeException>(() => Q.AddEnrolment(new Mock<ICourseEnrolment>().Object));
        }

        [Test]
        public void IsReady_NoProf_ReturnsFalse()
        {
            AddRequiredHours();

            Assert.IsFalse(Q.IsReady());
        }

        [Test]
        public void IsReady_ScheduledSessionsDoesNotMatchRequiredHours_ReturnsFalse()
        {
            Q.Professor = QProfessor.Object;
            AddSchedule();

            Assert.IsFalse(Q.IsReady());
        }

        [Test]
        public void IsReady_AllGood_ReturnsTrue()
        {
            AddRequiredHours();

            Q.Professor = QProfessor.Object;

            Assert.IsTrue(Q.IsReady());
        }

        [Test]
        public void Schedule_AddsToSchedule()
        {
            var booking = AddSchedule();

            CollectionAssert.Contains(Q.ScheduledSessions, booking.Object);
        }

        [Test]
        public void RemoveRoomBooking_Does()
        {
            var booking = AddSchedule();

            Q.RemoveRoomBooking(booking.Object);

            CollectionAssert.DoesNotContain(Q.ScheduledSessions, booking.Object);
        }

        [Test]
        public void GetCapacity_ReturnsLowestRoomCapacity()
        {
            var smallBooking = AddScheduleInRoomWithCapacity(10);
            var bigBooking = AddScheduleInRoomWithCapacity(50);

            Assert.AreEqual(10, Q.GetCapacity());
        }

        private void AddRequiredHours()
        {
            AddScheduleInRoomWithCapacity(2);
            AddScheduleInRoomWithCapacity(2);
        }

        private Mock<IRoomBooking> AddSchedule()
        {
            var schedule = new Mock<IRoomBooking>();
            Q.AddRoomBooking(schedule.Object);
            return schedule;
        }

        private Mock<IRoomBooking> AddScheduleInRoomWithCapacity(int capacity)
        {
            var room = new Mock<Room>();
            room.SetupGet(r => r.Capacity).Returns(capacity);
            var booking = new Mock<IRoomBooking>();
            booking.SetupGet(b => b.Room).Returns(room.Object);
            Q.AddRoomBooking(booking.Object);
            return booking;
        }
    }
}
