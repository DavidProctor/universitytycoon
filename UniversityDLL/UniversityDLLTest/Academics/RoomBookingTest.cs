﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;
using Moq;
using UniversityDLL;
using UniversityDLL.Academics;
using UniversityDLL.Rooms;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class RoomBookingTest
    {
        private RoomBooking Q;

        private Mock<Room> QRoom;
        private Mock<ICourseInstance> QCourse;
        private DayAndTime QDayAndTime = DayAndTime.FromDetails(DayOfWeek.Thursday, 14);

        [SetUp]
        public void BeforeEachTest()
        {
            QRoom = new Mock<Room>();
            QCourse = new Mock<ICourseInstance>();

            Q = new RoomBooking(QRoom.Object, QCourse.Object, QDayAndTime);
        }

        [Test]
        public void Ctor_SetsRoomCourseAndDayAndTime()
        {
            Assert.AreEqual(QRoom.Object, Q.Room);
            Assert.AreEqual(QCourse.Object, Q.Course);
            Assert.AreEqual(QDayAndTime, Q.DayAndTime);
        }

        [Test]
        public void Ctor_NotifiesCourse()
        {
            QCourse.Verify(c => c.AddRoomBooking(Q));
        }

        [Test]
        public void Destroy_NotifiesCourse()
        {
            Q.Destroy();

            QCourse.Verify(c => c.RemoveRoomBooking(Q));
        }
    }
}
