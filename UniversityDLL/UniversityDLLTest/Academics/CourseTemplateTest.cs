﻿using NUnit.Framework;
using Moq;
using UniversityDLL.Academics;

namespace UniversityDLLTest.Academics
{
    [TestFixture]
    class CourseTemplateTest
    {
        private CourseTemplate _courseTemplate;
        private Mock<IDepartment> _mockDepartment;

        private const string CourseName = "VCR Repair 101";
        private const int RequiredHours = 2;
        private readonly Year Year = Year.Second;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockDepartment = new Mock<IDepartment>();
            _courseTemplate = new CourseTemplate(CourseName, RequiredHours, Year, _mockDepartment.Object);
        }

        [Test]
        public void Ctor_SetsNameRequirementsYearDepartmentAndSemester()
        {
            Assert.AreEqual(CourseName, _courseTemplate.Name);
            Assert.AreEqual(RequiredHours, _courseTemplate.RequiredHours);
            Assert.AreEqual(Year, _courseTemplate.Year);
            Assert.AreEqual(_mockDepartment.Object, _courseTemplate.Department);
        }

        [Test]
        public void ToString_ReturnsCourseName()
        {
            Assert.AreEqual(CourseName, _courseTemplate.ToString());
        }
    }
}
