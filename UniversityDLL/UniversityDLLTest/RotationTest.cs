﻿using NUnit.Framework;
using UniversityDLL;

namespace UniversityDLLTest
{
    [TestFixture]
    public class RotationTest
    {
        private OrientationTracker _orientationTracker;

        [SetUp]
        public void BeforeEachTest()
        {
            _orientationTracker = new OrientationTracker();
        }

        [Test]
        public void Orientation_DefaultsToForward()
        {
            Assert.AreEqual(Orientation.Forward, _orientationTracker.Orientation);
        }

        [Test]
        public void BaseOrientation_DefaultsToForward()
        {
            Assert.AreEqual(Orientation.Forward, _orientationTracker.BaseOrientation);
        }

        [Test]
        public void NextRotation_Works()
        {
            Assert.AreEqual(Orientation.Right, _orientationTracker.NextRotation());
            Assert.AreEqual(Orientation.Reverse, _orientationTracker.NextRotation());

            _orientationTracker.NextRotation();
            Assert.AreEqual(Orientation.Left, _orientationTracker.Orientation);

            _orientationTracker.NextRotation();
            Assert.AreEqual(Orientation.Forward, _orientationTracker.Orientation);
        }

        [Test]
        public void SetOrientation_Works()
        {
            _orientationTracker.SetOrientation(Orientation.Left);

            Assert.AreEqual(Orientation.Left, _orientationTracker.Orientation);
        }

        [Test]
        public void SetOrientationFromParent_Works()
        {
            _orientationTracker.SetOrientationFromParent(Orientation.Reverse);
            Assert.AreEqual(Orientation.Reverse, _orientationTracker.Orientation);

            _orientationTracker = new OrientationTracker(Orientation.Left);

            _orientationTracker.SetOrientationFromParent(Orientation.Left);
            Assert.AreEqual(Orientation.Reverse, _orientationTracker.Orientation);

            _orientationTracker.SetOrientationFromParent(Orientation.Reverse);
            Assert.AreEqual(Orientation.Right, _orientationTracker.Orientation);
        }
    }
}