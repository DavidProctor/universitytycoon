﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ActivityPointTests
{
    [TestFixture]
    public class LecternActivityPointTest
    {

        private ActivityPoint _lectern;

        private Mock<BookableRoom> _mockRoom;
        private Mock<IRoomSchedule> _mockRoomSchedule;
        private Mock<IRoomBooking> _mockRoomBooking;
        private Mock<ICourseInstance> _mockCourseInstance;
        private Mock<ITile> _mockTile;
        private Mock<IProfessor> _mockProfessor;
        private List<ActivityPoint> _mockActivityPoints;
        private Mock<ITileContent> _mockTileContent;
        private Mock<ICalendar> _mockCalendar;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockRoom = new Mock<BookableRoom>();
            _mockRoomSchedule = new Mock<IRoomSchedule>();
            _mockCourseInstance = new Mock<ICourseInstance>();
            _mockTile = new Mock<ITile>();
            _mockProfessor = new Mock<IProfessor>();
            _mockActivityPoints = new List<ActivityPoint>();
            _mockRoomBooking = new Mock<IRoomBooking>();
            _mockCourseInstance = new Mock<ICourseInstance>();
            _mockCalendar = new Mock<ICalendar>();
            _mockTileContent = new Mock<ITileContent>();

            _mockTile.SetupGet(t => t.Room).Returns(_mockRoom.Object);
            _mockRoom.Setup(r => r.GetActivityPoints()).Returns(_mockActivityPoints);
            _mockRoom.Setup(r => r.GetSchedule(It.IsAny<Semester>())).Returns(_mockRoomSchedule.Object);
            _mockRoomSchedule.Setup(s => s.GetBooking(It.IsAny<DayAndTime>())).Returns(_mockRoomBooking.Object);
            _mockRoomBooking.SetupGet(b => b.Course).Returns(_mockCourseInstance.Object);
            _mockCalendar.SetupGet(c => c.CurrentTime).Returns(new TimeUnit(1988, Season.Fall, 2, DayOfWeek.Monday));
            _mockProfessor.SetupGet(p => p.TeachingSkill).Returns(5);

            _lectern = new LecternActivityPoint(_mockCalendar.Object, _mockTileContent.Object)
            {
                Tile = _mockTile.Object
            };
            _lectern.TrySetOccupant(_mockProfessor.Object);
            _mockActivityPoints.Add(_lectern);
        }

        [Test]
        public void ExecuteActivity_SmallClass_ProfDistributesAllPoints()
        {
            var students = AddStudents(24);

            _lectern.ExecuteActivity();

            AssertStudyCalledOnStudentsWithPoints(students, 5);
        }

        

        [Test]
        public void ExecuteActivity_MediumClass_ProfEffectivenessReduced()
        {
            var students = AddStudents(25);

            _lectern.ExecuteActivity();

            AssertStudyCalledOnStudentsWithPoints(students, 4);
        }

        [Test]
        public void ExecuteActivity_LargeClass_ProfEffectivenessIsAtLeast1()
        {
            var students = AddStudents(150);

            _lectern.ExecuteActivity();

            AssertStudyCalledOnStudentsWithPoints(students, 1);
        }

        [Test]
        public void ExecuteActivity_NoProf_DoesNothing()
        {
            _lectern.Vacate();
            var mockStudent = AddStudent();

            _lectern.ExecuteActivity();

            mockStudent.Verify(s => s.Study(It.IsAny<ICourseInstance>(), It.IsAny<int>()), Times.Never);
        }

        [Test]
        public void ExecuteActivity_InvalidRoom_DoesNothing()
        {
            _mockTile.SetupGet(t => t.Room).Returns(new Mock<Room>().Object);
            var mockStudent = AddStudent();

            _lectern.ExecuteActivity();

            mockStudent.Verify(s => s.Study(It.IsAny<ICourseInstance>(), It.IsAny<int>()), Times.Never);
        }

        [Test]
        public void ExecuteActivity_InvalidOccupant_DoesNothing()
        {
            var mockCharacter = new Mock<ICharacter>();
            _lectern.Vacate();
            _lectern.TrySetOccupant(mockCharacter.Object);
            var mockStudent = AddStudent();

            _lectern.ExecuteActivity();

            mockStudent.Verify(s => s.Study(It.IsAny<ICourseInstance>(), It.IsAny<int>()), Times.Never);
        }

        private Mock<IStudent>[] AddStudents(int count)
        {
            var students = new Mock<IStudent>[count];
            for( int i = 0; i < count; ++i )
            {
                students[i] = AddStudent();
            }
            return students;
        }

        private Mock<IStudent> AddStudent()
        {
            var fakeSeat = new Fakes.TestableClassSeatActivityPoint();
            _mockActivityPoints.Add(fakeSeat);

            var student = new Mock<IStudent>();
            fakeSeat.TrySetOccupant(student.Object);

            return student;
        }

        private void AssertStudyCalledOnStudentsWithPoints(Mock<IStudent>[] students, int points)
        {
            foreach( var student in students )
            {
                student.Verify(s => s.Study(_mockCourseInstance.Object, points));
            }
        }
    }
}