﻿using System;
using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ActivityPointTests
{
    [TestFixture]
    public class PubSeatActivityPointTest
    {
        private ActivityPoint _pubSeatActivityPoint;
        private Mock<ITile> _mockTile;
        private Mock<ICharacter> _mockCharacter;
        private Mock<ITileContent> _mockTileContent;
        private Mock<Room> _mockRoom;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockTile = new Mock<ITile>();
            _mockCharacter = new Mock<ICharacter>();
            _mockRoom = new Mock<Room>();
            _mockTileContent = new Mock<ITileContent>();
            _mockTile.SetupGet(t => t.Room).Returns(_mockRoom.Object);

            _pubSeatActivityPoint = new PubSeatActivityPoint(_mockTileContent.Object);
        }

        [Test]
        public void Ctor_IsNotOccupied()
        {
            Assert.IsFalse(_pubSeatActivityPoint.IsOccupied());
        }

        [Test]
        public void IsOccupied_ActuallyIs_Works()
        {
            _pubSeatActivityPoint.Tile = _mockTile.Object;
            _pubSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(_pubSeatActivityPoint.IsOccupied());
        }

        [Test]
        public void TrySetCharacter_NoTile_Fails()
        {
            var result = _pubSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
        }

        [Test]
        public void TrySetCharacter_Works()
        {
            _pubSeatActivityPoint.Tile = _mockTile.Object;
            var result = _pubSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(result);
            Assert.AreEqual(_mockCharacter.Object, _pubSeatActivityPoint.Occupant);
        }

        [Test]
        public void TrySetCharacter_Occupied_Fails()
        {
            _pubSeatActivityPoint.Tile = _mockTile.Object;
            var oldeCharacter = new Mock<ICharacter>();
            _pubSeatActivityPoint.TrySetOccupant(oldeCharacter.Object);

            var result = _pubSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
            Assert.AreEqual(oldeCharacter.Object, _pubSeatActivityPoint.Occupant);
        }

        [Test]
        public void ExecuteActivity_Throws()
        {
            Assert.Throws<NotImplementedException>(() => _pubSeatActivityPoint.ExecuteActivity());
        }

        [Test]
        public void GetNewInstance_Works()
        {
            var actual = _pubSeatActivityPoint.GetNewInstance();

            Assert.AreNotSame(actual, _pubSeatActivityPoint);
            Assert.IsTrue(actual is PubSeatActivityPoint);
        }
    }
}