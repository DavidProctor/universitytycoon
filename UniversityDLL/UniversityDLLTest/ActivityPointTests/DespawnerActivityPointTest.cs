﻿using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ActivityPointTests
{
    [TestFixture]
    public class DespawnerActivityPointTest
    {
        private ActivityPoint _despawner;

        private Mock<ITile> _mockTile;
        private Mock<ICharacter> _mockCharacter;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockTile = new Mock<ITile>();
            _mockCharacter = new Mock<ICharacter>();

            _despawner = new DespawnerActivityPoint();
        }

        [Test]
        public void IsOccupied_Works()
        {
            _despawner.Tile = _mockTile.Object;
            Assert.IsFalse(_despawner.IsOccupied());

            _despawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(_despawner.IsOccupied());
        }

        [Test]
        public void TryAddOccupant_NoTile_Fails()
        {
            var result = _despawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
        }

        [Test]
        public void TryAddOccupant_Works()
        {
            _despawner.Tile = _mockTile.Object;
            var result = _despawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(result);
            Assert.AreEqual(_mockCharacter.Object, _despawner.Occupant);
        }

        [Test]
        public void TryAddOccupant_Occupied_DoesNotWork()
        {
            _despawner.Tile = _mockTile.Object;
            var occupant = new Mock<ICharacter>();
            _despawner.TrySetOccupant(occupant.Object);

            var result = _despawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
            Assert.AreEqual(occupant.Object, _despawner.Occupant);
        }

        [Test]
        public void ExecuteActivity_NotOccupied_DoesNotThrow()
        {
            _despawner.ExecuteActivity();
        }

        [Test]
        public void ExecuteActivity_Occupied_DespawnsAndFreesOccupant()
        {
            _despawner.Tile = _mockTile.Object;
            _despawner.TrySetOccupant(_mockCharacter.Object);

            _despawner.ExecuteActivity();

            _mockCharacter.Verify(c => c.Despawn());
            Assert.IsFalse(_despawner.IsOccupied());
        }

        [Test]
        public void GetNewInstance_Works()
        {
            var actual = _despawner.GetNewInstance();

            Assert.AreNotSame(actual, _despawner);
            Assert.IsTrue(actual is DespawnerActivityPoint);
        }
    }
}