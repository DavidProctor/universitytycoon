﻿using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ActivityPointTests
{
    internal class TestableActivityPoint : ActivityPoint
    {
        public override void ExecuteActivity()
        {
        }

        public override ActivityPoint GetNewInstance()
        {
            return new TestableActivityPoint();
        }
    }

    [TestFixture]
    public class ActivityPointTests
    {
        private ActivityPoint _activityPoint;

        private Mock<Room> _mockRoom;
        private Mock<ITile> _mockTile;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockRoom = new Mock<Room>();
            _mockTile = new Mock<ITile>();
            _mockTile.SetupGet(t => t.Room).Returns(_mockRoom.Object);

            _activityPoint = new TestableActivityPoint
            {
                Tile = _mockTile.Object
            };
        }

        [Test]
        public void Vacate_RemovesOccupant()
        {
            var occupant = new Mock<ICharacter>();
            _activityPoint.TrySetOccupant(occupant.Object);

            _activityPoint.Vacate();

            Assert.IsFalse(_activityPoint.IsOccupied());
        }

        [Test]
        public void Vacate_IsEmpty_DoesNotThrow()
        {
            _activityPoint.Vacate();
        }

        [Test]
        public void Vacate_Occupied_NotifiesRoom()
        {
            var occupant = new Mock<ICharacter>();
            _activityPoint.TrySetOccupant(occupant.Object);

            _activityPoint.Vacate();
            
            _mockRoom.Verify(r => r.VacateActivityPoint(_activityPoint));
        }

        [Test]
        public void TrySetOccupant_Successful_NotifiesRoom()
        {
            var occupant = new Mock<ICharacter>();

            _activityPoint.TrySetOccupant(occupant.Object);

            _mockRoom.Verify(r => r.OccupyActivityPoint(_activityPoint));
        }
    }
}