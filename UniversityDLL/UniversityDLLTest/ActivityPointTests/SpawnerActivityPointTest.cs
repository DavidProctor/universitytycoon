﻿using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ActivityPointTests
{
    [TestFixture]
    public class SpawnerActivityPointTest
    {
        private ActivityPoint _spawner;

        private Mock<ITile> _mockTile;
        private Mock<ICharacter> _mockCharacter;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockTile = new Mock<ITile>();
            _mockCharacter = new Mock<ICharacter>();

            _spawner = new SpawnerActivityPoint();
        }

        [Test]
        public void IsOccupied_Works()
        {
            _spawner.Tile = _mockTile.Object;
            Assert.IsFalse(_spawner.IsOccupied());

            _spawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(_spawner.IsOccupied());
        }

        [Test]
        public void TryAddOccupant_NoTile_Fails()
        {
            var result = _spawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
        }

        [Test]
        public void TryAddOccupant_Works()
        {
            _spawner.Tile = _mockTile.Object;
            var result = _spawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(result);
            Assert.AreEqual(_mockCharacter.Object, _spawner.Occupant);
        }

        [Test]
        public void TryAddOccupant_Occupied_DoesNotWork()
        {
            _spawner.Tile = _mockTile.Object;
            var occupant = new Mock<ICharacter>();
            _spawner.TrySetOccupant(occupant.Object);

            var result = _spawner.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
            Assert.AreEqual(occupant.Object, _spawner.Occupant);
        }

        [Test]
        public void ExecuteActivity_NotOccupied_DoesNotThrow()
        {
            _spawner.ExecuteActivity();
        }

        [Test]
        public void ExecuteActivity_Occupied_SpawnsOccupantAndSetsUnoccupied()
        {
            _spawner.Tile = _mockTile.Object;
            _spawner.TrySetOccupant(_mockCharacter.Object);

            _spawner.ExecuteActivity();
            
            _mockCharacter.Verify(c => c.Spawn(_mockTile.Object));
            Assert.IsFalse(_spawner.IsOccupied());
        }

        [Test]
        public void GetNewInstance_Works()
        {
            var actual = _spawner.GetNewInstance();

            Assert.AreNotSame(actual, _spawner);
            Assert.IsTrue(actual is SpawnerActivityPoint);
        }
    }
}