﻿using System;
using Moq;
using NUnit.Framework;
using UniversityDLL.ActivityPoints;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Tiles;

namespace UniversityDLLTest.ActivityPointTests
{
    [TestFixture]
    public class ClassSeatActivityPointTest
    {
        private ActivityPoint _classSeatActivityPoint;
        private Mock<ITile> _mockTile;
        private Mock<ICharacter> _mockCharacter;
        private Mock<ITileContent> _mockTileContent;
        private Mock<Room> _mockRoom;

        [SetUp]
        public void BeforeEachTest()
        {
            _mockTile = new Mock<ITile>();
            _mockCharacter = new Mock<ICharacter>();
            _mockRoom = new Mock<Room>();
            _mockTile.SetupGet(t => t.Room).Returns(_mockRoom.Object);
            _mockTileContent = new Mock<ITileContent>();

            _classSeatActivityPoint = new ClassSeatActivityPoint(_mockTileContent.Object);
        }

        [Test]
        public void Ctor_IsNotOccupied()
        {
            Assert.IsFalse(_classSeatActivityPoint.IsOccupied());
        }

        [Test]
        public void IsOccupied_ActuallyIs_Works()
        {
            _classSeatActivityPoint.Tile = _mockTile.Object;
            _classSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(_classSeatActivityPoint.IsOccupied());
        }

        [Test]
        public void TrySetCharacter_NoTile_Fails()
        {
            var result = _classSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
        }

        [Test]
        public void TrySetCharacter_Works()
        {
            _classSeatActivityPoint.Tile = _mockTile.Object;
            var result = _classSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsTrue(result);
            Assert.AreEqual(_mockCharacter.Object, _classSeatActivityPoint.Occupant);
        }

        [Test]
        public void TrySetCharacter_Occupied_Fails()
        {
            _classSeatActivityPoint.Tile = _mockTile.Object;
            var oldeCharacter = new Mock<ICharacter>();
            _classSeatActivityPoint.TrySetOccupant(oldeCharacter.Object);

            var result = _classSeatActivityPoint.TrySetOccupant(_mockCharacter.Object);

            Assert.IsFalse(result);
            Assert.AreEqual(oldeCharacter.Object, _classSeatActivityPoint.Occupant);
        }

        [Test]
        public void ExecuteActivity_Throws()
        {
            Assert.Throws<NotImplementedException>(() => _classSeatActivityPoint.ExecuteActivity());
        }

        [Test]
        public void GetNewInstance_Works()
        {
            var actual = _classSeatActivityPoint.GetNewInstance();

            Assert.AreNotSame(actual, _classSeatActivityPoint);
            Assert.IsTrue(actual is ClassSeatActivityPoint);
        }
    }
}