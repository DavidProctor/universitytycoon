﻿using System;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.CalendarTests
{
    [TestFixture]
    class TimeUnitTest
    {
        private readonly Semester _semester = Semester.GetSemester(2006, Season.Fall);
        private DayOfWeek _day = DayOfWeek.Friday;

        [Test]
        public void Tick_AdvancesMinute()
        {
            var time = new TimeUnit(_semester, 1, _day, 1, QuarterHour.Zero);

            var actual = time.Tick();

            var expected = new TimeUnit(_semester, 1, _day, 1, QuarterHour.Fifteen);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Tick_AdvancesHour()
        {
            var time = new TimeUnit(_semester, 1, _day, 1, QuarterHour.Fortyfive);

            var actual = time.Tick();

            var expected = new TimeUnit(_semester, 1, _day, 2, QuarterHour.Zero);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Tick_AdvancesDay()
        {
            var time = new TimeUnit(_semester, 1, _day, 23, QuarterHour.Fortyfive);

            var actual = time.Tick();

            var expected = new TimeUnit(_semester, 1, DayOfWeek.Saturday, 0, QuarterHour.Zero);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Tick_AdvancesWeek()
        {
            var time = new TimeUnit(_semester, 1, DayOfWeek.Saturday, 23, QuarterHour.Fortyfive);

            var actual = time.Tick();

            var expected = new TimeUnit(_semester, 2, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Tick_AdvancesSemester()
        {
            var time = new TimeUnit(_semester, 6, DayOfWeek.Saturday, 23, QuarterHour.Fortyfive);

            var actual = time.Tick();

            var expected = new TimeUnit(Semester.GetSemester(2007, Season.Winter), 1, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AddHours_Works()
        {
            var time = new TimeUnit(_semester, 1, _day, 1, QuarterHour.Thirty);

            var actual = time.AddHours(2);

            var expected = new TimeUnit(_semester, 1, _day, 3, QuarterHour.Thirty);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AddDays_Works()
        {
            var time = new TimeUnit(_semester, 1, DayOfWeek.Thursday, 12, QuarterHour.Fifteen);

            var actual = time.AddDays(3);

            var expected = new TimeUnit(_semester, 2, DayOfWeek.Sunday, 12, QuarterHour.Fifteen);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AddSemesters_Works()
        {
            var time = new TimeUnit(1901, Season.Fall, 1, DayOfWeek.Thursday);

            var actual = time.AddSemesters(5);

            var expected = new TimeUnit(1903, Season.Summer, 1, DayOfWeek.Thursday);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DateEquals_Works()
        {
            var time = new TimeUnit(_semester, 1, _day, 0, QuarterHour.Zero);
            var equals = new TimeUnit(_semester, 1, _day, 23, QuarterHour.Thirty);
            var notEquals = new TimeUnit(_semester, 1, DayOfWeek.Saturday, 1, QuarterHour.Zero);

            Assert.IsTrue(time.DateEquals(equals));
            Assert.IsFalse(time.DateEquals(notEquals));
        }

        [Test]
        public void QuarterHour_Works()
        {
            var time1 = new TimeUnit(3);
            var time2 = new TimeUnit(4 * 1000 + 3);

            Assert.AreEqual(QuarterHour.Fortyfive, time1.QuarterHour);
            Assert.AreEqual(QuarterHour.Fortyfive, time2.QuarterHour);
        }

        [Test]
        public void Hour_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Fortyfive);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 1, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Winter), 2, DayOfWeek.Monday, 19, QuarterHour.Thirty);

            Assert.AreEqual(0, time1.Hour);
            Assert.AreEqual(1, time2.Hour);
            Assert.AreEqual(19, time3.Hour);
        }

        [Test]
        public void Day_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Thursday, 11, QuarterHour.Fortyfive);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual(DayOfWeek.Sunday, time1.Day);
            Assert.AreEqual(DayOfWeek.Thursday, time2.Day);
            Assert.AreEqual(DayOfWeek.Tuesday, time3.Day);
        }

        [Test]
        public void DayAndTime_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Thursday, 11, QuarterHour.Fortyfive);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual(DayAndTime.FromDetails(DayOfWeek.Sunday, 0), time1.DayAndTime);
            Assert.AreEqual(DayAndTime.FromDetails(DayOfWeek.Thursday, 11), time2.DayAndTime);
            Assert.AreEqual(DayAndTime.FromDetails(DayOfWeek.Tuesday, 23), time3.DayAndTime);
        }

        [Test]
        public void Week_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 3, DayOfWeek.Thursday, 11, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual(0, time1.Week);
            Assert.AreEqual(3, time2.Week);
            Assert.AreEqual(5, time3.Week);
        }

        [Test]
        public void Semester_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Fall), 3, DayOfWeek.Thursday, 11, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual(Semester.GetSemester(0, Season.Winter), time1.Semester);
            Assert.AreEqual(Semester.GetSemester(0, Season.Fall), time2.Semester);
            Assert.AreEqual(Semester.GetSemester(2016, Season.Summer), time3.Semester);
        }

        [Test]
        public void ToDateString_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Fall), 3, DayOfWeek.Thursday, 11, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual("Sunday, week 0, Winter 0", time1.ToDateString());
            Assert.AreEqual("Thursday, week 3, Fall 0", time2.ToDateString());
            Assert.AreEqual("Tuesday, week 5, Summer 2016", time3.ToDateString());
        }

        [Test]
        public void ToTimeString_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Fall), 3, DayOfWeek.Thursday, 11, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual("12:00 AM", time1.ToTimeString());
            Assert.AreEqual("11:00 AM", time2.ToTimeString());
            Assert.AreEqual("11:15 PM", time3.ToTimeString());
        }

        [Test]
        public void ToString_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Sunday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Fall), 3, DayOfWeek.Thursday, 11, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(2016, Season.Summer), 5, DayOfWeek.Tuesday, 23, QuarterHour.Fifteen);

            Assert.AreEqual("Sunday, week 0, Winter 0 12:00 AM", time1.ToString());
            Assert.AreEqual("Thursday, week 3, Fall 0 11:00 AM", time2.ToString());
            Assert.AreEqual("Tuesday, week 5, Summer 2016 11:15 PM", time3.ToString());
        }

        [Test]
        public void OperatorGt_IsGreaterThan_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);

            Assert.IsTrue(time1 > time2);
        }

        [Test]
        public void OperatorGt_IsNotGreaterThan_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);

            Assert.IsFalse(time1 > time2);
        }

        [Test]
        public void OperatorGt_AreEqual_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);

            Assert.IsFalse(time1 > time2);
        }

        [Test]
        public void OperatorLt_IsLessThan_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);

            Assert.IsTrue(time1 < time2);
        }

        [Test]
        public void OperatorLt_IsNotLessThan_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);

            Assert.IsFalse(time1 < time2);
        }

        [Test]
        public void OperatorLt_AreEqual_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);

            Assert.IsFalse(time1 < time2);
        }

        [Test]
        public void OperatorGte_IsTrue_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);
            var time3 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);

            Assert.IsTrue(time1 >= time2);
            Assert.IsTrue(time1 >= time3);
        }

        [Test]
        public void OperatorGte_IsFalse_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);

            Assert.IsFalse(time1 >= time2);
        }

        [Test]
        public void OperatorLte_IsTrue_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);
            var time3 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);

            Assert.IsTrue(time1 <= time2);
            Assert.IsTrue(time1 <= time3);
        }

        [Test]
        public void OperatorLte_IsFalse_Works()
        {
            var time1 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Fifteen);
            var time2 = new TimeUnit(Semester.GetSemester(0, Season.Winter), 0, DayOfWeek.Friday, 0, QuarterHour.Zero);

            Assert.IsFalse(time1 <= time2);
        }
    }
}
