﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.People;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.CalendarTests
{
    [TestFixture]
    class AdmissionsCalendarEventTest
    {
        private AdmissionsCalendarEvent _admissionsCalendarEvent;
        private Mock<IGameState> _gameController;
        private Mock<IObjectFactoryFacade> _objectFactory;
        private MockCoursePair _course1;
        private MockCoursePair _course2;
        private MockCoursePair _course3;
        private MockCoursePair _course4;
        private Mock<IDepartment> _department;

        private readonly TimeUnit _date = new TimeUnit(1887, Season.Winter, 4, DayOfWeek.Wednesday);

        [SetUp]
        public void BeforeEachTest()
        {
            _gameController = new Mock<IGameState>();
            _objectFactory = new Mock<IObjectFactoryFacade>();
            _department = new Mock<IDepartment>();

            _course1 = DataFactory.MakeMockCoursePair(50, Year.First, true, _department.Object);
            _course2 = DataFactory.MakeMockCoursePair(50, Year.First, true, _department.Object);
            _course3 = DataFactory.MakeMockCoursePair(50, Year.Second, true, _department.Object);
            _course4 = DataFactory.MakeMockCoursePair(50, Year.First, false, _department.Object);

            _department.SetupGet(v => v.AdmissionsCapacity).Returns(66);
            _department.Setup(v => v.GetScheduledCourses(It.IsAny<Semester>())).Returns(new List<ICourseInstance> {
                _course1.Instance.Object,
                _course2.Instance.Object,
                _course3.Instance.Object,
                _course4.Instance.Object,
            });

            var departments = new List<IDepartment>{ _department.Object };
            _gameController.Setup(g => g.Departments).Returns(departments);

            _admissionsCalendarEvent = new AdmissionsCalendarEvent(_date, _gameController.Object, _objectFactory.Object);
        }

        [Test]
        public void AutoReschedule_AddsOneSemester()
        {
            var expectedDate = new TimeUnit(1887, Season.Summer, 4, DayOfWeek.Wednesday);
            Assert.AreEqual(expectedDate, _admissionsCalendarEvent.GetRescheduleDate());
        }

        [Test]
        public void Ctor_SetsDate()
        {
            Assert.AreEqual(_date, _admissionsCalendarEvent.Date);
        }

        [Test]
        [Ignore("Full attendance until there's time for polishing")]
        public void Execute_AddsStudentsForAdmissionsCapacity()
        {
            _admissionsCalendarEvent.Execute();

            _gameController.Verify(g => g.EnrolStudent(It.IsAny<IStudent>()), Times.Exactly(66));
        }

        [Test]
        public void Execute_ObtainsStudentsFromFactory()
        {
            _admissionsCalendarEvent.Execute();

            _objectFactory.Verify(g => g.MakeStudent(), Times.AtLeastOnce);
        }
    }
}
