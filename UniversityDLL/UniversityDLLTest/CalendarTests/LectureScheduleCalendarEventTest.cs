﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.GameMangement.Factories;
using UniversityDLL.People;
using UniversityDLL.Rooms;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.CalendarTests
{
    [TestFixture]
    public class LectureScheduleCalendarEventTest
    {
        private ICalendarEvent _lectureScheduleEvent;

        private Mock<IGameState> _gameController;
        private Mock<IDepartment> _department1;
        private Mock<ICourseTemplate> _courseTemplate1;
        private Mock<IDepartment> _department2;
        private Mock<ICourseTemplate> _courseTemplate2;
        private Mock<BookableRoom> _bigLectureHall;
        private RoomSchedule _bigLectureHallSchedule;
        private Mock<BookableRoom> _smallLectureHall;
        private RoomSchedule _smalLectureHallSchedule;
        private Mock<ICharacterFactory> _mockCharacterFactory;
        private Mock<IProfessor> _mockProfessor;

        private readonly TimeUnit _time = new TimeUnit(2006, Season.Fall, 1, DayOfWeek.Sunday);
        private readonly Semester _semester = Semester.GetSemester(2006, Season.Fall);

        [SetUp]
        public void BeforeEachTest()
        {
            _department1 = MakeMockDepartment();
            _courseTemplate1 = MakeMockCourseAndAddToDepartment(3, _department1);

            _department2 = MakeMockDepartment();
            _courseTemplate2 = MakeMockCourseAndAddToDepartment(3, _department2);

            _bigLectureHall = new Mock<BookableRoom>();
            _bigLectureHall.SetupGet(h => h.Capacity).Returns(110);
            _bigLectureHallSchedule = new RoomSchedule(_bigLectureHall.Object, _semester);
            _bigLectureHall.Setup(h => h.GetSchedule(_semester)).Returns(_bigLectureHallSchedule);

            _smallLectureHall = new Mock<BookableRoom>();
            _smallLectureHall.SetupGet(h => h.Capacity).Returns(35);
            _smalLectureHallSchedule = new RoomSchedule(_smallLectureHall.Object, _semester);
            _smallLectureHall.Setup(h => h.GetSchedule(_semester)).Returns(_smalLectureHallSchedule);

            _gameController = new Mock<IGameState>();
            _gameController.SetupGet(g => g.Departments).Returns(new List<IDepartment> {_department1.Object, _department2.Object});
            _gameController.Setup(g => g.GetLectureHalls()).Returns(new List<BookableRoom> {_bigLectureHall.Object, _smallLectureHall.Object});

            _mockProfessor = new Mock<IProfessor>();
            _mockCharacterFactory = new Mock<ICharacterFactory>();
            _mockCharacterFactory.Setup(f => f.MakeProfessor()).Returns(_mockProfessor.Object);

            _lectureScheduleEvent = new LectureScheduleCalendarEvent(_time, _gameController.Object, _mockCharacterFactory.Object);
        }

        [Test]
        public void GetRescheduleDate_OneSemesterLater()
        {
            var actual = _lectureScheduleEvent.GetRescheduleDate();

            var expected = new TimeUnit(2007, Season.Winter, 1, DayOfWeek.Sunday);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Execute_SchedulesClassesForAllDepartments()
        {
            _lectureScheduleEvent.Execute();

            _department1.Verify(d => d.AddScheduledCourse(It.IsAny<ICourseInstance>()));
            _bigLectureHall.Verify(h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), _semester));
        }

        [Test]
        public void Execute_AllScheduledCoursesAreComplete()
        {
            var courseToSessionCountMap = RecordCourseSessionCount();

            _lectureScheduleEvent.Execute();

            foreach (var course in courseToSessionCountMap.Keys)
            {
                Assert.AreEqual(course.Template.RequiredHours, courseToSessionCountMap[course]);
            }
        }

        [Test]
        public void Execute_AllBookingsAreAtUniqueTimes()
        {
            var courseTimes = RecordCourseTimesForRoom(_bigLectureHall);

            _lectureScheduleEvent.Execute();

            CollectionAssert.AllItemsAreUnique(courseTimes);
        }

        [Test]
        public void Execute_DoesNotBookOnWeekends()
        {
            _courseTemplate1.SetupGet(t => t.RequiredHours).Returns(45);
            var courseTimes = RecordCourseTimesForRoom(_bigLectureHall);

            _lectureScheduleEvent.Execute();

            Assert.False(courseTimes.Any(t => t.Day == DayOfWeek.Sunday || t.Day == DayOfWeek.Saturday));
        }

        [Test]
        public void Execute_HiresProfessor()
        {
            _lectureScheduleEvent.Execute();

            _mockCharacterFactory.Verify(f => f.MakeProfessor(), Times.Exactly(2));
        }

        [Test]
        public void Execute_RoomScheduleOverflow_TriesNextRoom()
        {
            _courseTemplate1.SetupGet(t => t.RequiredHours).Returns(40);
            _courseTemplate2.SetupGet(t => t.RequiredHours).Returns(3);
            RecordCourseTimesForRoom(_bigLectureHall);
            
            _lectureScheduleEvent.Execute();

            _smallLectureHall.Verify(h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), It.IsAny<Semester>()));
        }

        [Test]
        public void Execute_ManyCoursesAndDepartments_CyclesThroughDepartments()
        {
            var secondCourse = MakeMockCourseAndAddToDepartment(1, _department1);
            _courseTemplate1.SetupGet(c => c.RequiredHours).Returns(1);
            _courseTemplate2.SetupGet(c => c.RequiredHours).Returns(1);
            var coursesScheduledInOrder = new List<ICourseTemplate>();
            _bigLectureHall.Setup(
                h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), It.IsAny<Semester>()))
                .Callback<DayAndTime, ICourseInstance, Semester>((d, c, s) =>
                {
                    coursesScheduledInOrder.Add(c.Template);
                });

            _lectureScheduleEvent.Execute();

            Assert.AreEqual(_courseTemplate1.Object, coursesScheduledInOrder[0]);
            Assert.AreEqual(_courseTemplate2.Object, coursesScheduledInOrder[1]);
            Assert.AreEqual(secondCourse.Object, coursesScheduledInOrder[2]);
        }

        [Test]
        public void Execute_CoursesExceedSpace_DestroysBookingAndReturns()
        {
            RecordCourseTimesForRoom(_bigLectureHall);
            RecordCourseTimesForRoom(_smallLectureHall);
            _courseTemplate1.SetupGet(c => c.RequiredHours).Returns(150);
            _courseTemplate2.SetupGet(c => c.RequiredHours).Returns(20);

            _lectureScheduleEvent.Execute();

            _smallLectureHall.Verify(h => h.AddBooking(It.IsAny<DayAndTime>(),It.IsAny<ICourseInstance>(), It.IsAny<Semester>()), Times.Never());
        }

        [Test]
        public void Execute_AllSessionsWithinCourseHaveDistinctTimes()
        {
            _courseTemplate1.SetupGet(c => c.RequiredHours).Returns(81);
            _courseTemplate2.SetupGet(c => c.RequiredHours).Returns(81);
            RecordCourseTimesForRoom(_bigLectureHall);
            RecordCourseTimesForRoom(_smallLectureHall);

            _lectureScheduleEvent.Execute();

            _smallLectureHall.Verify(h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), It.IsAny<Semester>()), Times.Never());
            _bigLectureHall.Verify(h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), It.IsAny<Semester>()), Times.Never());
        }

        [Test]
        public void Execute_SchedulesFirstYearClassesInLargestHalls()
        {
            _lectureScheduleEvent.Execute();

            _department1.Verify(d => d.AddScheduledCourse(It.Is<ICourseInstance>(i => i.Template.Year == Year.First 
                && i.ScheduledSessions.TrueForAll(s => s.Room == _bigLectureHall.Object))));
        }

        private Mock<IDepartment> MakeMockDepartment()
        {
            var department = new Mock<IDepartment>();

            var scheduledCourses = new List<ICourseInstance>();
            department.Setup(d => d.AddScheduledCourse(It.IsAny<ICourseInstance>()))
                .Callback<ICourseInstance>(c => scheduledCourses.Add(c));

            var courses = new List<ICourseTemplate>();
            department.Setup(d => d.AddCourse(It.IsAny<ICourseTemplate>()))
                .Callback <ICourseTemplate>(c => courses.Add(c));
            department.Setup(d => d.GetCourses()).Returns(courses);

            department.Setup(d => d.GetScheduledCourses(It.IsAny<Semester>())).Returns(scheduledCourses);
            return department;
        }

        private Mock<ICourseTemplate> MakeMockCourseAndAddToDepartment(int credits, Mock<IDepartment> department)
        {
            var courseTemplate = new Mock<ICourseTemplate>();
            courseTemplate.SetupGet(c => c.RequiredHours).Returns(credits);
            courseTemplate.SetupGet(c => c.Department).Returns(_department1.Object);
            department.Object.AddCourse(courseTemplate.Object);
            return courseTemplate;
        }

        private Dictionary<ICourseInstance, int> RecordCourseSessionCount()
        {
            var courseToSessionCountMap = new Dictionary<ICourseInstance, int>();
            _bigLectureHall.Setup(h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), It.IsAny<Semester>()))
                .Callback<DayAndTime, ICourseInstance, Semester>((d, c, s) =>
                {
                    if( !courseToSessionCountMap.ContainsKey(c) ) courseToSessionCountMap.Add(c, 0);
                    courseToSessionCountMap[c]++;
                    _bigLectureHall.Object.GetSchedule(_semester).Schedule(d, c);
                });
            return courseToSessionCountMap;
        }

        private List<DayAndTime> RecordCourseTimesForRoom(Mock<BookableRoom> room)
        {
            var courseTimes = new List<DayAndTime>();
            room.Setup(
                h => h.AddBooking(It.IsAny<DayAndTime>(), It.IsAny<ICourseInstance>(), It.IsAny<Semester>()))
                .Callback<DayAndTime, ICourseInstance, Semester>((d, c, s) =>
                {
                    courseTimes.Add(d);
                    _bigLectureHall.Object.GetSchedule(_semester).Schedule(d, c);
                });
            return courseTimes;
        }
    }
}