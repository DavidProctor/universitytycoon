﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.People;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.CalendarTests
{
    [TestFixture]
    class CourseSelectionCalendarEventTest
    {
        private CourseSelectionCalendarEvent _courseSelectionCalendarEvent;
        private List<IStudent> _students;
        private Mock<IGameState> _mockGameState;
        private CompleteMockDepartment _mockMajor;
        private CompleteMockDepartment _mockElective;
        private Mock<IStudent> _mockStudent;

        [SetUp]
        public void BeforeEachTest()
        {
            _students = new List<IStudent>();

            _mockGameState = new Mock<IGameState>();
            _mockGameState.SetupGet(g => g.Students).Returns(_students);
            var calendar = new Mock<ICalendar>();
            calendar.SetupGet(c => c.CurrentTime).Returns(new TimeUnit(1996, Season.Summer, 2, DayOfWeek.Tuesday));
            _mockGameState.SetupGet(g => g.Calendar).Returns(calendar.Object);

            _mockMajor = DataFactory.MakeCompleteMockDepartment(1, 10);
            _mockElective = DataFactory.MakeCompleteMockDepartment(10, 10);
            _mockGameState.SetupGet(g => g.Departments).Returns(new List<IDepartment>() {
                _mockMajor.Department.Object, _mockElective.Department.Object });

            _mockStudent = MakeStudent();
            _mockStudent.SetupGet(s => s.Major).Returns(_mockMajor.Department.Object);

            var date = new TimeUnit(1993, Season.Summer, 3, DayOfWeek.Monday);
            _courseSelectionCalendarEvent = new CourseSelectionCalendarEvent(date, _mockGameState.Object);
        }

        [Test]
        public void AutoReschedule_EverySemester()
        {
            var expected = new TimeUnit(1993, Season.Fall, 3, DayOfWeek.Monday);
            Assert.AreEqual(expected, _courseSelectionCalendarEvent.GetRescheduleDate());
        }

        [Test]
        public void Execute_RunsStudentsInYearThenGPAOrder()
        {
            var fourthYearFuckup = MakeStudent(Year.Fourth, 2.2f);
            var firstYearGenius = MakeStudent(Year.First, 4f);
            var firstYearFuckup = MakeStudent(Year.First, 2.2f);
            var studentsProcessed = new List<Mock<IStudent>>();
            int sequence = 0;
            fourthYearFuckup.Setup(s => s.AddEnrolment(It.IsAny<ICourseEnrolment>())).Callback(() =>
            {
                if( !studentsProcessed.Contains(fourthYearFuckup) )
                {
                    Assert.AreEqual(0, sequence++);
                    studentsProcessed.Add(fourthYearFuckup);
                }
            });
            firstYearGenius.Setup(s => s.AddEnrolment(It.IsAny<ICourseEnrolment>())).Callback(() =>
            {
                if( !studentsProcessed.Contains(firstYearGenius) )
                {
                    Assert.AreEqual(1, sequence++);
                    studentsProcessed.Add(firstYearGenius);
                }
            });
            firstYearFuckup.Setup(s => s.AddEnrolment(It.IsAny<ICourseEnrolment>())).Callback(() =>
            {
                if( !studentsProcessed.Contains(firstYearFuckup) )
                {
                    Assert.AreEqual(2, sequence++);
                    studentsProcessed.Add(firstYearFuckup);
                }
            });

            _courseSelectionCalendarEvent.Execute();

            Assert.AreEqual(3, sequence);
        }

        [Test]
        public void Execute_AddsUpTo9Credits()
        {
            var student = MakeStudent();
            var credits = 0;
            student.Setup(s => s.AddEnrolment(It.IsAny<ICourseEnrolment>()))
                .Callback<ICourseEnrolment>(e => credits += e.Course.Template.RequiredHours);
            _students.Clear();
            _students.Add(student.Object);

            _courseSelectionCalendarEvent.Execute();

            Assert.AreEqual(9, credits);
        }

        [Test]
        public void Execute_PrioritizesMajor()
        {
            _courseSelectionCalendarEvent.Execute();

            _mockStudent.Verify(s => s.AddEnrolment(It.Is<ICourseEnrolment>(e => e.Course.Template.Department == _mockMajor.Department.Object)), Times.Exactly(3));
            _mockStudent.Verify(s => s.AddEnrolment(It.Is<ICourseEnrolment>(e => e.Course.Template.Department == _mockElective.Department.Object)), Times.Never());
        }

        [Test]
        public void Execute_AddsSingleCourseExactlyOnce()
        {
            var student = MakeStudent();
            var department = DataFactory.MakeCompleteMockDepartment(1, 1);
            student.SetupGet(s => s.Major).Returns(department.Department.Object);
            _mockGameState.SetupGet(g => g.Departments).Returns(new List<IDepartment>() { department.Department.Object });

            _courseSelectionCalendarEvent.Execute();
            _courseSelectionCalendarEvent.Execute();
            _courseSelectionCalendarEvent.Execute();

            var enrolledCourses = student.Object.Enrolments.Select(e => e.Course.Template);
            CollectionAssert.AllItemsAreUnique(enrolledCourses);
        }

        [Test]
        public void Execute_PrioritizesLowerYears()
        {
            _courseSelectionCalendarEvent.Execute();
            _courseSelectionCalendarEvent.Execute();
            _courseSelectionCalendarEvent.Execute();

            var enrolledCourses = _mockStudent.Object.Enrolments.Select(e => e.Course.Template);
            Assert.AreEqual(6, enrolledCourses.Count(c => c.Year == Year.First));
            Assert.AreEqual(3, enrolledCourses.Count(c => c.Year == Year.Second));
        }

        [Test]
        public void Execute_FillsExtraWithElectives()
        {
            _courseSelectionCalendarEvent.Execute();
            _courseSelectionCalendarEvent.Execute();

            var enrolledCourses = _mockStudent.Object.Enrolments.Select(e => e.Course.Template);
            Assert.AreEqual(4, enrolledCourses.Count(c => c.Department == _mockMajor.Department.Object
                && c.Year == Year.First));
            Assert.AreEqual(2, enrolledCourses.Count(c => c.Department != _mockMajor.Department.Object));
        }

        private Mock<IStudent> MakeStudent(Year year = Year.Second, float GPA = 3f)
        {
            var student = new Mock<IStudent>();
            student.SetupGet(s => s.Year).Returns(year);
            student.SetupGet(s => s.GPA).Returns(GPA);
            student.SetupGet(s => s.Major).Returns(_mockMajor.Department.Object);

            var enrolments = new List<ICourseEnrolment>();
            student.SetupGet(s => s.Enrolments).Returns(enrolments);
            student.Setup(s => s.AddEnrolment(It.IsAny<ICourseEnrolment>())).Callback<ICourseEnrolment>(e => enrolments.Add(e));

            _students.Add(student.Object);
            return student;
        }
    }
}
