﻿using System;
using System.Linq;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.CalendarTests
{
    [TestFixture]
    class CalendarTest
    {
        private Calendar _calendar;

        private Mock<ICalendarEvent> _mockCalendarEvent;

        private TimeUnit _testDate;
        private TimeUnit _beforeMidnight;
        private TimeUnit _oneDayLater;
        private TimeUnit _tenDaysLater;
        
        [SetUp]
        public void BeforeEachTest()
        {
            _testDate = new TimeUnit(Semester.GetSemester(1988, Season.Fall), 3, DayOfWeek.Monday);
            _beforeMidnight = _testDate.AddHours(23).Tick().Tick().Tick();
            _oneDayLater = _testDate.AddDays(1);
            _tenDaysLater = _testDate.AddDays(10);

            _mockCalendarEvent = new Mock<ICalendarEvent>();
            _mockCalendarEvent.SetupProperty(e => e.Date);

            _calendar = new Calendar();
        }

        [Test]
        public void Ctor_SetsDefaultStartDate()
        {
            Assert.AreEqual(new TimeUnit(Semester.GetSemester(2006, Season.Fall), 1, DayOfWeek.Monday, 12, QuarterHour.Zero), _calendar.CurrentTime);
        }

        [Test]
        public void SetAndGetDate_Works()
        {
            _calendar.CurrentTime = _testDate;

            Assert.AreEqual(_testDate, _calendar.CurrentTime);
        }

        [Test]
        public void Ctor_SetsDefaultSecondsPerHourToPaused()
        {
            Assert.AreEqual(-1f, _calendar.SecondsPerTick, 0.01);
        }

        [Test]
        public void Tick_AdvancesOneHour()
        {
            _calendar.CurrentTime = _testDate;

            _calendar.Tick();

            var expectedDate = new TimeUnit(_testDate.Ticks + 1);
            Assert.AreEqual(expectedDate, _calendar.CurrentTime);
        }

        [Test]
        public void Tick_ExecutesEvents()
        {
            SetupEventForNextDate();
            _calendar.CurrentTime = new TimeUnit(Semester.GetSemester(1988, Season.Fall), 3, DayOfWeek.Monday, 23, QuarterHour.Fortyfive);

            _calendar.Tick();

            _mockCalendarEvent.Verify(e => e.Execute());
        }

        [Test]
        public void AddEvent_AddsEvent()
        {
            _calendar.AddEvent(_mockCalendarEvent.Object);

            CollectionAssert.Contains(_calendar.FutureEvents, _mockCalendarEvent.Object);
        }

        [Test]
        public void MultipleEventsOnSingleDay_AllAreFired()
        {
            SetupEventForNextDate();
            var secondEvent = new Mock<ICalendarEvent>();
            secondEvent.Setup(e => e.Date).Returns(_oneDayLater);
            _calendar.AddEvent(secondEvent.Object);

            _calendar.Tick();

            _mockCalendarEvent.Verify(e => e.Execute());
            secondEvent.Verify(e => e.Execute());
        }

        [Test]
        public void Execute_AutoReschedules()
        {
            SetupEventForNextDate();
            _mockCalendarEvent.Setup(e => e.GetRescheduleDate()).Returns(_tenDaysLater);

            _calendar.Tick();

            var nextEvent = _calendar.FutureEvents.First();
            Assert.AreEqual(_tenDaysLater, nextEvent.Date);
        }

        private void SetupEventForNextDate()
        {
            _calendar.CurrentTime = _beforeMidnight;
            _mockCalendarEvent.Object.Date = _oneDayLater;
            _calendar.AddEvent(_mockCalendarEvent.Object);
        }
    }
}
