﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using UniversityDLL.Academics;
using UniversityDLL.GameMangement;
using UniversityDLL.Scheduling;

namespace UniversityDLLTest.CalendarTests
{
    [TestFixture]
    class TuitionCalendarEventTest
    {
        private TuitionCalendarEvent Q;

        private Mock<IGameState> QGameController;
        private Mock<IScore> QScore;
        private List<ICourseEnrolment> QEnrolments;

        private readonly TimeUnit Date = new TimeUnit(1931, Season.Fall, 5, DayOfWeek.Sunday);

        [SetUp]
        public void BeforeEachTest()
        {
            QScore = new Mock<IScore>();
            QEnrolments = new List<ICourseEnrolment>()
            {
                new Mock<ICourseEnrolment>().Object,
                new Mock<ICourseEnrolment>().Object
            };

            QGameController = new Mock<IGameState>();
            QGameController.Setup(g => g.Score).Returns(QScore.Object);
            QGameController.Setup(g => g.Enrolments).Returns(QEnrolments);

            Q = new TuitionCalendarEvent(Date, QGameController.Object);
        }

        [Test]
        public void Ctor_SetsDate_Score_Enrolments()
        {
            Assert.AreEqual(Date, Q.Date);
            Assert.AreEqual(QScore.Object, Q.Score);
        }

        [Test]
        public void Execute_Adds450BucksPerStudent()
        {
            Q.Execute();

            QScore.Verify(s => s.AddMoney(900));
        }

        [Test]
        public void AutoReschedule_AddsOneSemester()
        {
            var expected = new TimeUnit(1932, Season.Winter, 5, DayOfWeek.Sunday);
            Assert.AreEqual(expected, Q.GetRescheduleDate());
        }
    }
}
