﻿using NUnit.Framework;
using Moq;
using UnityEngine;
using UniversityDLL;
using UniversityDLL.GameMangement;
using UniversityDLL.Tiles;

namespace UniversityDLLTest
{
    [TestFixture]
    public class NullGameObjectTest
    {
        private IGameObject _nullGameObject;

        [SetUp]
        public void BeforeEachTest()
        {
            _nullGameObject = new NullGameObject();
        }

        [Test]
        public void Name_ReturnsNullGameObject()
        {
            Assert.AreEqual("NullLayer GameObject", _nullGameObject.Name);
        }

        [Test]
        public void GameObject_ReturnsNull()
        {
            Assert.IsNull(_nullGameObject.GameObject);
        }

        [Test]
        public void GetClone_GetsClone()
        {
            _nullGameObject.Name = "fakename";

            var actual = _nullGameObject.GetClone();

            Assert.IsTrue(actual is NullGameObject);
            Assert.AreNotSame(actual, _nullGameObject);
            Assert.AreEqual("fakename", actual.Name);
        }

        [Test]
        public void SetClickableObject_DoesNotThrow()
        {
            _nullGameObject.SetClickableObject(new Mock<IGameEntity>().Object);
        }

        [Test]
        public void Render_ReturnsSelf()
        {
            var actual = _nullGameObject.Render(1f, 2f, 3f);

            Assert.AreSame(_nullGameObject, actual);
        }

        [Test]
        public void Render_WithColor_ReturnsSelf()
        {
            var actual = _nullGameObject.Render(100f, 200f, 300f, Color.clear);

            Assert.AreSame(_nullGameObject, actual);
        }

        [Test]
        public void Destroy_DoesNotThrow()
        {
            _nullGameObject.Destroy();
        }

        [Test]
        public void Hide_DoesNotThrow()
        {
            _nullGameObject.Hide();
        }

        [Test]
        public void Show_DoesNotThrow()
        {
            _nullGameObject.Show();
        }

        [Test]
        public void SetOrientation_DoesNotThrow()
        {
            _nullGameObject.SetOrientation(Orientation.Reverse);
        }

        [Test]
        public void Orientation_ReturnsDefault()
        {
            var actual = _nullGameObject.Orientation;

            Assert.AreEqual(default(Orientation), actual);
        }
    }
}